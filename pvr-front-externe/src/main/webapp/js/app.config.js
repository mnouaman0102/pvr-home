//Constants
myApp.constant("appConstants", {
	msgObligatoire : "Il faut renseigner tous les champs obligatoires",
	msgObligatoireAll : "Tous les champs sont obligatoires",
	msgSucces      : "L'opération a été effectuée avec succès",
	msgError       : "L'opération a échouée",
	minCaractere: "Il faut renseigner au minimum 3 caractères",
	rechercheVide: "Aucun résultat n'est trouvé",
	msgDateDebut : "La date de début n'est pas renseignée.",
	msgDateFin : "La date de fin n'est pas renseignée."
});

//Configuration des routes
myApp.config(function($httpProvider, $routeProvider) {
	$httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=UTF-8';

	// Fournisseur
	$routeProvider.when('/fournisseur', {
		templateUrl: 'js/modules/fournisseur/views/fournisseur.tpl.html',
		controller: 'fournisseurController',
		permission: ['ROLE_FOURNISSEUR']
	});

	// Error 404
	$routeProvider.when('/404', {
		templateUrl: 'js/modules/error404/views/error404.tpl.html',
		controller: 'error404Controller',
		permission: ['ROLE_FOURNISSEUR']
	});

	// CONSULTATION
	$routeProvider.when('/consultation', {
		templateUrl: 'js/modules/consultation/views/consultation.tpl.html',
		controller: 'consultationController',
		permission: ['ROLE_FOURNISSEUR']
	});
	
	// REMISE CONTROLE
	$routeProvider.when('/remise-controle', {
		templateUrl: 'js/modules/remiseControle/views/remiseList.tpl.html',
		controller: 'remiseListController',
		permission: ['ROLE_FOURNISSEUR']
	});
	
	$routeProvider.when('/add-remise', {
		templateUrl: 'js/modules/remiseControle/views/remiseControle.tpl.html',
		controller: 'remiseControleController',
		permission: ['ROLE_FOURNISSEUR']
	});
	
	$routeProvider.when('/edit-remise/:id', {
		templateUrl: 'js/modules/remiseControle/views/remiseModif.tpl.html',
		controller: 'remiseModifController',
		permission: ['ROLE_FOURNISSEUR']
	});

	$routeProvider.otherwise({redirectTo: '/404'}); // remplacer par error404
	$httpProvider.responseInterceptors.push('myHttpInterceptor');
});
