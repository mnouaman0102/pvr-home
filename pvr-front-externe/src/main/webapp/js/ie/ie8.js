/*
var console = console!=null?console:{};
console.log = console.log || function() {};
window.console = window.console || {};
window.console.log = window.console.log || function() {};
//*/
document.createElement('ng-include');
document.createElement('ng-pluralize');
document.createElement('ng-view');
// déclarer les différentes directives
document.createElement('flot');
document.createElement('top-menu');
document.createElement('left-menu');
document.createElement('left-menu-indicateurs');
document.createElement('left-menu-incidents');
document.createElement('left-menu-rm');
document.createElement('flash-messages');
document.createElement('loading-spinner');
//document.createElement('left-menu');
//// Optionally these for CSS
//document.createElement('ng:include');
//document.createElement('ng:pluralize');
//document.createElement('ng:view');


if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement , fromIndex) {
    var i,
        pivot = (fromIndex) ? fromIndex : 0,
        length;

    if (!this) {
      throw new TypeError();
    }

    length = this.length;

    if (length === 0 || pivot >= length) {
      return -1;
    }

    if (pivot < 0) {
      pivot = length - Math.abs(pivot);
    }

    for (i = pivot; i < length; i++) {
      if (this[i] === searchElement) {
        return i;
      }
    }
    return -1;
  };
}


if (!Array.prototype.filter)
{
    Array.prototype.filter = function(fun /*, thisp */)
    {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== "function")
            throw new TypeError();

        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++)
        {
            if (i in t)
            {
                var val = t[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}