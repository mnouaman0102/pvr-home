/**
 * UTILISATION:
 * 
 * dans HTML:
 * <flash-messages id="global-FM"></flash-messages>
 * 
 * dans JS:
 * flashMessage("danger", "Un text message pour utilisateur","global-FM", 2000);
 * 
 */

/**
 * flashMessage(level, text, htmlId, duration);
 * 
 * Levels : success, info, warning, danger
 * 
 * Duration in milliseconds
 * 
 */

angular.module('flashMessage', []).factory('flashMessage', function($rootScope, $timeout) {
  var messages = [];
  var htmlTargetId = '';
  var duration = 2000;

  var reset;
  var cleanup = function() {
    $timeout.cancel(reset);
    reset = $timeout(function() {
      messages = [];
    });
  };

  var emit = function() {
    $rootScope.$emit('flashMessage:message', messages, cleanup);
  };
  
  var emitClean = function() {
    $rootScope.$emit('flashMessage:clean', cleanup);
  };

  $rootScope.$on('$routeChangeSuccess', emit);

  var asMessage = function(level, text) {
    if (!text) {
      text = level;
      level = 'success';
    }
    return { level: level, text: text};
  };

  var asArrayOfMessages = function(level, text, htmlTargetId, duration) {
    if (level instanceof Array) return level.map(function(message) {
      return message.text ? message : asMessage(message);
    }); 
    return text ? [{ level: level, text: text, htmlTargetId: htmlTargetId, duration: duration}] : [asMessage(level)];
  };
  
  return function(level, text, htmlTargetId, duration) {
      htmlTargetId=htmlTargetId;
      duration=duration;
      if (level === "clean"){
        emitClean();
      }else{
        emit(messages = asArrayOfMessages(level, text, htmlTargetId, duration));
      }
  };
})

.directive('flashMessages', function() {
  var directive = { restrict: 'EAC', replace: true };

  var template =
  '<div id="{{flashMsgTag}}-alert" ng-show="{{isAlreadyAlerted}}">' +
    '<div ng-repeat="m in messages" class="alert alert-{{m.level}}"><button type="button" class="close" data-dismiss="alert">&times;</button>'+
      '{{m.text}}'+
    '</div>'+
  '</div>';

  directive.controller = function($scope, $rootScope, $element, $compile, $timeout) {
    $rootScope.$on('flashMessage:message', function(_, messages, done) {
      if (messages.length > 0) {
        htmlTargetId = messages[0].htmlTargetId;
        duration = messages[0].duration;
        elm = $element[0];
        if (elm.id == htmlTargetId && !$scope.isAlreadyAlerted) {
          $scope.messages = messages;
          $scope.flashMsgTag = htmlTargetId;
          $scope.isAlreadyAlerted = true;
          angular.element(elm).append($compile(template)($scope));
          $timeout(
            function () { 
              $('#'+htmlTargetId+'-alert').remove();
              htmlTargetId='';
              $scope.messages = []; 
              $scope.flashMsgTag='';
              $scope.isAlreadyAlerted = false;
            },
          duration);
          done();
        }
      }
    });
    $rootScope.$on('flashMessage:clean', function(_, done) {
        $scope.messages=[];
        $scope.flashMsgTag = '';
        $scope.isAlreadyAlerted = false;
        done();
    });
  };
  directive.link = function ($scope, element, attrs) {};
  directive.controller.$inject = ['$scope','$rootScope','$element','$compile', '$timeout'];
  return directive;
});