// controle la fermeture du modal
myApp.factory('bootstrapModal', ["$document",
    function($document) {

        return {
            // call this function when deal with MANY opened popup modal
            // this function close popup modal by id
            // when called, it hides the opened popup modal with the specified id
            closeById: function(id) {
                $document.find('#'+id).modal('hide');
            },
            // call this function when deal with ONE popup modal
            // this function close popup modal by class name
            // when called, it removes all opened popup modal
            close: function() {
                $('.modal-open').removeClass("modal-open"); 
                $('.modal-backdrop').remove(); 
                $document.find('.modal').modal('hide');
            },
            open:function(id){
                $document.find('#'+id).modal();
                /**
                 * MOVE & RESIZE POP UP
                 * @author Z.BELGHAOUTI
                 */
                $('.modal-content').resizable({
                      minHeight: 300,
                      minWidth: 300
                });
                $('.modal-dialog').draggable();

                $('.movePopUp').on('show.bs.modal', function() {
                  $(this).find('.modal-body').css({
                    'max-height': '100%'
                  });
                });
            }
        }
    }
]);
