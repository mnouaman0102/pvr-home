// Intercepteur HTTP
myApp.factory('myHttpInterceptor', function ($q, $rootScope, $window, $location) {
    return function (promise) {
        return promise.then(function (response) {
            return response;
        }, function (response) {
            if(response.status === 401){
            	var num = $location.search().numOperation;
            	var idtRemise = $location.search().remise;
            	if((num != undefined && num != null && num != "")
            			|| (idtRemise != undefined && idtRemise != null && idtRemise != "")){
            		var getUrl = window.location.href;
            		var part = getUrl.split("#");
                	$q.reject(response);
                	$window.location.href="login.html#"+part[1];
            	}else{
            		$q.reject(response);
                    $window.location.href="login.html";
            	}
                
            }else if(response.status === 403){
            	if($rootScope._authRoles && $rootScope._authRoles != null && $rootScope._authRoles != undefined){
            		$q.reject(response);
                    $window.location.href="index.html";
            	}else{
            		return $q.reject(response);
            	}
            }else{
                return $q.reject(response);
            }
        });
    };
});

