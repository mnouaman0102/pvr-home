/**
 * Utilisation:
 * 
 * <red-star></red-star>
 * 
 * exemple: <label>Email <red-star></red-star></label>  =>  Email *
 */
myApp.directive('redStar', function () {
    return {
        restrict: 'E',
        template: '<span style="font-size: 12px;color: red;" title="Champ obligatoire">*</span>'
    };
});