myApp.directive('isNumber', function () {
    return {
        restrict: 'EA',
        template: '<input class="form-control" name="{{inputName}}" ng-model="inputValue" />',
        scope: {
            inputValue: '=',
            inputName: '=',
            maxlength: '=maxlength',
            disabled: '=isdisabled',
            alpha: '=alpha'
        },
        link: function (scope, element) {
        	if (scope.disabled == true) {
        		if (scope.inputValue == null) {
        			scope.inputValue = '';
        		}
        		element.html('<input class="form-control" name="{{inputName}}" ng-model="inputValue" disabled="disabled" value="' + scope.inputValue + '" />');
        	}
        	
        	if (scope.alpha == true) {
        		scope.$watch('inputValue', function(newValue, oldValue) {
	                var arr = String(newValue).split("");
	                if (arr.length === 0) return;
	                if (!/^[ a-z|A-Z|.]+$/.test(newValue) || (newValue != null && newValue.length > scope.maxlength)) {
	                	scope.inputValue = oldValue;
	                }
	            });
        	} else {
	            scope.$watch('inputValue', function(newValue, oldValue) {
	                var arr = String(newValue).split("");
	                if (arr.length === 0) return;
	                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
	                if (arr.length === 2 && newValue === '-.') return;
	                if (isNaN(newValue) || (newValue != null && newValue.length > scope.maxlength)) {
	                    scope.inputValue = oldValue;
	                }
	            });
        	}
        }
    };
});