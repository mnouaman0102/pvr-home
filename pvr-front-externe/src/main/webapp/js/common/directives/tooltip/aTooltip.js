myApp.directive('aTooltip', function() {
    var directive = {
        restrict: 'ECA',
        replace: true
    };
    directive.scope = {
        text: '=text'
    };
    
    directive.link = function(scope, elem, attrs) {
        elem.aToolTip({
            tipContent: scope.text
        });
        // correction du bug d'affichage quand on click
        // sur un lien alors que le tooltip est affiché.
        elem.find("a").click(function(ev) {
           $("#aToolTip").hide();
        });
    };
    
    return directive;
});