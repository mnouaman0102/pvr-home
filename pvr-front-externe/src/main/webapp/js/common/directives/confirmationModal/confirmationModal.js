/**
 * @author Hatim HADDY
 * 
 * Dans l'HTML, mettre:
 * <div confirmation-modal></div>
 * 
 * Dans le controller:
 * injecter 'confirmation' et confirmationModalService
 * 
 * Exemple d'utilisation:
 * 
 * <button ng-click="supprimerProduit(produit)"></button>
 * 
 * var produitsModule = angular.module('produitsModule', ['confirmation']);
 * produitsModule.controller('produitsController', function ($scope, $rootScope, confirmationModalService, produitsService) {
 * 
 * ...
 * 
 *  $scope.supprimerProduit = function (produit) {
        // Utiliser \n pour le saut de la ligne
        confirmationModalService.confirmAction("Êtes-vous sûr ?", function() {
            // code à executer si l'action est confirmée
            console.log('Supprimmer Produit');
        });
        confirmationModalService.close(function() {
            // code à executer si l'action est annulée
            console.log('Annulation');
        });
    };
 * }
 * 
 */


angular.module('confirmation', [])
.factory('confirmationModalService', function($rootScope) {
    var service = {};
    
    service.confirmAction = function(message,confirmCallback){
        $rootScope.$emit('confirmationModal:openModal', message,confirmCallback);
    };
    
    service.close = function(closeCallback){
        $rootScope.$emit('confirmationModal:closeModal',closeCallback);
    };
    
    return service;
}).directive('confirmationModal', function($parse) {
    var directive = { restrict: 'EAC', replace: true };
    directive.templateUrl = "js/common/directives/confirmationModal/confirmationModal.tpl.html";
    
    directive.scope = {};
    
    directive.controller = function($scope, $rootScope,bootstrapModal) {

        $rootScope.$on('confirmationModal:openModal', function(_, message,confirmCallback) {
            $scope.modalMessage = message.split("\n"); 
            $scope.confirmCallback = function(){
                confirmCallback();
                bootstrapModal.closeById('confirmModal');
            };
            bootstrapModal.open('confirmModal');
        });

        $rootScope.$on('confirmationModal:closeModal', function(_,closeCallback) {
            if (closeCallback != undefined && closeCallback != null) {
                $scope.closeCallback = function(){
                    closeCallback();
                    bootstrapModal.closeById('confirmModal');
                };
            }
        });
        
    };
    directive.controller.$inject = ['$scope','$rootScope','bootstrapModal'];
    
    return directive;
});