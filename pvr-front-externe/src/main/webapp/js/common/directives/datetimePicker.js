myApp.directive('datetimePicker', function($parse) {
    return {
        restrict: 'ECA',
        scope: {
            ngModel: '=',
            reset :	'=reset'
        },
        link: function(scope, elem, attrs) {
            /**
             * Values for minValue
                0 or 'hour' for the hour view
                1 or 'day' for the day view
                2 or 'month' for month view (the default)
                3 or 'year' for the 12-month overview
                4 or 'decade' for the 10-year overview. Useful for date-of-birth datetimepickers.

             */
//            var minView =1;// attrs.minView==null?0:parseInt(attrs.minView);
            var dateFormat = "dd/mm/yyyy hh:00";
            var minView = 0;
            switch(attrs.minView){
                case "month":
                    dateFormat = "dd/mm/yyyy";
                    minView = 2;
                break;
                case "hour":
                    dateFormat = "dd/mm/yyyy hh:00";
                    minView = 1;
                break;
                case "minute":
                    dateFormat = "dd/mm/yyyy hh:ii";
                    minView = 0;
                break;
            }
            
            var newval = '';
            initDatePicker();
            
            function initDatePicker() {
            	elem.datetimepicker({
                    autoclose: 1,
                    todayBtn: 1,
                    minView:minView,
                    format:dateFormat,
                    minuteStep:5
                }).on('changeDate', function(ev) {
//                	if (ev.date != null) {
//                		ev.date.setMinutes(0);
//                		ev.date.setSeconds(0);
//                	}
                	if (ev.date != null) {
	                    newval = new Date(ev.date);
	                    newval.setMinutes(newval.getMinutes() + newval.getTimezoneOffset());
	                    //newval.setHours(newval.getUTCHours());//FIX for datepicker
                	}else{
                		newval = ev.date;
                	}
                    
                    scope.$apply(setDate);
                });
            }
            
            if(scope.ngModel!=null && scope.ngModel!="") {
                elem.datetimepicker("setDate",scope.ngModel);
            }

            scope.$watch('ngModel', function() {                
                if (!scope.ngModel) {
                    elem[0].children[0].value = '';
                } else {
                    elem.datetimepicker("setDate",scope.ngModel);
                }
            });
            
            scope.$watch('reset', function() {
            	if (scope.reset == true) {
            		elem[0].children[0].value = '';
            		scope.reset = false;
                }
            });
            
            function setDate() {
                scope.ngModel = newval;
            }
        }
    };
});