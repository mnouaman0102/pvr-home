// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

var myApp = angular.module('app',
        [
            'ngRoute',
            'ngCookies',
            'ui.bootstrap',
            'ajaxModule',
            'loadingSpinner',
            'flashMessage',
            'fileReader',
            'ui.select2',
            'ui.sortable',
            'confirmation',
            'autocomplete',
			'checklist-model',
            'userModule',
            'indexModule',
            'fournisseurModule',
            'error404Module',
            'consultationModule',
            'remiseListModule',
            'remiseControleModule',
            'remiseModifModule',
        ]);

/**
 * Z.BELGHAOUTI
 */
myApp.run(['$rootScope', '$location', 'UserService', '$window', '$templateCache', function ($rootScope, $location, UserService, $window, $templateCache) {

    $rootScope.$on('$routeChangeStart', function (event, next) {
    	var permission = next.permission;
    	
    	if (permission != null && permission.length > 0) {
    		
    		UserService.getAuthentifiedUserRoles(function(roles) {
				var authRoles = {};
				var listBooleanRole = [];
				for (var i = 0; i < roles.length; i++) {
					authRoles[roles[i]] = true;
				}
				
	    		for (var i = 0; i < permission.length; i++) {
					if(authRoles[permission[i].trim()])
						listBooleanRole.push(true);
				}
	    		if(listBooleanRole != null && listBooleanRole.length > 0){
	    			// Having permission
	    		}else{
	    			if(authRoles['ROLE_ADMINISTRATEUR'])
	    				$location.path('/administration/utilisateur');
	    			else
	    				$location.path('/dashboard');
	    		}
			});
    	}

    });
    
    $rootScope.$on('$viewContentLoaded', function() {
        $templateCache.removeAll();
     });
    
    
}]);

//configuration des appels Ajax
myApp.config(function($httpProvider, $routeProvider) {
    $httpProvider.defaults.transformRequest = function(data) {
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    };
});


myApp.factory('$exceptionHandler', function($injector) {
    preventDefault = true;
    return function(exception, cause) {
        var rScope = $injector.get('$rootScope');
        if (rScope) {
            rScope.$broadcast('exception', exception, cause);
        }
    };
});


function getDateStr(date, includeHours) {
    if (date == null)
        return "";

    var ret = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    if (includeHours) {
        ret += " " + date.getHours() + ":" + date.getMinutes();
    }
    return ret;
};


//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
myApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

myApp.filter("groupBy",["$parse", "$filter", function($parse, $filter){
	  return function(array,groupByField){
	    var result	= [];
      var prev_item = null;
      var groupKey = false;
      var filteredData = $filter('orderBy')(array,groupByField);
      for(var i=0;i<filteredData.length;i++){
        groupKey = false;
        if(prev_item !== null){
          if(prev_item[groupByField] !== filteredData[i][groupByField]){
            groupKey = true;
          }
        } else {
          groupKey = true;  
        }
        if(groupKey){
          filteredData[i]['group_by_key'] =true;  
        } else {
          filteredData[i]['group_by_key'] =false;  
        }
        result.push(filteredData[i]);
        prev_item = filteredData[i];
      }
      return result;
	  }
}]);

