loginModule.factory('loginService', function (ajaxService) {
    var service = {};

    service.sendLogin = function (login, pass, successCallback, errorCallback) {
        ajaxService.post("j_spring_security_check", {
            j_username: login,
            j_password: pass
        }, successCallback, errorCallback);
        successCallback()

    };

    service.logout = function (callback) {
        ajaxService.post("j_spring_security_logout", {}, callback);
    };
    
    service.removeAllCookies = function() {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
            var cle = $.trim(cookies[i].split("=")[0]);
            $cookieStore.remove(cle);
        }
    };
    
    service.saveUserCookie = function(username,profiles){
        $cookieStore.put("profiles",profiles);
        $cookieStore.put("USERNAME",username);
    };

    return service;
});
