var loginModule = angular.module('loginModule', ['ajaxModule','loadingSpinner','flashMessage','userModule'])
.controller('loginController', function($scope, $log, loginService, $window, flashMessage, $location, UserService, $rootScope) {


	// isGranted
	$rootScope.isGranted = function() {
		if ($rootScope._authRoles == null){             
			return false;
		}     
		for (var i = 0; i < arguments.length; i++) {            
			var argToTest = arguments[i];
			if ($rootScope._authRoles[argToTest]){
				return $rootScope._authRoles[argToTest]; 
			}     
		}
		return false;
	};

	$scope.connexion = function() { 
		loginService.logout(function(){   
			loginService.sendLogin($scope.username, $scope.pwd, successCallback, errorCallback); 
		});
	}

	function successCallback(data) {                
		$rootScope.login=$scope.username;
		UserService.getAuthentifiedUserRoles(function(roles) {
			var _authRoles = {};
			for (var i = 0; i < roles.length; i++) {
				_authRoles[roles[i]] = true;
			}
			$rootScope._authRoles = _authRoles;
			if((typeof roles) == 'object'){
				var num = $location.search().numOperation;
				var idtRemise = $location.search().remise;
				//Case FOURNISSEUR
				if($rootScope.isGranted('ROLE_FOURNISSEUR')){
					if((num != undefined && num != null && num != "")
							|| (idtRemise != undefined && idtRemise != null && idtRemise != "")){
						var getUrl = window.location.href;
						var part = getUrl.split("#");
						$window.location.href="index.html#"+part[1];
					}else{
						$window.location = "index.html#/fournisseur";
					}
				//Case not Fournisseur
				}else{
					if(num != undefined && num != null && num != ""){
	            		var getUrl = window.location.href;
	            		var part = getUrl.split("#");
	                	$window.location.href="index.html#"+part[1];
	            	}else{
	            		flashMessage("danger", "Vous n'avez pas les droits nécessaires pour accéder à l’application.","global-FM", 4000);
	            	}
			}
				
			}else{
			flashMessage("danger", "Vous n'avez pas les droits nécessaires pour accéder à l’application.","global-FM", 4000);
			}
		});
	}   
	function errorCallback(data, status) {
		if (status == 401) {
			switch (data.code) {
			case 'F001':
				flashMessage("danger", "Les paramètres de connexion sont erronés","global-FM", 4000);
				break;
			case 'F002':
				flashMessage("danger", "Compte désactivé.","global-FM", 4000);
				break;
			case 'F003':
				flashMessage("danger", "Compte verrouillé","global-FM", 4000);
				break;
			case 'F004':
				flashMessage("danger", "Mot de passe expiré","global-FM", 4000);
				break;
			case 'F005':
				flashMessage("danger", "Compte Suspendu", "global-FM", 4000);
				break;
			case 'F006':
				flashMessage("danger", "Une autre session est en cours d'utilisation, cette session va être fermée", "global-FM", 4000);
				break;
			default:
				flashMessage("warning", "Erreur inattendue.","global-FM", 4000);
			break;
			}
		} else if(status == 403){
			flashMessage("danger", "Vous n'avez pas les droits nécessaires pour accéder à l’application.","global-FM", 4000);
		} else {
			flashMessage("warning", "Erreur inattendue.","global-FM", 4000);
		}
	}
});