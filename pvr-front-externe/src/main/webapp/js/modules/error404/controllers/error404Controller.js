/**
 * Z.BELGHAOUTI
 */
var error404Module = angular.module('error404Module', ['ajaxModule']);
error404Module.controller('error404Controller', function ($scope, $rootScope, $timeout, $window, UserService, $location){

	$scope.isAdmin = false;
	
	$scope.isGranted = function(roleToTest, _authRoles) {
		if (_authRoles != null) {
			if (_authRoles[roleToTest] == true) {
				return true;
			}
		}
		return false;
	};
	
	UserService.getAuthentifiedUserRoles(function(roles) {
        var _authRoles = {};
        for (var i = 0; i < roles.length; i++) {
            _authRoles[roles[i]] = true;
        }
        if((typeof roles) == 'object'){
        	if($scope.isGranted('ROLE_ADMINISTRATEUR', _authRoles)){
        		$scope.isAdmin = true;
            }else{
            	$scope.isAdmin = false;
            }
        }
	});
});
