var fournisseurModule = angular.module('fournisseurModule', []);

fournisseurModule.controller('fournisseurController', function (fournisseurService, $location, $scope, $rootScope, $timeout, $window, $compile, bootstrapModal, confirmationModalService, flashMessage, appConstants) {

	// select menu
	$('#nav-menu li').each(function () {
		$(this).removeClass('active');
	});
	$('#menu-FOURNI').addClass('active');

	//To clear pjs
	$scope.indexPjExp = 0;
	$scope.indexPjOpt = 0;
	$scope.indexPjDcc = 0;
	$scope.indexPjDccEnv = 0;
	$scope.indexPjQos = 0;

	//OperationToUpdate
	$scope.operation = {
		idt: '',
		idtContrat: '',
		idtSite: '',
		idtProjet: '',
		pjExploi: [],
		pjDcc: [],
		pjOptim: [],
		pjDccEnv: [],
		pjQos: []
	};

	// variables pieces jointes
	$scope.indexPj = 0;
	$scope.error_commentaire = false;

	// STATUT OPERATION
	$scope.OK = 'Validé';
	$scope.NOK = 'Refusé';
	$scope.PENDING = 'En cours';
	$scope.DCC = 'Examiné';

	$scope.currentPage = 1;
	$scope.totalRecords = 0;
	$scope.totalPages = 0;
	$scope.pageSize = 10;
	pageNumber = 1;



	//for initialization : reset
	$scope.fourniCriDto = {
		idtsContrat: [],
		idtsProjet: [],
		idtsDr: [],
		idtsSite: [],
		idtsEtat: [1],
		numOperation: []
	};

	//for numero Operation
	$scope.numOperations = [];
	fournisseurService.getAllNumOperations(function (data) {
		$scope.numOperations = data;
	});
	// get all Projects
	$scope.listProjects = [];
	fournisseurService.getAllProjects(function (data) {
		angular.forEach(data, function (value, key) {
			$scope.listProjects.push(value);
		});
	});

	// get all DR
	$scope.listDr = [];
	fournisseurService.getAllDr(function (data) {
		angular.forEach(data, function (value, key) {
			$scope.listDr.push(value);
		});
	});

	// get Etats
	$scope.listEtats = [];
	fournisseurService.getAllEtats(function (data) {
		$scope.listEtats = data;

	});

	// get all Site By DR
	$scope.listSite = [];
	$scope.changeDR = function (idtsDr) {
		if (idtsDr != null && idtsDr.length > 0) {
			fournisseurService.getAllSiteByDr(idtsDr, function (data) {
				$scope.listSite = data;
			}, function (error) {
			});
		}
	};


	$scope.listContrat = [];
	fournisseurService.getListContratByFournisseur(function (data) {
		$scope.fourniCriDto.idtsContrat = [];
		angular.forEach(data, function (value, key) {
			$scope.listContrat.push(value);
		});
	});

	// reset
	$scope.reset = function () {
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");

		//for initialization : reset    
		$scope.fourniCriDto = {
			idtsContrat: [],
			idtsProjet: [],
			idtsDr: [],
			idtsSite: [],
			idtsEtat: [1],
			numOperation: []
		};

		$scope.totalRecords = 0;
		$scope.totalPages = 0;
		$scope.listMES = [];
		$scope.listSite = [];
	}


	// function search
	$scope.searchMesPagination = function (pageNumber) {
		$scope.numberPage = pageNumber;
		if (pageNumber > 0 && pageNumber <= $scope.totalPages) {
			fournisseurService.searchMes($scope.fourniCriDto, pageNumber, $scope.pageSize, function (data) {
				$scope.listMES = data;
				$scope.currentPage = pageNumber;
			});
		} else {
			if (pageNumber <= 0) {
				fournisseurService.searchMes($scope.fourniCriDto, 1, $scope.pageSize, function (data) {
					$scope.listMES = data;
					$scope.currentPage = 1;
				});
			} else if (pageNumber > $scope.totalPages) {
				fournisseurService.searchMes($scope.fourniCriDto, $scope.totalPages, $scope.pageSize, function (data) {
					$scope.listMES = data;
					$scope.currentPage = $scope.totalPages;
				});
			}
		}
	}


	$scope.search = function () {
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");

		fournisseurService.getTotalMesByCriteres($scope.fourniCriDto, function (totalRec) {
			$scope.totalRecords = totalRec;
			if (totalRec > 0) {
				$scope.totalRec = totalRec;
			} else {
				$scope.totalRec = 1;
			}
			$scope.totalPages = Math.ceil($scope.totalRec / $scope.pageSize);
			$scope.totalPagesToTest = $scope.totalPages;
			page = 1;
			$window.pagObj = $('#pagination').twbsPagination({
				totalPages: $scope.totalPages,
				visiblePages: 10,
				onPageClick: function (event, page) {
					$scope.searchMesPagination(page);
				}
			}).on('page', function (event, page) {
			});
		});
	}

	$scope.openPopupToAddPj = function (idtMes, entite) {
		$scope.idtMesToAddPj = idtMes;
		if (entite == 2) {
			$scope.selectedEntite = 'Exploi';
		} else if (entite == 3) {
			$scope.selectedEntite = 'Optim';
		} else if (entite == 6) {
			$scope.selectedEntite = 'Qos';
		} else if (entite == 4) {
			$scope.selectedEntite = 'Dcc';
		} else if (entite == 5) {
			$scope.selectedEntite = 'DccEnv';
		}
		$('#pjInputList').html('');
		$scope.indexPj = 0;
		bootstrapModal.open("popupAddPj");
	};


	$scope.appendHtmlInputFile = function () {
		$scope.indexPj++;
		var newEle = angular.element("\
			<div class='row bloc_line '>\
				<div class='form-group col-md-10 col-sm-10 col-xs-10'>\
				<input type='file' class='form-control inputFile'>\
				</div>\
				<div class='col-md-2 col-sm-2 col-xs-2'>\
				<button type='button' ng-click='removeHtmlInputFile("+ ($scope.indexPj) + ")'\
					class='btn btn-xs btn-warning bnt-"+ ($scope.indexPj) + "'>\
					<i class='glyphicon glyphicon-remove'></i>\
				</button>\
			</div>\
		");
		var pieceJointe = document.getElementById('pjInputList');
		angular.element(pieceJointe).append($compile(newEle)($scope));
	};

	$scope.removeHtmlInputFile = function (indice) {
		$('.bnt-' + indice).parents(".bloc_line").remove();
	};

	$scope.requalifOperation = function (idtMes, idtEntite) {
		confirmationModalService.confirmAction("Êtes-vous sûr de cette action ?", function () {
			fournisseurService.requalifOperation(idtMes, idtEntite, function (data) {
				flashMessage("success", appConstants.msgSucces, "result-FM", 3000);
				$scope.searchMesPagination($scope.numberPage);
				if (data != null && data != undefined && data != "") {
					$scope.emails = data;
					fournisseurService.sendEmails($scope.emails, function (data) {
					}, function (error) {
						flashMessage("danger", appConstants.msgError, "global-FM", 3000);
					})
				}
			}, function (error) {
				flashMessage("danger", appConstants.msgError, "result-FM", 3000);
			});
		});
	};

	$scope.addPjToMesRefuse = function () {
		var formData = new FormData();
		var fileName = '';
		var fileNamePrefix = '';
		var fileExtPrefix = '';
		var fileExtention = '';
		var fileNameOrigin = '';
		var fileNameOriginPrefix = '';

		var operationToAddPj = {
			idt: $scope.idtMesToAddPj,
			idtContrat: '',
			idtSite: '',
			idtProjet: '',
			date: '',
			pjExploi: [],
			pjDcc: [],
			pjDccEnv: [],
			pjOptim: [],
			pjQos: []
		};
		var j = 0;
		var isFormDataEmpty = true;

		if ($scope.selectedEntite == 'Optim') {

			fileNamePrefix = 'Opt';
			fileExtPrefix = 'extOpt';
			fileNameOriginPrefix = 'originOpt';

		} else if ($scope.selectedEntite == 'Exploi') {

			fileNamePrefix = 'Exp';
			fileExtPrefix = 'extExp';
			fileNameOriginPrefix = 'originExp';

		} else if ($scope.selectedEntite == 'Qos') {

			fileNamePrefix = 'Qos';
			fileExtPrefix = 'extQos';
			fileNameOriginPrefix = 'originQos';

		} else if ($scope.selectedEntite == 'Dcc') {

			fileNamePrefix = 'Dcc';
			fileExtPrefix = 'extDcc';
			fileNameOriginPrefix = 'originDcc';

		} else if ($scope.selectedEntite == 'DccEnv') {

			fileNamePrefix = 'DccEnv';
			fileExtPrefix = 'extDccEnv';
			fileNameOriginPrefix = 'originDccEnv';

		}

		$('#pjInputList .inputFile').each(function () {
			var myFile = $(this)[0].files[0];
			if (myFile) {
				isFormDataEmpty = false;

				j++;

				fileName = fileNamePrefix + j;
				fileNameOrigin = fileNameOriginPrefix + j;
				fileExtention = myFile.name.split('.')[1];
				fileExt = fileExtPrefix + j;

				formData.append(fileName, myFile);
				formData.append(fileNameOrigin, myFile.name.split('.')[0]);
				formData.append(fileExt, fileExtention);
			}
		});


		if (isFormDataEmpty) {
			flashMessage("danger", "Veuillez choisir des fichiers", "popup-FM", 3000);
		} else {
			confirmationModalService.confirmAction("Êtes-vous sûr de cette action ?", function () {
				fournisseurService.uploadFile(formData, function (data) {
					operationToAddPj.pjExploi = data.pjExploi;
					operationToAddPj.pjOptim = data.pjOptim;
					operationToAddPj.pjDcc = data.pjDcc;
					operationToAddPj.pjDccEnv = data.pjDccEnv;
					operationToAddPj.pjQos = data.pjQos;
					fournisseurService.addMorePj(operationToAddPj, function (data) {
						bootstrapModal.close("popupAddPj");
						flashMessage("success", appConstants.msgSucces, "result-FM", 3000);
						operationToAddPj = {
							idt: '',
							idtContrat: '',
							idtSite: '',
							idtProjet: '',
							date: '',
							pjExploi: [],
							pjDcc: [],
							pjDccEnv: [],
							pjOptim: [],
							pjQos: []
						};
						$scope.searchMesPagination($scope.numberPage);
					}, function (error) {
						flashMessage("danger", appConstants.msgError, "global-FM", 3000);
					});

				}, function (error) {
					flashMessage("danger", "Erreur de chargement du fichier.", "global-FM", 3000);
				});
			});
		}
	};

	$scope.downloadFile = function (fileName) {
		if (fileName != null && fileName != "") {
			$window.open("services/fourni/getPjDeploi?fileName=" + fileName);
		}
	};

	/**
	 * PopUp Pieces jointes
	 */
	$scope.openPopUpPJ = function (pjExploi, pjOptim, pjDcc, pjDccEnv, idtTypeProjet, pjQos) {
		$scope.idtTypeProjet = idtTypeProjet;
		$scope.listPjExpl = pjExploi;
		$scope.listPjOptim = pjOptim;
		$scope.listPjDcc = pjDcc;
		$scope.listPjDccEnv = pjDccEnv;
		$scope.listPjQos = pjQos;
		bootstrapModal.open("openPopUpPJ");
	};

	var num = $location.search().numOperation;
	if (num != undefined && num != null && num != "") {
		$scope.fourniCriDto = {
			idtContrat: 0,
			idtProjet: 0,
			idtDr: 0,
			idtSite: 0,
			numOperation: num
		};
		$scope.search();
	}

	/**
	 * PopUp Consult OPTIM Pieces jointes Qualification et Commentaire
	 */
	$scope.openPopupConsultOpt = function (pjsQualif, comment) {
		$scope.listPjQualif = pjsQualif;
		$scope.commentaire = comment;
		bootstrapModal.open("openPopUpConsult");
	};
	/**
	 * PopUp Consult EXP Pieces jointes Qualification et Commentaire
	 */
	$scope.openPopupConsultExp = function (pjsQualif, comment) {
		$scope.listPjQualif = pjsQualif;
		$scope.commentaire = comment;
		bootstrapModal.open("openPopUpConsult");
	};
	/**
	 * PopUp Consult QOS Pieces jointes Qualification et Commentaire
	 */
	$scope.openPopupConsultQos = function (pjsQualif, comment) {
		$scope.listPjQualif = pjsQualif;
		$scope.commentaire = comment;
		bootstrapModal.open("openPopUpConsult");
	};
	/**
	 * PopUp Consult DCC Pieces jointes Qualification et Commentaire
	 */
	$scope.openPopupConsultDcc = function (pjsQualif, comment) {
		$scope.listPjQualif = pjsQualif;
		$scope.commentaire = comment;
		bootstrapModal.open("openPopUpConsult");
	};
	/**
	 * PopUp Ajout Pieces jointes Entitées
	 */
	$scope.openPopUpAddPjs = function (deploiDto) {
		$scope.workFlow = deploiDto.workflow;
		bootstrapModal.open("popUpAddPjs");
		if (deploiDto.idt > 0) {
			fournisseurService.getOperationByIdt(deploiDto.idt, function (data) {
				$scope.operationEditDto = data;

				$scope.idtFournisseur = data.idtFournisseur;
				$scope.idtDr = data.idtDr;
				$scope.idtTypeProjet = data.idtTypeProjet;
				$scope.operationEditDto.comment = data.comment;

				$scope.updateOperation = function (operation) {
					var formData = null;
					var fileExtention = null;
					var fileNameOrigin = '';

					if ($scope.idtFournisseur == null || $scope.idtFournisseur == "") {
						$scope.error_founisseur = true;
					} else if (operation.idtContrat == null || operation.idtContrat == "") {
						$scope.error_contrat = true;
					} else if (operation.idtProjet == null || operation.idtProjet == "") {
						$scope.error_projet = true;
					} else {

						$scope.operation.idt = operation.idt;
						$scope.operation.idtContrat = operation.idtContrat;
						$scope.operation.idtSite = operation.idtSite;
						$scope.operation.idtProjet = operation.idtProjet;
						$scope.operation.comment = operation.comment;

						formData = new FormData();
						var j = 0;
						//To add Pjs Exp
						$('#pjExpForm  .inputFileExp').each(function () {
							var myFile = $(this)[0].files[0];
							if (myFile) {
								j++;

								fileNameExp = 'Exp' + j;
								fileNameOrigin = 'originExp' + j;
								fileExtention = myFile.name.split('.')[1];
								fileExtExp = 'extExp' + j;

								formData.append(fileNameExp, myFile);
								formData.append(fileNameOrigin, myFile.name.split('.')[0]);
								formData.append(fileExtExp, fileExtention);
							}

						});
						//To Remove Pjs Exp
						$('#pjExpForm .existePjExp').each(function () {
							if ($(this).hasClass("pjRetiree")) {
								pj = {
									idt: Number($(this)[0].id),
									fileNameSaved: null,
									originFileName: null
								}
								$scope.operation.pjExploi.push(pj);
							}
						});
						j = 0;
						//To add Pjs Optim
						$('#pjOptForm  .inputFileOpt').each(function () {
							var myFile = $(this)[0].files[0];
							if (myFile) {
								j++;

								fileNameOpt = 'Opt' + j;
								fileNameOrigin = 'originOpt' + j;
								fileExtention = myFile.name.split('.')[1];
								fileExtOpt = 'extOpt' + j;

								formData.append(fileNameOpt, myFile);
								formData.append(fileNameOrigin, myFile.name.split('.')[0]);
								formData.append(fileExtOpt, fileExtention);
							}
						});
						//To Remove Pjs Optim
						$('#pjOptForm .existePjOpt').each(function () {
							if ($(this).hasClass("pjRetiree")) {
								pj = {
									idt: Number($(this)[0].id),
									fileNameSaved: null,
									originFileName: null
								}
								$scope.operation.pjOptim.push(pj);
							}
						});
						j = 0;
						//To add Pjs Dcc
						$('#pjDccForm .inputFileDcc').each(function () {
							var myFile = $(this)[0].files[0];
							if (myFile) {
								j++;

								fileNameDcc = 'Dcc' + j;
								fileNameOrigin = 'originDcc' + j;
								fileExtention = myFile.name.split('.')[1];
								fileExtDcc = 'extDcc' + j;

								formData.append(fileNameDcc, myFile);
								formData.append(fileNameOrigin, myFile.name.split('.')[0]);
								formData.append(fileExtDcc, fileExtention);
							}
						});
						//To Remove Pjs Dcc
						$('#pjDccForm .existePjDcc').each(function () {
							if ($(this).hasClass("pjRetiree")) {
								pj = {
									idt: Number($(this)[0].id),
									fileNameSaved: null,
									originFileName: null
								}
								$scope.operation.pjDcc.push(pj);
							}
						});
						j = 0;
						//To add Pjs DccEnv
						$('#pjDccEnvForm .inputFileDccEnv').each(function () {
							var myFile = $(this)[0].files[0];
							if (myFile) {
								j++;

								fileNameDccEnv = 'DccEnv' + j;
								fileNameOrigin = 'originDccEnv' + j;
								fileExtention = myFile.name.split('.')[1];
								fileExtDccEnv = 'extDccEnv' + j;

								formData.append(fileNameDccEnv, myFile);
								formData.append(fileNameOrigin, myFile.name.split('.')[0]);
								formData.append(fileExtDccEnv, fileExtention);
							}
						});
						//To Remove Pjs DccEnv
						$('#pjDccEnvForm .existePjDccEnv').each(function () {
							if ($(this).hasClass("pjRetiree")) {
								pj = {
									idt: Number($(this)[0].id),
									fileNameSaved: null,
									originFileName: null
								}
								$scope.operation.pjDccEnv.push(pj);
							}
						});
						j = 0;
						//To add Pjs Qos
						$('#pjQosForm  .inputFileQos').each(function () {
							var myFile = $(this)[0].files[0];
							if (myFile) {
								j++;

								fileNameQos = 'Qos' + j;
								fileNameOrigin = 'originQos' + j;
								fileExtention = myFile.name.split('.')[1];
								fileExtQos = 'extQos' + j;

								formData.append(fileNameQos, myFile);
								formData.append(fileNameOrigin, myFile.name.split('.')[0]);
								formData.append(fileExtQos, fileExtention);
							}
						});
						//To Remove Pjs Qos
						$('#pjQosForm .existePjQos').each(function () {
							if ($(this).hasClass("pjRetiree")) {
								pj = {
									idt: Number($(this)[0].id),
									fileNameSaved: null,
									originFileName: null
								}
								$scope.operation.pjQos.push(pj);
							}
						});

						confirmationModalService.confirmAction("Êtes-vous sûr de cette action ?", function () {
							fournisseurService.uploadPjsUpdateOperation(formData, function (data) {

								angular.forEach(data.pjExploi, function (pj, key) {
									$scope.operation.pjExploi.push(pj);
								});

								angular.forEach(data.pjOptim, function (pj, key) {
									$scope.operation.pjOptim.push(pj);
								});

								angular.forEach(data.pjDcc, function (pj, key) {
									$scope.operation.pjDcc.push(pj);
								});

								angular.forEach(data.pjDccEnv, function (pj, key) {
									$scope.operation.pjDccEnv.push(pj);
								});

								angular.forEach(data.pjQos, function (pj, key) {
									$scope.operation.pjQos.push(pj);
								});

								fournisseurService.updateOperation($scope.operation, function (data) {
									flashMessage("success", appConstants.msgSucces, "result-FM", 5000);
									bootstrapModal.close("popUpAddPjs");
									$scope.cleanOperation();
									$scope.searchMesPagination($scope.numberPage);
									$timeout(function () {
										$window.location = "index.html#/fournisseur";
									}, 1000);

									// Clear Files EXP
									if ($scope.indexPjExp > 0) {
										for (var i = 1; i <= $scope.indexPjExp; i++) {
											$('.bntExp-' + i).parents(".bloc_line").remove();
										}
									}
									$('.icon-clearExp-0').parents(".bloc_line").find('input.inputFileExp').val("");

									// Clear Files OPT
									if ($scope.indexPjOpt > 0) {
										for (var i = 1; i <= $scope.indexPjOpt; i++) {
											$('.bntOpt-' + i).parents(".bloc_line").remove();
										}
									}
									$('.icon-clearOpt-0').parents(".bloc_line").find('input.inputFileOpt').val("");

									// Clear Files QOS
									if ($scope.indexPjQos > 0) {
										for (var i = 1; i <= $scope.indexPjQos; i++) {
											$('.bntQos-' + i).parents(".bloc_line").remove();
										}
									}
									$('.icon-clearQos-0').parents(".bloc_line").find('input.inputFileQos').val("");

									// Clear Files DCC RADIO
									if ($scope.indexPjDcc > 0) {
										for (var i = 1; i <= $scope.indexPjDcc; i++) {
											$('.bntDcc-' + i).parents(".bloc_line").remove();
										}
									}
									$('.icon-clearDcc-0').parents(".bloc_line").find('input.inputFileDcc').val("");

									// Clear Files DCC ENV
									if ($scope.indexPjDccEnv > 0) {
										for (var i = 1; i <= $scope.indexPjDccEnv; i++) {
											$('.bntDccEnv-' + i).parents(".bloc_line").remove();
										}
									}
									$('.icon-clearDccEnv-0').parents(".bloc_line").find('input.inputFileDccEnv').val("");
									//Envoie d'emails
									if (data != "" && data != null && data != undefined) {
										$scope.emails = data;
									}
								}, function (error) {
									flashMessage("danger", appConstants.msgError, "global-FM", 3000);
								});

							}, function (error) {
								flashMessage("danger", "Erreur de chargement du fichier.", "global-FM", 3000);
							});
						});

					}
					//Griser bouton 
					var btn = $(".btn-primary");
					btn.prop('disabled', true);
					setTimeout(function () {
						btn.prop('disabled', false);
					}, 1000);
				};

				$scope.plusPjExp = function () {

					$scope.indexPjExp++;
					var newEle = angular.element("<div class='row bloc_line '>\
						<div class='form-group col-md-9 col-sm-9 col-xs-9'>\
						<input type='file' class='form-control inputFileExp'>\
						</div>\
						<div class='col-md-1 col-sm-1 col-xs-1'>\
						<i class='glyphicon glyphicon-remove-sign icon-clearExp-"+ ($scope.indexPjExp) + "' title='Supprimer le fichier' style='margin-top: 8px; color: red; cursor: pointer; margin-left: -22px;'\
						ng-click='cleanFileExp("+ ($scope.indexPjExp) + ")'></i>\
						</div>\
						<div class='col-md-2 col-sm-2 col-xs-2'>\
						<button type='button' ng-click='removePjExp("+ ($scope.indexPjExp) + ")'\
						class='btn btn-xs btn-warning bntExp-"+ ($scope.indexPjExp) + "' style='margin-top: 5px;'>\
						<i class='glyphicon glyphicon-minus'></i>\
						</button>\
				</div>");
					var pieceJointe = document.getElementById('pjExpForm');
					angular.element(pieceJointe).append($compile(newEle)($scope));

				};
				$scope.plusPjOpt = function () {

					$scope.indexPjOpt++;
					var newEle = angular.element("<div class='row bloc_line '>\
						<div class='form-group col-md-9 col-sm-9 col-xs-9'>\
						<input type='file' class='form-control inputFileOpt'>\
						</div>\
						<div class='col-md-1 col-sm-1 col-xs-1'>\
						<i class='glyphicon glyphicon-remove-sign icon-clearOpt-"+ ($scope.indexPjOpt) + "' title='Supprimer le fichier' style='margin-top: 8px; color: red; cursor: pointer; margin-left: -22px;'\
						ng-click='cleanFileOpt("+ ($scope.indexPjOpt) + ")'></i>\
						</div>\
						<div class='col-md-2 col-sm-2 col-xs-2'>\
						<button type='button' ng-click='removePjOpt("+ ($scope.indexPjOpt) + ")'\
						class='btn btn-xs btn-warning bntOpt-"+ ($scope.indexPjOpt) + "'>\
						<i class='glyphicon glyphicon-minus'></i>\
						</button>\
				</div>");
					var pieceJointe = document.getElementById('pjOptForm');
					angular.element(pieceJointe).append($compile(newEle)($scope));

				};
				$scope.plusPjDcc = function () {

					$scope.indexPjDcc++;
					var newEle = angular.element("<div class='row bloc_line '>\
						<div class='form-group col-md-9 col-sm-9 col-xs-9'>\
						<input type='file' class='form-control inputFileDcc'>\
						</div>\
						<div class='col-md-1 col-sm-1 col-xs-1'>\
						<i class='glyphicon glyphicon-remove-sign icon-clearDcc-"+ ($scope.indexPjDcc) + "' title='Supprimer le fichier' style='margin-top: 8px; color: red; cursor: pointer; margin-left: -22px;'\
						ng-click='cleanFileDcc("+ ($scope.indexPjDcc) + ")'></i>\
						</div>\
						<div class='col-md-2 col-sm-2 col-xs-2'>\
						<button type='button' ng-click='removePjDcc("+ ($scope.indexPjDcc) + ")'\
						class='btn btn-xs btn-warning bntDcc-"+ ($scope.indexPjDcc) + "'>\
						<i class='glyphicon glyphicon-minus'></i>\
						</button>\
				</div>");
					var pieceJointe = document.getElementById('pjDccForm');
					angular.element(pieceJointe).append($compile(newEle)($scope));

				};
				$scope.plusPjDccEnv = function () {

					$scope.indexPjDccEnv++;
					var newEle = angular.element("<div class='row bloc_line '>\
						<div class='form-group col-md-9 col-sm-9 col-xs-9'>\
						<input type='file' class='form-control inputFileDccEnv'>\
						</div>\
						<div class='col-md-1 col-sm-1 col-xs-1'>\
						<i class='glyphicon glyphicon-remove-sign icon-clearDccEnv-"+ ($scope.indexPjDccEnv) + "' title='Supprimer le fichier' style='margin-top: 8px; color: red; cursor: pointer; margin-left: -22px;'\
						ng-click='cleanFileDccEnv("+ ($scope.indexPjDccEnv) + ")'></i>\
						</div>\
						<div class='col-md-2 col-sm-2 col-xs-2'>\
						<button type='button' ng-click='removePjDccEnv("+ ($scope.indexPjDccEnv) + ")'\
						class='btn btn-xs btn-warning bntDccEnv-"+ ($scope.indexPjDccEnv) + "'>\
						<i class='glyphicon glyphicon-minus'></i>\
						</button>\
				</div>");
					var pieceJointe = document.getElementById('pjDccEnvForm');
					angular.element(pieceJointe).append($compile(newEle)($scope));

				};

				$scope.plusPjQos = function () {

					$scope.indexPjQos++;
					var newEle = angular.element("<div class='row bloc_line '>\
						<div class='form-group col-md-9 col-sm-9 col-xs-9'>\
						<input type='file' class='form-control inputFileQos'>\
						</div>\
						<div class='col-md-1 col-sm-1 col-xs-1'>\
						<i class='glyphicon glyphicon-remove-sign icon-clearQos-"+ ($scope.indexPjQos) + "' title='Supprimer le fichier' style='margin-top: 8px; color: red; cursor: pointer; margin-left: -22px;'\
						ng-click='cleanFileQos("+ ($scope.indexPjQos) + ")'></i>\
						</div>\
						<div class='col-md-2 col-sm-2 col-xs-2'>\
						<button type='button' ng-click='removePjQos("+ ($scope.indexPjQos) + ")'\
						class='btn btn-xs btn-warning bntQos-"+ ($scope.indexPjQos) + "'>\
						<i class='glyphicon glyphicon-minus'></i>\
						</button>\
				</div>");
					var pieceJointe = document.getElementById('pjQosForm');
					angular.element(pieceJointe).append($compile(newEle)($scope));

				};
				//Remove PJ
				$scope.removePjExp = function (indice) {
					$('.bntExp-' + indice).parents(".bloc_line").remove();
				};
				$scope.removePjOpt = function (indice) {
					$('.bntOpt-' + indice).parents(".bloc_line").remove();
				};
				$scope.removePjDcc = function (indice) {
					$('.bntDcc-' + indice).parents(".bloc_line").remove();
				};
				$scope.removePjDccEnv = function (indice) {
					$('.bntDccEnv-' + indice).parents(".bloc_line").remove();
				};
				$scope.removePjQos = function (indice) {
					$('.bntQos-' + indice).parents(".bloc_line").remove();
				};



				$scope.cleanFileExp = function (index) {

					$('.icon-clearExp-' + index).parents(".bloc_line").find('input.inputFileExp').val("");
				};

				$scope.cleanFileOpt = function (index) {

					$('.icon-clearOpt-' + index).parents(".bloc_line").find('input.inputFileOpt').val("");
				};
				$scope.cleanFileDcc = function (index) {

					$('.icon-clearDcc-' + index).parents(".bloc_line").find('input.inputFileDcc').val("");
				};
				$scope.cleanFileQos = function (index) {

					$('.icon-clearQos-' + index).parents(".bloc_line").find('input.inputFileQos').val("");
				};
				$scope.cleanFileDccEnv = function (index) {

					$('.icon-clearDccEnv-' + index).parents(".bloc_line").find('input.inputFileDccEnv').val("");
				};

				$scope.cleanOperation = function () {
					$scope.idtFournisseur = '';
					$scope.idtDr = '';
					$scope.idtEntite = '';
					$scope.sites = [];
					$scope.contrats = [];
					$scope.operation = {
						idt: '',
						idtContrat: '',
						idtSite: '',
						idtProjet: '',
						pjExploi: [],
						pjDcc: [],
						pjOptim: [],
						pjDccEnv: [],
						pjQos: []
					};
				};
				//Exploitation
				$timeout(function () {
					/**
					 * APPEND Existe PJs Exp
					 */
					$scope.indexExistePjExp = 0;
					listElement = $('#pjsExisteExp');
					listElement.html('');
					angular.forEach($scope.operationEditDto.pjExploi, function (pjs, key) {
						$scope.indexExistePjExp++;
						newElement = angular.element("\
					<div class='row bloc_line'>\
						<div class='col-md-9 col-sm-9 col-xs-9'>\
							<span disabled class='form-control existePjExp' id='"+ (pjs.idt) + "'>" + (pjs.originFileName) + "</span>\
						</div>\
						<div class='col-md-3 col-sm-3 col-xs-3'>\
							<button type='button' ng-click='removeExistePjExp("+ ($scope.indexExistePjExp) + ")'\
								class='btn btn-xs btn-warning bntExistePjExp-"+ ($scope.indexExistePjExp) + "' style='margin-top: 5px;'>\
								Retirer <i class='glyphicon glyphicon-remove'></i>\
							</button>\
						</div>\
					</div>");
						angular.element(listElement).append($compile(newElement)($scope));
					});
				}, 1000);

				/**
				 * REMOVE ALREADY SAVED PJ Exp
				 */
				$scope.removeExistePjExp = function (indice) {
					btnElement = $('.bntExistePjExp-' + indice);
					spanElement = btnElement.parents(".bloc_line").find(".existePjExp");
					if (btnElement.hasClass("btn-warning")) {
						btnElement.html("");
						btnElement.removeClass("btn-warning");
						btnElement.append("Recycler <i class='glyphicon glyphicon-refresh'></i>");
						spanElement.addClass("pjRetiree");
					} else {
						btnElement.html("");
						btnElement.addClass("btn-warning");
						btnElement.html("Retirer <i class='glyphicon glyphicon-remove'></i>");
						spanElement.removeClass("pjRetiree");
					}
				};
				//Optim
				$timeout(function () {
					/**
					 * APPEND Existe PJs Opt
					 */
					$scope.indexExistePjOpt = 0;
					listElement = $('#pjsExisteOpt');
					listElement.html('');
					angular.forEach($scope.operationEditDto.pjOptim, function (pjs, key) {
						$scope.indexExistePjOpt++;
						newElement = angular.element("\
					<div class='row bloc_line'>\
						<div class='col-md-9 col-sm-9 col-xs-9'>\
							<span disabled class='form-control existePjOpt' id='"+ (pjs.idt) + "'>" + (pjs.originFileName) + "</span>\
						</div>\
						<div class='col-md-3 col-sm-3 col-xs-3'>\
							<button type='button' ng-click='removeExistePjOpt("+ ($scope.indexExistePjOpt) + ")'\
								class='btn btn-xs btn-warning bntExistePjOpt-"+ ($scope.indexExistePjOpt) + "' style='margin-top: 5px;'>\
								Retirer <i class='glyphicon glyphicon-remove'></i>\
							</button>\
						</div>\
					</div>");
						angular.element(listElement).append($compile(newElement)($scope));
					});
				}, 1000);

				/**
				 * REMOVE ALREADY SAVED PJ Opt
				 */
				$scope.removeExistePjOpt = function (indice) {
					btnElement = $('.bntExistePjOpt-' + indice);
					spanElement = btnElement.parents(".bloc_line").find(".existePjOpt");
					if (btnElement.hasClass("btn-warning")) {
						btnElement.html("");
						btnElement.removeClass("btn-warning");
						btnElement.append("Recycler <i class='glyphicon glyphicon-refresh'></i>");
						spanElement.addClass("pjRetiree");
					} else {
						btnElement.html("");
						btnElement.addClass("btn-warning");
						btnElement.html("Retirer <i class='glyphicon glyphicon-remove'></i>");
						spanElement.removeClass("pjRetiree");
					}
				};

				//Qos
				$timeout(function () {
					/**
					 * APPEND Existe PJs Qos
					 */
					$scope.indexExistePjQos = 0;
					listElement = $('#pjsExisteQos');
					listElement.html('');
					angular.forEach($scope.operationEditDto.pjQos, function (pjs, key) {
						$scope.indexExistePjQos++;
						newElement = angular.element("\
					<div class='row bloc_line'>\
						<div class='col-md-9 col-sm-9 col-xs-9'>\
							<span disabled class='form-control existePjQos' id='"+ (pjs.idt) + "'>" + (pjs.originFileName) + "</span>\
						</div>\
						<div class='col-md-3 col-sm-3 col-xs-3'>\
							<button type='button' ng-click='removeExistePjQos("+ ($scope.indexExistePjQos) + ")'\
								class='btn btn-xs btn-warning bntExistePjQos-"+ ($scope.indexExistePjQos) + "' style='margin-top: 5px;'>\
								Retirer <i class='glyphicon glyphicon-remove'></i>\
							</button>\
						</div>\
					</div>");
						angular.element(listElement).append($compile(newElement)($scope));
					});
				}, 1000);

				/**
				 * REMOVE ALREADY SAVED PJ Qos
				 */
				$scope.removeExistePjQos = function (indice) {
					btnElement = $('.bntExistePjQos-' + indice);
					spanElement = btnElement.parents(".bloc_line").find(".existePjQos");
					if (btnElement.hasClass("btn-warning")) {
						btnElement.html("");
						btnElement.removeClass("btn-warning");
						btnElement.append("Recycler <i class='glyphicon glyphicon-refresh'></i>");
						spanElement.addClass("pjRetiree");
					} else {
						btnElement.html("");
						btnElement.addClass("btn-warning");
						btnElement.html("Retirer <i class='glyphicon glyphicon-remove'></i>");
						spanElement.removeClass("pjRetiree");
					}
				};

				//DCC
				$timeout(function () {
					/**
					 * APPEND Existe PJs Dcc
					 */
					$scope.indexExistePjDcc = 0;
					listElement = $('#pjsExisteDcc');
					listElement.html('');
					angular.forEach($scope.operationEditDto.pjDcc, function (pjs, key) {
						$scope.indexExistePjDcc++;
						newElement = angular.element("\
					<div class='row bloc_line'>\
						<div class='col-md-9 col-sm-9 col-xs-9'>\
							<span disabled class='form-control existePjDcc' id='"+ (pjs.idt) + "'>" + (pjs.originFileName) + "</span>\
						</div>\
						<div class='col-md-3 col-sm-3 col-xs-3'>\
							<button type='button' ng-click='removeExistePjDcc("+ ($scope.indexExistePjDcc) + ")'\
								class='btn btn-xs btn-warning bntExistePjDcc-"+ ($scope.indexExistePjDcc) + "' style='margin-top: 5px;'>\
								Retirer <i class='glyphicon glyphicon-remove'></i>\
							</button>\
						</div>\
					</div>");
						angular.element(listElement).append($compile(newElement)($scope));
					});
				}, 1000);

				/**
				 * REMOVE ALREADY SAVED PJ Dcc
				 */
				$scope.removeExistePjDcc = function (indice) {
					btnElement = $('.bntExistePjDcc-' + indice);
					spanElement = btnElement.parents(".bloc_line").find(".existePjDcc");
					if (btnElement.hasClass("btn-warning")) {
						btnElement.html("");
						btnElement.removeClass("btn-warning");
						btnElement.append("Recycler <i class='glyphicon glyphicon-refresh'></i>");
						spanElement.addClass("pjRetiree");
					} else {
						btnElement.html("");
						btnElement.addClass("btn-warning");
						btnElement.html("Retirer <i class='glyphicon glyphicon-remove'></i>");
						spanElement.removeClass("pjRetiree");
					}
				};
				//DCC ENV
				$timeout(function () {
					/**
					 * APPEND Existe PJs Dcc Env
					 */
					$scope.indexExistePjDccEnv = 0;
					listElement = $('#pjsExisteDccEnv');
					listElement.html('');
					angular.forEach($scope.operationEditDto.pjDccEnv, function (pjs, key) {
						$scope.indexExistePjDccEnv++;
						newElement = angular.element("\
					<div class='row bloc_line'>\
						<div class='col-md-9 col-sm-9 col-xs-9'>\
							<span disabled class='form-control existePjDccEnv' id='"+ (pjs.idt) + "'>" + (pjs.originFileName) + "</span>\
						</div>\
						<div class='col-md-3 col-sm-3 col-xs-3'>\
							<button type='button' ng-click='removeExistePjDccEnv("+ ($scope.indexExistePjDccEnv) + ")'\
								class='btn btn-xs btn-warning bntExistePjDccEnv-"+ ($scope.indexExistePjDccEnv) + "' style='margin-top: 5px;'>\
								Retirer <i class='glyphicon glyphicon-remove'></i>\
							</button>\
						</div>\
					</div>");
						angular.element(listElement).append($compile(newElement)($scope));
					});
				}, 1000);

				/**
				 * REMOVE ALREADY SAVED PJ DccEnv
				 */
				$scope.removeExistePjDccEnv = function (indice) {
					btnElement = $('.bntExistePjDccEnv-' + indice);
					spanElement = btnElement.parents(".bloc_line").find(".existePjDccEnv");
					if (btnElement.hasClass("btn-warning")) {
						btnElement.html("");
						btnElement.removeClass("btn-warning");
						btnElement.append("Recycler <i class='glyphicon glyphicon-refresh'></i>");
						spanElement.addClass("pjRetiree");
					} else {
						btnElement.html("");
						btnElement.addClass("btn-warning");
						btnElement.html("Retirer <i class='glyphicon glyphicon-remove'></i>");
						spanElement.removeClass("pjRetiree");
					}
				};
			}, function (error) {
				$window.location = "index.html#/fournisseur";
			});

		} else {
			$window.location = "index.html#/fournisseur";
		}
	};

});