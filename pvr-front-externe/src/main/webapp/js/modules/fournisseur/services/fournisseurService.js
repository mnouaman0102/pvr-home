fournisseurModule.factory('fournisseurService', function (ajaxService) {
    var service = {};

    service.getAllProjects = function (callback, errorCallback) {
        ajaxService.get("services/parameters/getAllProjects", {}, callback, errorCallback);
    };

    service.getAllDr = function (callback, errorCallback) {
        ajaxService.get("services/parameters/getAllDr", {}, callback, errorCallback);
    };

/*    service.getAllSiteByDr = function (idtDr, callback, errorCallback) {
        ajaxService.get("services/parameters/getAllSitesByDr", { idtDr: idtDr }, callback, errorCallback);
    };*/
    
    service.getAllSiteByDr = function(idtsDr, callback, errorCallback) {
        ajaxService.get("services/parameters/getAllSitesByListDr", {idtsDr: idtsDr}, callback, errorCallback);
    };

    service.getAllNumOperations = function (callback, errorCallback) {
        ajaxService.get("services/parameters/getAllNumOperationsForDeploi", {}, callback, errorCallback);
    };
    
    service.getAllEtats = function(callback, errorCallback) {
        ajaxService.get("services/parameters/getOperMesRtEtats", {}, callback, errorCallback);
    };

    service.getListContratByFournisseur = function (callback) {
        ajaxService.get("services/fourni/getListContratByFournisseur", {}, callback);
    };

    service.getTotalMesByCriteres = function (fourniCriDto, callback, errorCallback) {
        ajaxService.postJson("services/fourni/getTotalMesByCriteres", fourniCriDto, callback, errorCallback);
    };

    service.searchMes = function (fourniCriDto, pageNumber, pageSize, callback, errorCallback) {
        ajaxService.postJson("services/fourni/getListMesIdtForDeploi?pageNumber=" + pageNumber + "&pageSize=" + pageSize, fourniCriDto, callback, errorCallback);
    };

    service.addMorePj = function (operation, callback, errorCallback) {
        ajaxService.postJson("services/fourni/addMorePj", operation, callback, errorCallback);
    };

    service.requalifOperation = function (idtMes, idtEntite, callback, errorCallback) {
        ajaxService.get("services/fourni/requalifOperation", { idtMes: idtMes, idtEntite: idtEntite }, callback, errorCallback);
    };
    service.uploadFile = function (file, callback, errorCallback) {
        ajaxService.uploadFile("services/fourni/uploadFile", file, callback, errorCallback);
    };

    service.getOperationByIdt = function (idtOperation, callback, errorCallback) {
        ajaxService.get("services/fourni/getOperationByIdt", { idtOperation: idtOperation }, callback, errorCallback);
    };

    service.uploadPjsUpdateOperation = function (file, callback, errorCallback) {
        ajaxService.uploadFile("services/fourni/uploadPjsUpdateOperation", file, callback, errorCallback);
    };

    service.updateOperation = function (operation, callback, errorCallback) {
        ajaxService.postJson("services/fourni/updateOperation", operation, callback, errorCallback);
    };
    service.sendEmails = function (emails, callback, errorCallback) {
        ajaxService.postJson("services/fourni/sendEmails", emails, callback, errorCallback);
    };
    return service;
});