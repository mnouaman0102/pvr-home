consultationModule.factory('consultationService', function(ajaxService){
	var service = {};

    service.getAllDr = function (callback, errorCallback) {
        ajaxService.get("services/parameters/getAllDr", {}, callback, errorCallback);
    };

    service.getAllSiteByDr = function (idtDr, callback, errorCallback) {
        ajaxService.get("services/parameters/getAllSitesByDr", { idtDr: idtDr }, callback, errorCallback);
    };

    service.getAllProjects = function (callback, errorCallback) {
        ajaxService.get("services/parameters/getAllProjects", {}, callback, errorCallback);
    };

	service.getAllEtat = function(callback) {
		ajaxService.get("services/parameters/getAllEtat", {}, callback);
	};

    service.getListContratByFournisseur = function (callback) {
        ajaxService.get("services/fourni/getListContratByFournisseur", {}, callback);
    };

	/**
	 * function search 
	 * 
	 * @param {*} consultCriDto 
	 * @param {*} callback 
	 * @param {*} errorCallback 
	 */
	service.searchMes = function (consultCriDto, pageNumber, pageSize, callback, errorCallback) {
		ajaxService.postJson("services/consultation/consultMesByCriteresPagination?pageNumber="+pageNumber+"&pageSize="+pageSize, consultCriDto, callback, errorCallback);  
	};

	/**
	 * function total by search 
	 * 
	 * @param {*} consultCriDto 
	 * @param {*} callback 
	 * @param {*} errorCallback 
	 */
	service.getTotalMesByCriteres = function (consultCriDto, callback, errorCallback) {
		ajaxService.postJson("services/consultation/consultTotalMesByCriteres", consultCriDto, callback, errorCallback);  
	};


	return service;	
});