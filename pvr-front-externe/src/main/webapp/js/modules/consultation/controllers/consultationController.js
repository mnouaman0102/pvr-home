var consultationModule = angular.module('consultationModule', []);
consultationModule.controller('consultationController', function ($scope, $window, $compile, $rootScope, bootstrapModal, $modal, flashMessage, appConstants, consultationService, $timeout) {

	// select menu
	$('#nav-menu li').each(function () {
		$(this).removeClass('active');
	});
	$('#menu-CONSULTATION').addClass('active');

	$scope.currentPage = 1;
	$scope.totalRecords = 0;
	$scope.totalPages = 0;
	$scope.pageSize = 5;
	pageNumber = 1;
	$scope.indexReserve = 0;
	$scope.reserve = [];
	$scope.statut = [];
	//Cocher les Colones à Afficher 
	$scope.cmdShow = false;
	$scope.rtShow = false;
	$scope.pjShow = false;
	$scope.dateRtShow = false;
	$scope.pvrShow = false;
	$scope.datePvrShow = false;
	$scope.pvdShow = false;
	$scope.datePvdShow = false;
	$scope.etatShow = false;
	$scope.commentShow = false;
	$scope.listCheckMes = [

		{
			idt: 1,
			label: "Commande"
		},

		{
			idt: 2,
			label: "RT"
		},
		{
			idt: 3,
			label: "PJ"
		},
		{
			idt: 4,
			label: "Date RT"
		},
		{
			idt: 5,
			label: "PVR"
		},
		{
			idt: 7,
			label: "Date PVR"
		},
		{
			idt: 8,
			label: "PVD"
		},
		{
			idt: 10,
			label: "Date PVD"
		},
		{
			idt: 11,
			label: "Etat"
		},
		{
			idt: 12,
			label: "Commentaires"
		}
	];
	$scope.checks = [];
	$scope.resultChecks = [];

	//for initialization : reset
	$scope.consultCriDto = {
		idtsProjet: [],
		idtsContrat: [],
		idtsEtat: [],
		idtsDr: [],
		idtsSite: []
	};



	//pour l'édition
	$scope.error_length_reserve = false;
	$scope.listMES = [];

	$scope.mes = {
		idt: "",
		statutReserveDtos: []
	};
	// get all Etats
	$scope.listEtat = [];
	consultationService.getAllEtat(function (data) {
		angular.forEach(data, function (value, key) {
			$scope.listEtat.push(value);
		});
	});
	// get all Contrats
	$scope.listContrat = [];
	consultationService.getListContratByFournisseur(function (data) {
		$scope.listContrat.push({
			idt: 0,
			label: "Tous"
		});
		angular.forEach(data, function (value, key) {
			$scope.listContrat.push(value);
		});
		$scope.consultCriDto.idtContrat = 0;
	});

	// get all Projets
	$scope.listProjet = [];
	consultationService.getAllProjects(function (data) {
		angular.forEach(data, function (value, key) {
			$scope.listProjet.push(value);
		});
	});
	// get all DR
	$scope.listDr = [];
	consultationService.getAllDr(function (data) {
		angular.forEach(data, function (value, key) {
			$scope.listDr.push(value);
		});
	});	

	// get all Site By DR
	$scope.listSite = [];
	$scope.changeDR = function (idtsDr) {
		$scope.listSite = [];
		$scope.consultCriDto.idtsSite = [];
		if (idtsDr != null && idtsDr.length > 0) {
			for (var i = 0; i < idtsDr.length; i++) {
				consultationService.getAllSiteByDr(idtsDr[i], function (data) {
					angular.forEach(data, function (value, key) {
						$scope.listSite.push(value);
					});
				}, function (error) {
				});
			}
		}
	};

	// disabled paste
	$scope.onPaste = function (e) {
		e.preventDefault();
		return false;
	}

	$scope.checkKey = function (evt) {
		var theEvent = evt || window.event;
		var key = theEvent.keyCode || theEvent.which;
		theEvent.preventDefault();
	}

	// reset
	$scope.reset = function () {
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");

		//for initialization : reset    
		$scope.consultCriDto = {
			idtsProjet: [],
			idtsContrat: [],
			idtsEtat: [],
			idtsDr: [],
			idtsSite: []
		};
		if($scope.isDrOnly){
			$scope.consultCriDto.idtsDr.push($scope.userDr);
			$scope.getSites($scope.userDr);
		}
		else{
			$scope.getSites();
		}
		$scope.totalRecords = 0;
		$scope.totalPages = 0;
		$scope.listMES = [];
		$scope.listSite = [];
		$scope.listContrat = [];
		
	};



	// function search reserve in view
	$scope.search = function () {
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");
		consultationService.getTotalMesByCriteres($scope.consultCriDto, function (totalRec) {
			$scope.totalRecords = totalRec;
			if (totalRec > 0) {
				$scope.totalRec = totalRec;
			} else {
				$scope.totalRec = 1;
			}
			$scope.totalPages = Math.ceil($scope.totalRec / $scope.pageSize);
			page = 1;
			$window.pagObj = $('#pagination').twbsPagination({
				totalPages: $scope.totalPages,
				visiblePages: 10,
				onPageClick: function (event, page) {

					$scope.searchMesPagination(page);
				}
			}).on('page', function (event, page) {
			});
		});

	}
	// function search reserve pagination in scope.search
	$scope.searchMesPagination = function (pageNumber) {
		$scope.numberPage = pageNumber;
		if (pageNumber > 0 && pageNumber <= $scope.totalPages) {
			consultationService.searchMes($scope.consultCriDto, pageNumber, $scope.pageSize, function (data) {
				$scope.listMES = data;
				$scope.currentPage = pageNumber;
			});
		} else {
			if (pageNumber <= 0) {
				consultationService.searchMes($scope.consultCriDto, 1, $scope.pageSize, function (data) {
					$scope.listMES = data;
					$scope.currentPage = 1;
				});
			} else if (pageNumber > $scope.totalPages) {
				consultationService.searchMes($scope.consultCriDto, $scope.totalPages, $scope.pageSize, function (data) {
					$scope.listMES = data;
					$scope.currentPage = $scope.totalPages;
				});
			}
		}
	}

	/**
	 * Export Search result
	 */
	$scope.exportReport = function (idtsContrat, idtsProjet, idtsEtat, idtsDr, idtsSite, fileType) {
		$window.open("reporting/consultMes." + fileType + "?idtsContrat=" + idtsContrat + "&idtsProjet=" + idtsProjet + "&idtsEtat=" + idtsEtat + "&idtsDr=" + idtsDr + "&idtsSite=" + idtsSite);
	};

	// On Change Reserve : Edition Reserve MES
	$scope.onChangeReserve = function (str) {
		if (str != null && str != undefined && str != "" && str.length > 255)
			$scope.error_length_reserve = true;
		else $scope.error_length_reserve = false;
	}

	/**
	 * PopUp Pieces jointes Entités
	 */
	$scope.openPopUpPJ = function (pjExploitation, pjOptim, pjDcc, pjDccEnv, pjDccSuivi, pjDccEnvSuivi, pjQos, pjDeploiement, pjRt, pjRtEnv, pjPvr, pjPvd, pjQualifOptim, pjQualifExploitation, pjQualifQos, pjQualifDcc, pjQualifDccEnv, idtTypeProjet) {
		$scope.idtTypeProjet = idtTypeProjet;
		$scope.listPjExpl = pjExploitation;
		$scope.listPjOptim = pjOptim;
		$scope.listPjDcc = pjDcc;
		$scope.listPjDccEnv = pjDccEnv;
		$scope.listPjDccSuivi = pjDccSuivi;
		$scope.listPjDccEnvSuivi = pjDccEnvSuivi;
		$scope.listPjQos = pjQos;
		$scope.pjDeploiement = pjDeploiement;
		$scope.listPjRt = pjRt;
		$scope.listPjRtEnv = pjRtEnv;
		$scope.listPjPvr = pjPvr;
		$scope.listPjPvd = pjPvd;

		$scope.listPjQualifOptim = pjQualifOptim;
		$scope.listPjQualifExploitation = pjQualifExploitation;
		$scope.listPjQualifQos = pjQualifQos;
		$scope.listPjQualifDcc = pjQualifDcc;
		$scope.listPjQualifDccEnv = pjQualifDccEnv;
		
		bootstrapModal.open("openPopUpPJ");
	};

	/**
	 * Download PJ
	 */
	$scope.downloadFile = function (fileName) {
		if (fileName != null && fileName != "") {
			$window.open("services/consultation/getPjs?fileName=" + fileName);
		}

	};


	$scope.changeCheck = function (check, event) {
		if (event == true) {
			$scope.resultChecks.push(check.idt);
		}
		if (event == false) {
			$scope.resultChecks.splice($scope.resultChecks.indexOf(check.idt), 1);
		}
	};

	function includes(container, value) {
		var returnValue = false;
		var pos = container.indexOf(value);
		if (pos >= 0) {
			returnValue = true;
		}
		return returnValue;
	};

	$scope.addColums = function (checks) {
		if ($scope.resultChecks != null && $scope.resultChecks.length > 0) {
			//Commande
			if (includes($scope.resultChecks, 1))
				$scope.cmdShow = true;
			else $scope.cmdShow = false;
			//R&eacute;f. RT
			if (includes($scope.resultChecks, 2))
				$scope.rtShow = true;
			else $scope.rtShow = false;
			//P.J. RT
			if (includes($scope.resultChecks, 3))
				$scope.pjShow = true;
			else $scope.pjShow = false;
			//Date RT			
			if (includes($scope.resultChecks, 4))
				$scope.dateRtShow = true;
			else $scope.dateRtShow = false;
			//R&eacute;f. PVR			
			if (includes($scope.resultChecks, 5))
				$scope.pvrShow = true;
			else $scope.pvrShow = false;

			//Date PVR			
			if (includes($scope.resultChecks, 7))
				$scope.datePvrShow = true;
			else $scope.datePvrShow = false;

			//R&eacute;f. PVD			
			if (includes($scope.resultChecks, 8))
				$scope.pvdShow = true;
			else $scope.pvdShow = false;

			//Date PVD			
			if (includes($scope.resultChecks, 10))
				$scope.datePvdShow = true;
			else $scope.datePvdShow = false;
			//Etat Reception
			if (includes($scope.resultChecks, 11))
				$scope.etatShow = true;
			else $scope.etatShow = false;
			//Commentaire
			if (includes($scope.resultChecks, 12))
				$scope.commentShow = true;
			else $scope.commentShow = false;
		} else {
			$scope.cmdShow = false;
			$scope.rtShow = false;
			$scope.pjShow = false;
			$scope.dateRtShow = false;
			$scope.pvrShow = false;
			$scope.datePvrShow = false;
			$scope.pvdShow = false;
			$scope.datePvdShow = false;
			$scope.etatShow = false;
			$scope.commentShow = false;
		}
		bootstrapModal.close("popupMesDetails");
	};



	/**
	 * Ouvrir popup cocher les colones à afficher
	 */
	$scope.mesDetailsPopup = function (mes) {
		bootstrapModal.open("popupMesDetails");
	};

	/**
	 * PopUp Commentaires
	 */
	$scope.openPopUpComment = function (commentaireExploi, commentaireOptim, commentaireQos, commentaire, commentaireRt, idtTypeProjet) {
		$scope.idtTypeProjet = idtTypeProjet;
		$scope.commentExp = commentaireExploi;
		$scope.commentOpt = commentaireOptim;
		$scope.commentQos = commentaireQos;
		$scope.commentDeploi = commentaire;
		$scope.commentRt = commentaireRt;
		bootstrapModal.open("openPopUpComment");
	};

	/*************NAV TABS of modal visualisation Site*************/
	var tabClasses;

	function initTabs() {
		tabClasses = ["", ""];
	};

	$scope.getTabClass = function (tabNum) {
		return tabClasses[tabNum];
	};

	$scope.getTabPaneClass = function (tabNum) {
		return "tab-pane " + tabClasses[tabNum];
	};

	$scope.setActiveTab = function (tabNum) {
		initTabs();
		tabClasses[tabNum] = "active";
	};

	//Initialize 
	initTabs();
	$scope.setActiveTab(1);
	$('#openPopUpPJ').on('hidden.bs.modal', function () {
		initTabs();
		$scope.setActiveTab(1);
	})

});