/**
 * Z.BELGHAOUTI
 */
var remiseControleModule = angular.module('remiseControleModule', []);
remiseControleModule.controller('remiseControleController', function (remiseControleService, $scope, $rootScope, $window, $timeout, $compile, appConstants, bootstrapModal, flashMessage, confirmationModalService, $location) {

	window.localStorage.removeItem('remise_mes_modif');
	
	/**
	 * SELECT MENU
	 */
	$('#nav-menu li').each(function () {
		$(this).removeClass('active');
	});
	$('#menu-REMISE-CONTROLE').addClass('active');
	$('#dp-dateDebut').datepicker($.datepicker.regional["fr"]);
	$('#dp-dateFin').datepicker($.datepicker.regional["fr"]);
	$('#dp-dateRemise').datetimepicker({
	    format: "dd/mm/yyyy hh:ii",
	});

	/**
	 * VARIABLES PAGINATION
	 */
	$scope.currentPage = 1;
	$scope.totalRecords = 0;
	$scope.totalPages = 0;
	$scope.pageSize = 10;
	pageNumber = 1;

	/**
	 * ERROR REAMISE
	 */
	$scope.error_titre = false;
	$scope.error_solution = false;
	$scope.error_date = false;
	$scope.error_files = false;

	/**
	 * CRITERIA MES DTO
	 */
	$scope.mesCriDto = {
		idtContrat: 0,
		dateDebut: '',
		dateFin: '',
		selectedMesDto: [],
		idtDr: 0,
		idtSite: '',
		idtProjet: 0
	};
	/**
	 * LIST OF SELECTED IDT
	 */
	$scope.selectedMesDto = [];

	/**
	 * RESULT MES LIST PAGINATION
	 */
	$scope.listMES = [];

	/**
	 * OBJECT REMISE TO SAVE
	 */
	$scope.remise = {
		idt: '',
		titre: '',
		solution: '',
		dateCreation: '',
		listMesToUpdate: [],
		pjRemise: []
	};
	
	/**
	 * LIST CONTRAT PAR FOURNISSEUR
	 */
	remiseControleService.getAllContratsByFounisseur(function (data) {
		$scope.listContrat = data;
	}, function (err) {
		
	});

	/**
	 * LIST DR
	 */
	$scope.listDr = [];
	remiseControleService.getAllDr(function (data) {
		$scope.listDr.push({
			idt: 0,
			label: "Tous"
		});
		angular.forEach(data, function (value, key) {
			$scope.listDr.push(value);
		});
	});

	// get all Projets
	$scope.listProjet = [];
	remiseControleService.getAllProjects(function (data) {
		$scope.listProjet.push({
			idt: 0,
			label: "Tous"
		});
		angular.forEach(data, function (value, key) {
			$scope.listProjet.push(value);
		});
	});

	/**
	 * LIST SITES
	 */
	$scope.listSite = [];

	/**
	 * GET SITES BY DR
	 */
	$scope.changeDR = function (idtDr) {
		$scope.listSite = [];
		$scope.mesCriDto.idtSite = 0;
		remiseControleService.getAllSiteByDr(idtDr, function (data) {
			$scope.listSite = data;
		}, function (error) {
		});
	};

	/**
	 * CHANGE CONTRAT
	 */
	$scope.updateContrat = function () {
		$scope.selectedMesDto = [];
		$scope.listMES = [];
		$scope.totalRecords = 0;
		$scope.totalPages = 0;
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");
	};

	/**************************************** COLONNE A AFFICHER ***************************************/
	/**
	 * COCHER LES COLONNES A AFFICHER
	 */
	$scope.cmdShow = false;
	$scope.pjShow = false;
	$scope.fournisseurShow = false;
	$scope.contratShow = false;
	$scope.projetShow = false;
	$scope.listCheckMes = [
		{
			idt: 1,
			label: "Fournisseur"
		},
		{
			idt: 2,
			label: "Contrat"
		},
		{
			idt: 3,
			label: "Projet"
		},
		{
			idt: 4,
			label: "PJ"
		},
		{
			idt: 5,
			label: "Commande"
		}
	];

	$scope.checks = [];
	$scope.resultChecks = [];

	/**
	 * POP UP CASE A COCHER
	 */
	$scope.mesDetailsPopup = function () {
		bootstrapModal.open("popupMesDetails");
	};

	$scope.changeCheck = function (check, event) {
		if (event == true) {
			$scope.resultChecks.push(check.idt);
		}
		if (event == false) {
			$scope.resultChecks.splice($scope.resultChecks.indexOf(check.idt), 1);
		}
	};

	function includes(container, value) {
		var returnValue = false;
		var pos = container.indexOf(value);
		if (pos >= 0) {
			returnValue = true;
		}
		return returnValue;
	};

	$scope.addColums = function (checks) {
		if ($scope.resultChecks != null && $scope.resultChecks.length > 0) {

			if (includes($scope.resultChecks, 1))
				$scope.fournisseurShow = true;
			else $scope.fournisseurShow = false;

			if (includes($scope.resultChecks, 2))
				$scope.contratShow = true;
			else $scope.contratShow = false;
			if (includes($scope.resultChecks, 3))
				$scope.projetShow = true;
			else $scope.projetShow = false;
			if (includes($scope.resultChecks, 4))
				$scope.pjShow = true;
			else $scope.pjShow = false;

			if (includes($scope.resultChecks, 5))
				$scope.cmdShow = true;
			else $scope.cmdShow = false;

		} else {
			$scope.cmdShow = false;
			$scope.pjShow = false;
			$scope.fournisseurShow = false;
			$scope.contratShow = false;
			$scope.projetShow = false;
		}
		bootstrapModal.close("popupMesDetails");
	};

	/******************************************* CHANGE VALUES *************************************/
	$scope.onChangeTitre = function (str) {
		if (str == null || str == undefined || str == "")
			$scope.error_titre = true;
		else $scope.error_titre = false;
	};

	$scope.onChangeSolution = function (str) {
		if (str == null || str == undefined || str == "")
			$scope.error_solution = true;
		else $scope.error_solution = false;
	};

	$scope.onChangeDate = function (str) {
		if (str == null || str == undefined || str == "")
			$scope.error_date = true;
		else $scope.error_date = false;
	};

	/***************************************** SEARCH ********************************************************/
    /**
     * SEARCH PAGINATION
     */
	$scope.searchMesPagination = function (pageNumber) {
		$scope.numberPage = pageNumber;
		if (pageNumber > 0 && pageNumber <= $scope.totalPages) {
			remiseControleService.searchMes($scope.mesCriDto, pageNumber, $scope.pageSize, function (data) {
				$scope.listMES = data;
				$scope.currentPage = pageNumber;
			});
		} else {
			if (pageNumber <= 0) {
				remiseControleService.searchMes($scope.mesCriDto, 1, $scope.pageSize, function (data) {
					$scope.listMES = data;
					$scope.currentPage = 1;
				});
			} else if (pageNumber > $scope.totalPages) {
				remiseControleService.searchMes($scope.mesCriDto, $scope.totalPages, $scope.pageSize, function (data) {
					$scope.listMES = data;
					$scope.currentPage = $scope.totalPages;
				});
			}
		}
	};

    /**
     * VALIDATING SITES ENTRY ON SEARCH FILTER
     */
	$scope.errorSites = "";
	$scope.error_sites = false;
	$scope.validSites = function () {
		remiseControleService.valideSitesCriteres($scope.mesCriDto, function (errors) {
			$scope.error_sites = false;
			if (errors.length > 0) {
				msgErrors = "";
				for (var i = 0; i < errors.length; i++) {
					if (msgErrors != "") {
						msgErrors += "; "
					}
					msgErrors += errors[i];
				}
				$scope.errorSites = msgErrors;
				$scope.error_sites = true;
			}
		});
	}

    /**
     * SEARCH WITH CONDITIONS
     */
	$scope.search = function () {
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");
		$scope.selectedMesDto = [];
		$scope.listMES = [];

		if ($scope.mesCriDto.idtContrat > 0) {
			if (($scope.mesCriDto.dateDebut != '' && $scope.mesCriDto.dateFin != '') || ($scope.mesCriDto.dateDebut == '' && $scope.mesCriDto.dateFin == '')) {
				$scope.validSites();
				/**
				 * PANIER
				 */
				var list_Mes = [];
				$scope.nbrMesPanier = 0;
				if (window.localStorage.getItem('remise_mes_add') != null && window.localStorage.getItem('remise_mes_add') != "") {
					var str_mes = window.localStorage.getItem('remise_mes_add');
					var part = str_mes.split(',');
					for (var i = 0; i < part.length; i++) {
						if (part[i] != null && part[i] != "") {
							list_Mes.push(part[i]);
							$scope.nbrMesPanier++;
						}
					}
				}
				$scope.mesCriDto.selectedMesDto = list_Mes;
				/**
				 * END
				 */
				remiseControleService.getTotalMesByCriteres($scope.mesCriDto, function (totalRec) {
					$scope.totalRecords = totalRec;
					if (totalRec > 0) {
						$scope.totalRec = totalRec;
					} else {
						$scope.totalRec = 1;
					}
					$scope.totalPages = Math.ceil($scope.totalRec / $scope.pageSize);
					page = 1;
					$window.pagObj = $('#pagination').twbsPagination({
						totalPages: $scope.totalPages,
						visiblePages: 10,
						onPageClick: function (event, page) {
							$scope.searchMesPagination(page);
							$timeout(function () {
								refreshSelectedRows();
							}, 1000);
						}
					}).on('page', function (event, page) {
					});
				});
			} else {
				flashMessage("danger", "Veuillez spécifier Date Début et Date Fin.", "resultListe-FM", 3000);
			}

		} else {
			flashMessage("danger", "Veuillez spécifier un contrat.", "resultListe-FM", 3000);
		}
	};

	/**
	 * RESET
	 */
	$scope.reset = function () {

		$scope.errorSites = "";
		$scope.error_sites = false;

		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");

		$scope.mesCriDto = {
			idtContrat: 0,
			dateDebut: '',
			dateFin: '',
			selectedMesDto: [],
			idtDr: 0,
			idtSite: '',
			idtProjet: 0
		};

		$scope.totalRecords = 0;
		$scope.totalPages = 0;
		$scope.listMES = [];
		$scope.listSite = [];
	};

	/**********************************PIECES JOINTES ***********************************/

	/**
	 * PopUp Pieces jointes
	 */
	$scope.openPopUpPJ = function (pjExploitation, pjOptim, pjDcc, pjRt, pjPvr, pjQos) {
		$scope.listPjExpl = pjExploitation;
		$scope.listPjOptim = pjOptim;
		$scope.listPjDcc = pjDcc;
		$scope.listPjRt = pjRt;
		$scope.listPjPvr = pjPvr;
		$scope.listPjQos = pjQos;
		bootstrapModal.open("openPopUpPJ");
	};

	/**
	 * DOWNLOAD FILES
	 */
	$scope.downloadFile = function (fileName) {
		if (fileName != null && fileName != "") {
			$window.open("services/remise-controle/getPjsRemise?fileName=" + fileName);
		}
	};

	/**************************************** CHECKED ROWS ********************************************/
	/**
	 * ACTUALISE SELECTED ROWS
	 */
	function refreshSelectedRows() {

		$('#resultTable .clickable-row').each(function () {
			var idtMes = $(this).find('#idtMes').text();

			if (includes($scope.selectedMesDto, Number(idtMes))) {
				$(this).addClass('active_iam');
			} else {
				$(this).removeClass('active_iam');
			}
		});
	}

	/**
	 * SELECT ROW
	 */
	$('#resultTable').on('click', '.clickable-row', function (event) {
		if (!$(event.target).parents().andSelf().hasClass('excludeFromRowClick')) {
			var idtMes = $(this).find('#idtMes').text();
			if ($(this).hasClass('active_iam')) {
				// DESLECT
				$(this).removeClass('active_iam');
				$(this).find('input.checkSelectRow').prop('checked', false);
				$scope.selectedMesDto.splice($scope.selectedMesDto.indexOf(Number(idtMes)), 1);
			} else {
				// SELECT
				$(this).addClass('active_iam');
				$(this).find('input.checkSelectRow').prop('checked', true);
				$scope.selectedMesDto.push(Number(idtMes));
			}
		}
	});

	/**
	 *   SELECT/UNSELECT ALL ROWS
	 */
	$scope.isAllChecked = false;
	$scope.selectAllMES = function () {
		if ($scope.isAllChecked) {
			if ($scope.listMES.length == 0) {
				flashMessage("danger", "La liste des MES est vide.", "resultListe-FM", 3000);
			}
			else {
				confirmationModalService.confirmAction("Ce bouton séléctionne tous les MES dans toutes les pages, Voulez-vous confirmer cette action ?", function () {
					$scope.toggleAll();
				});
				confirmationModalService.close(function () {
					$scope.isAllChecked = false;
				});
			}
		}
		else {
			$scope.toggleAll();
		}
	};

	$scope.toggleAll = function () {
		var toggleStatus = $scope.isAllChecked;
		if (toggleStatus) {
			remiseControleService.searchIdMes($scope.mesCriDto, function (data) {
				$scope.selectedMesDto = [];
				if (data.length > 0) {
					$scope.selectedMesDto = data;
					$timeout(function () {
						refreshSelectedRows();
					}, 1300);
					flashMessage("success", "Tous les MES sont Sélectionnés !", "resultListe-FM", 4000);
				}
				else {
					$scope.isAllChecked = false;
				}
			});
			
		} else {
			$scope.selectedMesDto = [];
			$timeout(function () {
				refreshSelectedRows();
			}, 1000);

			flashMessage("success", "Tous les MES sont désélectionnés !", "resultListe-FM", 4000);
		}
	}

	/**
	 * RECUPERE LIST MES
	 */
	function listMesRemise() {
		var list_Mes = [];
		$scope.listMesPanier = [];
		var str_mes = window.localStorage.getItem('remise_mes_add');
		var part = str_mes.split(',');
		for (var i = 0; i < part.length; i++) {
			list_Mes.push(part[i]);
		}

		if (list_Mes.length > 0) {
			remiseControleService.getListMesPanier(list_Mes, function (data) {
				$scope.listMesPanier = data;
				remiseControleService.getAllContratsByFounisseur(function (data1) {
					$scope.listContrat = data1;
					$scope.mesCriDto.idtContrat = $scope.listMesPanier[0].idtContrat;
				});
			});
		}
	}
	
	/********************************************** PANIER **************************************************/

	/**
	 * INITIALIZE NUMBER OF MES IN PANIER
	 */
	$scope.nbrMesPanierFnct = function () {
		$scope.nbrMesPanier = 0;
		if (window.localStorage.getItem('remise_mes_add') != null && window.localStorage.getItem('remise_mes_add') != "") {
			var str_mes = window.localStorage.getItem('remise_mes_add');
			var part = str_mes.split(',');
			for (var i = 0; i < part.length; i++) {
				if (part[i] != null && part[i] != "") {
					$scope.nbrMesPanier++;
				}
			}
		}
	};

	$scope.nbrMesPanierFnct();

	/**
	 * ADD TO PANIER
	 */
	$scope.addPanier = function () {

		var list_Mes = [];
		var is_draft_rt = false;
		if ($scope.mesCriDto.idtContrat > 0) {
			if ($scope.selectedMesDto.length > 0) {
				if (window.localStorage.getItem('remise_mes_add') != null && window.localStorage.getItem('remise_mes_add') != "") {
					var str_mes = window.localStorage.getItem('remise_mes_add');
					var part = str_mes.split(',');
					for (var i = 0; i < part.length; i++) {
						list_Mes.push(part[i]);
					}
				}

				remiseControleService.getListMesPanier($scope.selectedMesDto, function (data) {
//					angular.forEach(data, function (value, key) {
//						if (value.listRt != null && value.listRt.length > 0) {
//							angular.forEach(value.listRt, function (val, ind) {
//								if (val.statutEditionIdt === 1) {
//									is_draft_rt = true;
//									return;
//								}
//							});
//						}
//					});
//					if (!is_draft_rt) {
						if (list_Mes != null && list_Mes.length > 0) {

							remiseControleService.getMesByIDT(Number(list_Mes[0]), function (data) {
								if ($scope.mesCriDto.idtContrat == data.idtContrat) {
									angular.forEach($scope.selectedMesDto, function (value, key) {
										list_Mes.push(value);
									});
									window.localStorage.setItem('remise_mes_add', list_Mes);
									$scope.search();
								} else {
									flashMessage("danger", "Veuillez spécifier la même contrat que vous avez dans votre panier.", "resultListe-FM", 3000);
								}
							});
						} else {
							angular.forEach($scope.selectedMesDto, function (value, key) {
								list_Mes.push(value);
							});
							window.localStorage.setItem('remise_mes_add', list_Mes);
							$scope.search();
						}
//					} else {
//						flashMessage("danger", "Veuillez ne pas sélectionner des sites avec Remise au contôle en mode Draft.", "resultListe-FM", 3000);
//					}
				});

			} else {
				// impossible de creer rt sans selectionner des sites
				flashMessage("danger", "Veuillez sélectionner d'abord des sites.", "resultListe-FM", 3000);
			}
		} else {
			flashMessage("danger", "Veuillez spécifier un contrat.", "resultListe-FM", 3000);
		}
	};

	/**
	 * RECUPERE LIST MES PANIER
	 */
	function listMesPanierRT() {
		var list_Mes = [];
		$scope.listMesPanier = [];
		var str_mes = window.localStorage.getItem('remise_mes_add');
		var part = str_mes.split(',');
		for (var i = 0; i < part.length; i++) {
			list_Mes.push(part[i]);
		}

		if (list_Mes.length > 0) {
			remiseControleService.getListMesPanier(list_Mes, function (data) {
				$scope.listMesPanier = data;
				remiseControleService.getAllContratsByFounisseur(function (data) {
					$scope.listContrat = data;
					$scope.mesCriDto.idtContrat = $scope.listMesPanier[0].idtContrat;
				}, function (err) {
					
				});
				
			});
		}
	}

	/**
	 * PopUp Consult Panier
	 */
	$scope.consultPanier = function () {

		if (window.localStorage.getItem('remise_mes_add') != null && window.localStorage.getItem('remise_mes_add') != "") {
			listMesPanierRT();
			bootstrapModal.open("popupConsultPanier");
		} else {
			flashMessage("danger", "Aucun site ajouté au panier.", "resultListe-FM", 3000);
		}

	};

	/**
	 * REMOVE LINE FROM LIST PANIER
	 */
	$scope.removeLine = function (ind) {
		$scope.listMesPanier.splice(ind, 1);
	};

	/**
	 * VALIDATE REMOVE FROM PANIER
	 */
	$scope.validateRemove = function () {
		var list_Mes = [];
		angular.forEach($scope.listMesPanier, function (value, key) {
			list_Mes.push(value.idt);
		});
		window.localStorage.setItem('remise_mes_add', list_Mes);
		bootstrapModal.close("popupConsultPanier");
		$scope.search();
	};

	/******************************************* SAVE REMISE **********************************************/

	/**
	 * PopUp CREATE REMISE
	 */
	$scope.createRemise = function () {
		
		$scope.list_Mes_update = [];
		if (window.localStorage.getItem('remise_mes_add') != null && window.localStorage.getItem('remise_mes_add') != "") {
			var str_mes = window.localStorage.getItem('remise_mes_add');
			var part = str_mes.split(',');
			for (var i = 0; i < part.length; i++) {
				$scope.list_Mes_update.push(part[i]);
			}
			if ($scope.list_Mes_update.length > 0) {
				remiseControleService.getListMesPanier($scope.list_Mes_update, function (data) {
					remiseControleService.getAllContratsByFounisseur(function (data1) {
						$scope.listContrat = data1;
						$scope.mesCriDto.idtContrat = data[0].idtContrat;
					}, function (err) {
						
					});
				});
			}
			bootstrapModal.open("popupCreateRemise");
		} else {
			flashMessage("danger", "Veuillez ajouter d'abord des sites au panier.", "resultListe-FM", 3000);
		}
	};
	/**
	 * CONSULTATON SAVE REMISE
	 */
	$scope.consultationRemise = function () {
		$scope.lengthPJ = 0;
		var isFormDataEmpty = false;
		var isFormDataEmptyLog = false;
		$scope.error_files = false;
		if ($scope.remise.titre == null || $scope.remise.titre == "") {
			$scope.error_titre = true;
		} else if ($scope.remise.solution == null || $scope.remise.solution == "") {
			$scope.error_solution = true;
		} else if ($scope.remise.dateCreation == null || $scope.remise.dateCreation == "") {
			$scope.error_date = true;
		} else {
			
			var j = 0;
			$('#listPjForm .inputFile').each(function () {
				var myFile = $(this)[0].files[0];
				if (myFile) {
					j++;
					isFormDataEmpty = true;
				}
			});

			$('#listPjForm .inputFile1').each(function () {
				var myFileLog = $(this)[0].files[0];
				if (myFileLog) {
					isFormDataEmptyLog = true;
					j++;
				}
			});
			if(!isFormDataEmpty && !isFormDataEmptyLog) {
				$scope.error_files = true;
			} else {
				$scope.error_files = false;
				listMesRemise();
				$scope.lengthPJ = j;
				bootstrapModal.closeById("popupCreateRemise");
				$timeout(function () {
					bootstrapModal.open("popupConsultationRemise");
				}, 500);
			}
		}
	};
	/**
	 * CANCEL CONSULT SAVE REMISE
	 */
	$scope.cancelConsultationRemise = function () {
		bootstrapModal.closeById("popupConsultationRemise");
		bootstrapModal.open("popupCreateRemise");
	};

	/**
	 * ADD NEW PIECE JOINT SOLUTION PAR SITE
	 */
	$scope.indexPj = 0;
	$scope.appendHtmlInputFile = function () {
		$scope.indexPj++;
		var newEle = angular.element("<div class='row bloc_line'>\
			<div class='col-md-12'>\
				<div class='col-md-9 col-sm-9 col-xs-9'>\
					<input type='file' class='form-control inputFile' id='fileAttach["+ ($scope.indexPj) + "]'>\
				</div>\
				<div class='col-md-1 col-sm-1 col-xs-1'>\
					<i class='glyphicon glyphicon-remove-sign icon-clear-" + ($scope.indexPj) + "'\
						title='Supprimer le fichier'\
						style='margin-top: 8px; color: red; cursor: pointer; margin-left: -22px;'\
						ng-click='cleanFile(" + ($scope.indexPj) + ")'></i>\
				</div>\
				<div class='col-md-2 col-sm-2 col-xs-2'>\
					<button type='button' ng-click='removeHtmlInputFile("+ ($scope.indexPj) + ")'\
						class='btn btn-xs btn-warning bnt-" + ($scope.indexPj) + "' style='margin-top: 5px;'>\
						<i class='glyphicon glyphicon-remove'></i>\
					</button>\
				</div>\
			</div>\
		</div>");
		var pieceJointe = document.getElementById('pjInputList');
		angular.element(pieceJointe).append($compile(newEle)($scope));
	};
	
	/**
	 * ADD NEW PIECE JOINT LOGS
	 */
	$scope.indexPjLog = 0;
	$scope.appendHtmlInputFile1 = function () {
		$scope.indexPjLog++;
		var newEle = angular.element("<div class='row bloc_line1'>\
			<div class='col-md-12'>\
				<div class='col-md-9 col-sm-9 col-xs-9'>\
					<input type='file' class='form-control inputFile1' id='fileAttach1["+ ($scope.indexPjLog) + "]'>\
				</div>\
				<div class='col-md-1 col-sm-1 col-xs-1'>\
					<i class='glyphicon glyphicon-remove-sign icon-clear-log-" + ($scope.indexPjLog) + "'\
						title='Supprimer le fichier'\
						style='margin-top: 8px; color: red; cursor: pointer; margin-left: -22px;'\
						ng-click='cleanFile1(" + ($scope.indexPjLog) + ")'></i>\
				</div>\
				<div class='col-md-2 col-sm-2 col-xs-2'>\
					<button type='button' ng-click='removeHtmlInputFile1("+ ($scope.indexPjLog) + ")'\
						class='btn btn-xs btn-warning bnt-log-" + ($scope.indexPjLog) + "' style='margin-top: 5px;'>\
						<i class='glyphicon glyphicon-remove'></i>\
					</button>\
				</div>\
			</div>\
		</div>");
		var pieceJointe = document.getElementById('pjInputList1');
		angular.element(pieceJointe).append($compile(newEle)($scope));
	};

	/**
	 * REMOVE PIECE JOINT SOLUTION PAR SITE
	 */
	$scope.removeHtmlInputFile = function (indice) {
		$('.bnt-' + indice).parents(".bloc_line").remove();
	};
	
	/**
	 * REMOVE PIECE JOINT LOGS
	 */
	$scope.removeHtmlInputFile1 = function (indice) {
		$('.bnt-log-' + indice).parents(".bloc_line1").remove();
	};
	
	$scope.cleanFile = function (index) {
		$('.icon-clear-' + index).parents(".bloc_line").find('input.inputFile').val("");
	};
	
	$scope.cleanFile1 = function (index) {
		$('.icon-clear-log-' + index).parents(".bloc_line1").find('input.inputFile1').val("");
	};

	/**
	 * SAVE REMISE
	 */
	$scope.saveRemise = function () {

		var formData = new FormData();
		var fileName = '';
		var fileNameOrigin = '';
		var fileExtention = '';
		var j = 0;
		var isFormDataEmpty = false;
		
		var fileNameLog = '';
		var fileNameOriginLog = '';
		var fileExtentionLog = '';
		var x = 0;
		var isFormDataEmptyLog = false;

		if ($scope.remise.titre == null || $scope.remise.titre == "") {
			$scope.error_titre = true;
		} else if ($scope.remise.solution == null || $scope.remise.solution == "") {
			$scope.error_solution = true;
		} else if ($scope.remise.dateCreation == null || $scope.remise.dateCreation == "") {
			$scope.error_date = true;
		} else {

			$('#listPjForm .inputFile').each(function () {
				var myFile = $(this)[0].files[0];
				if (myFile) {
					isFormDataEmpty = true;
					j++;
					fileName = 'REMISE' + j;
					fileNameOrigin = 'originRemise' + j;
					fileExtention = myFile.name.split('.')[1];
					var fileExt = 'extRemise' + j;
					formData.append(fileName, myFile);
					formData.append(fileNameOrigin, myFile.name.split('.')[0]);
					formData.append(fileExt, fileExtention);
				}
			});

			$('#listPjForm .inputFile1').each(function () {
				var myFileLog = $(this)[0].files[0];
				if (myFileLog) {
					isFormDataEmptyLog = true;
					x++;
					fileNameLog = 'REMISE_LOG' + x;
					fileNameOriginLog = 'originRemiseLog' + x;
					fileExtentionLog = myFileLog.name.split('.')[1];
					var fileExtLog = 'extRemiseLog' + x;
					formData.append(fileNameLog, myFileLog);
					formData.append(fileNameOriginLog, myFileLog.name.split('.')[0]);
					formData.append(fileExtLog, fileExtentionLog);
				}
			});
			if (isFormDataEmpty || isFormDataEmptyLog) {
				confirmationModalService.confirmAction("Voulez-vous créer cette remise au contrôle ?", function () {
					
					remiseControleService.uploadFile(formData, function (data) {
						$scope.remise.pjRemise = data;
						$scope.remise.listMesToUpdate = $scope.list_Mes_update;
						remiseControleService.saveRemise($scope.remise, function (data) {
							flashMessage("success", appConstants.msgSucces, "global-FM", 3000);
							bootstrapModal.close("popupCreateRemise");
							window.localStorage.removeItem('remise_mes_add');
							$timeout(function () {
								$window.location.reload();
							}, 2000);
						}, function (err) {
							flashMessage("danger", appConstants.msgError, "global-FM", 3000);
						});
					}, function (error) {
						flashMessage("danger", "Erreur de chargement du fichier.", "global-FM", 3000);
					});
				});
			} else {
				$scope.error_files = true;
			}
		}
	};

	/**
	 * Go TO LIST REMISE
	 */
	$scope.listRemise = function () {
		$location.path('/remise-controle');
	};

});