var remiseListModule = angular.module('remiseListModule', []);

remiseListModule.controller('remiseListController', function (remiseListService, $scope, $rootScope, appConstants, $window, $timeout, $compile, bootstrapModal, flashMessage, confirmationModalService, $location) {

	// select menu
	$('#nav-menu li').each(function () {
		$(this).removeClass('active');
	});
	$('#menu-REMISE-CONTROLE').addClass('active');

	$scope.currentPage = 1;
	$scope.totalRecords = 0;
	$scope.totalPages = 0;
	$scope.pageSize = 10;
	var idtRemise = $location.search().remise;
	if (idtRemise != undefined && idtRemise != null && idtRemise != '') {
		$scope.remiseCri = {
			codeEtat: 'Tous',
			idtRemise: idtRemise
		};
	} else {
		$scope.remiseCri = {
			codeEtat: 'Tous',
			idtRemise: null
		};
	}
	

	$scope.addRemise = function () {
		$location.path('/add-remise');
	};
	
	$scope.gotoEdit = function (idtRemise) {
		$window.location = "index.html#/edit-remise/"+idtRemise;
	};

	$scope.listRemiseDtos = [];
	$scope.listEtats = [];

	remiseListService.getAllEtatRemise(function (data) {
		$scope.listEtats.push({
			idt: 0,
			code: 'Tous',
			label: 'Tous'
		}, {
			idt: -1,
			code: 'DRAFT',
			label: 'Draft'
		});
		angular.forEach(data, function (value, key) {
			$scope.listEtats.push(value);
		});
		$scope.search();
	}, function (error) {
		$scope.listEtats = [];
	});
	
	// function search reserve in view
	$scope.search = function () {
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");
		remiseListService.getTotalRemiseControle($scope.remiseCri, function (totalRec) {
			$scope.totalRecords = totalRec;
			if (totalRec > 0) {
				$scope.totalRec = totalRec;
			} else {
				$scope.totalRec = 1;
			}
			$scope.totalPages = Math.ceil($scope.totalRec / $scope.pageSize);
			page = 1;
			$window.pagObj = $('#pagination').twbsPagination({
				totalPages: $scope.totalPages,
				visiblePages: 10,
				onPageClick: function (event, page) {
					$scope.searchRemisePagination(page);
				}
			}).on('page', function (event, page) {

			});
		});
	}

	// function search reserve pagination in scope.search
	$scope.searchRemisePagination = function (pageNumber) {
		$scope.numberPage = pageNumber;
		if (pageNumber > 0 && pageNumber <= $scope.totalPages) {
			remiseListService.getListRemiseControle($scope.remiseCri, pageNumber, $scope.pageSize, function (data) {
				$scope.listRemiseDtos = data;
				$scope.currentPage = pageNumber;
			});
		} else {
			if (pageNumber <= 0) {
				remiseListService.getListRemiseControle($scope.remiseCri, 1, $scope.pageSize, function (data) {
					$scope.listRemiseDtos = data;
					$scope.currentPage = 1;
				});
			} else if (pageNumber > $scope.totalPages) {
				remiseListService.getListRemiseControle($scope.remiseCri, $scope.totalPages, $scope.pageSize, function (data) {
					$scope.listRemiseDtos = data;
					$scope.currentPage = $scope.totalPages;
				});
			}
		}
	}

	$scope.reset = function () {
		$('#pagination').empty();
		$('#pagination').removeData("twbs-pagination");
		$('#pagination').unbind("page");

		$scope.totalRecords = 0;
		$scope.totalPages = 0;
		$scope.listRemiseDtos = [];
		$scope.remiseCri = {
			codeEtat: 'Tous',
			idtRemise: null
		};
		$scope.search();
	};

	/**
	 * DOWNLOAD FILES
	 */
	$scope.downloadFile = function (fileName) {
		if (fileName != null && fileName != "") {
			$window.open("services/remise-controle/getPjsRemise?fileName=" + fileName);
		}
	};
	
	/**
	 * PopUp Pieces jointes
	 */
	$scope.openPopUpPJ = function (pjRt) {
		$scope.listPjRt = pjRt;
		bootstrapModal.open("openPopUpPJ");
	};
	
	/**
	 * PopUp Visualisation
	 */
	$scope.popUpVisualiser = function (remise) {
		$scope.remise = remise;
		$scope.lengthPJ = 0;
		if(remise.pjsRemise != null && remise.pjsRemise.length > 0) {
			$scope.lengthPJ = remise.pjsRemise.length;
		}
		remiseListService.getListMesByRemise(remise.idt, function (data) {
			$scope.listMesPanier = data;
			bootstrapModal.open("popupVisualisation");
		}, function (error) {
			$scope.listEtats = [];
		});
		
	};
	
	/**
	 * CANCEL CONSULT REMISE
	 */
	$scope.cancelConsultationRemise = function () {
		bootstrapModal.closeById("popupVisualisation");
	};
	
	/**
	 * PopUp Pieces jointes
	 */
	$scope.openPopUpPJMes = function (pjExploitation, pjOptim, pjDcc, pjRt, pjPvr, pjQos) {
		$scope.listPjExpl = pjExploitation;
		$scope.listPjOptim = pjOptim;
		$scope.listPjDcc = pjDcc;
		$scope.listPjRt = pjRt;
		$scope.listPjPvr = pjPvr;
		$scope.listPjQos = pjQos;
		bootstrapModal.closeById("popupVisualisation");
		$timeout(function () {
			bootstrapModal.open("openPopUpPJMes");
		}, 500);
	};
	
	$scope.cancelPjMes = function () {
		bootstrapModal.closeById("openPopUpPJMes");
		$timeout(function () {
			bootstrapModal.open("popupVisualisation");
		}, 500);
	};
	
	/**
	 * DELETE REMISE
	 */
	$scope.deleteRemise = function (idt) {
		
		confirmationModalService.confirmAction("Voulez-vous vraiment supprimer cette remise au contrôle ?", function () {
			remiseListService.deleteRemise(idt, function (data) {
				flashMessage("success", "Suppression de la remise au contrôle avec succès", "result-FM", 5000);
				$scope.search();
			}, function (error) {
				flashMessage("danger", char_convert(error.message), "result-FM", 5000);
			});
		});
	};
	
	/**
	 * VALIDATE REMISE DRAFT
	 */
	$scope.validateRemiseDraft = function (idt) {
		
		confirmationModalService.confirmAction("Voulez-vous vraiment valider ce Draft ?", function () {
			remiseListService.validerRemiseDraft(idt, function (data) {
				flashMessage("success", "Validation de la remise au contrôle avec succès", "result-FM", 5000);
				$scope.search();
			}, function (error) {
				flashMessage("danger", char_convert(error.message), "result-FM", 5000);
			});
		});
	};
	
	/**
	 * EXPORT
	 */
	$scope.exportReport = function (idtRemise, fileType) {
		$window.open("reporting/remiseMes." + fileType + "?idtRemise=" + idtRemise);
	};
	
	function char_convert(str) {

	    var chars = ["©","Û","®","ž","Ü","Ÿ","Ý","$","Þ","%","¡","ß","¢","à","£","á","À","¤","â",
	                 "Á","¥","ã","Â","¦","ä","Ã","§","å","Ä","¨","æ","Å","©","ç","Æ","ª","è","Ç",
	                 "«","é","È","¬","ê","É","­","ë","Ê","®","ì","Ë","¯","í","Ì","°","î","Í","±",
	                 "ï","Î","²","ð","Ï","³","ñ","Ð","´","ò","Ñ","µ","ó","Õ","¶","ô","Ö","·","õ",
	                 "Ø","¸","ö","Ù","¹","÷","Ú","º","ø","Û","»","ù","Ü","@","¼","ú","Ý","½","û",
	                 "Þ","€","¾","ü","ß","¿","ý","à","‚","À","þ","á","ƒ","Á","ÿ","å","„","Â","æ",
	                 "…","Ã","ç","†","Ä","è","‡","Å","é","ˆ","Æ","ê","‰","Ç","ë","Š","È","ì","‹",
	                 "É","í","Œ","Ê","î","Ë","ï","Ž","Ì","ð","Í","ñ","Î","ò","‘","Ï","ó","’","Ð",
	                 "ô","“","Ñ","õ","”","Ò","ö","•","Ó","ø","–","Ô","ù","—","Õ","ú","˜","Ö","û",
	                 "™","×","ý","š","Ø","þ","›","Ù","ÿ","œ","Ú"]; 
	    var codes = ["&copy;","&#219;","&reg;","&#158;","&#220;","&#159;","&#221;","&#36;","&#222;",
	                 "&#37;","&#161;","&#223;","&#162;","&#224;","&#163;","&#225;","&Agrave;","&#164;",
	                 "&#226;","&Aacute;","&#165;","&#227;","&Acirc;","&#166;","&#228;","&Atilde;",
	                 "&#167;","&#229;","&Auml;","&#168;","&#230;","&Aring;","&#169;","&#231;","&AElig;",
	                 "&#170;","&#232;","&Ccedil;","&#171;","&#233;","&Egrave;","&#172;","&#234;",
	                 "&Eacute;","&#173;","&#235;","&Ecirc;","&#174;","&#236;","&Euml;","&#175;","&#237;",
	                 "&Igrave;","&deg;","&#238;","&Iacute;","&#177;","&#239;","&Icirc;","&#178;","&#240;",
	                 "&Iuml;","&#179;","&#241;","&ETH;","&#180;","&#242;","&Ntilde;","&#181;","&#243;",
	                 "&Otilde;","&#182;","&#244;","&Ouml;","&#183;","&#245;","&Oslash;","&#184;","&#246;",
	                 "&Ugrave;","&#185;","&#247;","&Uacute;","&#186;","&#248;","&Ucirc;","&#187;","&#249;",
	                 "&Uuml;","&#64;","&#188;","&#250;","&Yacute;","&#189;","&#251;","&THORN;","&#128;",
	                 "&#190;","&#252","&szlig;","&#191;","&#253;","&agrave;","&#130;","&#192;","&#254;",
	                 "&aacute;","&#131;","&#193;","&#255;","&aring;","&#132;","&#194;","&aelig;","&#133;",
	                 "&#195;","&ccedil;","&#134;","&#196;","&egrave;","&#135;","&#197;","&eacute;","&#136;",
	                 "&#198;","&ecirc;","&#137;","&#199;","&euml;","&#138;","&#200;","&igrave;","&#139;",
	                 "&#201;","&iacute;","&#140;","&#202;","&icirc;","&#203;","&iuml;","&#142;","&#204;",
	                 "&eth;","&#205;","&ntilde;","&#206;","&ograve;","&#145;","&#207;","&oacute;","&#146;",
	                 "&#208;","&ocirc;","&#147;","&#209;","&otilde;","&#148;","&#210;","&ouml;","&#149;",
	                 "&#211;","&oslash;","&#150;","&#212;","&ugrave;","&#151;","&#213;","&uacute;","&#152;",
	                 "&#214;","&ucirc;","&#153;","&#215;","&yacute;","&#154;","&#216;","&thorn;","&#155;",
	                 "&#217;","&yuml;","&#156;","&#218;"];

	    for(var x = 0; x < codes.length; x++){
	    	var regex = new RegExp(codes[x], "g");
	    	if(str != null && str != undefined){
	    		str = str.replace(regex, chars[x]);
	    	}
	    }
	    return str;
	 };

});