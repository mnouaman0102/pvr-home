remiseListModule.factory('remiseListService',function (ajaxService) {

    var service = {};

    service.getAllEtatRemise = function(callback, errorCallback) {
        ajaxService.get("services/remise-controle/getAllEtatRemise", {}, callback, errorCallback);
    };

    service.getListRemiseControle = function (mesCriDto, pageNumber, pageSize, callback, errorCallback) {
    	ajaxService.postJson("services/remise-controle/getListRemiseControle?pageNumber="+pageNumber+"&pageSize="+pageSize, mesCriDto, callback, errorCallback);
    };
    
    service.getTotalRemiseControle = function (mesCriDto, callback, errorCallback) {
    	ajaxService.postJson("services/remise-controle/getTotalRemiseControle", mesCriDto, callback, errorCallback);
    };
	
    service.getListMesByRemise = function(idtRemise, callback, errorCallback) {
        ajaxService.get("services/remise-controle/getListMesByRemise", {idtRemise: idtRemise}, callback, errorCallback);
    };
    
    service.deleteRemise = function(idtRemise, callback, errorCallback) {
        ajaxService.get("services/remise-controle/deleteRemise", {idtRemise: idtRemise}, callback, errorCallback);
    };
    
    service.validerRemiseDraft = function(idtRemise, callback, errorCallback) {
        ajaxService.get("services/remise-controle/validerRemiseDraft", {idtRemise: idtRemise}, callback, errorCallback);
    };
    
    return service;
});