remiseControleModule.factory('remiseControleService',function (ajaxService) {
    var service = {};
    
    service.getAllContratsByFounisseur = function(callback, errorCallback) {
        ajaxService.get("services/remise-controle/getAllContratsByFounisseur", {}, callback, errorCallback);
    };
    
    service.uploadFile = function (file, callback, errorCallback) {
        ajaxService.uploadFile("services/remise-controle/uploadFile", file, callback, errorCallback);
    };

    service.saveRemise = function(remise, callback, errorCallback) {
        ajaxService.postJson("services/remise-controle/saveRemiseControle", remise , callback,errorCallback);
    };

    service.searchMes = function (mesCriDto, pageNumber, pageSize, callback, errorCallback) {
		ajaxService.postJson("services/remise-controle/getMesByCriteresMultiSites?pageNumber="+pageNumber+"&pageSize="+pageSize, mesCriDto, callback, errorCallback);  
	};

    service.searchIdMes = function (mesCriDto, callback, errorCallback) {
		ajaxService.postJson("services/remise-controle/getIdMesByCriteres", mesCriDto, callback, errorCallback);  
	};
	
	service.getTotalMesByCriteres = function (mesCriDto, callback, errorCallback) {
		ajaxService.postJson("services/remise-controle/getTotalMesByCriteresMultiSites", mesCriDto, callback, errorCallback);  
	};
	
	service.valideSitesCriteres = function (mesCriDto, callback, errorCallback) {
		ajaxService.postJson("services/remise-controle/validSitesCriteres", mesCriDto, callback, errorCallback);  
	};
	
	service.getListMesByIDTS = function(list_Mes, callback, errorCallback) {
        ajaxService.postJson("services/remise-controle/getListMesByIDTS", list_Mes, callback, errorCallback);
    };
    
    service.getMesByIDT = function(idtMes, callback, errorCallback) {
        ajaxService.get("services/remise-controle/getMesByIDT", {idtMes: idtMes}, callback, errorCallback);
    };

    service.getAllDr = function(callback, errorCallback) {
        ajaxService.get("services/remise-controle/getAllDr", {}, callback, errorCallback);
    };
    
    service.getAllSiteByDr = function(idtDr, callback, errorCallback) {
        ajaxService.get("services/remise-controle/getAllSitesByDr", {idtDr: idtDr}, callback, errorCallback);
    };

	service.getAllProjects = function(callback, errorCallback) {
		ajaxService.get("services/remise-controle/getAllProjects", {}, callback, errorCallback);
    };
    
    service.getListMesPanier = function(list_Mes, callback, errorCallback) {
        ajaxService.postJson("services/remise-controle/getListMesByIDTS", list_Mes, callback, errorCallback);
    };
    
    return service;
});