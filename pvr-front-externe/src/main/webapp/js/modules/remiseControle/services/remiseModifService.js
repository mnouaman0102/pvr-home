remiseModifModule.factory('remiseModifService',function (ajaxService) {
    var service = {};
       
    service.getAllContratsByFounisseur = function(callback, errorCallback) {
        ajaxService.get("services/remise-controle/getAllContratsByFounisseur", {}, callback, errorCallback);
    };
    
    service.uploadFile = function (file, callback, errorCallback) {
        ajaxService.uploadFile("services/remise-controle/uploadFile", file, callback, errorCallback);
    };

    service.updateRemiseDraft = function(rt, callback, errorCallback) {
        ajaxService.postJson("services/remise-controle/updateRemiseDraft", rt , callback,errorCallback);
    };

    service.searchMes = function (mesCriDto, pageNumber, pageSize, callback, errorCallback) {
		ajaxService.postJson("services/remise-controle/getMesByCriteresMultiSites?pageNumber="+pageNumber+"&pageSize="+pageSize, mesCriDto, callback, errorCallback);  
	};
	
	service.getTotalMesByCriteres = function (mesCriDto, callback, errorCallback) {
		ajaxService.postJson("services/remise-controle/getTotalMesByCriteresMultiSites", mesCriDto, callback, errorCallback);  
	};
	
	service.getListMesByIDTS = function(list_Mes, callback, errorCallback) {
        ajaxService.postJson("services/remise-controle/getListMesByIDTS", list_Mes, callback, errorCallback);
    };
	
    service.getAllDr = function(callback, errorCallback) {
        ajaxService.get("services/remise-controle/getAllDr", {}, callback, errorCallback);
    };
    
    service.getAllSiteByDr = function(idtDr, callback, errorCallback) {
        ajaxService.get("services/remise-controle/getAllSitesByDr", {idtDr: idtDr}, callback, errorCallback);
    };

	service.getAllProjects = function(callback, errorCallback) {
		ajaxService.get("services/remise-controle/getAllProjects", {}, callback, errorCallback);
    };
    
    service.getListMesPanier = function(list_Mes, callback, errorCallback) {
        ajaxService.postJson("services/remise-controle/getListMesByIDTS", list_Mes, callback, errorCallback);
    };
    
    service.getRemiseDraftByIdt = function(idtRemise, callback, errorCallback) {
        ajaxService.get("services/remise-controle/getRemiseDraftByIdt", {idtRemise: idtRemise}, callback, errorCallback);
    };
    
    service.getMesByIDT = function(idtMes, callback, errorCallback) {
        ajaxService.get("services/remise-controle/getMesByIDT", {idtMes: idtMes}, callback, errorCallback);
    };
	
    return service;
});