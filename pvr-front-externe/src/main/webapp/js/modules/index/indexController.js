// var indexModule = angular.module('indexModule', ['ngCookies'])
// 						 .controller('indexController', function($scope,$rootScope,$window, $location, $cookieStore, $log, ajaxService, UserService, flashMessage) {
//
// 	// exceptions handle.
//     $scope.$on('exception', function(e, cause) {
//     	console.log('error catched ==> ');
//         console.log(' cause ');
//         console.log(cause);
//         console.log(' event ');
//         console.log(e.toString());
//     });
//
//     $rootScope.currentLogin = "";
// 	// Get current user login
// 	UserService.getCurrentLogin(function (data){
// 		if (data == undefined || data == null) {
// 			gotoLoginPage();
// 		}else{
// 			$rootScope.currentLogin = data;
// 			// Get authentified user roles
// 			UserService.getAuthentifiedUserRoles(function(roles) {
// 				$rootScope._authRoles = {};
// 				for (var i = 0; i < roles.length; i++) {
// 					$rootScope._authRoles[roles[i]] = true;
// 				}
// 			});
// 		}
// 	},function (error) {
// 		gotoLoginPage();
// 	});
//
//
//     // Logout
//     $scope.logout = function() {
//         ajaxService.post("j_spring_security_logout", {}, function(){
//             $rootScope.removeAllCookies();
//             //window.localStorage.removeItem('con');
// 			gotoLoginPage();
//         });
//     };
//
// 	// clear cookies
// 	$rootScope.removeAllCookies = function() {
//         var cookies = document.cookie.split(";");
//         for (var i = 0; i < cookies.length; i++) {
//             var cle = $.trim(cookies[i].split("=")[0]);
//             $cookieStore.remove(cle);
//         }
// 	};
//
//     // Go to location
//     $scope.gotoLocation=function(destination){
//         $location.path(destination);
//     };
//
//
//     // Is Granted
//    	$rootScope.isGranted = function(roleToTest) {
// 		if ($rootScope._authRoles != null) {
// 			for ( var i in roleToTest) {
// 				if ($rootScope._authRoles[roleToTest[i]] == true) {
// 					return true;
// 				}
// 			}
// 		}
// 		return false;
// 	};
//
//     // Utils
//     // To string date
//     $rootScope.toStringDate = function(longDate) {
//     	if (longDate == null)  {
//     		return "";
//     	}
// 		var date = new Date(longDate);
// 		return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
// 	};
//
// 	// list to string
// 	$rootScope.listToString = function(list) {
// 		if (list == null || list.length < 1) {
// 			return "";
// 		}
// 		var result = "";
// 		for ( var i = 0; i < list.length; i++) {
// 			result += (list[i] + ",");
// 		}
// 		return result.substring(0, result.length - 1);
// 	};
//
// 	// redirection vers page login
// 	function gotoLoginPage() {
// 		$window.location.href="login.html";
// 	}
//
// });
//
// indexModule.filter('minLength', function () {
// 	return function(input,len){
// 		input = input.toString();
// 		if(input.length >= len) return input;
// 		else return("000000"+input).slice(-len);
// 	}
// });
