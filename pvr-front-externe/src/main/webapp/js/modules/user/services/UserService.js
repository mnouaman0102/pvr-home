var userModule = angular.module('userModule', []);
userModule.factory('UserService', function(ajaxService) {
    var service={};

    service.getCurrentLogin = function (successCallback, errorCallback) {
        ajaxService.get("services/security/getCurrentLogin", {}, successCallback, errorCallback);
    };
    
	/**
	 * Get Authentified User Roles
	 */
    service.getAuthentifiedUserRoles=function(callback, errorCallback){
        if(service.getAuthentifiedUserRoles.cache!=null){
            if(typeof callback=="function"){
                callback(service.getAuthentifiedUserRoles.cache);
            }
        }else{
            ajaxService.get("services/security/getAuthentifiedRoles", {}, function(data){
                service.getAuthentifiedUserRoles.cache = data;
                callback(service.getAuthentifiedUserRoles.cache);
            },callback ,errorCallback);
        }
    };

    return service;
});
