Highcharts.setOptions({
    global : {
        useUTC : false
    }
});
Highcharts.setOptions({
    lang: {
        months: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
            'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
        weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi',
            'Jeudi', 'Vendredi', 'Samedi'],
        shortMonths: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil',
            'Aout', 'Sept', 'Oct', 'Nov', 'Déc'],
        decimalPoint: ',',
        downloadPNG: 'Télécharger en image PNG',
        downloadJPEG: 'Télécharger en image JPEG',
        downloadPDF: 'Télécharger en document PDF',
        downloadSVG: 'Télécharger en document Vectoriel',
        exportButtonTitle: 'Export du graphique',
        loading: 'Chargement en cours...',
        printButtonTitle: 'Imprimer le graphique',
        resetZoom: 'Réinitialiser le zoom',
        resetZoomTitle: 'Réinitialiser le zoom au niveau 1:1',
        thousandsSep: ' ',
        decimalPoint: ','
    }
});
myApp.directive('hightChart', function($parse) {
    var directive={
        scope: {
            options: '=options'
        }
    };
    directive.link= function(scope, elem, attrs) {
        scope.$watch('options', function(options) {
            if(options==null) return;
            
            elem.highcharts(options);
        });
    };
    
    
    return directive;
});