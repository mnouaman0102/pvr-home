package ma.iam.pvr.exception;


import ma.iam.pvr.codeError.ErrorCode;

public class AdminCannotRemoveHisOwnAdminRole extends TdbBusinessException{

	private static final long serialVersionUID = -4196323792990481187L;

	public AdminCannotRemoveHisOwnAdminRole(ErrorCode code, String message) {
		super(code, message);
	}
}
