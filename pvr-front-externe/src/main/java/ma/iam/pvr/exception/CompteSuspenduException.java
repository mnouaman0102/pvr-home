package ma.iam.pvr.exception;


import org.springframework.security.core.AuthenticationException;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@SuppressWarnings("serial")
public class CompteSuspenduException extends AuthenticationException {

	public CompteSuspenduException(String msg) {
		super(msg);
	}

}
