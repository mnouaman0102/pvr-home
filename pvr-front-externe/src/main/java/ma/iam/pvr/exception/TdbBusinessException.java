package ma.iam.pvr.exception;

import ma.iam.pvr.codeError.ErrorCode;

/**
 * @author n.ezziani
 * classe des exceptions métier qui hérite de la classe BusinessException
 */
public class TdbBusinessException extends BusinessException 
{

	private static final long serialVersionUID = 2323955941477995965L;

	/** le Constructeur
	* @param code code d'erreur
	* @param message le message d'erreur
	* 
	*/
	public TdbBusinessException(ErrorCode code, String message)
	{
		super(code, message);

	}
	/** le Constructeur
	* @param code code d'erreur
	* @param message le message d'erreur
	* @param cause Throwable
	* 
	*/
	public TdbBusinessException(ErrorCode code, String message, Throwable cause) 
	{
		super(code, message, cause);

	}

}
