package ma.iam.pvr.exception;


import ma.iam.pvr.codeError.ErrorCode;

/**
* Classe d'exceptions  m�tier
* 
* @author urbanisation
* @version 1.0
*/
public class MailBusinessException extends BusinessException {

	private static final long serialVersionUID = 2323955941477995965L;

	/** le Constructeur
	* @param code code d'erreur
	* @param message le message d'erreur
	* 
	*/
	public MailBusinessException(ErrorCode code, String message) {
		super(code, message);

	}
	/** le Constructeur
	* @param code code d'erreur
	* @param message le message d'erreur
	* @param cause Throwable
	* 
	*/
	public MailBusinessException(ErrorCode code, String message, Throwable cause) {
		super(code, message, cause);

	}

}
