package ma.iam.pvr.exception;

//import ma.iam.socle.exception.BusinessException;
//import ma.iam.socle.exception.ErrorCode;

import ma.iam.pvr.codeError.ErrorCode;

/**
 * Classe d'exception metier
 * 
 * @author K.ELBAGUARI
 *
 */
public class MyAppBusinessException extends BusinessException {

	private static final long serialVersionUID = 2323955941477995965L;

	/** le Constructeur
	* @param code code d'erreur
	* @param message le message d'erreur
	* 
	*/
	public MyAppBusinessException(ErrorCode code, String message) {
		super(code, message);

	}
	/** le Constructeur
	* @param code code d'erreur
	* @param message le message d'erreur
	* @param cause Throwable
	* 
	*/
	public MyAppBusinessException(ErrorCode code, String message, Throwable cause) {
		super(code, message, cause);

	}

}
