package ma.iam.pvr.exception;

//import ma.iam.socle.exception.ErrorCode;


import ma.iam.pvr.codeError.ErrorCode;

/**
 * @author K.ELBAGUARI
 *
 */

public class MyAppException extends MyAppBusinessException{

	private static final long serialVersionUID = 1L;

	public MyAppException(ErrorCode code, String message) {
		super(code, message);		
	}

}
