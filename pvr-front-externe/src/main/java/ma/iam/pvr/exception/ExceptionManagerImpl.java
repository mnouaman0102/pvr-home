package ma.iam.pvr.exception;

import java.io.IOException;


import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import ma.iam.pvr.dto.dtos.ExceptionDto;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

/**
 * transforme les exceptions java sous format JSON
 *
 */
/**
 * for web xml file
 *
 */
@Provider
public class ExceptionManagerImpl implements ExceptionMapper<Exception> {
	@SuppressWarnings("deprecation")
	public Response toResponse(Exception e) {
		String result = null;
		try {
		ExceptionDto jsonException = new ExceptionDto(e);
		ObjectMapper mapper = new ObjectMapper();
	    AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
	    mapper.getDeserializationConfig().setAnnotationIntrospector(introspector);
	    mapper.getSerializationConfig().setAnnotationIntrospector(introspector);
	    result = mapper.writeValueAsString(jsonException);
	} catch (JsonGenerationException e1) {
		e1.printStackTrace();
	} catch (JsonMappingException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}
		return Response.serverError().entity(result).type(MediaType.APPLICATION_JSON).build();
	}
}