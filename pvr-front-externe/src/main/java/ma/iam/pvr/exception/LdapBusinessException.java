package ma.iam.pvr.exception;


@SuppressWarnings("serial")
public class LdapBusinessException extends Exception {

	public LdapBusinessException() {
		super();
	}
	public LdapBusinessException(String message) {
		super(message);
	}

}
