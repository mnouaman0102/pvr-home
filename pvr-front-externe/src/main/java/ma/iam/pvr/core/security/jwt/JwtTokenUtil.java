package ma.iam.pvr.core.security.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

import static ma.iam.pvr.constants.CommonConstants.Authentication.EXPIRATION_TIME;
import static ma.iam.pvr.constants.CommonConstants.Authentication.SECRET_KEY;

@Component
public class JwtTokenUtil {
    // private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);

    public String generateJwtToken(Authentication authentication) {

        UserDetails userPrincipal = (UserDetails) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + EXPIRATION_TIME))
                .signWith(key(), SignatureAlgorithm.HS256)
                .compact();
    }

    private Key key() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY));
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parserBuilder().setSigningKey(key()).build()
                .parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parserBuilder().setSigningKey(key()).build().parse(authToken);
            return true;
        } catch (MalformedJwtException e) {
            // logger.error("Invalid JWT token: {}", e.getMessage());
            System.out.println("Invalid JWT token: {}"+e.getMessage());
        } catch (ExpiredJwtException e) {
            // logger.error("JWT token is expired: {}", e.getMessage());
            System.out.println("JWT token is expired: {}"+e.getMessage());
        } catch (UnsupportedJwtException e) {
            // logger.error("JWT token is unsupported: {}", e.getMessage());
            System.out.println("JWT token is unsupported: {}"+e.getMessage());
        } catch (IllegalArgumentException e) {
            //  logger.error("JWT claims string is empty: {}", e.getMessage());
            System.out.println("JWT claims string is empty: {}"+e.getMessage());
        }

        return false;
    }
}

