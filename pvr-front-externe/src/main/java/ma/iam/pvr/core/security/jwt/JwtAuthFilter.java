package ma.iam.pvr.core.security.jwt;


import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import java.io.IOException;

import static ma.iam.pvr.constants.CommonConstants.Authentication.AUTHORIZATION;
import static ma.iam.pvr.constants.CommonConstants.Authentication.BEARER_KEY;


/**
 * Create and configure filter for jwt authentication
 * This Filter will be called on every http request
 * It allow to verify the current jwt token and delegate the request to spring security holder
 */
public class JwtAuthFilter extends OncePerRequestFilter {
    private UserDetailsService jwtUserDetailsService;
    private JwtTokenUtil jwtTokenUtil;

    /**
     * Performs the actual filtering of the request and response.
     *
     * @param request the HttpServletRequest object
     * @param response the HttpServletResponse object
     * @param filterChain m filterChain the FilterChain to invoke the next filter in the chain
     * @throws ServletException if a servlet-related error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = parseJwt(request);
            if (jwt != null && jwtTokenUtil.validateJwtToken(jwt)) {
                String username = jwtTokenUtil.getUserNameFromJwtToken(jwt);

                UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(
                                userDetails,
                                null,
                                userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception e) {
            logger.error("Cannot set user authentication: {}", e);
        }

        filterChain.doFilter(request, response);
    }

    /**
     * Parse the current request to obtain the jwt token from the request header
     * @param request the HttpServletRequest object
     * @return the jwt token if exists, null otherwise
     */
    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader(AUTHORIZATION);

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith(BEARER_KEY)) {
            return headerAuth.substring(7);
        }
        return null;
    }

    @Resource
    public void setJwtUserDetailsService(UserDetailsService jwtUserDetailsService) {
        this.jwtUserDetailsService = jwtUserDetailsService;
    }

    @Resource
    public void setJwtTokenUtil(JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }
}
