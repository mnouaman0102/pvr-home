package ma.iam.pvr.core.security.jwt;

import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.services.interfaces.IUserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/***
 * Implementation of UserDetailsService that manage the user
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {

    private IUserService userService;

    /***
     *
     * @param username the username identifying the user whose data is required.
     * @return UserDetails
     * @throws UsernameNotFoundException if the user not found
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utilisateur currentUser = userService.getUserByUsername(username);
        if(Objects.isNull(currentUser)){
            throw new UsernameNotFoundException("User not found with username = "+username);
        }
        List<SimpleGrantedAuthority> authorities= currentUser.getRoles().stream().map(role->new SimpleGrantedAuthority(role.getCode())).collect(Collectors.toList());
        return new User(currentUser.getLogin(), currentUser.getPassword(),authorities);
    }
    @Resource
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}

