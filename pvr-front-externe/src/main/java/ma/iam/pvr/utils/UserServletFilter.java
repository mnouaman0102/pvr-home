package ma.iam.pvr.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.MDC;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * for web.xml
 * Crée un filtre sur les requetes http Permet de positionner les valeurs MDC
 * (login et remoteAdress) Pour pouvoir les utiliser dans les logs
 * 
 * @author y.alamisounni
 * 
 *         Faut ajouter ces lignes dans le web.xml <filter>
 *         <filter-name>UserServletFilter</filter-name> <filter-class>
 *         ma.iam.pvr.util.UserServletFilter </filter-class> </filter>
 *         <filter-mapping> <filter-name>UserServletFilter</filter-name>
 *         <url-pattern>/*</url-pattern> </filter-mapping>
 * 
 */
public class UserServletFilter implements Filter {

	private static boolean userRegistered = false;
	private final String userKey = "username";
	final static protected Log logger = LogFactory.getLog(UserServletFilter.class);

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		String username = "";
		String url = "";
		String ip = request.getRemoteAddr();
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			username = SecurityContextHolder.getContext().getAuthentication().getName();
			if (username != null) {
				registerUsername(username, ip);
				if (request instanceof HttpServletRequest) {
					url = ((HttpServletRequest) request).getRequestURL().toString();
					logger.info("Requette login: " + username + " ip: " + ip + " url: " + url);
				}
			}
		}
		try {
			chain.doFilter(request, response);
		} finally {
			if (userRegistered) {
				MDC.remove(userKey);
			}
		}
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

	private void registerUsername(String username, String remoteAdress) {
		if (username != null && username.trim().length() > 0) {
			MDC.put(userKey, username);
			MDC.put("ipAddress", remoteAdress);
			userRegistered = true;
		}
	}
}
