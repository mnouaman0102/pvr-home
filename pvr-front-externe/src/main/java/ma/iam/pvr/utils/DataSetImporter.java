package ma.iam.pvr.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe DataSetImporter.
 * 
 * @author SAL
 * @version 0.0.1-SNAPSHOT
 */
public class DataSetImporter {

	Logger logger = LoggerFactory.getLogger(DataSetImporter.class);
	
	/** Connexion courante. */
	private Connection conn;

	
	public DataSetImporter( ) {

	}

	/**
	 * Constructeur de la classe.
	 * 
	 * @param url
	 *            l'url de connection à la base
	 * @param driver
	 *            le nom de la classe driver d'acces à la base
	 * @param username
	 *            le username de connection à la base
	 * @param pwd
	 * 
	 * @throws SQLException
	 *             en cas d'erreur
	 */
	public DataSetImporter(String url, String driver, String username, String pwd) 
		throws SQLException {
		init(url, driver, username, pwd);
	}
	/**
	 * Constructeur de la classe avec param datasource.
	 * 
	 * @param ds
	 *            data source
	 * 
	 * @throws SQLException
	 *             en cas d'erreur
	 */
	public DataSetImporter(DataSource ds) throws SQLException {
		init(ds);
	}
	/**
	 * la classe init avec datasource.
	 * 
	 * @param ds
	 *            data source
	 * 
	 * @throws SQLException
	 *             en cas d'erreur
	 */
	protected final void init(DataSource ds) throws SQLException {
		this.setConn(ds.getConnection());
	}

	/**
	 * Initialisation de la classe.
	 * 
	 * @param url       url de connexion à la base
	 * @param driver    le nom de la classe driver d'acces à la base
	 * @param username  l'username de connection à la base
	 * @param pwd
	 * 
	 * @throws SQLException
	 *             en cas d'erreur
	 */
	protected final void init(String url, String driver, String username, String pwd) throws SQLException {
		final BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(driver);
		ds.setUsername(username);
		ds.setPassword(pwd);
		ds.setUrl(url);
		init(ds);
	}

	/**
	 * Methode pour exporter par dbunit les donnàes de la base au format xml. Le
	 * format est hierarchique (tables et colonnes puis donnàes) par opposition à 
	 * plat (un record par ligne). Les donnàes sont triàes en fonction des clefs
	 * àtrangàres pour pouvoir àtre insàràes par dbunit.
	 * 
	 * @param files liste files
	 * @param schema le schema
	 * @throws SQLException
	 *             un cas d'erreur SQL
	 * @throws IOException IOException
	 * @throws DatabaseUnitException DatabaseUnitException
	 */
	public void importDB(String[] files, String schema) throws SQLException,
			IOException, DatabaseUnitException {

		try
		{
		final IDatabaseConnection databaseConnection;
		if (!schema.equals("")) {
			databaseConnection = new DatabaseConnection(getConn(), schema);
		} else {
			databaseConnection = new DatabaseConnection(getConn());
		}
		
		DatabaseConfig config = databaseConnection.getConfig();
	     config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, 
	                        new HsqlDataTypeFactory());
		
		for (String file : files) {
			IDataSet dataSet = new FlatXmlDataSet(new File(file));	
			ReplacementDataSet dataSetReplac = new ReplacementDataSet(dataSet); 
			dataSetReplac.addReplacementObject("[null]", null);
			DatabaseOperation.INSERT.execute(databaseConnection, dataSetReplac);
			
		}
		databaseConnection.close();
	   }
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * Sets the conn.
	 * 
	 * @param conn
	 *            the new conn
	 */
	public void setConn(Connection conn) {
		this.conn = conn;
	}

	/**
	 * Gets the conn.
	 * 
	 * @return the conn
	 */
	public Connection getConn() {
		return conn;
	}

}
