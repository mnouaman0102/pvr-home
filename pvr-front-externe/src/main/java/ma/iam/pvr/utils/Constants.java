package ma.iam.pvr.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class Constants {

	
	{System.setProperty("file.encoding", "UTF-8");}
	
	/** Format de date. */
	public final static DateFormat dfDateHeure = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	public final static DateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");


	
	public final static String CSV_LOG_SPLIT = ";";
	public final static String USER_INFO_PREFIX = "SDR";
	public final static String USER_INFO_SPLIT = "_";
	public final static int ICM_LOG_MAXLENGHT=1000;
	public final static String ENDPOINT_ACK_URL="https://10.96.6.125:8097/sdk";

	public static final String errorSpecCarac = "contient des caraceres non autorises";
	public static final String errorSiteInexistant = "C";
	public static final String errorProjetInexistant = "Projet n\'existe pas ";
	public static final String errorNumOpMsg = "Numero d\'opération existe deja";
	public static final String errorCodeReserveMsg = "Code reserve deja existe pour ce projet";
	public static final String errorRtDraftDeleteMsg = "Impossible de supprimer ce Rt, il n\'est pas en mode draft";
	
	public static final String errorRtInexistant = "Rt n\'existe pas";
	public static final String errorRtNonDraft = "Rt n\'est pas en mode draft";
	public static final String errorPvrDraftDeleteMsg = "Impossible de supprimer ce Pvr, il n\'est pas en mode draft";
	public static final String errorPvdDraftDeleteMsg = "Impossible de supprimer ce Pvd, il n\'est pas en mode draft";
	public static final String errorPvrInexistant = "PVR n\'existe pas";
	public static final String errorPvrNonDraft = "PVR n\'est pas en mode draft";
	public static final String errorPvdInexistant = "PVD n\'existe pas";
	public static final String errorPvdNonDraft = "PVD n\'est pas en mode draft";
	public static final String errorOperationDeleteMsg = "Impossible de supprimer cette Operation";
	public static final String errorOperationInexistante = "Operation n\'existe pas";
	public static final String errorSiteMES = "Site deja mis en service";
	public static final String errorSiteDrExist = "duplicate|Ce Site existe deja.";
	
	public final static int LOCK_INACTIVE = 60; //60 jrs
	
	/* AUDIT */
	public static final String ERROR_SPEC_CHAR = "contient des caractères non autorisés";
	public static final String SUPPORTED_FILE_TYPE = "txt,csv,doc,docx,xls,xlsx,ppt,pptx";
	public static final String SUPPORTED_MEDIA_TYPE = "jpeg,gif,png,jpg";
	public static final String ERROR_PATH_FILNAME_LABEL = "Le chemin/nom du fichier est invalide";
	public static final String ERROR_FILNAME_EXTENSION = "L\'extension du fichier est invalide";
	public static final String ERROR_MEDIA_EXTENSION = "L\'extension du media est invalide";
	public static final String ERROR_LABEL_VIDE = "La valeur est vide";
	
	/* Remise Controle */
	public static final String errorRemiseInexistante = "Remise au contr&ocirc;le n\'existe pas";
	public static final String errorRemiseNonInstance = "Vous ne pouvez pas supprimer une remise valid&eacute;e ou refus&eacute;e !!";
	public static final String errorDeleteRemise = "Vous n'avez pas le droit de supprimer la remise au contr&ocirc;le !!";
	public static final String errorRemiseNonDraft = "Remise au controle n\'est pas en mode draft";
	public static final String errorRemiseEdit = "Aucune Remise au controle";
}
