package ma.iam.pvr.utils;


import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import ma.iam.pvr.codeError.AppErrorCode;
import ma.iam.pvr.exception.MyAppBusinessException;



public class CryptUtil {

	private static final String UNICODE_FORMAT  = "UTF8";
	private static final String ALGO = "AES";
	private static final String ALGO_MOD = "AES/CBC/PKCS5Padding";
	private static final byte[] keyValue =
			new byte[]{'i', 'n', 'p', 's', 'm', 't', 'g',
		's', 'e', 'c', 'r', 'e', 't', 'k', 'e', 'y'};

	//pour obtenir un resultat fixe à chaque execution de cryptage/decryptage
	byte[] iv =   { 17, -15, 20, 85, 55, 02, 40, -90, -5, -78, 15, 15, 40, 85, -9, 0 };
	IvParameterSpec ivspec = new IvParameterSpec(iv);
	
	public CryptUtil(String pswdLdap) throws MyAppBusinessException {
		pswLdap = decrypt(pswdLdap);
	}



	private String pswLdap;


	public void setPswLdap(String pswLdap) {
		this.pswLdap = pswLdap;
	}



	public String getPswLdap() {
		return pswLdap;
	}


	/**
	 * Constructeur par défaut
	 */
	public CryptUtil() {
	}

	/**
	 * Constructeur
	 *
	 * @param encryptedData chaine à décrypter
	 * @throws Exception
	 */

	/**
	 * Permet de crypter une chaine de caractères
	 *
	 * @param data chaine à crypter
	 * @return chaine cryptée
	 * @throws Exception
	 */
	public String encrypt(String data) throws MyAppBusinessException {
		String encryptedValue = "";
		try {
			SecureRandom random = new SecureRandom();
			byte[] iv = random.generateSeed(16);
			IvParameterSpec ivspec = new IvParameterSpec(iv);
			Key key = generateKey();
			Cipher c = Cipher.getInstance(ALGO_MOD);

			c.init(Cipher.ENCRYPT_MODE, key,ivspec);

			byte[] encVal = c.doFinal(data.getBytes(UNICODE_FORMAT));
			encryptedValue = DatatypeConverter.printBase64Binary(encVal);
		} catch (Exception e) {	
			e.getMessage();
			throw new MyAppBusinessException(AppErrorCode.ERROR_PSW,"Probleme encrypt password");

		} 
		return encryptedValue;
	}

	/**
	 * Permet de décrypter une chaine de caractères
	 *
	 * @param encryptedData chaine à décrypter
	 * @return chaine décryptée
	 * @throws MyAppBusinessException 
	 * @throws Exception
	 */
	public final String decrypt(String encryptedData) throws MyAppBusinessException {


		String decryptedValue="";    	

		Cipher c;
		try {
			Key key = generateKey();
			c = Cipher.getInstance(ALGO_MOD);
			c.init(Cipher.DECRYPT_MODE, key,ivspec);
			byte[] decordedValue = DatatypeConverter.parseBase64Binary(encryptedData);
			byte[] decValue = c.doFinal(decordedValue);
			decryptedValue = new String(decValue);
		} catch (Exception e) {	
			e.getMessage();
			throw new MyAppBusinessException(AppErrorCode.ERROR_PSW,"Probleme decrypt Password");

		} 


		return decryptedValue;
	}

	/**
	 * Génère une clé
	 *
	 * @return clé généré
	 * @throws Exception
	 */




	private Key generateKey() throws MyAppBusinessException {
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}

	/**
	 * @return data
	 */


/*	public static void main(String [] arg) throws MyAppBusinessException{
		System.out.println(new CryptUtil().encrypt(""));
		System.out.println(new CryptUtil().decrypt(""));		
	}*/
}