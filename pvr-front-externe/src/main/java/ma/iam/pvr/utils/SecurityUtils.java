package ma.iam.pvr.utils;

import java.sql.Timestamp;
import java.util.Date;

import ma.iam.pvr.domain.Utilisateur;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class SecurityUtils {

    /**
     * Verifier la Suspension de l'acces apres 2 mois d'inactivite
     *
     * @param u L'utilisateur a  verifier
     */
    public static boolean verifierReglesConnexionInactivite(Utilisateur u) {
        if (u.getDateLastConnection() != null) {
            Timestamp sysdate = new Timestamp(new Date().getTime());
            long nbreJour = (sysdate.getTime() - u.getDateLastConnection().getTime()) / (1000 * 3600 * 24);
            if (nbreJour >= Constants.LOCK_INACTIVE) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Permet de verifier si un compte est suspendu
     *
     * @param utilisateur l'utilisateur a verifier
     * @return true si le compte est suspendu
     */
    public static boolean isLocked(Utilisateur utilisateur) {
        return (verifierReglesConnexionInactivite(utilisateur));
    }
    
}
