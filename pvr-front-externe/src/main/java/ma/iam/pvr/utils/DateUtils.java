package ma.iam.pvr.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class DateUtils {
	
		
	public static String dateToString(Date date){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(date);
	}
	public static String dateToStringSlash(Date date){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		return df.format(date);
	}
	
	public static Date stringToDate(String date) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		return sdf.parse(date);
	}
	
	public static String dateTimeToString(Date date){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return df.format(date);
	}
	
	public static Date stringToDateTime(String date) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return sdf.parse(date);
	}
	
	public static Date stringToDateSlash(String date) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		return sdf.parse(date);
	}
	
	public static Date stringToDateTimeSlash(String date) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return sdf.parse(date);
	}
	
	public static String dateTimeToStringMinutes(Date date){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return df.format(date);
	}
	
	public static Date stringToDateTimeMinute(String date) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return sdf.parse(date);
	}
}
