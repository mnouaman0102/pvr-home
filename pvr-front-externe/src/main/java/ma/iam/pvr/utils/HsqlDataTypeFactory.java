package ma.iam.pvr.utils;

import java.sql.Types;

import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.dbunit.dataset.datatype.DefaultDataTypeFactory;
/**
 * Classe HsqlDataTypeFactory.
 * 
 * @author SAL
 * @version 0.0.1-SNAPSHOT
 */
public class HsqlDataTypeFactory
  extends DefaultDataTypeFactory
{
	/**
	 * crée un data type .
	 * @param sqlType  sqlType
	 * @param sqlTypeName  sqlTypeName
	 * @return dataType
	 * @throws DataTypeException DataTypeException
	 */
   public DataType createDataType(int sqlType, String sqlTypeName)
     throws DataTypeException
   {
      if (sqlType == Types.BOOLEAN)
      {
         return DataType.BOOLEAN;
       }
  
      return super.createDataType(sqlType, sqlTypeName);
    }
}