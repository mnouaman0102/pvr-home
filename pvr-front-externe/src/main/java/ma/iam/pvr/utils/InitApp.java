package ma.iam.pvr.utils;

import java.io.IOException;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InitApp {


private boolean initDatas = false;
	
	Logger logger = LoggerFactory.getLogger(InitApp.class);

	private DataSource dataSource;

	/**
	 *@return DataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 *@param  dataSource DataSource
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	/**
	 *
	 *@throws SQLException SQLException
	 *@throws IOException IOException
	 *@throws DatabaseUnitException DatabaseUnitException
	 */
	public void init() throws SQLException, IOException, DatabaseUnitException {
		logger.info("Initialisation des données : " + initDatas);
		if (initDatas){
			new ImportDatabaseForClientDev().importDB(getDataSource());
		}
	}
	/**
	 *@return boolean
	 */
	public boolean isInitDatas() {
		return initDatas;
	}

/**
 *@param initDatas initDatas
 */
	public void setInitDatas(boolean initDatas) {
		this.initDatas = initDatas;
	}
	
	
	
	
}
