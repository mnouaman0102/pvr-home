package ma.iam.pvr.utils;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe ImportDatabaseForClientDev.
 * 
 * @author SAL
 * @version 0.0.1-SNAPSHOT
 */
public class ImportDatabaseForClientDev {

	Logger logger = LoggerFactory.getLogger(ImportDatabaseForClientDev.class);

	/**
	 * @param args
	 *            args
	 * @throws SQLException
	 *             SQLException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws DataSetException
	 * @throws ClassNotFoundException
	 */
/*	public static void main(String[] args) throws SQLException {
		DataSetImporter importerHQL = new DataSetImporter(
				"jdbc:hsqldb:mem:testDB", "org.hibernate.dialect.HSQLDialect",
				"SA", "");

		new ImportDatabaseForClientDev().importDB(importerHQL);
	}*/

	/**
	 * @param ds
	 *            dataSource args
	 */
	public void importDB(DataSource ds) {
		DataSetImporter importer;
		try {
			importer = new DataSetImporter(ds);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			return;
		}
		importDB(importer);
	}

	/**
	 * @param importer
	 *            DatasetImporte permettant d'importer les fichiers xml de test
	 */
	public void importDB(DataSetImporter importer) {

		String url = "src/test/resources/ma/iam/app/data/";
                String[] files = {
                    url + "user.xml"
                    ,url + "role.xml"
                    ,url + "userrole.xml"
                };

		try {
			logger.info("Import des données de test");
			importer.importDB(files, null);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return;
		}
		return;
	}

}
