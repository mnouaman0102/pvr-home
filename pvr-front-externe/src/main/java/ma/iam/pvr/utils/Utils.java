package ma.iam.pvr.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.MDC;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class Utils {


	/**
	 * @return login utilisateur connecté
	 */
	public static String getLoggedUser(){
		String logParam="Requette login:"+MDC.get("username") +", Action: " ;
		return logParam;
	}
	/**
	 * @return adresse ip et login utilisateur connecté
	 */
	public static String getLogParam(){
		String logParam="Requette login:"+MDC.get("username")+ " ip:"+MDC.get("ipAddress")+", Action: " ;
		return logParam;
	}
	/**
	 * Split
	 * 
	 * @param string
	 * @return Long list
	 */
	public static List<Long> split(String string) {
		if (string == null || string.trim().equals("")) {
			return null;
		}
		String[] strList = string.split(",");
		List<Long> results = new ArrayList<Long>(0);
		for (String number : strList) {
			results.add(Long.valueOf(number));
		}
		return results;
	}
        
	/**
	 * Convertit une chaine au format en un objet Date
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date getDateFromString(String dateStr, boolean includeHours) {
		String dateFormat = includeHours ? "dd/MM/yyyy HH:mm" : "dd/MM/yyyy";

		Date date = null;
		try {
			date = new SimpleDateFormat(dateFormat).parse(dateStr);
		} catch (ParseException ex) {
			date = null;
		}

		return date;
	}
	/**
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date getSimpleDateFromString(String dateStr) {
		String dateFormat = "yyyy-MM-dd";

		Date date = null;
		try {
			date = new SimpleDateFormat(dateFormat).parse(dateStr);
		} catch (ParseException ex) {
			date = null;
		}

		return date;
	}
	/**
	 * 
	 * @param numero
	 * @return
	 */
	public static Boolean isTelephoneNumber(String numero){
		Pattern pattern1 = Pattern.compile("[0-9]{10}");
		Pattern pattern2 = Pattern.compile("[0-9]{12}");
	    Matcher matcher1 = pattern1.matcher(numero);
	    Matcher matcher2 = pattern2.matcher(numero);
		 if (matcher1.matches() || matcher2.matches())
	    	  return true;
		 else return false;
	 }
	/**
	 * 
	 * @param alphanumeric
	 * @return
	 */
	public static Boolean isOnlyAlphaNumeric(String alphanumeric){
		 Pattern pattern = Pattern.compile("[a-zA-Z0-9]+");
	    Matcher matcher = pattern.matcher(alphanumeric);
		 if (matcher.matches()) {

	    	  return true;
	      }
	      else
	      {

	    	  return false;
	      }
	 }
	/**
	 * 
	 * @param text
	 * @return
	 */
	
	/**
	 * 	Evolution Date Operation en input 	
	 */
	public static boolean isValidStringDate(String text) {
		SimpleDateFormat df = null;
	    if (text == null || !text.matches("[0-3]\\d/[01]\\d/\\d{4}") && !text.matches("[0-3]\\d/[01]\\d/\\d{4}\\s[0-2]\\d((:[0-5]\\d)?){2}"))
	        return false;
	    if(text.matches("[0-3]\\d/[01]\\d/\\d{4}"))
	    	df = new SimpleDateFormat("dd/MM/yyyy");
	    else df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    
	    df.setLenient(false);
	    try {
	        df.parse(text);
	        return true;
	    } catch (ParseException ex) {
	        return false;
	    }
	}
	/**
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isValidEmailAddress(String email) {
	   boolean result = true;
	   try {
	      InternetAddress emailAddr = new InternetAddress(email);
	      emailAddr.validate();
	   } catch (AddressException ex) {
	      result = false;
	   }
	   return result;
	}

	public static Boolean isSpecAlphaNumeric(String str){
		if(str.contains("$") || str.contains("&")){
			return true;
		}
		 return false;
	    
	}
	/**
	 * @author Z.BELGHAOUTI
	 * @return Path Project
	 */
	public static String getFullPathProject(){
		 
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if(null != requestAttributes && requestAttributes instanceof ServletRequestAttributes) {
		  HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
		  if ( ( request.getServerPort() == 80 ) || ( request.getServerPort() == 443 ) )
			      return request.getScheme() + "://" +
			             request.getServerName() +
			             request.getContextPath();
			    else
			      return request.getScheme() + "://" +
			             request.getServerName() + ":" + request.getServerPort() +
			             request.getContextPath();
		}
		return null;
    }
	
	/**
	 * Audit
	 */
	
	public static Boolean containSpecChar(String str){
		if(str.contains("!") || str.contains("?") || str.contains("&") ||
				str.contains("$") || str.contains("@") || str.contains("#") ||
				str.contains(">") || str.contains("<") || str.contains("%") ||
				str.contains("\"") || str.contains("+")|| str.contains(";") ||
				str.contains("/") || str.contains("*") || str.contains("=") ||
				str.contains("(") || str.contains(")") || str.contains("{") ||
				str.contains("}") || str.contains("[") || str.contains("]") || 
				str.contains("|") || str.contains(":") || str.contains(",")){
			return true;
		}
		return false;
	}
	
	public static Boolean contSpecChar(String str){
		if(str.contains("!") || str.contains("?") || str.contains("&") ||
				str.contains("$") || str.contains("@") || str.contains("#") ||
				str.contains(">") || str.contains("<") || str.contains("%") ||
				str.contains("\"") || str.contains("+")|| str.contains(";") ||
				str.contains("/") || str.contains("*") || str.contains(":") ||
				str.contains("(") || str.contains(")") || str.contains("{") ||
				str.contains("}") || str.contains("[") || str.contains("]") || 
				str.contains("|")){
			return true;
		}
		return false;
	}
	
	public static Boolean validePathAndFileName(String fileName){
		if(fileName != null && !fileName.equals("")){
			if(!fileName.contains("../") && !fileName.contains("/")){
				return true;
			}				
		}
		return false;
	}
	
	public static Boolean valideExtensionFile(String extension){
		if(extension != null && !extension.equals("")){
			if(extension.contains(Constants.SUPPORTED_FILE_TYPE)){
				return true;
			}				
		}
		return false;
	}
	
	public static Boolean valideExtensionMedia(String extension){
		if(extension != null && !extension.equals("")){
			if(extension.contains(Constants.SUPPORTED_MEDIA_TYPE)){
				return true;
			}				
		}
		return false;
	}
	

	
	public static Boolean valideStrChar(String str){
		if(!str.contains("%") && !str.contains("=") && !str.contains("'")){
			return true;
		}
		 return false;
	}
	
	public static Boolean equalsChaineVide(String str){
		if(str == null || str.equals("")){
			return true;
		}
		 return false;
	}
}
