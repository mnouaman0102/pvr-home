/**
 * 
 */
package ma.iam.pvr.utils;

/**
 * @author K.ELBAGUARI
 *
 */
public enum ActionCode {

	TENTATIVE_CONNEXION("tcnx"), CONNEXION("cnx"), DECONNEXION("dcnx"), ADD("ajout"), MODIF("modification"), DELETE("supression"), 
	CONSULTATION("consultation"), DOWNLOAD("Telechargement"), UPLOAD("Chargement"), EXPORT("Report");

	private String myValue;

	private ActionCode(String value) {
		this.myValue = value;
	}

	public String value() {
		return myValue;
	}

	public String toString() {
		return myValue;
	}
}
