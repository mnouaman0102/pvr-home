package ma.iam.pvr.codeError;

//import ma.iam.socle.exception.ErrorCode;

/**
 * AppErrorCode
 */
public enum AppErrorCode implements ErrorCode {

	DOESNT_EXIST("0"),
	ALREADY_EXISTING_RESULTAT("1"),
	DATE_EXISTING("2"),
	FONCTIONNAL_EXCEPTION("3"),
	DATE_ORDER_ERROR("4"),
	DISCONNECTED("5"),
	
	ERROR_PASSWORD("6"),
	SAME_AS_OLD_PWD("7"),
	SAME_AS_LOGIN("8"),
	NOT_EXISTING_USER_IN_DB("9"),
	ALREADY_EXISTING_USER("10"),
	ERROR_LOGIN("11"),
	ATTACHED_ACCOUNT("12"),
	ERROR_NEW_PASSWORD("13"),
	ERROR("14"),
	ERROR_PSW("15");

	private String code;

	/**
	 * Constructeur
	 * 
	 * @param code
	 */
	private AppErrorCode(String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return code
	 */
	public String value() {
		return code;
	}

	/**
	 * 
	 * @return code
	 */
	public String toString() {
		return code;
	}
}
