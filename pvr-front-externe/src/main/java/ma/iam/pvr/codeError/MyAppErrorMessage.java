/**
 * 
 */
package ma.iam.pvr.codeError;

//import ma.iam.socle.exception.ErrorCode;

/**
 * @author K.ELBAGUARI
 *
 */
public enum MyAppErrorMessage  implements ErrorCode{
	
	ND_NON_CONFORME("Le ND doit être composé de 12 numero et doit respecter le format 2126........"),
	CODE_TYPE_OPERATION_INEXISTANT("Le Code Type Operation saisi n\'existe pas"),
	CODE_SI_INEXISTANT("Le Code SI saisi n\'existe pas"),	
	ERROR("Erreur");

	private String code;

	/**
	 * @param code
	 */
	private MyAppErrorMessage(String code) {
		this.code = code;
	}

	public String value() {
		return code;
	}
}
