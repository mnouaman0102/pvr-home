
package ma.iam.pvr.codeError;

//import ma.iam.socle.exception.ErrorCode;

/**
 * @author K.ELBAGUARI
 *
 */


public enum MyAppErrorCode implements ErrorCode {
		
	CODE_SITE_INEXISTANT("100"),
	CODE_PROJET_INEXISTANT("101"),
	NUM_OPE_EXIST("102"),
	CODE_EMAIL_PROJECT("103"),
	CODE_RESERVE_EXIST("104"),
	CODE_RT_DRAFT_DELETE("105"),
	RT_INEXISTANT("106"),
	RT_NOT_DRAFT("107"),
	CODE_PVR_DRAFT_DELETE("108"),
	CODE_PVD_DRAFT_DELETE("109"),
	PVR_INEXISTANT("110"),
	PVR_NOT_DRAFT("111"),
	PVD_INEXISTANT("112"),
	PVD_NOT_DRAFT("113"),
	CODE_OPERATION_DELETE("114"),
	OPERATION_INEXISTANTE("115"),
	SITE_ALREADY_MES("116"),
	SITE_DR_ALREADY_EXIST("117"),
	REMISE_INEXISTANT("118"),
	REMISE_NON_INSTANCE("119"),
	REMISE_DELETE("120"),
	REMISE_NOT_DRAFT("121"),
	REMISE_EDIT("122"),
	REMISE_EXPORT("123")
	;

	private String code;

	/**
	 * @param code
	 */
	private MyAppErrorCode(String code) {
		this.code = code;
	}

	public String value() {
		return code;
	}
	
}
