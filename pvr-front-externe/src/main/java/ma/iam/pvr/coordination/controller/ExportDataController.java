package ma.iam.pvr.coordination.controller;

import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.util.StringUtils;
import ma.iam.pvr.domain.Contrat;
import ma.iam.pvr.domain.Dr;
import ma.iam.pvr.domain.Etat;
import ma.iam.pvr.domain.Projet;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.dto.dtos.ConsultMesReportDto;
import ma.iam.pvr.services.interfaces.IConsultMesReportService;
import ma.iam.pvr.services.interfaces.IRemiseMesReportService;
import ma.iam.pvr.utils.DateUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ExportDataController extends AppAbstractReportingController<Object> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String JASPER_FILE_CONSULT = "consultMes.jasper";

	private IConsultMesReportService consultMesReportService;
	
	private IRemiseMesReportService remiseMesReportService;

	public void setConsultMesReportService(
			IConsultMesReportService consultMesReportService) {
		this.consultMesReportService = consultMesReportService;
	}
	public void setRemiseMesReportService(
			IRemiseMesReportService remiseMesReportService) {
		this.remiseMesReportService = remiseMesReportService;
	}

	@RequestMapping("/consultMes.pdf")
	public void renderPdfConsultMesFrais(		
			@RequestParam(value = "idtsContrat") String idtsContrat,
			@RequestParam(value = "idtsProjet") String idtsProjet,
			@RequestParam(value = "idtsEtat") String idtsEtat,
			@RequestParam(value = "idtsDr") String idtsDr,
			@RequestParam(value = "idtsSite") String idtsSite,
			OutputStream out,
			HttpServletResponse response)
					throws Exception {
		response.setContentType("application/pdf;charset=UTF-8");
		String dateExport = DateUtils.dateTimeToString(new Date());
		response.setHeader("Content-Disposition", "inline; filename=sdrConsultMes"+ dateExport +".pdf");
			renderExportConsultMes(idtsContrat, idtsProjet, idtsEtat, idtsDr, idtsSite, out, "pdf");
	}

	@RequestMapping("/consultMes.xls")
	public void renderXlsConsultMesFrais(		
			@RequestParam(value = "idtsContrat") String idtsContrat,
			@RequestParam(value = "idtsProjet") String idtsProjet,
			@RequestParam(value = "idtsEtat") String idtsEtat,
			@RequestParam(value = "idtsDr") String idtsDr,
			@RequestParam(value = "idtsSite") String idtsSite,
			OutputStream out,
			HttpServletResponse response)
					throws Exception {
		response.setContentType("application/vnd.ms-excel");
		String dateExport = DateUtils.dateTimeToString(new Date());
		response.setHeader("Content-Disposition", "attachment; filename=sdrConsultMes" + dateExport + ".xls");
			renderExportConsultMes(idtsContrat, idtsProjet, idtsEtat, idtsDr, idtsSite, out, "xls");
	}	

	private void renderExportConsultMes(String idtsContrat, String idtsProjet, String idtsEtat, String idtsDr, String idtsSite,
			OutputStream out, String fileType) throws Exception {
		Map<Object, Object> parameters = new HashMap<Object, Object>();
		// Recuperation des donnees
		List<ConsultMesReportDto> listResultats = consultMesReportService.consultMesByCriteres(idtsContrat, idtsProjet, idtsEtat, idtsDr, idtsSite);

		//Parameters
		List<Long> idtContrat = StringUtil.toListLong(idtsContrat, ",");
		List<Long> idtProjet = StringUtil.toListLong(idtsProjet, ",");
		List<Long> idtEtat = StringUtil.toListLong(idtsEtat, ",");
		List<Long> idtDr = StringUtil.toListLong(idtsDr, ",");
		List<Long> idtSite = StringUtil.toListLong(idtsSite, ",");
		
		if(idtContrat != null && !idtContrat.isEmpty()){
			String contratLabel = "";
			for(Long idt : idtContrat) {
				Contrat contrat = consultMesReportService.getContratByIdt(idt);
				if(contrat != null) {
					if(!StringUtils.isNullOrEmpty(contratLabel)) {
						contratLabel+=", ";
					}
					contratLabel += contrat.getLabel();
				}
			}
			parameters.put("contrat", contratLabel);
		}else{
			parameters.put("contrat", "Tous");
		}

		if(idtDr != null && !idtDr.isEmpty()){
			String drLabel = "";
			for(Long idt : idtDr) {
				Dr dr = consultMesReportService.getDrByIdt(idt);
				if(dr != null) {
					if(!StringUtils.isNullOrEmpty(drLabel)) {
						drLabel+=", ";
					}
					drLabel += dr.getLabel();
				}
			}
			parameters.put("dr", drLabel);
		}else{
			parameters.put("dr", "Tous");
		}

		if(idtSite != null && !idtSite.isEmpty()){
			String siteLabel = "";
			for(Long idt : idtSite) {
				Site site = consultMesReportService.getSiteByIdt(idt);
				if(site != null) {
					if(!StringUtils.isNullOrEmpty(siteLabel)) {
						siteLabel+=", ";
					}
					siteLabel += site.getLabel();
				}
			}
			parameters.put("site", siteLabel);
		}else{
			parameters.put("site", "Tous");
		}

		if(idtProjet != null && !idtProjet.isEmpty()){
			String projetLabel = "";
			for(Long idt : idtProjet) {
				Projet projet = consultMesReportService.getProjetByIdt(idt);
				if(projet != null) {
					if(!StringUtils.isNullOrEmpty(projetLabel)) {
						projetLabel+=", ";
					}
					projetLabel += projet.getLabel();
				}
			}
			parameters.put("projet", projetLabel);
		}else{
			parameters.put("projet", "Tous");
		}

		if(idtEtat != null && !idtEtat.isEmpty()){
			String etatLabel = "";
			for(Long idt : idtEtat) {
				Etat etat = consultMesReportService.getEtatByIdt(idt);
				if(etat != null) {
					if(!StringUtils.isNullOrEmpty(etatLabel)) {
						etatLabel+=", ";
					}
					etatLabel += etat.getLabel();
				}
			}
			parameters.put("etat", etatLabel);
		}else{
			parameters.put("etat", "Tous");
		}

		try {
			if ("pdf".equalsIgnoreCase(fileType)) {
				if(listResultats != null && listResultats.size() > 0)
					out.write(generatePdf(JASPER_FILE_CONSULT, listResultats, parameters));
			} 
			else if ("xls".equalsIgnoreCase(fileType)) {
				if(listResultats != null && listResultats.size() > 0)
					out.write(generateXls(JASPER_FILE_CONSULT, listResultats, parameters));
			}
			else if ("csv".equalsIgnoreCase(fileType)) {
				if(listResultats != null && listResultats.size() > 0)
					out.write(generateCsv(JASPER_FILE_CONSULT, listResultats, parameters));
			}
		} catch (Exception e) {
			logger.error("Erreur lors de la generation de l'export des perfs : ", e);
			throw e;
		}
	}
	
	@RequestMapping("/remiseMes.pdf")
	public void renderPdfRemiseMes(		
			@RequestParam(value = "idtRemise") Long idtRemise,
			OutputStream out,
			HttpServletResponse response)
					throws Exception {
		response.setContentType("application/pdf;charset=UTF-8");
		String dateExport = DateUtils.dateTimeToString(new Date());
		response.setHeader("Content-Disposition", "inline; filename=remiseMes-"+ dateExport +".pdf");
		renderExportRemiseMes(idtRemise, out, "pdf");
	}

	@RequestMapping("/remiseMes.xls")
	public void renderXlsRemiseMes(		
			@RequestParam(value = "idtRemise") Long idtRemise,
			OutputStream out,
			HttpServletResponse response)
					throws Exception {
		response.setContentType("application/vnd.ms-excel");
		String dateExport = DateUtils.dateTimeToString(new Date());
		response.setHeader("Content-Disposition", "attachment; filename=remiseMes-" + dateExport + ".xls");
		renderExportRemiseMes(idtRemise, out, "xls");
	}
	
	private void renderExportRemiseMes(Long idtRemise,
			OutputStream out, String fileType) throws Exception {
		Map<Object, Object> parameters = new HashMap<Object, Object>();		
		// Recuperation des donnees
		List<ConsultMesReportDto> listResultats = remiseMesReportService.getRemiseMesByCriteres(idtRemise);

		try {
			if ("pdf".equalsIgnoreCase(fileType)) {
				if(listResultats != null && listResultats.size() > 0)
					out.write(generatePdf(JASPER_FILE_CONSULT, listResultats, parameters));
			} 
			else if ("xls".equalsIgnoreCase(fileType)) {
				if(listResultats != null && listResultats.size() > 0)
					out.write(generateLongXls(JASPER_FILE_CONSULT, listResultats, parameters));
			}
			else if ("csv".equalsIgnoreCase(fileType)) {
				if(listResultats != null && listResultats.size() > 0)
					out.write(generateCsv(JASPER_FILE_CONSULT, listResultats, parameters));
			}
		} catch (Exception e) {
			logger.error("Erreur lors de la generation de l'export des Remises MES : ", e);
			throw e;
		}
	}
}
