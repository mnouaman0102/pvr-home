package ma.iam.pvr.coordination.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

//import ma.iam.socle.coordination.GenericController;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ServletContextAware;

public abstract class AppAbstractReportingController<T> /*extends GenericController<T>*/ implements ServletContextAware {
	private final Logger internalLogger = LoggerFactory.getLogger(AppAbstractReportingController.class);
	
	@Autowired
	DataSource dataSource;
	
	ServletContext servletContext;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public ServletContext getServletContext() {
		return servletContext;
	}
	
	Map<Object, Object> setLogoImagePath(Map<Object, Object> parameters, String imageRelativePath, String rptImageParamName) {
		String imagePath = servletContext.getRealPath(imageRelativePath);
		parameters.put(rptImageParamName, imagePath);
		return parameters;
	}
	
	public byte[] generatePdf(String report, List<?> datas, Map<? extends Object, Object> parameters) throws JRException {

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream stream = null ;
		JRDataSource dataSource = getDataSource(datas);
		JasperPrint jasperPrint = null;
		
		try{
			stream = classLoader.getResourceAsStream(report);
			if(stream != null){
				if (dataSource != null) {
					jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, dataSource);
				} else {
					try {
						jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, this.dataSource.getConnection());
					} catch (Exception e) {
						e.getMessage();
					}finally{
						closeDataSourceSafely(this.dataSource);
					}
				}
				jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, dataSource);
			}
		}catch(Exception e){
			e.getMessage();
		}finally{
			closeInputStreamSafely(stream);
		}
				
		byte[] pdfReport = JasperExportManager.exportReportToPdf(jasperPrint);
		return pdfReport;
	}
	public byte[] generatePdfWithHtmlMarkUp(String report, List<?> datas, Map<? extends Object, Object> parameters) throws JRException {

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream stream = null ;
		JRDataSource dataSource = getDataSource(datas);
		JasperPrint jasperPrint = null;
		JRPdfExporter pdfExporter = new JRPdfExporter();
        ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
		try{
			stream = classLoader.getResourceAsStream(report);
			if(stream != null){
				if (dataSource != null) {
					jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, dataSource);
				} else {
					try {				
						jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, this.dataSource.getConnection());
					} catch (Exception e) {
						e.getMessage();
					}finally{
						closeDataSourceSafely(this.dataSource);
					}
				}
			}
		}catch(Exception e){
			e.getMessage();
		}finally{
			closeInputStreamSafely(stream);
		}
		pdfExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
		pdfExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, pdfReportStream);
		pdfExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		pdfExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		pdfExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		pdfExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        pdfExporter.exportReport();
        byte[] xlsReport = pdfReportStream.toByteArray();       
        
        return xlsReport;
	}
	
	public byte[] generateXls(String report, List<?> datas, Map<? extends Object, Object> parameters) throws JRException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream = null ;         
        JRDataSource dataSource = getDataSource(datas);
		JasperPrint jasperPrint= null;
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		JRXlsExporter exporterXLS = new JRXlsExporter();
		try{
			stream = classLoader.getResourceAsStream(report);
			if(stream != null){
//				if (dataSource != null) {
//					jasperPrint = JasperFillManager.fillReport(stream, parameters, dataSource);
//				} else {
//					try {				
//						jasperPrint = JasperFillManager.fillReport(stream, parameters, this.dataSource.getConnection());
//					} catch (Exception e) {
//						e.getMessage();
//					}finally{
//						closeDataSourceSafely(this.dataSource);
//					}
//				}
				jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, dataSource);
		        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
		        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputByteArray);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		        exporterXLS.exportReport();
		        byte[] xlsReport = outputByteArray.toByteArray();       
		        
		        return xlsReport;
			}
		}catch(Exception e){
			e.getMessage();
		}finally{
			closeInputStreamSafely(stream);
		}
      return null;  

    }
	public byte[] generateLongXls(String report, List<?> datas, Map<? extends Object, Object> parameters) throws JRException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream = null ;         
        JRDataSource dataSource = getDataSource(datas);
		JasperPrint jasperPrint= null;
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		JRExporter exporterXLS = new JRXlsExporter();
		try{
			stream = classLoader.getResourceAsStream(report);
			if(stream != null){
				if (dataSource != null) {
					jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, dataSource);
				} else {
					try {				
						jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, this.dataSource.getConnection());
					} catch (Exception e) {
						e.getMessage();
					}finally{
						closeDataSourceSafely(this.dataSource);
					}
				}
		        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
		        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputByteArray);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		        exporterXLS.exportReport();
		        byte[] xlsReport = outputByteArray.toByteArray();      
		        return xlsReport;
			}
		}catch(Exception e){
			e.getMessage();
		}finally{
			closeInputStreamSafely(stream);
		}
      return null;  

    }
	public byte[] generateCsv(String report, List<?> datas, Map<? extends Object, Object> parameters) throws JRException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream =null;   
        JRDataSource dataSource = getDataSource(datas);
		JasperPrint jasperPrint= null;
		jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, dataSource);
        ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
        JRCsvExporter exporterCSV=new JRCsvExporter();
        try{
			stream = classLoader.getResourceAsStream(report);
			if(stream != null){
				if (dataSource != null) {
					jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, dataSource);
				} else {
					try {				
						jasperPrint = JasperFillManager.fillReport(stream, (Map<String, Object>) parameters, this.dataSource.getConnection());
					} catch (Exception e) {
						e.getMessage();
					}finally{
						closeDataSourceSafely(this.dataSource);
					}
				}
        exporterCSV.setParameter(JRCsvExporterParameter.JASPER_PRINT, jasperPrint);
        exporterCSV.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, outputByteArray);
        exporterCSV.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, ";");
        exporterCSV.exportReport();
        byte[] csvReport = outputByteArray.toByteArray();       
        
        return csvReport;
			}
		}catch(Exception e){
			e.getMessage();
		}finally{
			closeInputStreamSafely(stream);
		}
      return null;  

    }

	public byte[] generateDocx(String report, List<?> datas, Map<String, Object> parameters) throws JRException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream = null ;         
        JRDataSource dataSource = getDataSource(datas);
		JasperPrint jasperPrint= null;
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		JRDocxExporter exporterDocx = new JRDocxExporter();
		try{
			stream = classLoader.getResourceAsStream(report);
			if(stream != null){
				if (dataSource != null) {
					jasperPrint = JasperFillManager.fillReport(stream, parameters, dataSource);
				} else {
					try {				
						jasperPrint = JasperFillManager.fillReport(stream, parameters, this.dataSource.getConnection());
					} catch (Exception e) {
						e.getMessage();
					}finally{
						closeDataSourceSafely(this.dataSource);
					}
				}
		        exporterDocx.setParameter(JRDocxExporterParameter.JASPER_PRINT, jasperPrint);
		        exporterDocx.setParameter(JRDocxExporterParameter.OUTPUT_STREAM, outputByteArray);
		        exporterDocx.exportReport();
		        byte[] docxReport = outputByteArray.toByteArray();       
		        
		        return docxReport;
			}
		}catch(Exception e){
			e.getMessage();
		}finally{
			closeInputStreamSafely(stream);
		}
      return null;  

    }

	private JRDataSource getDataSource(List<?> datas) {
		JRDataSource dataSource=null;        
        if (datas != null && datas.size() != 0)
        	dataSource = new JRBeanCollectionDataSource(datas);
        else
        	internalLogger.debug("Le rapport est genere sans aucune donnee");
		return dataSource;
	}
	
	public static void closeDataSourceSafely(DataSource datasource){
		if(datasource != null){
			try {
				datasource.getConnection().close();
			} catch (Exception e) {
				e.getMessage();
			}	
		}
	}
	
	public static void closeInputStreamSafely(InputStream stream){
		if(stream != null){
			try {
				stream.close();
			} catch (Exception e) {
				e.getMessage();
			}	
		}
	}
}
