package ma.iam.pvr;

import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.domain.enums.impl.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ldap.LdapAutoConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
@Transactional
@org.springframework.boot.autoconfigure.SpringBootApplication(exclude = LdapAutoConfiguration.class)
public class SpringBootApp implements CommandLineRunner {
    @Autowired
    private IUtilisateurDao utilisateurDao;
    private PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    };
    public static void main(String[] args) {
        SpringApplication.run(SpringBootApp.class,args);
    }

    @Override
    public void run(String... args) throws Exception {

        Utilisateur utilisateur= new Utilisateur();
        utilisateur.setLogin("nouaman@gmail.com");
        utilisateur.setPassword(passwordEncoder().encode("12345"));
        utilisateur.setRoles(Collections.singletonList(Role.FOURNISSEUR));
        utilisateurDao.save(utilisateur);
    }
}
