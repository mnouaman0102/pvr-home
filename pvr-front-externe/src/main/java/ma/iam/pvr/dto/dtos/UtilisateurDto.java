package ma.iam.pvr.dto.dtos;



import javax.ws.rs.FormParam;

import java.util.List;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class UtilisateurDto {

	private Long idt;
	
	@FormParam("login")
	private String login;
	
	@FormParam("nom")
	private String nom;
	
	@FormParam("prenom")
	private String prenom;
	
	@FormParam("roles")
	private String roles;
	
	@FormParam("matricule")
	private String matricule;
	
	@FormParam("email")
	private String email;
	
	@FormParam("dr")
	private Long dr;
	
	private List<ProfilDto> listProfilDto;
	
	public Long getIdt() {
		return idt;
	}
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	
	public List<ProfilDto> getListProfilDto() {
		return listProfilDto;
	}
	public void setListProfilDto(List<ProfilDto> profilDtos) {
		this.listProfilDto = profilDtos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getDr() {
		return dr;
	}
	public void setDr(Long dr) {
		this.dr = dr;
	}
	
}
