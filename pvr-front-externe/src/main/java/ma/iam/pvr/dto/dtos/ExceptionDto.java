package ma.iam.pvr.dto.dtos;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="exception")
public class ExceptionDto {

	/**
	 * Type de l'erreur
	 */

	private String type;
	/**
	 * La cause de l'erreur
	 */

	private String cause;

	/**
	 * Le detail de l'erreur
	 */
	private String message;

	/**
	 * @return the cause
	 */
	public String getCause() {
		return cause;
	}

	/**
	 * @param cause
	 *            the cause to set
	 */
	public void setCause(String cause) {
		this.cause = cause;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public ExceptionDto(Exception exception) {
		this.cause = exception.getStackTrace()[0].toString();
		this.message = exception.getMessage();
		this.type = exception.getClass().getSimpleName();
	}

	public ExceptionDto() {
	}
}
