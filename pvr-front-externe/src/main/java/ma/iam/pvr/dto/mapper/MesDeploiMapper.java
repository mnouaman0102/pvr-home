package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.dao.interfaces.IMesReserveDao;
import ma.iam.pvr.dao.interfaces.IPjOperationDao;
import ma.iam.pvr.dao.interfaces.IPjQualificationsDao;
import ma.iam.pvr.dao.interfaces.IQualificationDao;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.MesReserve;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjOperation;
import ma.iam.pvr.domain.PjQualifications;
import ma.iam.pvr.domain.Qualification;
import ma.iam.pvr.dto.dtos.DeploiDto;
import ma.iam.pvr.dto.dtos.PieceJointeDto;
import ma.iam.pvr.dto.dtos.ReserveDto;
import ma.iam.pvr.utils.DateUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Component
public class MesDeploiMapper {

	@Autowired private IQualificationDao qualificationDaoImpl;	
	@Autowired private IMesReserveDao mesReserveDao;	
	@Autowired private IPjOperationDao pjOperationDaoImpl;
	@Autowired private IPjQualificationsDao pjQualificationsDaoImpl;
	public DeploiDto toDto(Mes domain){
		DeploiDto dto = null;
		if(domain != null){
			dto = new DeploiDto();	
			dto.setIdt(domain.getId());
			if(domain.getContrat() != null){
				dto.setContrat(domain.getContrat().getLabel());
				if(domain.getContrat().getFournisseur() != null)
					dto.setFournisseur(domain.getContrat().getFournisseur().getLabel());
			}

			if(domain.getSite() != null){
				dto.setSite(domain.getSite().getLabel());
				if(domain.getSite().getDr() != null)
					dto.setDr(domain.getSite().getDr().getLabel());
			}else{
				dto.setSite(domain.getCommentaireOperation());
			}
			if(domain.getEtat() != null){
				dto.setEtat(domain.getEtat().getLabel());
				dto.setIdtEtat(domain.getEtat().getId());				
			}

			dto.setNumOperation(domain.getNumOperation());
			if(domain.getDate() != null)
				dto.setDate(DateUtils.dateToString(domain.getDate()));	
			if(domain.getProjet() != null){
				dto.setProjet(domain.getProjet().getLabel());
				dto.setWorkflow(domain.getProjet().getWorkflow());
			}				

			if(domain.getTypeProjet() != null){
				dto.setTypeProjetIdt(domain.getTypeProjet().getId());
				dto.setTypeProjetLabel(domain.getTypeProjet().getLabel());
			}

			List<Qualification> listQualif = qualificationDaoImpl.getAllQualifsForDeploiByMesId(domain.getId());			

			if(listQualif != null && listQualif.size() > 0){
				for (Qualification qualification:listQualif) {
					if(qualification != null){
						List<PjQualifications> pjsQualif = pjQualificationsDaoImpl.getAllPjQualifByQualifIdt(qualification.getId());
						List<PieceJointeDto> pjExpList = new ArrayList<PieceJointeDto>();
						List<PieceJointeDto> pjOptList = new ArrayList<PieceJointeDto>();
						List<PieceJointeDto> pjDccList = new ArrayList<PieceJointeDto>();
						List<PieceJointeDto> pjQosList = new ArrayList<PieceJointeDto>();

						if(listQualif != null && listQualif.size() > 0){
							for(PjQualifications pj: pjsQualif){
								if(pj.getPieceJointe() != null){
									if(qualification.getEntite() != null){
										if(qualification.getEntite().getId() == 2L){
											pjExpList.add(toPjDto(pj.getPieceJointe()));
										}
										if(qualification.getEntite().getId() == 3L){
											pjOptList.add(toPjDto(pj.getPieceJointe()));
										}
										if(qualification.getEntite().getId() == 4L || qualification.getEntite().getId() == 5L){
											pjDccList.add(toPjDto(pj.getPieceJointe()));
										}
										if(qualification.getEntite().getId() == 6L){
											pjQosList.add(toPjDto(pj.getPieceJointe()));
										}
									}

								}								
							}
						}

						if(qualification.getStatutOperation() != null){
							if(qualification.getEntite() != null){
							switch (qualification.getEntite().getId().intValue()) {
							case 2:
								dto.setQualifExploi(qualification.getStatutOperation().getLabel());
								dto.setCommentaireExp(qualification.getCommentaire());
								dto.setPjQualifExp(pjExpList);
								break;
							case 3:
								dto.setQualifOptim(qualification.getStatutOperation().getLabel());
								dto.setCommentaireOptim(qualification.getCommentaire());
								dto.setPjQualifOptim(pjOptList);
								break;
							case 4:
								dto.setQualifDcc(qualification.getStatutOperation().getLabel());
								dto.setCommentaireDcc(qualification.getCommentaire());
								dto.setPjQualifDcc(pjDccList);
								break;
							case 5:
								dto.setQualifDcc(qualification.getStatutOperation().getLabel());
								dto.setCommentaireDcc(qualification.getCommentaire());
								dto.setPjQualifDcc(pjDccList);
								break;
							case 6:
								dto.setQualifQos(qualification.getStatutOperation().getLabel());
								dto.setCommentaireQos(qualification.getCommentaire());
								dto.setPjQualifQos(pjQosList);
								break;
							}
						}
						}

					}
				}
			}

			// remplir la liste des reserves A normalise
			List<MesReserve> mesReserves = mesReserveDao.getReservesNotNormaliseByMesId(domain.getId());
			List<ReserveDto> reserveDtos = new ArrayList<ReserveDto>();
			if(mesReserves != null && mesReserves.size() > 0){
				for(MesReserve mesReserve: mesReserves){
					ReserveDto reserveDto = new ReserveDto();
					if(mesReserve.getReserve() != null){
						reserveDto.setIdt(mesReserve.getReserve().getId());
						reserveDto.setLabel(mesReserve.getReserve().getLabel());
						reserveDto.setIdtStatut(mesReserve.getStatut().getId());
						reserveDto.setLabelStatut(mesReserve.getStatut().getLabel());
						reserveDto.setResponsabilite(mesReserve.getReserve().getResponsabilite());
						reserveDtos.add(reserveDto);
					}
				}
			}
			dto.setReserves(reserveDtos);

			List<PjOperation> pjOperExploitation = pjOperationDaoImpl.getPJOperationExploitationListByMesIdt(domain.getId());
			List<PjOperation> pjOperOptim = pjOperationDaoImpl.getPJOperationOptimListByMesIdt(domain.getId());
			List<PjOperation> pjOperDcc = pjOperationDaoImpl.getPJOperationDccListByMesIdt(domain.getId());
			List<PjOperation> pjOperDccEnv = pjOperationDaoImpl.getPJOperationDccEnvListByMesIdt(domain.getId());
			List<PjOperation> pjOperQos = pjOperationDaoImpl.getPJOperationQosListByMesIdt(domain.getId());

			List<PieceJointeDto> pjExploitation = new ArrayList<PieceJointeDto>();
			if(pjOperExploitation != null && pjOperExploitation.size() > 0){
				for(PjOperation pj: pjOperExploitation){
					if(pj.getPieceJointe() != null){
						pjExploitation.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}

			List<PieceJointeDto> pjOptim = new ArrayList<PieceJointeDto>();
			if(pjOperOptim != null && pjOperOptim.size() > 0){
				for(PjOperation pj: pjOperOptim){
					if(pj.getPieceJointe() != null){
						pjOptim.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}
			List<PieceJointeDto> pjDcc = new ArrayList<PieceJointeDto>();
			if(pjOperDcc != null && pjOperDcc.size() > 0){
				for(PjOperation pj: pjOperDcc){
					if(pj.getPieceJointe() != null){
						pjDcc.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}

			List<PieceJointeDto> pjDccEnv = new ArrayList<PieceJointeDto>();
			if(pjOperDccEnv != null && pjOperDccEnv.size() > 0){
				for(PjOperation pj: pjOperDccEnv){
					if(pj.getPieceJointe() != null){
						pjDccEnv.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}

			List<PieceJointeDto> pjQos = new ArrayList<PieceJointeDto>();
			if(pjOperQos != null && pjOperQos.size() > 0){
				for(PjOperation pj: pjOperQos){
					if(pj.getPieceJointe() != null){
						pjQos.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}

			dto.setPjExploitation(pjExploitation);
			dto.setPjOptim(pjOptim);
			dto.setPjDcc(pjDcc);	
			dto.setPjDccEnv(pjDccEnv);
			dto.setPjQos(pjQos);

			return dto;
		}
		return null;
	}
	private PieceJointeDto toPjDto(PieceJointe pj) {
		PieceJointeDto pjDto = new PieceJointeDto();
		pjDto.setIdt(pj.getId());
		pjDto.setFileName(pj.getFileName());
		pjDto.setOriginFileName(pj.getOriginFileName() == null || pj.getOriginFileName().equals("") ? pj.getFileName():pj.getOriginFileName());
		return pjDto;
	}
	public List<DeploiDto> toDtos(List<Mes> domains){
		List<DeploiDto> dtos = new ArrayList<DeploiDto>();
		if(domains != null && domains.size() > 0){
			for(Mes domain : domains){
				dtos.add(toDto(domain));
			}
			return dtos;
		}
		return null;
	}
}
