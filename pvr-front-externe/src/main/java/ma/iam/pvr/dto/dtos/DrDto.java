/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;


/**
 * @author K.ELBAGUARI
 *
 */
public class DrDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7617525703442895775L;
	private Long idt;	
	private String code;
	private String label;
	
	
	public DrDto() {
		
	}
	/**
	 * @return the idt
	 */
	public Long getIdt() {
		return idt;
	}
	/**
	 * @param idt the idt to set
	 */
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
}
