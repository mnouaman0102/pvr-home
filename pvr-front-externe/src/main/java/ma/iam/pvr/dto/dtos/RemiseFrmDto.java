package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class RemiseFrmDto implements Serializable {

	private static final long serialVersionUID = -5207242822725449361L;

	private Long idt;
	private Long idtFournisseur;
	private String fournisseurLabel;
	private String titre;
	private String solution;
	private String dateCreation;
	private String pjFileName;
	private List<Long> listMesToUpdate;
	private String originFileName;
	private List<PjDto> pjRemise;
	// For Edit
	private List<MesRemiseDto> mesList;

	private Long idtEtatRemise;
	private String codeEtatRemise;
	private String etatRemise;

	private Long idtStatutEdition;
	private String statutEdition;

	private String creerPar;
	private String dateValidation;

	public RemiseFrmDto() {

	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public Long getIdtFournisseur() {
		return idtFournisseur;
	}

	public void setIdtFournisseur(Long idtFournisseur) {
		this.idtFournisseur = idtFournisseur;
	}

	public String getFournisseurLabel() {
		return fournisseurLabel;
	}

	public void setFournisseurLabel(String fournisseurLabel) {
		this.fournisseurLabel = fournisseurLabel;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getPjFileName() {
		return pjFileName;
	}

	public void setPjFileName(String pjFileName) {
		this.pjFileName = pjFileName;
	}

	public List<Long> getListMesToUpdate() {
		return listMesToUpdate;
	}

	public void setListMesToUpdate(List<Long> listMesToUpdate) {
		this.listMesToUpdate = listMesToUpdate;
	}

	public String getOriginFileName() {
		return originFileName;
	}

	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}

	public List<PjDto> getPjRemise() {
		return pjRemise;
	}

	public void setPjRemise(List<PjDto> pjRemise) {
		this.pjRemise = pjRemise;
	}

	public List<MesRemiseDto> getMesList() {
		return mesList;
	}

	public void setMesList(List<MesRemiseDto> mesList) {
		this.mesList = mesList;
	}

	public Long getIdtEtatRemise() {
		return idtEtatRemise;
	}

	public void setIdtEtatRemise(Long idtEtatRemise) {
		this.idtEtatRemise = idtEtatRemise;
	}

	public String getCodeEtatRemise() {
		return codeEtatRemise;
	}

	public void setCodeEtatRemise(String codeEtatRemise) {
		this.codeEtatRemise = codeEtatRemise;
	}

	public String getEtatRemise() {
		return etatRemise;
	}

	public void setEtatRemise(String etatRemise) {
		this.etatRemise = etatRemise;
	}

	public Long getIdtStatutEdition() {
		return idtStatutEdition;
	}

	public void setIdtStatutEdition(Long idtStatutEdition) {
		this.idtStatutEdition = idtStatutEdition;
	}

	public String getStatutEdition() {
		return statutEdition;
	}

	public void setStatutEdition(String statutEdition) {
		this.statutEdition = statutEdition;
	}

	public String getCreerPar() {
		return creerPar;
	}

	public void setCreerPar(String creerPar) {
		this.creerPar = creerPar;
	}

	public String getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(String dateValidation) {
		this.dateValidation = dateValidation;
	}

}
