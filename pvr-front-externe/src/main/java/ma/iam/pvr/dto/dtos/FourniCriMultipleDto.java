package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;

/**
 * @author K.ELBAGUARI
 *
 */
public class FourniCriMultipleDto implements Serializable {

	   
	/**
	 * 
	 */
	private static final long serialVersionUID = -6964823806831289845L;

	private List<Long> idtsContrat;
	   
	private List<Long> idtsProjet;
	   
	private List<Long> idtsDr;
	   
	private List<Long> idtsSite;
	
	private List<Long> idtsEtat;
	   
	private List<String> numOperation;
	   
	public FourniCriMultipleDto() { }

	public List<Long> getIdtsContrat() {
		return idtsContrat;
	}

	public void setIdtsContrat(List<Long> idtsContrat) {
		this.idtsContrat = idtsContrat;
	}

	public List<Long> getIdtsProjet() {
		return idtsProjet;
	}

	public void setIdtsProjet(List<Long> idtsProjet) {
		this.idtsProjet = idtsProjet;
	}

	public List<Long> getIdtsDr() {
		return idtsDr;
	}

	public void setIdtsDr(List<Long> idtsDr) {
		this.idtsDr = idtsDr;
	}

	public List<Long> getIdtsSite() {
		return idtsSite;
	}

	public void setIdtsSite(List<Long> idtsSite) {
		this.idtsSite = idtsSite;
	}

	public List<Long> getIdtsEtat() {
		return idtsEtat;
	}

	public void setIdtsEtat(List<Long> idtsEtat) {
		this.idtsEtat = idtsEtat;
	}

	public List<String> getNumOperation() {
		return numOperation;
	}

	public void setNumOperation(List<String> numOperation) {
		this.numOperation = numOperation;
	}

}
