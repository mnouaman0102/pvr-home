/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pvr.dao.interfaces.IMesReserveDao;
import ma.iam.pvr.dao.interfaces.IMesRtDao;
import ma.iam.pvr.dao.interfaces.IQualificationDao;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.MesReserve;
import ma.iam.pvr.domain.MesRt;
import ma.iam.pvr.domain.Qualification;
import ma.iam.pvr.dto.dtos.ConsultMesReportDto;
import ma.iam.pvr.utils.DateUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
public class ConsultMesReportMapper {

	@Autowired private IMesRtDao mesRtDaoImpl;
	@Autowired private IMesReserveDao mesReserveDao;
	@Autowired private IQualificationDao qualificationDaoImpl;
	
	public ConsultMesReportDto toDto(Mes domain){
		ConsultMesReportDto dto = null;
		if(domain != null){
			if(domain.getId() != null){
				dto = new ConsultMesReportDto();	
				if(domain.getContrat() != null){
					dto.setContrat(domain.getContrat().getLabel());
					if(domain.getContrat().getFournisseur() != null)
						dto.setFournisseur(domain.getContrat().getFournisseur().getLabel());
				}
				
				if(domain.getTypeProjet() != null){
					dto.setTypeProjet(domain.getTypeProjet().getLabel());					
				}

				MesRt mesRt = mesRtDaoImpl.getMesRtByMes(domain.getId());
				if(mesRt != null) {
					if(mesRt.getRt() != null){			
						dto.setRt(mesRt.getRt().getReference());
						dto.setCommentaireRt(mesRt.getRt().getCommentaire());
						if(mesRt.getRt().getDate() != null)
							dto.setDateRt(DateUtils.dateToString(mesRt.getRt().getDate()));
					}
				}

				if(domain.getPvr() != null){
					dto.setPvr(domain.getPvr().getReference());
					if(domain.getPvr().getDate() != null)
						dto.setDatePvr(DateUtils.dateToString(domain.getPvr().getDate()));
				}
				
				if(domain.getPvd() != null){
					if(domain.getPvd().getReference() != null)
						dto.setPvd(domain.getPvd().getReference());
					if(domain.getPvd().getDate() != null)
						dto.setDatePvd(DateUtils.dateToString(domain.getPvd().getDate()));
				}
				if(domain.getSite() != null){
					dto.setSite(domain.getSite().getLabel());
					dto.setCodeSite(domain.getSite().getCode());
					if(domain.getSite().getDr() != null)
						dto.setDr(domain.getSite().getDr().getLabel());
				}
				if(domain.getEtat() != null)
					dto.setEtat(domain.getEtat().getLabel());
				
				if(domain.getProjet() != null)
					dto.setProjet(domain.getProjet().getLabel());		
				
				//NumOperation
				dto.setNumOperation(domain.getNumOperation());
				
				//Date Operation
				if(domain.getDate() != null)
					dto.setDate(DateUtils.dateToString(domain.getDate()));				
				/**
				 * Reserve format String
				 */
				//List<MesReserve> mesReservesList = mesReserveDao.getReservesByMesId(domain.getId()); ??
				List<MesReserve> mesReserve = mesReserveDao.getReservesByMesId(domain.getId());
				if(mesReserve != null && mesReserve.size() > 0){
					Map<String, List<String>> responsabiliteToReserves = getReserveByResponsability(mesReserve);
					String res = formatReserveByResponsability(responsabiliteToReserves);
					/*for(MesReserve mesRes: mesReserve){			
						if(mesRes.getReserve() != null){
							res+="<div>&#9658;"+mesRes.getReserve().getResponsabilite()+
								"</div><div>&emsp;&emsp;&#8211;"+mesRes.getReserve().getLabel()+"</div>";						
						}
					}*/
					dto.setReserve(res);
				}
				
				//Statut MES
				dto.setStatut(domain.getStatut());				
				
				//Qualification
				List<Qualification> listQualif = qualificationDaoImpl.getAllQualifsForDeploiByMesId(domain.getId());
				if(listQualif != null && listQualif.size() > 0){
					for (Qualification qualification:listQualif) {
						if(qualification != null){
							if(qualification.getStatutOperation() != null){
								switch (qualification.getEntite().getId().intValue()) {
								case 2:
									dto.setQualifExploi(qualification.getStatutOperation().getLabel());
									dto.setCommentaireExploi(qualification.getCommentaire());
									break;
								case 3:
									dto.setQualifOptim(qualification.getStatutOperation().getLabel());
									dto.setCommentaireOptim(qualification.getCommentaire());
									break;
								case 4:
									dto.setQualifDcc(qualification.getStatutOperation().getLabel());
									break;
								case 5:
									dto.setQualifDccEnv(qualification.getStatutOperation().getLabel());
									break;
								case 6:
									dto.setQualifQos(qualification.getStatutOperation().getLabel());
									dto.setCommentaireQos(qualification.getCommentaire());
									break;
								}
								
							}
						}
					}
				}
				//Date MES
				if(domain.getDateMES() != null)
					dto.setDateMes(DateUtils.dateToString(domain.getDateMES()));
				
				//Commentaire
				dto.setCommentaire(domain.getCommentaire());
				//Commande
				dto.setCommande(domain.getCommande());	
				
				// Date MES REELLE
				if(domain.getDateMESReelle() != null)
					dto.setDateMESReelle(DateUtils.dateToString(domain.getDateMESReelle()));
				
				return dto;
			}
		}
		return null;
	}    
	
	private Map<String, List<String>> getReserveByResponsability(List<MesReserve> mesReserves) {
		Map<String,List<String>> responsabiliteToReserves=new HashMap<String,List<String>>();
		if(mesReserves != null && mesReserves.size() > 0) {
			for( MesReserve mesReserve:mesReserves) {
				if(mesReserve.getReserve() != null) {
					if(responsabiliteToReserves.containsKey(mesReserve.getReserve().getResponsabilite())) {
						responsabiliteToReserves.get(mesReserve.getReserve().getResponsabilite()).add(mesReserve.getReserve().getLabel());
					}
					else {
						if(mesReserve.getReserve().getResponsabilite() != null) {
							responsabiliteToReserves.put(mesReserve.getReserve().getResponsabilite(), new ArrayList<String>());
							responsabiliteToReserves.get(mesReserve.getReserve().getResponsabilite()).add(mesReserve.getReserve().getLabel());
						}
					}
				}
			}
		}
		return responsabiliteToReserves;
	}
	private String formatReserveByResponsability(Map<String, List<String>> responsabiliteToReserves) {
		StringBuilder reserveByResponsabilityFormatted=new StringBuilder();
		 for (Map.Entry<String, List<String>> entry : responsabiliteToReserves.entrySet()) {
		       StringBuilder reserveByResponsability=new StringBuilder("<div>&#9658;"+entry.getKey()+"</div>");
		       for(String reserve:entry.getValue()) {
		    	   reserveByResponsability.append("<div>&emsp;&emsp;&#8211;"+reserve+"</div>");
		       }
		       reserveByResponsabilityFormatted.append(reserveByResponsability.toString());
		    }
		return reserveByResponsabilityFormatted.toString();
	}
	
	public List<ConsultMesReportDto> toDtos(List<Mes> domains){
		if(domains != null && domains.size() > 0){
			List<ConsultMesReportDto> dtos = new ArrayList<ConsultMesReportDto>();
			for(Mes domain : domains){
				dtos.add(toDto(domain));
			}
			return dtos;
		}
		return null;		
	}

}
