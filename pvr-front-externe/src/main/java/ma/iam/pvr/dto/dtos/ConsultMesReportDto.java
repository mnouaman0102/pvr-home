/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;



/**
 * @author K.ELBAGUARI
 *
 */
public class ConsultMesReportDto implements Serializable{
	
	   /**
	 * 
	 */
	private static final long serialVersionUID = -6688945389783384009L;
	private String fournisseur;
	   private String contrat;
	   private String commande;	  
	   private String site;
	   private String dr;
	   private String codeSite;
	   private String projet;
	   private String typeProjet;
	   private String numOperation;
	   private String date;	
	   private String rt;
	   private String dateRt;
	   private String pvr;
	   private String datePvr; 
	   private String pvd;	
	   private String datePvd; 
	   private String statut;
	   private String etat;
	   private String qualifDcc;
	   private String qualifDccEnv;
	   private String qualifOptim;
	   private String qualifExploi;
	   private String qualifQos;
	   private String dateMes; 
	   private String commentaireExploi;
	   private String commentaireOptim;		   
	   private String commentaireQos;
	   private String commentaire;	
	   private String commentaireRt;
	   private List<MiniReserveReportDto>  reserves;  
	   private String reserve;

		private String dateMESReelle;
		/**
		 * 
		 */
		public ConsultMesReportDto() {	}
		/**
		 * @return the fournisseur
		 */
		public String getFournisseur() {
			return fournisseur;
		}
		/**
		 * @param fournisseur the fournisseur to set
		 */
		public void setFournisseur(String fournisseur) {
			this.fournisseur = fournisseur;
		}
		/**
		 * @return the contrat
		 */
		public String getContrat() {
			return contrat;
		}
		/**
		 * @param contrat the contrat to set
		 */
		public void setContrat(String contrat) {
			this.contrat = contrat;
		}
		/**
		 * @return the commande
		 */
		public String getCommande() {
			return commande;
		}
		/**
		 * @param commande the commande to set
		 */
		public void setCommande(String commande) {
			this.commande = commande;
		}
		/**
		 * @return the site
		 */
		public String getSite() {
			return site;
		}
		/**
		 * @param site the site to set
		 */
		public void setSite(String site) {
			this.site = site;
		}
		/**
		 * @return the dr
		 */
		public String getDr() {
			return dr;
		}
		/**
		 * @param dr the dr to set
		 */
		public void setDr(String dr) {
			this.dr = dr;
		}
		/**
		 * @return the rt
		 */
		public String getRt() {
			return rt;
		}
		/**
		 * @param rt the rt to set
		 */
		public void setRt(String rt) {
			this.rt = rt;
		}
		/**
		 * @return the dateRt
		 */
		public String getDateRt() {
			return dateRt;
		}
		/**
		 * @param dateRt the dateRt to set
		 */
		public void setDateRt(String dateRt) {
			this.dateRt = dateRt;
		}
		/**
		 * @return the pvr
		 */
		public String getPvr() {
			return pvr;
		}
		/**
		 * @param pvr the pvr to set
		 */
		public void setPvr(String pvr) {
			this.pvr = pvr;
		}
		/**
		 * @return the datePvr
		 */
		public String getDatePvr() {
			return datePvr;
		}
		/**
		 * @param datePvr the datePvr to set
		 */
		public void setDatePvr(String datePvr) {
			this.datePvr = datePvr;
		}
		
		
		/**
		 * @return the reserves
		 */
		public List<MiniReserveReportDto> getReserves() {
			return reserves;
		}
		/**
		 * @param reserves the reserves to set
		 */
		public void setReserves(List<MiniReserveReportDto> reserves) {
			this.reserves = reserves;
		}
		/**
		 * @return the statut
		 */
		public String getStatut() {
			return statut;
		}
		/**
		 * @param statut the statut to set
		 */
		public void setStatut(String statut) {
			this.statut = statut;
		}
		/**
		 * @return the etat
		 */
		public String getEtat() {
			return etat;
		}
		/**
		 * @param etat the etat to set
		 */
		public void setEtat(String etat) {
			this.etat = etat;
		}
		/**
		 * @return the projet
		 */
		public String getProjet() {
			return projet;
		}
		/**
		 * @param projet the projet to set
		 */
		public void setProjet(String projet) {
			this.projet = projet;
		}
		/**
		 * @return the commentaire
		 */
		public String getCommentaire() {
			return commentaire;
		}
		/**
		 * @param commentaire the commentaire to set
		 */
		public void setCommentaire(String commentaire) {
			this.commentaire = commentaire;
		}
		/**
		 * @return the numOperation
		 */
		public String getNumOperation() {
			return numOperation;
		}
		/**
		 * @param numOperation the numOperation to set
		 */
		public void setNumOperation(String numOperation) {
			this.numOperation = numOperation;
		}
		/**
		 * @return the date
		 */
		public String getDate() {
			return date;
		}
		/**
		 * @param date the date to set
		 */
		public void setDate(String date) {
			this.date = date;
		}
		/**
		 * @return the pvd
		 */
		public String getPvd() {
			return pvd;
		}
		/**
		 * @param pvd the pvd to set
		 */
		public void setPvd(String pvd) {
			this.pvd = pvd;
		}
		/**
		 * @return the datePvd
		 */
		public String getDatePvd() {
			return datePvd;
		}
		/**
		 * @param datePvd the datePvd to set
		 */
		public void setDatePvd(String datePvd) {
			this.datePvd = datePvd;
		}
		/**
		 * @return the qualifDcc
		 */
		public String getQualifDcc() {
			return qualifDcc;
		}
		/**
		 * @param qualifDcc the qualifDcc to set
		 */
		public void setQualifDcc(String qualifDcc) {
			this.qualifDcc = qualifDcc;
		}
		/**
		 * @return the qualifOptim
		 */
		public String getQualifOptim() {
			return qualifOptim;
		}
		/**
		 * @param qualifOptim the qualifOptim to set
		 */
		public void setQualifOptim(String qualifOptim) {
			this.qualifOptim = qualifOptim;
		}
		/**
		 * @return the qualifExploi
		 */
		public String getQualifExploi() {
			return qualifExploi;
		}
		/**
		 * @param qualifExploi the qualifExploi to set
		 */
		public void setQualifExploi(String qualifExploi) {
			this.qualifExploi = qualifExploi;
		}
		/**
		 * @return the dateMes
		 */
		public String getDateMes() {
			return dateMes;
		}
		/**
		 * @param dateMes the dateMes to set
		 */
		public void setDateMes(String dateMes) {
			this.dateMes = dateMes;
		}
		/**
		 * @return the typeProjet
		 */
		public String getTypeProjet() {
			return typeProjet;
		}
		/**
		 * @param typeProjet the typeProjet to set
		 */
		public void setTypeProjet(String typeProjet) {
			this.typeProjet = typeProjet;
		}
		/**
		 * @return the reserve
		 */
		public String getReserve() {
			return reserve;
		}
		/**
		 * @param reserve the reserve to set
		 */
		public void setReserve(String reserve) {
			this.reserve = reserve;
		}
		/**
		 * @return the qualifQos
		 */
		public String getQualifQos() {
			return qualifQos;
		}
		/**
		 * @param qualifQos the qualifQos to set
		 */
		public void setQualifQos(String qualifQos) {
			this.qualifQos = qualifQos;
		}
		/**
		 * @return the qualifDccEnv
		 */
		public String getQualifDccEnv() {
			return qualifDccEnv;
		}
		/**
		 * @param qualifDccEnv the qualifDccEnv to set
		 */
		public void setQualifDccEnv(String qualifDccEnv) {
			this.qualifDccEnv = qualifDccEnv;
		}
		public String getCommentaireExploi() {
			return commentaireExploi;
		}
		public void setCommentaireExploi(String commentaireExploi) {
			this.commentaireExploi = commentaireExploi;
		}
		public String getCommentaireOptim() {
			return commentaireOptim;
		}
		public void setCommentaireOptim(String commentaireOptim) {
			this.commentaireOptim = commentaireOptim;
		}
		public String getCommentaireQos() {
			return commentaireQos;
		}
		public void setCommentaireQos(String commentaireQos) {
			this.commentaireQos = commentaireQos;
		}
		public String getCommentaireRt() {
			return commentaireRt;
		}
		public void setCommentaireRt(String commentaireRt) {
			this.commentaireRt = commentaireRt;
		}
		public String getCodeSite() {
			return codeSite;
		}
		public void setCodeSite(String codeSite) {
			this.codeSite = codeSite;
		}
		public String getDateMESReelle() {
			return dateMESReelle;
		}
		public void setDateMESReelle(String dateMESReelle) {
			this.dateMESReelle = dateMESReelle;
		}
		
}
