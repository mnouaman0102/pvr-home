/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class PieceJointeDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7746577200988376257L;

	private Long idt;
	
	private String fileName;

	private String originFileName;

	public PieceJointeDto() {
	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOriginFileName() {
		return originFileName;
	}

	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}

}
