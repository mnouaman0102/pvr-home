package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;


/**
 * @author K.ELBAGUARI
 *
 */
public class ConsultDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5496423876593090321L;
	private Long idt;
	private String fournisseur;
	private String contrat;
	private String commande;
	private String codeSite;
	private String site;
	private String dr;
	private String projet;
	private String numOperation;
	private String date;
	private String rt;
	private String dateRt;
	private String pvr;
	private String datePvr;
	private String pvd;
	private String datePvd;
	private String statut;
	private Long idtStatut;
	private String etat;
	private String qualifExploi;
	private String qualifOptim;
	private String qualifDcc;
	private String qualifDccEnv;
	private String qualifQos;
	private String dateMes;
	private PieceJointeDto pjDeploiement;
	private List<ReserveDto> reserves;
	private Long idtTypeProjet;
	private String typeProjetLabel;
	private List<PieceJointeDto> pjOptim;
	private List<PieceJointeDto> pjExploitation;
	private List<PieceJointeDto> pjQos;
	private List<PieceJointeDto> pjDcc;
	private List<PieceJointeDto> pjDccEnv;
	private List<PieceJointeDto> pjRt;
	private List<PieceJointeDto> pjRtEnv;
	private List<PieceJointeDto> pjPvr;
	private List<PieceJointeDto> pjPvd;
	private List<PieceJointeDto> pjDccSuivi;
	private List<PieceJointeDto> pjDccEnvSuivi;
	private String commentaireExploi;
	private String commentaireOptim;
	private String commentaireQos;
	private String commentaire;
	private String commentaireRt;

	// PJ Qualification
	private List<PieceJointeDto> pjQualifOptim;
	private List<PieceJointeDto> pjQualifExploitation;
	private List<PieceJointeDto> pjQualifQos;
	private List<PieceJointeDto> pjQualifDcc;
	private List<PieceJointeDto> pjQualifDccEnv;

	private String dateMESReelle;
	
	public ConsultDto() {
	}

	public String getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(String fournisseur) {
		this.fournisseur = fournisseur;
	}

	public String getContrat() {
		return contrat;
	}

	public void setContrat(String contrat) {
		this.contrat = contrat;
	}

	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}

	public String getDateRt() {
		return dateRt;
	}

	public void setDateRt(String dateRt) {
		this.dateRt = dateRt;
	}

	public String getPvr() {
		return pvr;
	}

	public void setPvr(String pvr) {
		this.pvr = pvr;
	}

	public String getDatePvr() {
		return datePvr;
	}

	public void setDatePvr(String datePvr) {
		this.datePvr = datePvr;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getDr() {
		return dr;
	}

	public void setDr(String dr) {
		this.dr = dr;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public String getCommande() {
		return commande;
	}

	public void setCommande(String commande) {
		this.commande = commande;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public String getCommentaireRt() {
		return commentaireRt;
	}

	public void setCommentaireRt(String commentaireRt) {
		this.commentaireRt = commentaireRt;
	}

	public List<ReserveDto> getReserves() {
		return reserves;
	}

	public void setReserves(List<ReserveDto> reserves) {
		this.reserves = reserves;
	}

	public Long getIdtStatut() {
		return idtStatut;
	}

	public void setIdtStatut(Long idtStatut) {
		this.idtStatut = idtStatut;
	}

	public String getDateMes() {
		return dateMes;
	}

	public void setDateMes(String dateMes) {
		this.dateMes = dateMes;
	}

	public String getProjet() {
		return projet;
	}

	public void setProjet(String projet) {
		this.projet = projet;
	}

	public String getNumOperation() {
		return numOperation;
	}

	public void setNumOperation(String numOperation) {
		this.numOperation = numOperation;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPvd() {
		return pvd;
	}

	public void setPvd(String pvd) {
		this.pvd = pvd;
	}

	public String getDatePvd() {
		return datePvd;
	}

	public void setDatePvd(String datePvd) {
		this.datePvd = datePvd;
	}

	public String getQualifDcc() {
		return qualifDcc;
	}

	public void setQualifDcc(String qualifDcc) {
		this.qualifDcc = qualifDcc;
	}

	public String getQualifOptim() {
		return qualifOptim;
	}

	public void setQualifOptim(String qualifOptim) {
		this.qualifOptim = qualifOptim;
	}

	public String getQualifExploi() {
		return qualifExploi;
	}

	public void setQualifExploi(String qualifExploi) {
		this.qualifExploi = qualifExploi;
	}

	public PieceJointeDto getPjDeploiement() {
		return pjDeploiement;
	}

	public void setPjDeploiement(PieceJointeDto pjDeploiement) {
		this.pjDeploiement = pjDeploiement;
	}

	public String getQualifDccEnv() {
		return qualifDccEnv;
	}

	public void setQualifDccEnv(String qualifDccEnv) {
		this.qualifDccEnv = qualifDccEnv;
	}

	public Long getIdtTypeProjet() {
		return idtTypeProjet;
	}

	public void setIdtTypeProjet(Long idtTypeProjet) {
		this.idtTypeProjet = idtTypeProjet;
	}

	public String getTypeProjetLabel() {
		return typeProjetLabel;
	}

	public void setTypeProjetLabel(String typeProjetLabel) {
		this.typeProjetLabel = typeProjetLabel;
	}

	public String getQualifQos() {
		return qualifQos;
	}

	public void setQualifQos(String qualifQos) {
		this.qualifQos = qualifQos;
	}

	public List<PieceJointeDto> getPjOptim() {
		return pjOptim;
	}

	public void setPjOptim(List<PieceJointeDto> pjOptim) {
		this.pjOptim = pjOptim;
	}

	public List<PieceJointeDto> getPjExploitation() {
		return pjExploitation;
	}

	public void setPjExploitation(List<PieceJointeDto> pjExploitation) {
		this.pjExploitation = pjExploitation;
	}

	public List<PieceJointeDto> getPjQos() {
		return pjQos;
	}

	public void setPjQos(List<PieceJointeDto> pjQos) {
		this.pjQos = pjQos;
	}

	public List<PieceJointeDto> getPjDcc() {
		return pjDcc;
	}

	public void setPjDcc(List<PieceJointeDto> pjDcc) {
		this.pjDcc = pjDcc;
	}

	public List<PieceJointeDto> getPjDccEnv() {
		return pjDccEnv;
	}

	public void setPjDccEnv(List<PieceJointeDto> pjDccEnv) {
		this.pjDccEnv = pjDccEnv;
	}

	public List<PieceJointeDto> getPjRt() {
		return pjRt;
	}

	public void setPjRt(List<PieceJointeDto> pjRt) {
		this.pjRt = pjRt;
	}

	public List<PieceJointeDto> getPjRtEnv() {
		return pjRtEnv;
	}

	public void setPjRtEnv(List<PieceJointeDto> pjRtEnv) {
		this.pjRtEnv = pjRtEnv;
	}

	public List<PieceJointeDto> getPjPvr() {
		return pjPvr;
	}

	public void setPjPvr(List<PieceJointeDto> pjPvr) {
		this.pjPvr = pjPvr;
	}

	public List<PieceJointeDto> getPjPvd() {
		return pjPvd;
	}

	public void setPjPvd(List<PieceJointeDto> pjPvd) {
		this.pjPvd = pjPvd;
	}

	public List<PieceJointeDto> getPjDccSuivi() {
		return pjDccSuivi;
	}

	public void setPjDccSuivi(List<PieceJointeDto> pjDccSuivi) {
		this.pjDccSuivi = pjDccSuivi;
	}

	public List<PieceJointeDto> getPjDccEnvSuivi() {
		return pjDccEnvSuivi;
	}

	public void setPjDccEnvSuivi(List<PieceJointeDto> pjDccEnvSuivi) {
		this.pjDccEnvSuivi = pjDccEnvSuivi;
	}

	public String getCommentaireOptim() {
		return commentaireOptim;
	}

	public void setCommentaireOptim(String commentaireOptim) {
		this.commentaireOptim = commentaireOptim;
	}

	public String getCommentaireExploi() {
		return commentaireExploi;
	}

	public void setCommentaireExploi(String commentaireExploi) {
		this.commentaireExploi = commentaireExploi;
	}

	public String getCommentaireQos() {
		return commentaireQos;
	}

	public void setCommentaireQos(String commentaireQos) {
		this.commentaireQos = commentaireQos;
	}

	public String getCodeSite() {
		return codeSite;
	}

	public void setCodeSite(String codeSite) {
		this.codeSite = codeSite;
	}

	public List<PieceJointeDto> getPjQualifOptim() {
		return pjQualifOptim;
	}

	public void setPjQualifOptim(List<PieceJointeDto> pjQualifOptim) {
		this.pjQualifOptim = pjQualifOptim;
	}

	public List<PieceJointeDto> getPjQualifExploitation() {
		return pjQualifExploitation;
	}

	public void setPjQualifExploitation(List<PieceJointeDto> pjQualifExploitation) {
		this.pjQualifExploitation = pjQualifExploitation;
	}

	public List<PieceJointeDto> getPjQualifQos() {
		return pjQualifQos;
	}

	public void setPjQualifQos(List<PieceJointeDto> pjQualifQos) {
		this.pjQualifQos = pjQualifQos;
	}

	public List<PieceJointeDto> getPjQualifDcc() {
		return pjQualifDcc;
	}

	public void setPjQualifDcc(List<PieceJointeDto> pjQualifDcc) {
		this.pjQualifDcc = pjQualifDcc;
	}

	public List<PieceJointeDto> getPjQualifDccEnv() {
		return pjQualifDccEnv;
	}

	public void setPjQualifDccEnv(List<PieceJointeDto> pjQualifDccEnv) {
		this.pjQualifDccEnv = pjQualifDccEnv;
	}

	public String getDateMESReelle() {
		return dateMESReelle;
	}

	public void setDateMESReelle(String dateMESReelle) {
		this.dateMESReelle = dateMESReelle;
	}

}
