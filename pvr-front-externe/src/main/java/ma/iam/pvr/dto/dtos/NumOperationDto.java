/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class NumOperationDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4436965661680098943L;
	private Long idt;
	private String numOperation;
	
	
	public NumOperationDto() {
		
	}


	public Long getIdt() {
		return idt;
	}


	public void setIdt(Long idt) {
		this.idt = idt;
	}


	public String getNumOperation() {
		return numOperation;
	}


	public void setNumOperation(String numOperation) {
		this.numOperation = numOperation;
	}
	
}
