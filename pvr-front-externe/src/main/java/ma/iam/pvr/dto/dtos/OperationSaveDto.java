package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;


/**
 * @author K.ELBAGUARI
 *
 */
public class OperationSaveDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5059208997269618955L;
	private Long idt; 
	private Long idtContrat;
	private Long idtSite;
	private Long idtProjet;
	private String date;	
	
	private List<PjDto> pjExploi;
	private List<PjDto> pjDcc;	
	private List<PjDto> pjOptim;
	private List<PjDto> pjDccEnv;
	private List<PjDto> pjQos;
	
	public Long getIdt() {
		return idt;
	}
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	public Long getIdtContrat() {
		return idtContrat;
	}
	public void setIdtContrat(Long idtContrat) {
		this.idtContrat = idtContrat;
	}
	public Long getIdtSite() {
		return idtSite;
	}
	public void setIdtSite(Long idtSite) {
		this.idtSite = idtSite;
	}
	public Long getIdtProjet() {
		return idtProjet;
	}
	public void setIdtProjet(Long idtProjet) {
		this.idtProjet = idtProjet;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<PjDto> getPjExploi() {
		return pjExploi;
	}
	public void setPjExploi(List<PjDto> pjExploi) {
		this.pjExploi = pjExploi;
	}
	public List<PjDto> getPjDcc() {
		return pjDcc;
	}
	public void setPjDcc(List<PjDto> pjDcc) {
		this.pjDcc = pjDcc;
	}
	public List<PjDto> getPjOptim() {
		return pjOptim;
	}
	public void setPjOptim(List<PjDto> pjOptim) {
		this.pjOptim = pjOptim;
	}
	public List<PjDto> getPjDccEnv() {
		return pjDccEnv;
	}
	public void setPjDccEnv(List<PjDto> pjDccEnv) {
		this.pjDccEnv = pjDccEnv;
	}
	public List<PjDto> getPjQos() {
		return pjQos;
	}
	public void setPjQos(List<PjDto> pjQos) {
		this.pjQos = pjQos;
	}
	
}
