/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ma.iam.pvr.dao.interfaces.IEntiteDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IPieceJointeDao;
import ma.iam.pvr.dao.interfaces.IPjOperationDao;
import ma.iam.pvr.dao.interfaces.IQualificationDao;
import ma.iam.pvr.dao.interfaces.IStatutOperationDao;
import ma.iam.pvr.domain.Entite;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjOperation;
import ma.iam.pvr.domain.Qualification;
import ma.iam.pvr.domain.StatutOperation;
import ma.iam.pvr.dto.dtos.OperationEditDto;
import ma.iam.pvr.dto.dtos.OperationUpdateDto;
import ma.iam.pvr.dto.dtos.PjDto;
import ma.iam.pvr.exception.MyAppException;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
public class OperationEditMapper {
	@Autowired 
	private IPjOperationDao pjOperationDaoImpl;		

	//To Update
	@Autowired
	private IMesDao mesDao;
	@Autowired
	private IEntiteDao entiteDaoImpl;	
	@Autowired
	private IStatutOperationDao statutOperationDaoImpl;
	@Autowired 
	private IPieceJointeDao pjDaoImpl;
	@Autowired
	private IQualificationDao qualificationDaoImpl;

	public OperationEditDto toDto(Mes domain){
		OperationEditDto dto = new OperationEditDto();
		if(domain != null){
			dto.setIdt(domain.getId());
			dto.setComment(domain.getCommentaireOperation());
			if(domain.getTypeProjet() != null)
				dto.setIdtTypeProjet(domain.getTypeProjet().getId());

			if(domain.getContrat() != null){
				dto.setIdtContrat(domain.getContrat().getId());
				if(domain.getContrat().getFournisseur() != null){
					dto.setIdtFournisseur(domain.getContrat().getFournisseur().getId());
				}					
			}

			if(domain.getSite() != null){
				dto.setIdtSite(domain.getSite().getId());
				if(domain.getSite().getDr() != null){
					dto.setIdtDr(domain.getSite().getDr().getId());
				}
			}
			if(domain.getProjet() != null){
				dto.setIdtProjet(domain.getProjet().getId());
			}
			List<PjOperation> pjOperExploitation = pjOperationDaoImpl.getPJOperationExploitationListByMesIdt(domain.getId());
			List<PjOperation> pjOperOptim = pjOperationDaoImpl.getPJOperationOptimListByMesIdt(domain.getId());
			List<PjOperation> pjOperDcc = pjOperationDaoImpl.getPJOperationDccListByMesIdt(domain.getId());
			List<PjOperation> pjOperDccEnv = pjOperationDaoImpl.getPJOperationDccEnvListByMesIdt(domain.getId());
			List<PjOperation> pjOperQos = pjOperationDaoImpl.getPJOperationQosListByMesIdt(domain.getId());

			List<PjDto> pjExploitation = new ArrayList<PjDto>();
			if(pjOperExploitation != null && pjOperExploitation.size() > 0){
				for(PjOperation pj: pjOperExploitation){
					if(pj.getPieceJointe() != null){
						PjDto pjDto = new PjDto();
						pjDto.setIdt(pj.getId());
						pjDto.setFileNameSaved(pj.getPieceJointe().getFileName());
						if(pj.getPieceJointe().getOriginFileName() != null && !pj.getPieceJointe().getOriginFileName().equals("")){
							String fileName = pj.getPieceJointe().getFileName();
							String extention = FilenameUtils.getExtension(fileName);
							pjDto.setOriginFileName(pj.getPieceJointe().getOriginFileName() + "." + extention);
						}
						else{
							pjDto.setOriginFileName(pj.getPieceJointe().getFileName());
						}

						pjExploitation.add(pjDto);
					}
				}
			}

			List<PjDto> pjOptim = new ArrayList<PjDto>();
			if(pjOperOptim != null && pjOperOptim.size() > 0){
				for(PjOperation pj: pjOperOptim){
					if(pj.getPieceJointe() != null){
						PjDto pjDto = new PjDto();
						pjDto.setIdt(pj.getId());
						pjDto.setFileNameSaved(pj.getPieceJointe().getFileName());
						if(pj.getPieceJointe().getOriginFileName() != null && !pj.getPieceJointe().getOriginFileName().equals("")){
							String fileName = pj.getPieceJointe().getFileName();
							String extention = FilenameUtils.getExtension(fileName);
							pjDto.setOriginFileName(pj.getPieceJointe().getOriginFileName() + "." + extention);
						}
						else{
							pjDto.setOriginFileName(pj.getPieceJointe().getFileName());
						}
						pjOptim.add(pjDto);
					}
				}
			}
			List<PjDto> pjDcc = new ArrayList<PjDto>();
			if(pjOperDcc != null && pjOperDcc.size() > 0){
				for(PjOperation pj: pjOperDcc){
					if(pj.getPieceJointe() != null){
						PjDto pjDto = new PjDto();
						pjDto.setIdt(pj.getId());
						pjDto.setFileNameSaved(pj.getPieceJointe().getFileName());
						if(pj.getPieceJointe().getOriginFileName() != null && !pj.getPieceJointe().getOriginFileName().equals("")){
							String fileName = pj.getPieceJointe().getFileName();
							String extention = FilenameUtils.getExtension(fileName);
							pjDto.setOriginFileName(pj.getPieceJointe().getOriginFileName() + "." + extention);
						}
						else{
							pjDto.setOriginFileName(pj.getPieceJointe().getFileName());
						}
						pjDcc.add(pjDto);
					}
				}
			}

			List<PjDto> pjDccEnv = new ArrayList<PjDto>();
			if(pjOperDccEnv != null && pjOperDccEnv.size() > 0){
				for(PjOperation pj: pjOperDccEnv){
					if(pj.getPieceJointe() != null){
						PjDto pjDto = new PjDto();
						pjDto.setIdt(pj.getId());
						pjDto.setFileNameSaved(pj.getPieceJointe().getFileName());
						if(pj.getPieceJointe().getOriginFileName() != null && !pj.getPieceJointe().getOriginFileName().equals("")){
							String fileName = pj.getPieceJointe().getFileName();
							String extention = FilenameUtils.getExtension(fileName);
							pjDto.setOriginFileName(pj.getPieceJointe().getOriginFileName() + "." + extention);
						}
						else{
							pjDto.setOriginFileName(pj.getPieceJointe().getFileName());
						}
						pjDccEnv.add(pjDto);
					}
				}
			}

			List<PjDto> pjQos = new ArrayList<PjDto>();
			if(pjOperQos != null && pjOperQos.size() > 0){
				for(PjOperation pj: pjOperQos){
					if(pj.getPieceJointe() != null){
						PjDto pjDto = new PjDto();
						pjDto.setIdt(pj.getId());
						pjDto.setFileNameSaved(pj.getPieceJointe().getFileName());
						if(pj.getPieceJointe().getOriginFileName() != null && !pj.getPieceJointe().getOriginFileName().equals("")){
							String fileName = pj.getPieceJointe().getFileName();
							String extention = FilenameUtils.getExtension(fileName);
							pjDto.setOriginFileName(pj.getPieceJointe().getOriginFileName() + "." + extention);
						}
						else{
							pjDto.setOriginFileName(pj.getPieceJointe().getFileName());
						}
						pjQos.add(pjDto);
					}
				}
			}

			dto.setPjExploi(pjExploitation);
			dto.setPjOptim(pjOptim);
			dto.setPjDcc(pjDcc);	
			dto.setPjDccEnv(pjDccEnv);
			dto.setPjQos(pjQos);

		}

		return dto;
	}

	public Mes toDomain(OperationUpdateDto dto) throws MyAppException{
		Mes mes = null;
		Long idtPj = null;
		final String pourQualif = "pour qualification";
		Entite entite = null;	
		PjOperation pjOperation = null;
		PieceJointe pieceJointe = null;
		Qualification qualification = null;
		StatutOperation statutOperation = null;
		if(dto != null && dto.getIdt() > 0){
			mes = mesDao.get(dto.getIdt());
			if(mes != null){
				/**
				 * Operation DCC RADIO
				 */
				if(mes.getTypeProjet() != null && mes.getTypeProjet().getId() == 1L){				
					//Ajouter les nouvelles PJs Exploitation
					if(dto.getPjExploi() != null && dto.getPjExploi().size() > 0){				
						for(PjDto pjDto : dto.getPjExploi()){
							if(pjDto != null && pjDto.getOriginFileName() !=null && pjDto.getFileNameSaved() != null){
								pieceJointe = new PieceJointe();
								pieceJointe.setFileName(pjDto.getFileNameSaved());
								pieceJointe.setOriginFileName(pjDto.getOriginFileName());
								idtPj = pjDaoImpl.save(pieceJointe).getId();
								PieceJointe savedPj = pjDaoImpl.get(idtPj);
								if(savedPj != null){
									pjOperation= new PjOperation();
									pjOperation.setPieceJointe(savedPj);
									pjOperation.setMes(mes);
									entite = entiteDaoImpl.get(2L);
									pjOperation.setEntite(entite);
									pjOperationDaoImpl.save(pjOperation);
								}
							}
						}
						qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(mes.getId(), 2L);
						statutOperation = statutOperationDaoImpl.get(1L);
						if(qualification == null){
							Qualification qualif = new Qualification();
							qualif.setStatutOperation(statutOperation);
							qualif.setCommentaire(null);
							qualif.setEntite(entite);
							qualif.setActionType(pourQualif);
							qualif.setMes(mes);
							qualif.setDate(new Date());
							qualificationDaoImpl.save(qualif);
						}

					}
					// Supprimer le lien vers les PJs Exploitation retire
					if(dto.getPjExploi() != null && dto.getPjExploi().size() > 0){
						List<Long> pjsExpToRemove = new ArrayList<Long>();
						for(PjDto pjDto: dto.getPjExploi()){
							if(pjDto != null && pjDto.getFileNameSaved() == null && pjDto.getOriginFileName() == null){
								pjsExpToRemove.add(pjDto.getIdt());
							}
						}
						List<PjOperation> pjsOperationExp = pjOperationDaoImpl.getPJOperationExploitationListByMesIdt(mes.getId());
						if(pjsOperationExp != null && pjsOperationExp.size() > 0){
							for(PjOperation pjOper: pjsOperationExp){
								if(pjsExpToRemove.contains(pjOper.getId())){
									pjOperationDaoImpl.delete(pjOper);
								}
							}
						}
					}

					//Ajouter les nouvelles PJs Optim
					if(dto.getPjOptim() != null && dto.getPjOptim().size() > 0){
						for(PjDto pjDto : dto.getPjOptim()){
							if(pjDto != null && pjDto.getOriginFileName() !=null && pjDto.getFileNameSaved() != null){
								pieceJointe = new PieceJointe();
								pieceJointe.setFileName(pjDto.getFileNameSaved());
								pieceJointe.setOriginFileName(pjDto.getOriginFileName());
								idtPj = pjDaoImpl.save(pieceJointe).getId();
								PieceJointe savedPj = pjDaoImpl.get(idtPj);
								if(savedPj != null){
									pjOperation= new PjOperation();
									pjOperation.setPieceJointe(savedPj);
									pjOperation.setMes(mes);
									entite = entiteDaoImpl.get(3L);
									pjOperation.setEntite(entite);
									pjOperationDaoImpl.save(pjOperation);
								}
							}
						}
						qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(mes.getId(), 3L);
						statutOperation = statutOperationDaoImpl.get(1L);
						if(qualification == null){
							Qualification qualif = new Qualification();
							qualif.setStatutOperation(statutOperation);
							qualif.setCommentaire(null);
							qualif.setEntite(entite);
							qualif.setActionType(pourQualif);
							qualif.setMes(mes);
							qualif.setDate(new Date());
							qualificationDaoImpl.save(qualif);
						}

					}
					// Supprimer le lien vers les PJs Optim retire
					if(dto.getPjOptim() != null && dto.getPjOptim().size() > 0){
						List<Long> pjsOptimToRemove = new ArrayList<Long>();
						for(PjDto pjDto: dto.getPjOptim()){
							if(pjDto != null && pjDto.getFileNameSaved() == null && pjDto.getOriginFileName() == null){
								pjsOptimToRemove.add(pjDto.getIdt());
							}
						}
						List<PjOperation> pjsOperationOptim = pjOperationDaoImpl.getPJOperationOptimListByMesIdt(mes.getId());
						if(pjsOperationOptim != null && pjsOperationOptim.size() > 0){
							for(PjOperation pjOper: pjsOperationOptim){
								if(pjsOptimToRemove.contains(pjOper.getId())){
									pjOperationDaoImpl.delete(pjOper);
								}
							}
						}
					}

					//Ajouter les nouvelles PJs Qos
					if(dto.getPjQos() != null && dto.getPjQos().size() > 0){	
						for(PjDto pjDto : dto.getPjQos()){
							if(pjDto != null && pjDto.getOriginFileName() !=null && pjDto.getFileNameSaved() != null){
								pieceJointe = new PieceJointe();
								pieceJointe.setFileName(pjDto.getFileNameSaved());
								pieceJointe.setOriginFileName(pjDto.getOriginFileName());
								idtPj = pjDaoImpl.save(pieceJointe).getId();
								PieceJointe savedPj = pjDaoImpl.get(idtPj);
								if(savedPj != null){
									pjOperation= new PjOperation();
									pjOperation.setPieceJointe(savedPj);
									pjOperation.setMes(mes);
									entite = entiteDaoImpl.get(6L);
									pjOperation.setEntite(entite);
									pjOperationDaoImpl.save(pjOperation);
								}
							}
						}
						qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(mes.getId(), 6L);
						statutOperation = statutOperationDaoImpl.get(1L);
						if(qualification == null){
							Qualification qualif = new Qualification();
							qualif.setStatutOperation(statutOperation);
							qualif.setCommentaire(null);
							qualif.setEntite(entite);
							qualif.setActionType(pourQualif);
							qualif.setMes(mes);
							qualif.setDate(new Date());
							qualificationDaoImpl.save(qualif);
						}

					}
					// Supprimer le lien vers les PJs Qos retire
					if(dto.getPjQos() != null && dto.getPjQos().size() > 0){
						List<Long> pjsQosToRemove = new ArrayList<Long>();
						for(PjDto pjDto: dto.getPjQos()){
							if(pjDto != null && pjDto.getFileNameSaved() == null && pjDto.getOriginFileName() == null){
								pjsQosToRemove.add(pjDto.getIdt());
							}
						}
						List<PjOperation> pjsOperationQos = pjOperationDaoImpl.getPJOperationQosListByMesIdt(mes.getId());
						if(pjsOperationQos != null && pjsOperationQos.size() > 0){
							for(PjOperation pjOper: pjsOperationQos){
								if(pjsQosToRemove.contains(pjOper.getId())){
									pjOperationDaoImpl.delete(pjOper);
								}
							}
						}
					}
					//Ajouter les nouvelles PJs DCC
					if(dto.getPjDcc() != null && dto.getPjDcc().size() > 0){
						for(PjDto pjDto : dto.getPjDcc()){
							if(pjDto != null && pjDto.getOriginFileName() !=null && pjDto.getFileNameSaved() != null){
								pieceJointe = new PieceJointe();
								pieceJointe.setFileName(pjDto.getFileNameSaved());
								pieceJointe.setOriginFileName(pjDto.getOriginFileName());
								idtPj = pjDaoImpl.save(pieceJointe).getId();
								PieceJointe savedPj = pjDaoImpl.get(idtPj);
								if(savedPj != null){
									pjOperation= new PjOperation();
									pjOperation.setPieceJointe(savedPj);
									pjOperation.setMes(mes);
									entite = entiteDaoImpl.get(4L);
									pjOperation.setEntite(entite);
									pjOperationDaoImpl.save(pjOperation);
								}
							}
						}
						qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(mes.getId(), 4L);
						statutOperation = statutOperationDaoImpl.get(1L);
						if(qualification == null){
							Qualification qualif = new Qualification();
							qualif.setStatutOperation(statutOperation);
							qualif.setCommentaire(null);
							qualif.setEntite(entite);
							qualif.setActionType(pourQualif);
							qualif.setMes(mes);
							qualif.setDate(new Date());
							qualificationDaoImpl.save(qualif);
						}

					}
					// Supprimer le lien vers les PJs Dcc retire
					if(dto.getPjDcc() != null && dto.getPjDcc().size() > 0){
						List<Long> pjsDccToRemove = new ArrayList<Long>();
						for(PjDto pjDto: dto.getPjDcc()){
							if(pjDto != null && pjDto.getFileNameSaved() == null && pjDto.getOriginFileName() == null){
								pjsDccToRemove.add(pjDto.getIdt());
							}
						}
						List<PjOperation> pjsOperationDcc = pjOperationDaoImpl.getPJOperationDccListByMesIdt(mes.getId());
						if(pjsOperationDcc != null && pjsOperationDcc.size() > 0){
							for(PjOperation pjOper: pjsOperationDcc){
								if(pjsDccToRemove.contains(pjOper.getId())){
									pjOperationDaoImpl.delete(pjOper);
								}
							}
						}
					}
				}
				/**
				 * Operation DCC ENV
				 */
				if(mes.getTypeProjet() != null && mes.getTypeProjet().getId() == 2L){		
					//Ajouter les nouvelles PJs DCC-Env
					if(dto.getPjDccEnv() != null && dto.getPjDccEnv().size() > 0){
						for(PjDto pjDto : dto.getPjDccEnv()){
							if(pjDto != null && pjDto.getOriginFileName() !=null && pjDto.getFileNameSaved() != null){
								pieceJointe = new PieceJointe();
								pieceJointe.setFileName(pjDto.getFileNameSaved());
								pieceJointe.setOriginFileName(pjDto.getOriginFileName());
								idtPj = pjDaoImpl.save(pieceJointe).getId();
								PieceJointe savedPj = pjDaoImpl.get(idtPj);
								if(savedPj != null){
									pjOperation= new PjOperation();
									pjOperation.setPieceJointe(savedPj);
									pjOperation.setMes(mes);
									entite = entiteDaoImpl.get(5L);
									pjOperation.setEntite(entite);
									pjOperationDaoImpl.save(pjOperation);
								}
							}
						}
						qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(mes.getId(), 5L);
						statutOperation = statutOperationDaoImpl.get(1L);
						if(qualification == null){
							Qualification qualif = new Qualification();
							qualif.setStatutOperation(statutOperation);
							qualif.setCommentaire(null);
							qualif.setEntite(entite);
							qualif.setActionType(pourQualif);
							qualif.setMes(mes);
							qualif.setDate(new Date());
							qualificationDaoImpl.save(qualif);
						}

					}
					// Supprimer le lien vers les PJs DccEnv retire
					if(dto.getPjDccEnv() != null && dto.getPjDccEnv().size() > 0){
						List<Long> pjsDccEnvToRemove = new ArrayList<Long>();
						for(PjDto pjDto: dto.getPjDccEnv()){
							if(pjDto != null && pjDto.getFileNameSaved() == null && pjDto.getOriginFileName() == null){
								pjsDccEnvToRemove.add(pjDto.getIdt());
							}
						}
						List<PjOperation> pjsOperationDccEnv = pjOperationDaoImpl.getPJOperationDccEnvListByMesIdt(mes.getId());
						if(pjsOperationDccEnv != null && pjsOperationDccEnv.size() > 0){
							for(PjOperation pjOper: pjsOperationDccEnv){
								if(pjsDccEnvToRemove.contains(pjOper.getId())){
									pjOperationDaoImpl.delete(pjOper);
								}
							}
						}
					}

				}

			}
		}
		return mes;
	}
}
