package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;


/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class MesRemiseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6059178413830735726L;

	private Long idt;

	private Long idtFournisseur;

	private String fournisseur;

	private Long idtContrat;

	private String contrat;

	private String commande;

	private String site;   

	private String dr;

	private String rt;

	private String dateRt;

	private String pvr;

	private String datePvr;  	   

	private String reserve;

	private List<ReserveDto>  reserves;	

	private String statut;

	private String etat;

	private String commentaire;

	private String file_name_pvr;

	private String file_name_rt;

	private String dateMES;

	private String numOperation;
	
	private List<RtStatutEditionDto> listRt;	
	
	private Long statutEditionPvrIdt;
	
	private String statutEditionPvrLabel;
	
	private Long typeProjetIdt;
	
	private String typeProjetLabel;
	
	private Long projetIdt;

	private String projetLabel;
	
	private List<PieceJointeDto> pjExploitation;
	private List<PieceJointeDto> pjOptim;
	private List<PieceJointeDto> pjDcc;
	private List<PieceJointeDto> pjDccEnv;	
	private List<PieceJointeDto> pjQos;
	private List<PieceJointeDto> pjRt;
	private List<PieceJointeDto> pjPvr;
	/**
	 * 
	 */
	 public MesRemiseDto() {}
	/**
	 * @return the fournisseur
	 */
	 public String getFournisseur() {
		return fournisseur;
	}
	/**
	 * @param fournisseur the fournisseur to set
	 */
	 public void setFournisseur(String fournisseur) {
		 this.fournisseur = fournisseur;
	 }
	 /**
	  * @return the contrat
	  */
	 public String getContrat() {
		 return contrat;
	 }
	 /**
	  * @param contrat the contrat to set
	  */
	 public void setContrat(String contrat) {
		 this.contrat = contrat;
	 }
	 /**
	  * @return the rt
	  */
	 public String getRt() {
		 return rt;
	 }
	 /**
	  * @param rt the rt to set
	  */
	 public void setRt(String rt) {
		 this.rt = rt;
	 }
	 /**
	  * @return the dateRt
	  */
	 public String getDateRt() {
		 return dateRt;
	 }
	 /**
	  * @param dateRt the dateRt to set
	  */
	 public void setDateRt(String dateRt) {
		 this.dateRt = dateRt;
	 }
	 /**
	  * @return the pvr
	  */
	 public String getPvr() {
		 return pvr;
	 }
	 /**
	  * @param pvr the pvr to set
	  */
	 public void setPvr(String pvr) {
		 this.pvr = pvr;
	 }
	 /**
	  * @return the datePvr
	  */
	 public String getDatePvr() {
		 return datePvr;
	 }
	 /**
	  * @param datePvr the datePvr to set
	  */
	 public void setDatePvr(String datePvr) {
		 this.datePvr = datePvr;
	 }
	 /**
	  * @return the site
	  */
	 public String getSite() {
		 return site;
	 }
	 /**
	  * @param site the site to set
	  */
	 public void setSite(String site) {
		 this.site = site;
	 }
	 /**
	  * @return the dr
	  */
	 public String getDr() {
		 return dr;
	 }
	 /**
	  * @param dr the dr to set
	  */
	 public void setDr(String dr) {
		 this.dr = dr;
	 }
	 /**
	  * @return the reserve
	  */
	 public String getReserve() {
		 return reserve;
	 }
	 /**
	  * @param reserve the reserve to set
	  */
	 public void setReserve(String reserve) {
		 this.reserve = reserve;
	 }
	 /**
	  * @return the statut
	  */
	 public String getStatut() {
		 return statut;
	 }
	 /**
	  * @param statut the statut to set
	  */
	 public void setStatut(String statut) {
		 this.statut = statut;
	 }
	 /**
	  * @return the commentaire
	  */
	 public String getCommentaire() {
		 return commentaire;
	 }
	 /**
	  * @param commentaire the commentaire to set
	  */
	 public void setCommentaire(String commentaire) {
		 this.commentaire = commentaire;
	 }

	 /**
	  * @return the commande
	  */
	 public String getCommande() {
		 return commande;
	 }
	 /**
	  * @param commande the commande to set
	  */
	 public void setCommande(String commande) {
		 this.commande = commande;
	 }
	 /**
	  * @return the etat
	  */
	 public String getEtat() {
		 return etat;
	 }
	 /**
	  * @param etat the etat to set
	  */
	 public void setEtat(String etat) {
		 this.etat = etat;
	 }
	 /**
	  * @return the idt
	  */
	 public Long getIdt() {
		 return idt;
	 }
	 /**
	  * @param idt the idt to set
	  */
	 public void setIdt(Long idt) {
		 this.idt = idt;
	 }
	 /**
	  * @return the file_name_pvr
	  */
	 public String getFile_name_pvr() {
		 return file_name_pvr;
	 }
	 /**
	  * @param file_name_pvr the file_name_pvr to set
	  */
	 public void setFile_name_pvr(String file_name_pvr) {
		 this.file_name_pvr = file_name_pvr;
	 }
	 /**
	  * @return the file_name_rt
	  */
	 public String getFile_name_rt() {
		 return file_name_rt;
	 }
	 /**
	  * @param file_name_rt the file_name_rt to set
	  */
	 public void setFile_name_rt(String file_name_rt) {
		 this.file_name_rt = file_name_rt;
	 }
	 public List<ReserveDto> getReserves() {
		 return reserves;
	 }
	 public void setReserves(List<ReserveDto> reserves) {
		 this.reserves = reserves;
	 }
	 public String getDateMES() {
		 return dateMES;
	 }
	 public void setDateMES(String dateMES) {
		 this.dateMES = dateMES;
	 }
	 public String getNumOperation() {
		 return numOperation;
	 }
	 public void setNumOperation(String numOperation) {
		 this.numOperation = numOperation;
	 }
	

	 public Long getIdtFournisseur() {
		 return idtFournisseur;
	 }
	 public void setIdtFournisseur(Long idtFournisseur) {
		 this.idtFournisseur = idtFournisseur;
	 }
	 public Long getIdtContrat() {
		 return idtContrat;
	 }
	 public void setIdtContrat(Long idtContrat) {
		 this.idtContrat = idtContrat;
	 }
	 public List<RtStatutEditionDto> getListRt() {
		 return listRt;
	 }
	 public void setListRt(List<RtStatutEditionDto> listRt) {
		 this.listRt = listRt;
	 }
	
	public Long getStatutEditionPvrIdt() {
		return statutEditionPvrIdt;
	}
	public void setStatutEditionPvrIdt(Long statutEditionPvrIdt) {
		this.statutEditionPvrIdt = statutEditionPvrIdt;
	}
	public String getStatutEditionPvrLabel() {
		return statutEditionPvrLabel;
	}
	public void setStatutEditionPvrLabel(String statutEditionPvrLabel) {
		this.statutEditionPvrLabel = statutEditionPvrLabel;
	}
	
	public Long getTypeProjetIdt() {
		return typeProjetIdt;
	}
	public void setTypeProjetIdt(Long typeProjetIdt) {
		this.typeProjetIdt = typeProjetIdt;
	}
	public String getTypeProjetLabel() {
		return typeProjetLabel;
	}
	public void setTypeProjetLabel(String typeProjetLabel) {
		this.typeProjetLabel = typeProjetLabel;
	}
	public Long getProjetIdt() {
		return projetIdt;
	}
	public void setProjetIdt(Long projetIdt) {
		this.projetIdt = projetIdt;
	}
	public String getProjetLabel() {
		return projetLabel;
	}
	public void setProjetLabel(String projetLabel) {
		this.projetLabel = projetLabel;
	}
	public List<PieceJointeDto> getPjExploitation() {
		return pjExploitation;
	}
	public void setPjExploitation(List<PieceJointeDto> pjExploitation) {
		this.pjExploitation = pjExploitation;
	}
	public List<PieceJointeDto> getPjOptim() {
		return pjOptim;
	}
	public void setPjOptim(List<PieceJointeDto> pjOptim) {
		this.pjOptim = pjOptim;
	}
	public List<PieceJointeDto> getPjDcc() {
		return pjDcc;
	}
	public void setPjDcc(List<PieceJointeDto> pjDcc) {
		this.pjDcc = pjDcc;
	}
	public List<PieceJointeDto> getPjDccEnv() {
		return pjDccEnv;
	}
	public void setPjDccEnv(List<PieceJointeDto> pjDccEnv) {
		this.pjDccEnv = pjDccEnv;
	}
	public List<PieceJointeDto> getPjQos() {
		return pjQos;
	}
	public void setPjQos(List<PieceJointeDto> pjQos) {
		this.pjQos = pjQos;
	}
	public List<PieceJointeDto> getPjRt() {
		return pjRt;
	}
	public void setPjRt(List<PieceJointeDto> pjRt) {
		this.pjRt = pjRt;
	}
	public List<PieceJointeDto> getPjPvr() {
		return pjPvr;
	}
	public void setPjPvr(List<PieceJointeDto> pjPvr) {
		this.pjPvr = pjPvr;
	}
	
}
