package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;



/**
 * @author K.ELBAGUARI
 *
 */
public class OperationPjUpdateDto implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9029192851732427046L;
	private List<PjDto> pjExploi ;
	private List<PjDto> pjDcc ;	
	private List<PjDto> pjOptim ;
	private List<PjDto> pjDccEnv ;
	private List<PjDto> pjQos;
	
	

	public List<PjDto> getPjExploi() {
		return pjExploi;
	}

	public void setPjExploi(List<PjDto> pjExploi) {
		this.pjExploi = pjExploi;
	}

	public List<PjDto> getPjDcc() {
		return pjDcc;
	}

	public void setPjDcc(List<PjDto> pjDcc) {
		this.pjDcc = pjDcc;
	}

	public List<PjDto> getPjOptim() {
		return pjOptim;
	}

	public void setPjOptim(List<PjDto> pjOptim) {
		this.pjOptim = pjOptim;
	}

	public List<PjDto> getPjDccEnv() {
		return pjDccEnv;
	}

	public void setPjDccEnv(List<PjDto> pjDccEnv) {
		this.pjDccEnv = pjDccEnv;
	}

	public List<PjDto> getPjQos() {
		return pjQos;
	}

	public void setPjQos(List<PjDto> pjQos) {
		this.pjQos = pjQos;
	}

	
}
