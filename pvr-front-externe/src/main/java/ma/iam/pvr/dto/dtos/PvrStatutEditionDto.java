package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class PvrStatutEditionDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idt;
	private String reference;
	private String statutEditionLabel;
	private Long statutEditionIdt;
	
	public PvrStatutEditionDto() {
		
	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getStatutEditionLabel() {
		return statutEditionLabel;
	}

	public void setStatutEditionLabel(String statutEditionLabel) {
		this.statutEditionLabel = statutEditionLabel;
	}

	public Long getStatutEditionIdt() {
		return statutEditionIdt;
	}

	public void setStatutEditionIdt(Long statutEditionIdt) {
		this.statutEditionIdt = statutEditionIdt;
	}

}
