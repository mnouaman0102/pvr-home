/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * @author K.ELBAGUARI
 *
 */
public class ReserveDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3533417975629868074L;
	private Long idt;	
	private String label;
	private Long idtStatut;	
	private String labelStatut;
	private String responsabilite;
	public ReserveDto() {
	}
	
	public Long getIdt() {
		return idt;
	}
	
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public Long getIdtStatut() {
		return idtStatut;
	}
	
	public void setIdtStatut(Long idtStatut) {
		this.idtStatut = idtStatut;
	}
	
	public String getLabelStatut() {
		return labelStatut;
	}
	public void setLabelStatut(String labelStatut) {
		this.labelStatut = labelStatut;
	}

	public String getResponsabilite() {
		return responsabilite;
	}

	public void setResponsabilite(String responsabilite) {
		this.responsabilite = responsabilite;
	}
	
}
