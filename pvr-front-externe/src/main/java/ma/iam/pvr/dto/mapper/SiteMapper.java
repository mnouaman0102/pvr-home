/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.List;
import ma.iam.pvr.dao.interfaces.IDrDao;
import ma.iam.pvr.dao.interfaces.ISiteDao;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.dto.dtos.SiteDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
public class SiteMapper {
	
	@Autowired 
	private ISiteDao siteDaoImpl;
	
	@Autowired IDrDao drDaoImpl;
	
//	public Site toDomain(SiteSaveDto dto) throws MyAppException{
//		
//		Site domain = null;
//		
//		if(dto != null){
//			Dr dr = drDaoImpl.get(dto.getIdtDr());
//			if(dr != null){
//				List<Site> sites = siteDaoImpl.getAllSitesByDr(dr.getIdt());
//				List<String> labelSites = new ArrayList<String>();
//				
//				//Ajout
//				if(dto.getIdt() == null || dto.getIdt() == 0 ){
//					domain = new Site();
//					for(Site s: sites){
//						if(s.getLabel() != null && s.getLabel().equalsIgnoreCase(dto.getLabel())){
//							labelSites.add(s.getLabel());
//						}
//					}
//					
//				}else{ // Modif
//					domain = siteDaoImpl.get(dto.getIdt());
//					
//					for(Site s: sites){			
//						if(s.getLabel() != null){
//							if(s.getIdt() != domain.getIdt() && s.getLabel() != null && s.getLabel().equalsIgnoreCase(dto.getLabel()))
//								labelSites.add(s.getLabel());
//						}
//					}
//					
//				}
//				
//				if(labelSites.contains(dto.getLabel())){
//					throw new MyAppException(MyAppErrorCode.SITE_DR_ALREADY_EXIST, Constants.errorSiteDrExist);
//				}
//				
//				domain.setDr(dr);
//				domain.setCode(dto.getCode());
//				domain.setLabel(dto.getLabel());
//				siteDaoImpl.save(domain);
//				
//			}
//			
//		}
//		return domain;
//		
//	}	
	
	
	public SiteDto toDto(Site domain){
		SiteDto dto = null;
		if(domain != null){
			dto = new SiteDto();
			dto.setIdt(domain.getId());
			dto.setLabel(domain.getLabel());	
			return dto;
		}
		return null;
	}   
	
	
    public List<SiteDto> toDtos(List<Site> domains){
    	List<SiteDto> dtos = null;
    	if(domains != null && domains.size()>0){
    			dtos = new ArrayList<SiteDto>();
        	for(Site domain : domains){
        		dtos.add(toDto(domain));
        	}
    		return dtos;
    	}
		return null;		
	}
    
    
//    public List<SiteDrDto> toDtos2(List<Site> domains){
//    	List<SiteDrDto> dtos = null;
//    	if(domains != null && domains.size()>0){
//    			dtos = new ArrayList<SiteDrDto>();
//        	for(Site domain : domains){
//        		SiteDrDto dto = new SiteDrDto();
//    			dto.setIdt(domain.getIdt());
//    			dto.setLabel(domain.getLabel());
//    			dto.setCode(domain.getCode());
//    			dto.setIdtDr(domain.getDr()==null?null:domain.getDr().getIdt());
//    			dto.setLabelDr(domain.getDr()==null?null:domain.getDr().getLabel());
//        		dtos.add(dto);
//        	}
//    		return dtos;
//    	}
//		return null;		
//	}

}
