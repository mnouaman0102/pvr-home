/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ma.iam.pvr.dao.interfaces.IEtatRemiseControleDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IPjDocumentsDao;
import ma.iam.pvr.dao.interfaces.IRemiseControleDao;
import ma.iam.pvr.dao.interfaces.IStatutEditionDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.EtatRemiseControle;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjDocuments;
import ma.iam.pvr.domain.RemiseControle;
import ma.iam.pvr.domain.StatutEdition;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.dto.dtos.PieceJointeDto;
import ma.iam.pvr.dto.dtos.RemiseControleDto;
import ma.iam.pvr.dto.dtos.RemiseSaveDto;
import ma.iam.pvr.dto.dtos.SiteDto;
import ma.iam.pvr.utils.DateUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author Z.BELGHAOUTI
 *
 */
@Service
public class RemiseControleMapper {
	
	@Autowired IMesDao mesDaoImpl;
	@Autowired IPjDocumentsDao pjDocumentsDaoImpl;
	@Autowired IRemiseControleDao remiseControleDaoImpl;
	@Autowired IEtatRemiseControleDao etatRemiseControleDaoImpl;
	@Autowired IUtilisateurDao utilisateurDaoImpl;
	@Autowired IStatutEditionDao statutEditionDaoImpl;

	public RemiseControle toDomain(RemiseSaveDto dto) throws ParseException {
		if(dto != null) {
			RemiseControle domain = null;
			if(dto.getIdt() == null || dto.getIdt() == 0) {
				domain = new RemiseControle();
				domain.setDateCreationReelle(new Date());
				EtatRemiseControle etat = etatRemiseControleDaoImpl.getByCode("INSTANCE");
				domain.setEtatRemiseControle(etat);
				String login = SecurityContextHolder.getContext().getAuthentication().getName();
				Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
				if(user != null && user.getFournisseur() != null) {
					domain.setUtilisateur(user);
					domain.setFournisseur(user.getFournisseur());
				}
				StatutEdition se = statutEditionDaoImpl.get(1L);
				domain.setStatutEdition(se);
			} else {
				domain = remiseControleDaoImpl.get(dto.getIdt());
				domain.setDateModification(new Date());
				String login = SecurityContextHolder.getContext().getAuthentication().getName();
				Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
				domain.setModifierPar(user);
				if(domain.getStatutEdition() == null)  {
					StatutEdition se = statutEditionDaoImpl.get(1L);
					domain.setStatutEdition(se);
				}
			}
			
			domain.setTitre(dto.getTitre());
			domain.setSolution(dto.getSolution());
			if(dto.getDateCreation() != null && !dto.getDateCreation().equals(""))
				domain.setDateCreation(DateUtils.stringToDateTimeMinute(dto.getDateCreation()));
			
			
			return domain;
		}
		return null;
	}

	
	public RemiseControleDto toDto(RemiseControle domain){
		RemiseControleDto dto = null;
		if(domain != null){
			dto = new RemiseControleDto();
			dto.setIdt(domain.getId());
			dto.setTitre(domain.getTitre());
			
			dto.setSolution(domain.getSolution());
			dto.setDateCreation(DateUtils.dateTimeToStringMinutes(domain.getDateCreation()));
			List<Mes> mes = mesDaoImpl.getMesListByRemiseId(domain.getId());
			if(mes != null && mes.size() > 0){
				List<SiteDto> siteDtos = new ArrayList<SiteDto>();
				for (Mes m : mes) {
					SiteDto siteDto = new SiteDto();
					if(m.getSite() != null){
						siteDto.setIdt(m.getSite().getId());
						siteDto.setLabel(m.getSite().getLabel());
					}
					siteDtos.add(siteDto);
				}

				dto.setListSite(siteDtos);
				if(domain.getEtatRemiseControle() != null){
					dto.setEtatRemiseIdt(domain.getEtatRemiseControle().getId());
					dto.setEtatRemiseCode(domain.getEtatRemiseControle().getCode());
					dto.setEtatRemiseLabel(domain.getEtatRemiseControle().getLabel());	
				}
				if(domain.getStatutEdition() != null) {
					dto.setStatutEditionIdt(domain.getStatutEdition().getId());
					dto.setStatutEdition(domain.getStatutEdition().getLabel());
				}
				
				List<PjDocuments> pjs = pjDocumentsDaoImpl.getPjRemiseByRemiseIdt(domain.getId());
				List<PieceJointeDto> pjsRemise = new ArrayList<PieceJointeDto>();
				if(pjs != null && pjs.size() > 0){
					for(PjDocuments pj: pjs){
						if(pj.getPieceJointe() != null){
							pjsRemise.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				dto.setPjsRemise(pjsRemise);

				return dto;
			}
		}
		return null;
	}    
	private PieceJointeDto toPjDto(PieceJointe pj) {
		PieceJointeDto pjDto = new PieceJointeDto();
		pjDto.setIdt(pj.getId());
		pjDto.setFileName(pj.getFileName());
		pjDto.setOriginFileName(pj.getOriginFileName() == null || pj.getOriginFileName().equals("") ? pj.getFileName():pj.getOriginFileName());
		return pjDto;
	}
    public List<RemiseControleDto> toDtos(List<RemiseControle> domains){
    	List<RemiseControleDto> dtos = null;
    	if(domains != null && domains.size()>0){
    			dtos = new ArrayList<RemiseControleDto>();
        	for(RemiseControle domain : domains){
        		RemiseControleDto dto = toDto(domain);
        		if(dto != null) // ce RT n'existe pas dans la table MES_RT
        			dtos.add(dto);
        	}
    		return dtos;
    	}
		return null;		
	}
    
}
