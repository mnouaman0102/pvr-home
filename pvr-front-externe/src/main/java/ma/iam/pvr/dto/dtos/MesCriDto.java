package ma.iam.pvr.dto.dtos;

import java.util.List;

/**
 * @author Z.BELGHAOUTI
 *
 */
public class MesCriDto {

	private Long idtContrat;

	private Long idtEtat;

	private String dateDebut;

	private String dateFin;

	private List<Long> selectedMesDto;

	private Long idtDr;

	private Long idtSite;

	private Long idtProjet;

	private String sites;

	public Long getIdtContrat() {
		return idtContrat;
	}

	public void setIdtContrat(Long idtContrat) {
		this.idtContrat = idtContrat;
	}

	public Long getIdtEtat() {
		return idtEtat;
	}

	public void setIdtEtat(Long idtEtat) {
		this.idtEtat = idtEtat;
	}

	public String getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	public List<Long> getSelectedMesDto() {
		return selectedMesDto;
	}

	public void setSelectedMesDto(List<Long> selectedMesDto) {
		this.selectedMesDto = selectedMesDto;
	}

	public Long getIdtDr() {
		return idtDr;
	}

	public void setIdtDr(Long idtDr) {
		this.idtDr = idtDr;
	}

	public Long getIdtSite() {
		return idtSite;
	}

	public void setIdtSite(Long idtSite) {
		this.idtSite = idtSite;
	}

	public Long getIdtProjet() {
		return idtProjet;
	}

	public void setIdtProjet(Long idtProjet) {
		this.idtProjet = idtProjet;
	}

	public String getSites() {
		return sites;
	}

	public void setSites(String sites) {
		this.sites = sites;
	}

}
