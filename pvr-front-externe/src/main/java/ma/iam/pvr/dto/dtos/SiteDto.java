/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;



/**
 * @author K.ELBAGUARI
 *
 */
public class SiteDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8351230807091239428L;
	private Long idt;	
	private String label;
	
	
	/**
	 * 
	 */
	public SiteDto() {
	}
	/**
	 * @return the idt
	 */
	public Long getIdt() {
		return idt;
	}
	/**
	 * @param idt the idt to set
	 */
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
}
