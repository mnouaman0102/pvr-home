package ma.iam.pvr.dto.dtos;

import java.io.Serializable;


public class EmailDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3713826199151431245L;
	private Long idtMes;
	private Long idtEntite;
	private Long idtQualification;
	private Long idtFournisseur;
	private Long idtDr;
	private Long idtRt;
	private String typeEmail;
	private boolean isQualif;

	public Long getIdtMes() {
		return idtMes;
	}

	public void setIdtMes(Long idtMes) {
		this.idtMes = idtMes;
	}

	public Long getIdtEntite() {
		return idtEntite;
	}

	public void setIdtEntite(Long idtEntite) {
		this.idtEntite = idtEntite;
	}

	public String getTypeEmail() {
		return typeEmail;
	}

	public void setTypeEmail(String typeEmail) {
		this.typeEmail = typeEmail;
	}

	public Long getIdtQualification() {
		return idtQualification;
	}

	public void setIdtQualification(Long idtQualification) {
		this.idtQualification = idtQualification;
	}

	public Long getIdtFournisseur() {
		return idtFournisseur;
	}

	public void setIdtFournisseur(Long idtFournisseur) {
		this.idtFournisseur = idtFournisseur;
	}

	public Long getIdtDr() {
		return idtDr;
	}

	public void setIdtDr(Long idtDr) {
		this.idtDr = idtDr;
	}

	public boolean isQualif() {
		return isQualif;
	}

	public Long getIdtRt() {
		return idtRt;
	}

	public void setIdtRt(Long idtRt) {
		this.idtRt = idtRt;
	}

	public void setQualif(boolean isQualif) {
		this.isQualif = isQualif;
	}
	
	
}
