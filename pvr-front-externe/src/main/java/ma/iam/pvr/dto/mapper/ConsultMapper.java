/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.iam.pvr.dao.interfaces.IMesReserveDao;
import ma.iam.pvr.dao.interfaces.IMesRtDao;
import ma.iam.pvr.dao.interfaces.IPjDocumentsDao;
import ma.iam.pvr.dao.interfaces.IPjOperationDao;
import ma.iam.pvr.dao.interfaces.IPjQualificationsDao;
import ma.iam.pvr.dao.interfaces.IQualificationDao;
import ma.iam.pvr.dao.interfaces.IStatutDao;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.MesReserve;
import ma.iam.pvr.domain.MesRt;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjDocuments;
import ma.iam.pvr.domain.PjOperation;
import ma.iam.pvr.domain.PjQualifications;
import ma.iam.pvr.domain.Qualification;
import ma.iam.pvr.domain.Statut;
import ma.iam.pvr.dto.dtos.ConsultDto;
import ma.iam.pvr.dto.dtos.PieceJointeDto;
import ma.iam.pvr.dto.dtos.ReserveDto;
import ma.iam.pvr.utils.DateUtils;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
public class ConsultMapper {
	@Autowired
	private IMesRtDao mesRtDaoImpl;

	@Autowired
	private IMesReserveDao mesReserveDao;

	@Autowired
	private IStatutDao statutDao;
	
	@Autowired 
	private IPjOperationDao pjOperationDaoImpl;
	
	@Autowired 
	private IQualificationDao qualificationDaoImpl;
	
	@Autowired 
	private IPjDocumentsDao pjDocumentsDaoImpl;
	
	@Autowired 
	private IPjQualificationsDao pjQualificationsDaoImpl;

	public ConsultDto toDto(Mes domain){
		ConsultDto dto = null;
		if(domain != null){
			if(domain.getId() != null){
				dto = new ConsultDto();
				dto.setIdt(domain.getId());
				/**
				 * Contrat && Fournisseur
				 */
				if(domain.getContrat() != null){
					dto.setContrat(domain.getContrat().getLabel());
					if(domain.getContrat().getFournisseur() != null)
						dto.setFournisseur(domain.getContrat().getFournisseur().getLabel());
				}
				
				/**
				 * RT && PJ RT => Dernier RT Valide
				 */

				// RT && PJ RT => Dernier RT Radio Valide
				MesRt mesRtRadio = mesRtDaoImpl.getLastMesRtRadioEnvValidByMes(domain.getId(), "radio");			
				List<PieceJointeDto> pjRtRad = new ArrayList<PieceJointeDto>();
				if(mesRtRadio != null) {
					if(mesRtRadio.getRt() != null){	
						if(mesRtRadio.getRt().getReference() != null)
							dto.setRt(mesRtRadio.getRt().getReference());					
						if(mesRtRadio.getRt().getDate() != null)
							dto.setDateRt(DateUtils.dateToString(mesRtRadio.getRt().getDate()));
						if(mesRtRadio.getRt().getCommentaire() != null)
							dto.setCommentaireRt(mesRtRadio.getRt().getCommentaire());
						List<PjDocuments> pjRtList = pjDocumentsDaoImpl.getPjRtByRtIdt(mesRtRadio.getRt().getId());
						if(pjRtList != null && pjRtList.size() > 0){
							for(PjDocuments pj: pjRtList){
								if(pj.getPieceJointe() != null){
									pjRtRad.add(toPjDto(pj.getPieceJointe()));
								}
							}
						}
					}
				}
				// RT && PJ RT => Dernier RT Env Valide
				MesRt mesRtEnv = mesRtDaoImpl.getLastMesRtRadioEnvValidByMes(domain.getId(), "env");
				List<PieceJointeDto> pjRtEnv = new ArrayList<PieceJointeDto>();
				if(mesRtEnv != null) {
					if(mesRtEnv.getRt() != null){	
						if(mesRtEnv.getRt().getReference() != null)
							dto.setRt(mesRtEnv.getRt().getReference());					
						if(mesRtEnv.getRt().getDate() != null)
							dto.setDateRt(DateUtils.dateToString(mesRtEnv.getRt().getDate()));
						if(mesRtEnv.getRt().getCommentaire() != null)
							dto.setCommentaireRt(mesRtEnv.getRt().getCommentaire());
						List<PjDocuments> pjRtList = pjDocumentsDaoImpl.getPjRtByRtIdt(mesRtEnv.getRt().getId());
						if(pjRtList != null && pjRtList.size() > 0){
							for(PjDocuments pj: pjRtList){
								if(pj.getPieceJointe() != null){
									pjRtEnv.add(toPjDto(pj.getPieceJointe()));
								}
							}
						}
					}
				}
				/**
				 * PVR
				 */
				List<PieceJointeDto> pjPvr = new ArrayList<PieceJointeDto>();
				if(domain.getPvr() != null){
					if(domain.getPvr().getReference() != null)
						dto.setPvr(domain.getPvr().getReference());
					if(domain.getPvr().getDate() != null)
						dto.setDatePvr(DateUtils.dateToString(domain.getPvr().getDate()));
					List<PjDocuments> pjPvrList = pjDocumentsDaoImpl.getPjPvrByPvrIdt(domain.getPvr().getId());
					if(pjPvrList != null && pjPvrList.size() > 0){
						for(PjDocuments pj: pjPvrList){
							if(pj.getPieceJointe() != null){
								pjPvr.add(toPjDto(pj.getPieceJointe()));
							}
						}
					}
				}
				/**
				 * PVD
				 */
				List<PieceJointeDto> pjPvd = new ArrayList<PieceJointeDto>();
				if(domain.getPvd() != null){
					if(domain.getPvd().getReference() != null)
						dto.setPvd(domain.getPvd().getReference());
					if(domain.getPvd().getDate() != null)
						dto.setDatePvd(DateUtils.dateToString(domain.getPvd().getDate()));
					List<PjDocuments> pjPvdList = pjDocumentsDaoImpl.getPjPvdByPvdIdt(domain.getPvd().getId());
					if(pjPvdList != null && pjPvdList.size() > 0){
						for(PjDocuments pj: pjPvdList){
							if(pj.getPieceJointe() != null){
								pjPvd.add(toPjDto(pj.getPieceJointe()));
							}
						}
					}
				}
				/**
				 * Site
				 */
				if(domain.getSite() != null){
					if(domain.getSite().getCode()!=null)
						dto.setCodeSite(domain.getSite().getCode());
					if(domain.getSite().getLabel() != null)
						dto.setSite(domain.getSite().getLabel());
					if(domain.getSite().getDr() != null)
						dto.setDr(domain.getSite().getDr().getLabel());
				}else{
					dto.setSite(domain.getCommentaireOperation());
				}
				/**
				 * Etat Reception
				 */
				if(domain.getEtat() != null)
					dto.setEtat(domain.getEtat().getLabel());
				/**
				 * Projet
				 */
				if(domain.getProjet() != null)
					dto.setProjet(domain.getProjet().getLabel());

				/**
				 * Numero Operation
				 */
				dto.setNumOperation(domain.getNumOperation());
				
				/**
				 * Date Operation
				 */
				if(domain.getDate() != null)
					dto.setDate(DateUtils.dateToString(domain.getDate()));
				
				/**
				 * Date MES REELLE
				 */
				if(domain.getDateMESReelle() != null)
					dto.setDateMESReelle(DateUtils.dateToString(domain.getDateMESReelle()));
				
				if(domain.getTypeProjet() != null){
					dto.setIdtTypeProjet(domain.getTypeProjet().getId());
					dto.setTypeProjetLabel(domain.getTypeProjet().getLabel());
				}
				
				/**
				 * Reserves
				 */
				List<MesReserve> mesReserves = mesReserveDao.getReservesByMesId(domain.getId());
				List<ReserveDto> reserveDtos = new ArrayList<ReserveDto>();
				if(mesReserves != null && mesReserves.size() > 0){
					for(MesReserve mesReserve: mesReserves){
						ReserveDto reserveDto = new ReserveDto();
						if(mesReserve.getReserve() != null){
							reserveDto.setIdt(mesReserve.getReserve().getId());
							reserveDto.setLabel(mesReserve.getReserve().getLabel());
							reserveDto.setResponsabilite(mesReserve.getReserve().getResponsabilite());
							//Statut in popUp Edit
							reserveDto.setIdtStatut(mesReserve.getStatut().getId());
							reserveDto.setLabelStatut(mesReserve.getStatut().getLabel());
							reserveDtos.add(reserveDto);
						}
					}

				}
				dto.setReserves(reserveDtos);
				/**
				 * Statut in Search
				 */
				dto.setStatut(domain.getStatut());
				if(domain.getStatut() != null){
					Statut statut = statutDao.getByLabel(domain.getStatut());
					if(statut != null)
						dto.setIdtStatut(statut.getId());
				}

				/**
				 * PJ Operation Entities	
				 */
				List<PjOperation> pjOperExploitation = pjOperationDaoImpl.getPJOperationExploitationListByMesIdt(domain.getId());
				List<PjOperation> pjOperOptim = pjOperationDaoImpl.getPJOperationOptimListByMesIdt(domain.getId());
				List<PjOperation> pjOperDcc = pjOperationDaoImpl.getPJOperationDccListByMesIdt(domain.getId());
				List<PjOperation> pjOperDccEnv = pjOperationDaoImpl.getPJOperationDccEnvListByMesIdt(domain.getId());
				List<PjOperation> pjOperQos = pjOperationDaoImpl.getPJOperationQosListByMesIdt(domain.getId());
				List<PjOperation> pjOperDccSuivi = pjOperationDaoImpl.getPJOperationDccSuiviListByMesIdt(domain.getId());
				List<PjOperation> pjOperDccEnvSuivi = pjOperationDaoImpl.getPJOperationDccEnvSuiviListByMesIdt(domain.getId());			
				
				List<PieceJointeDto> pjExploitation = new ArrayList<PieceJointeDto>();
				if(pjOperExploitation != null && pjOperExploitation.size() > 0){
					for(PjOperation pj: pjOperExploitation){
						if(pj.getPieceJointe() != null){
							pjExploitation.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjOptim = new ArrayList<PieceJointeDto>();
				if(pjOperOptim != null && pjOperOptim.size() > 0){
					for(PjOperation pj: pjOperOptim){
						if(pj.getPieceJointe() != null){
							pjOptim.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				List<PieceJointeDto> pjDcc = new ArrayList<PieceJointeDto>();
				if(pjOperDcc != null && pjOperDcc.size() > 0){
					for(PjOperation pj: pjOperDcc){
						if(pj.getPieceJointe() != null){
							pjDcc.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjDccEnv = new ArrayList<PieceJointeDto>();
				if(pjOperDccEnv != null && pjOperDccEnv.size() > 0){
					for(PjOperation pj: pjOperDccEnv){
						if(pj.getPieceJointe() != null){
							pjDccEnv.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				List<PieceJointeDto> pjQos = new ArrayList<PieceJointeDto>();
				if(pjOperQos != null && pjOperQos.size() > 0){
					for(PjOperation pj: pjOperQos){
						if(pj.getPieceJointe() != null){
							pjQos.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjDccSuivi = new ArrayList<PieceJointeDto>();
				if(pjOperDccSuivi != null && pjOperDccSuivi.size() > 0){
					for(PjOperation pj: pjOperDccSuivi){
						if(pj.getPieceJointe() != null){
							pjDccSuivi.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjDccEnvSuivi = new ArrayList<PieceJointeDto>();
				if(pjOperDccEnvSuivi != null && pjOperDccEnvSuivi.size() > 0){
					for(PjOperation pj: pjOperDccEnv){
						if(pj.getPieceJointe() != null){
							pjDccEnvSuivi.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				dto.setPjExploitation(pjExploitation);
				dto.setPjOptim(pjOptim);
				dto.setPjDcc(pjDcc);
				dto.setPjDccEnv(pjDccEnv);	
				dto.setPjQos(pjQos);
				dto.setPjRt(pjRtRad);
				dto.setPjRtEnv(pjRtEnv);
				dto.setPjPvr(pjPvr);
				dto.setPjPvd(pjPvd);
				dto.setPjDccSuivi(pjDccSuivi);
				dto.setPjDccEnvSuivi(pjDccEnvSuivi);
				 
				/**
				 * PJ Qualification Entities	
				 */
				List<PjQualifications> pjQualifExploitation = null;
				
				Qualification qualificationExp = qualificationDaoImpl.getQualifByMesIdAndEntite(domain.getId(), 2L);
				if(qualificationExp != null) {
					pjQualifExploitation = pjQualificationsDaoImpl.getAllPjQualifByQualifIdt(qualificationExp.getId());
				}
				List<PjQualifications> pjQualifOptim = null;
				Qualification qualificationOptim = qualificationDaoImpl.getQualifByMesIdAndEntite(domain.getId(), 3L);
				if(qualificationOptim != null) {
					pjQualifOptim = pjQualificationsDaoImpl.getAllPjQualifByQualifIdt(qualificationOptim.getId());
				}
				List<PjQualifications> pjQualifDcc = null;
				Qualification qualificationDcc = qualificationDaoImpl.getQualifByMesIdAndEntite(domain.getId(), 4L);
				if(qualificationDcc != null) {
					pjQualifDcc = pjQualificationsDaoImpl.getAllPjQualifByQualifIdt(qualificationDcc.getId());
				}
				List<PjQualifications> pjQualifDccEnv = null;
				Qualification qualificationDccEnv = qualificationDaoImpl.getQualifByMesIdAndEntite(domain.getId(), 5L);
				if(qualificationDccEnv != null) {
					pjQualifDccEnv = pjQualificationsDaoImpl.getAllPjQualifByQualifIdt(qualificationDccEnv.getId());
				}
				List<PjQualifications> pjQualifQos = null;
				Qualification qualificationQos = qualificationDaoImpl.getQualifByMesIdAndEntite(domain.getId(), 6L);
				if(qualificationQos != null) {
					pjQualifQos = pjQualificationsDaoImpl.getAllPjQualifByQualifIdt(qualificationQos.getId());
				}
				
				List<PieceJointeDto> pjQualifExp = new ArrayList<PieceJointeDto>();
				if(pjQualifExploitation != null && pjQualifExploitation.size() > 0){
					for(PjQualifications pj: pjQualifExploitation){
						if(pj.getPieceJointe() != null){
							pjQualifExp.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjQualifOpt = new ArrayList<PieceJointeDto>();
				if(pjQualifOptim != null && pjQualifOptim.size() > 0){
					for(PjQualifications pj: pjQualifOptim){
						if(pj.getPieceJointe() != null){
							pjQualifOpt.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjQualifDccRadio = new ArrayList<PieceJointeDto>();
				if(pjQualifDcc != null && pjQualifDcc.size() > 0){
					for(PjQualifications pj: pjQualifDcc){
						if(pj.getPieceJointe() != null){
							pjQualifDccRadio.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjQualifDccEnvironnement = new ArrayList<PieceJointeDto>();
				if(pjQualifDccEnv != null && pjQualifDccEnv.size() > 0){
					for(PjQualifications pj: pjQualifDccEnv){
						if(pj.getPieceJointe() != null){
							pjQualifDccEnvironnement.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				List<PieceJointeDto> pjQualifQ = new ArrayList<PieceJointeDto>();
				if(pjQualifQos != null && pjQualifQos.size() > 0){
					for(PjQualifications pj: pjQualifQos){
						if(pj.getPieceJointe() != null){
							pjQualifQ.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
				
				dto.setPjQualifExploitation(pjQualifExp);
				dto.setPjQualifOptim(pjQualifOpt);
				dto.setPjQualifDcc(pjQualifDccRadio);
				dto.setPjQualifDccEnv(pjQualifDccEnvironnement);
				dto.setPjQualifQos(pjQualifQ);
				
				/**
				 * Qualification && Commentaire
				 */
				List<Qualification> listQualif = qualificationDaoImpl.getAllQualifsForDeploiByMesId(domain.getId());
				if(listQualif != null && listQualif.size() > 0){
					for (Qualification qualification:listQualif) {
						if(qualification != null){
							if(qualification.getStatutOperation() != null){
								switch (qualification.getEntite().getId().intValue()) {
								case 2:
									dto.setQualifExploi(qualification.getStatutOperation().getLabel());
									dto.setCommentaireExploi(qualification.getCommentaire());
									break;
								case 3:
									dto.setQualifOptim(qualification.getStatutOperation().getLabel());
									dto.setCommentaireOptim(qualification.getCommentaire());
									break;
								case 4:
									dto.setQualifDcc(qualification.getStatutOperation().getLabel());
									break;
								case 5:
									dto.setQualifDccEnv(qualification.getStatutOperation().getLabel());
									break;
								case 6:
									dto.setQualifQos(qualification.getStatutOperation().getLabel());
									dto.setCommentaireQos(qualification.getCommentaire());
									break;
								}
							}
						}
					}
				}
				/**
				 * Date MES
				 */
				if(domain.getDateMES() != null)
					dto.setDateMes(DateUtils.dateToString(domain.getDateMES()));
				
				/**
				 * Commentaire Deploiement
				 */
				dto.setCommentaire(domain.getCommentaire());
				
				/**
				 * PJ Deploiement
				 */
				PjOperation pjOperDeploi = pjOperationDaoImpl.getPJOperationDeploiByMesIdt(domain.getId());
				if(pjOperDeploi != null && pjOperDeploi.getPieceJointe() != null)
					dto.setPjDeploiement(toPjDto(pjOperDeploi.getPieceJointe()));
				
				/**
				 * Commande
				 */
				dto.setCommande(domain.getCommande());				
				
				return dto;
			}
		}
		return null;
	} 
	private PieceJointeDto toPjDto(PieceJointe pj) {
		PieceJointeDto pjDto = new PieceJointeDto();
		pjDto.setIdt(pj.getId());
		pjDto.setFileName(pj.getFileName());
		pjDto.setOriginFileName(pj.getOriginFileName() == null || pj.getOriginFileName().equals("") ? pj.getFileName():pj.getOriginFileName());
		return pjDto;
	}
	public List<ConsultDto> toDtos(List<Mes> domains){
		List<ConsultDto> dtos = null;
		if(domains != null && domains.size() > 0){
			dtos = new ArrayList<ConsultDto>();
			for(Mes domain : domains){
				dtos.add(toDto(domain));
			}
			return dtos;
		}
		return null;		
	}

}
