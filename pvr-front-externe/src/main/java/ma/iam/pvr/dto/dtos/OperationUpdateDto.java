package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;


/**
 * @author K.ELBAGUARI
 *
 */
public class OperationUpdateDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3781195247940653159L;
	private Long idt; 
	private Long idtContrat;
	private Long idtSite;
	private Long idtProjet;
	private String date;	
	
	private List<PjDto> pjExploi ;
	private List<PjDto> pjDcc ;	
	private List<PjDto> pjOptim ;
	private List<PjDto> pjDccEnv ;
	private List<PjDto> pjQos;
	
	private String comment;
	
	/**
	 * @return the idt
	 */
	public Long getIdt() {
		return idt;
	}

	/**
	 * @param idt the idt to set
	 */
	public void setIdt(Long idt) {
		this.idt = idt;
	}

	

	/**
	 * @return the idtContrat
	 */
	public Long getIdtContrat() {
		return idtContrat;
	}

	/**
	 * @param idtContrat the idtContrat to set
	 */
	public void setIdtContrat(Long idtContrat) {
		this.idtContrat = idtContrat;
	}

	

	/**
	 * @return the idtSite
	 */
	public Long getIdtSite() {
		return idtSite;
	}

	/**
	 * @param idtSite the idtSite to set
	 */
	public void setIdtSite(Long idtSite) {
		this.idtSite = idtSite;
	}

	/**
	 * @return the idtProjet
	 */
	public Long getIdtProjet() {
		return idtProjet;
	}

	/**
	 * @param idtProjet the idtProjet to set
	 */
	public void setIdtProjet(Long idtProjet) {
		this.idtProjet = idtProjet;
	}

	

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	public List<PjDto> getPjExploi() {
		return pjExploi;
	}

	public void setPjExploi(List<PjDto> pjExploi) {
		this.pjExploi = pjExploi;
	}

	public List<PjDto> getPjDcc() {
		return pjDcc;
	}

	public void setPjDcc(List<PjDto> pjDcc) {
		this.pjDcc = pjDcc;
	}

	public List<PjDto> getPjOptim() {
		return pjOptim;
	}

	public void setPjOptim(List<PjDto> pjOptim) {
		this.pjOptim = pjOptim;
	}

	public List<PjDto> getPjDccEnv() {
		return pjDccEnv;
	}

	public void setPjDccEnv(List<PjDto> pjDccEnv) {
		this.pjDccEnv = pjDccEnv;
	}

	public List<PjDto> getPjQos() {
		return pjQos;
	}

	public void setPjQos(List<PjDto> pjQos) {
		this.pjQos = pjQos;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
