package ma.iam.pvr.dto.dtos;

import java.io.Serializable;



/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class FourniCriExplDto implements Serializable{
	   
	/**
	 * 
	 */
	private static final long serialVersionUID = 8748201649093736032L;

	private Long idtContrat;
	   
	private Long idtProjet;
	   
	private Long idtDr;
	   
	private Long idtSite;
	   
	private String numOperation;
	   
	
	public FourniCriExplDto() { }


	public Long getIdtContrat() {
		return idtContrat;
	}


	public void setIdtContrat(Long idtContrat) {
		this.idtContrat = idtContrat;
	}


	public Long getIdtProjet() {
		return idtProjet;
	}


	public void setIdtProjet(Long idtProjet) {
		this.idtProjet = idtProjet;
	}


	public Long getIdtDr() {
		return idtDr;
	}


	public void setIdtDr(Long idtDr) {
		this.idtDr = idtDr;
	}


	public Long getIdtSite() {
		return idtSite;
	}


	public void setIdtSite(Long idtSite) {
		this.idtSite = idtSite;
	}


	public String getNumOperation() {
		return numOperation;
	}


	public void setNumOperation(String numOperation) {
		this.numOperation = numOperation;
	}

}
