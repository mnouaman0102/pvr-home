/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.util.List;

/**
 * @author Z.BELGHAOUTI
 *
 */
public class RemiseControleDto {

	private Long idt;
	private String titre;
	private String solution;
	private String dateCreation;
	private Long etatRemiseIdt;
	private String etatRemiseLabel;
	private String etatRemiseCode;
	private List<PieceJointeDto> pjsRemise;
	private List<SiteDto> listSite;
	private Long statutEditionIdt;
	private String statutEdition;

	public RemiseControleDto() {

	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Long getEtatRemiseIdt() {
		return etatRemiseIdt;
	}

	public void setEtatRemiseIdt(Long etatRemiseIdt) {
		this.etatRemiseIdt = etatRemiseIdt;
	}

	public String getEtatRemiseLabel() {
		return etatRemiseLabel;
	}

	public void setEtatRemiseLabel(String etatRemiseLabel) {
		this.etatRemiseLabel = etatRemiseLabel;
	}

	public String getEtatRemiseCode() {
		return etatRemiseCode;
	}

	public void setEtatRemiseCode(String etatRemiseCode) {
		this.etatRemiseCode = etatRemiseCode;
	}

	public List<PieceJointeDto> getPjsRemise() {
		return pjsRemise;
	}

	public void setPjsRemise(List<PieceJointeDto> pjsRemise) {
		this.pjsRemise = pjsRemise;
	}

	public List<SiteDto> getListSite() {
		return listSite;
	}

	public void setListSite(List<SiteDto> listSite) {
		this.listSite = listSite;
	}

	public Long getStatutEditionIdt() {
		return statutEditionIdt;
	}

	public void setStatutEditionIdt(Long statutEditionIdt) {
		this.statutEditionIdt = statutEditionIdt;
	}

	public String getStatutEdition() {
		return statutEdition;
	}

	public void setStatutEdition(String statutEdition) {
		this.statutEdition = statutEdition;
	}

}
