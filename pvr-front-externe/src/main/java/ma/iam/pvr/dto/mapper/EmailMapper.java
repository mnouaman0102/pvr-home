package ma.iam.pvr.dto.mapper;

import ma.iam.pvr.domain.Entite;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.dto.dtos.EmailDto;

import org.springframework.stereotype.Service;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Service
public class EmailMapper {
	public EmailDto toDto(Mes mes, Entite entite, String typeEmail) {
		EmailDto dto = new EmailDto();
		if(mes!=null)
			dto.setIdtMes(mes.getId());
		if(entite!=null)
			dto.setIdtEntite(entite.getId());
		dto.setTypeEmail(typeEmail);
		return dto;
	}
	public EmailDto toDto(Mes mes, Entite entite, boolean isQualif) {
		EmailDto dto = new EmailDto();
		if(mes!=null)
			dto.setIdtMes(mes.getId());
		if(entite!=null)
			dto.setIdtEntite(entite.getId());
		dto.setQualif(isQualif);
		return dto;
	}

}
