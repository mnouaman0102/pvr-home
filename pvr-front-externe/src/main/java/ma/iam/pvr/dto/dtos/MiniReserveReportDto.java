/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;


/**
 * @author K.ELBAGUARI
 *
 */
public class MiniReserveReportDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8050292579746274509L;
	private Long idt;
	private String reserve;
	
	
	
	/**
	 * 
	 */
	public MiniReserveReportDto() {
	}


	/**
	 * @return the idt
	 */
	public Long getIdt() {
		return idt;
	}

	/**
	 * @param idt the idt to set
	 */
	public void setIdt(Long idt) {
		this.idt = idt;
	}

	/**
	 * @return the reserve
	 */
	public String getReserve() {
		return reserve;
	}

	/**
	 * @param reserve the reserve to set
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

	
}
