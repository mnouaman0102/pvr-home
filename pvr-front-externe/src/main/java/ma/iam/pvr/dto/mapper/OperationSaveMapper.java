/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import ma.iam.pvr.dao.interfaces.IEntiteDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IPieceJointeDao;
import ma.iam.pvr.dao.interfaces.IPjOperationDao;
import ma.iam.pvr.domain.Entite;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjOperation;
import ma.iam.pvr.dto.dtos.OperationSaveDto;
import ma.iam.pvr.dto.dtos.PjDto;
import ma.iam.pvr.exception.MyAppException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
public class OperationSaveMapper {
	@Autowired
	private IMesDao mesDaoImpl;
	@Autowired
	private IEntiteDao entiteDaoImpl;
	@Autowired 
	private IPjOperationDao pjOperationDaoImpl;
	@Autowired 
	private IPieceJointeDao pjDaoImpl;

	/*
	 * Requalification par Fourisseur
	 */
	public Mes addPj(OperationSaveDto dto) throws MyAppException{
		Long idtPj = null;
		Mes mes = null;
		PjOperation pjOperation = null;
		PieceJointe pieceJointe = null;
		Entite entite = null;
		Long idtEntite = 1L;

		if(dto != null){
			mes = mesDaoImpl.get(dto.getIdt());
			if(mes != null){
				/**
				 * Pj Exploitation
				 */
				if(dto.getPjExploi() != null && dto.getPjExploi().size() > 0){
					idtEntite = 2L;
					for(PjDto pj : dto.getPjExploi()){
						if(pj != null){
							pieceJointe = new PieceJointe();
							pieceJointe.setFileName(pj.getFileNameSaved());
							pieceJointe.setOriginFileName(pj.getOriginFileName());
							idtPj = pjDaoImpl.save(pieceJointe).getId();
							PieceJointe savedPj = pjDaoImpl.get(idtPj);
							if(savedPj != null){
								pjOperation= new PjOperation();
								pjOperation.setPieceJointe(savedPj);
								pjOperation.setMes(mes);
								entite = entiteDaoImpl.get(idtEntite);
								pjOperation.setEntite(entite);
								pjOperationDaoImpl.save(pjOperation);
							}
						}
					}
				}

				/**
				 * Pj Optim
				 */
				if(dto.getPjOptim() != null && dto.getPjOptim().size() > 0){
					idtEntite = 3L;
					for(PjDto pj : dto.getPjOptim()){
						if(pj != null){
							pieceJointe = new PieceJointe();
							pieceJointe.setFileName(pj.getFileNameSaved());
							pieceJointe.setOriginFileName(pj.getOriginFileName());
							idtPj = pjDaoImpl.save(pieceJointe).getId();
							PieceJointe savedPj = pjDaoImpl.get(idtPj);
							if(savedPj != null){
								pjOperation= new PjOperation();
								pjOperation.setPieceJointe(savedPj);
								pjOperation.setMes(mes);
								entite = entiteDaoImpl.get(idtEntite);
								pjOperation.setEntite(entite);
								pjOperationDaoImpl.save(pjOperation);
							}
						}
					}
				}
				
				/**
				 * Pj Qos
				 */
				if(dto.getPjQos() != null && dto.getPjQos().size() > 0){
					idtEntite = 6L;
					for(PjDto pj : dto.getPjQos()){
						if(pj != null){
							pieceJointe = new PieceJointe();
							pieceJointe.setFileName(pj.getFileNameSaved());
							pieceJointe.setOriginFileName(pj.getOriginFileName());
							idtPj = pjDaoImpl.save(pieceJointe).getId();
							PieceJointe savedPj = pjDaoImpl.get(idtPj);
							if(savedPj != null){
								pjOperation= new PjOperation();
								pjOperation.setPieceJointe(savedPj);
								pjOperation.setMes(mes);
								entite = entiteDaoImpl.get(idtEntite);
								pjOperation.setEntite(entite);
								pjOperationDaoImpl.save(pjOperation);
							}
						}
					}
				}
				
				/**
				 * Pj Dcc Radio
				 */
				if(dto.getPjDcc() != null && dto.getPjDcc().size() > 0){
					idtEntite = 4L;
					for(PjDto pj : dto.getPjDcc()){
						if(pj != null){
							pieceJointe = new PieceJointe();
							pieceJointe.setFileName(pj.getFileNameSaved());
							pieceJointe.setOriginFileName(pj.getOriginFileName());
							idtPj = pjDaoImpl.save(pieceJointe).getId();
							PieceJointe savedPj = pjDaoImpl.get(idtPj);
							if(savedPj != null){
								pjOperation= new PjOperation();
								pjOperation.setPieceJointe(savedPj);
								pjOperation.setMes(mes);
								entite = entiteDaoImpl.get(idtEntite);
								pjOperation.setEntite(entite);
								pjOperationDaoImpl.save(pjOperation);
							}
						}
					}
				}
				/**
				 * Pj Dcc Env
				 */
				if(dto.getPjDccEnv() != null && dto.getPjDccEnv().size() > 0){
					idtEntite = 5L;
					for(PjDto pj : dto.getPjDccEnv()){
						if(pj != null){
							pieceJointe = new PieceJointe();
							pieceJointe.setFileName(pj.getFileNameSaved());
							pieceJointe.setOriginFileName(pj.getOriginFileName());
							idtPj = pjDaoImpl.save(pieceJointe).getId();
							PieceJointe savedPj = pjDaoImpl.get(idtPj);
							if(savedPj != null){
								pjOperation= new PjOperation();
								pjOperation.setPieceJointe(savedPj);
								pjOperation.setMes(mes);
								entite = entiteDaoImpl.get(idtEntite);
								pjOperation.setEntite(entite);
								pjOperationDaoImpl.save(pjOperation);
							}
						}
					}
				}

			}			
		}

		return mes;
	}

}
