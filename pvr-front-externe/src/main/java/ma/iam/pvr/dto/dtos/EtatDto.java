package ma.iam.pvr.dto.dtos;

import java.io.Serializable;



/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class EtatDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7989672551522472302L;

	private Long idt;
	
	private String label;

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}
	

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	

}
