package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.dao.implementation.MesPvrDaoImpl;
import ma.iam.pvr.dao.interfaces.IMesReserveDao;
import ma.iam.pvr.dao.interfaces.IMesRtDao;
import ma.iam.pvr.dao.interfaces.IPjDocumentsDao;
import ma.iam.pvr.dao.interfaces.IPjOperationDao;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.MesPvr;
import ma.iam.pvr.domain.MesReserve;
import ma.iam.pvr.domain.MesRt;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjDocuments;
import ma.iam.pvr.domain.PjOperation;
import ma.iam.pvr.dto.dtos.MesRemiseDto;
import ma.iam.pvr.dto.dtos.PieceJointeDto;
import ma.iam.pvr.dto.dtos.PvrStatutEditionDto;
import ma.iam.pvr.dto.dtos.ReserveDto;
import ma.iam.pvr.dto.dtos.RtStatutEditionDto;
import ma.iam.pvr.utils.DateUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Service
public class MesRemiseMapper {

	@Autowired
	private IMesRtDao mesRtDaoImpl;
	@Autowired
	private IMesReserveDao mesReserveDaoImpl;
	@Autowired
	private IPjOperationDao pjOperationDaoImpl;
	@Autowired
	private IPjDocumentsDao pjDocumentsDaoImpl;
	@Autowired
	private MesPvrDaoImpl mesPvrDaoImpl;
	
	public MesRemiseDto toDto(Mes domain){
		MesRemiseDto dto = null;
		if(domain != null){
			dto = new MesRemiseDto();	
			dto.setIdt(domain.getId());
			if(domain.getContrat() != null){
				dto.setContrat(domain.getContrat().getLabel());
				dto.setIdtContrat(domain.getContrat().getId());
				if(domain.getContrat().getFournisseur() != null){
					dto.setFournisseur(domain.getContrat().getFournisseur().getLabel());
					dto.setIdtFournisseur(domain.getContrat().getFournisseur().getId());
				}
			}

			List<PieceJointeDto> pjPvr = new ArrayList<PieceJointeDto>();

			if(domain.getPvr() != null){
				dto.setPvr(domain.getPvr().getReference());
				dto.setFile_name_pvr(domain.getPvr().getPieceJointe());
				if(domain.getPvr().getDate() != null)
					dto.setDatePvr(DateUtils.dateToString(domain.getPvr().getDate()));
				if(domain.getPvr().getStatutEdition() != null){
					dto.setStatutEditionPvrIdt(domain.getPvr().getStatutEdition().getId());
					dto.setStatutEditionPvrLabel(domain.getPvr().getStatutEdition().getLabel());
				}

				List<PjDocuments> pjPvrList = pjDocumentsDaoImpl.getPjPvrByPvrIdt(domain.getPvr().getId());

				if(pjPvrList != null && pjPvrList.size() > 0){
					for(PjDocuments pj: pjPvrList){
						if(pj.getPieceJointe() != null){
							pjPvr.add(toPjDto(pj.getPieceJointe()));
						}
					}
				}
			}
			List<PvrStatutEditionDto> listPvr = new ArrayList<PvrStatutEditionDto>();
			List<MesPvr> mesPvrList = mesPvrDaoImpl.getListMesPvrByMes(domain.getId());
			if(mesPvrList != null && mesPvrList.size() > 0){
				for(MesPvr mPvr: mesPvrList){
					PvrStatutEditionDto pvrStatutEditionDto = new PvrStatutEditionDto();
					if(mPvr.getPvr() != null){
						pvrStatutEditionDto.setIdt(mPvr.getPvr().getId());
						pvrStatutEditionDto.setReference(mPvr.getPvr().getReference());
						if(mPvr.getPvr().getStatutEdition() != null){
							pvrStatutEditionDto.setStatutEditionIdt(mPvr.getPvr().getStatutEdition().getId());
							pvrStatutEditionDto.setStatutEditionLabel(mPvr.getPvr().getStatutEdition().getLabel());
						}
						listPvr.add(pvrStatutEditionDto);
						
						//PJ PVR
						List<PjDocuments> pjPvrList = pjDocumentsDaoImpl.getPjPvrByPvrIdt(mPvr.getPvr().getId());
						if(pjPvrList != null && pjPvrList.size() > 0){
							for(PjDocuments pj: pjPvrList){
								if(pj.getPieceJointe() != null){
									pjPvr.add(toPjDto(pj.getPieceJointe()));
								}
							}
						}
					}
				}
			}
			if(domain.getTypeProjet() != null){
				dto.setTypeProjetIdt(domain.getTypeProjet().getId());
				dto.setTypeProjetLabel(domain.getTypeProjet().getLabel());
			}
			if(domain.getProjet() != null){
				dto.setProjetIdt(domain.getProjet().getId());
				dto.setProjetLabel(domain.getProjet().getLabel());
			}
			if(domain.getSite() != null){
				dto.setSite(domain.getSite().getLabel());
				if(domain.getSite().getDr() != null)
					dto.setDr(domain.getSite().getDr().getLabel());
			}else{
				dto.setSite(domain.getCommentaireOperation());
			}
			if(domain.getEtat() != null)
				dto.setEtat(domain.getEtat().getLabel());

			dto.setCommande(domain.getCommande());

			List<MesReserve> mesReserves = mesReserveDaoImpl.getReservesByMesId(domain.getId());
			List<ReserveDto> reserveDtos = new ArrayList<ReserveDto>();

			if(mesReserves != null && mesReserves.size() > 0){
				for(MesReserve mesReserve: mesReserves){
					ReserveDto reserveDto = new ReserveDto();
					if(mesReserve.getReserve() != null && mesReserve.getStatut() != null && mesReserve.getStatut().getId() == 1L){
						reserveDto.setIdt(mesReserve.getReserve().getId());
						reserveDto.setLabel(mesReserve.getReserve().getLabel());
						reserveDtos.add(reserveDto);
					}
				}
			}
			dto.setReserves(reserveDtos);
			dto.setStatut(domain.getStatut());		
			dto.setCommentaire(domain.getCommentaire());

			dto.setNumOperation(domain.getNumOperation());
			if(domain.getDateMES() != null)
				dto.setDateMES(DateUtils.dateToString(domain.getDateMES()));


			List<PjOperation> pjOperExploitation = pjOperationDaoImpl.getPJOperationExploitationListByMesIdt(domain.getId());
			List<PjOperation> pjOperOptim = pjOperationDaoImpl.getPJOperationOptimListByMesIdt(domain.getId());
			List<PjOperation> pjOperDcc = pjOperationDaoImpl.getPJOperationDccListByMesIdt(domain.getId());
			List<PjOperation> pjOperDccEnv = pjOperationDaoImpl.getPJOperationDccEnvListByMesIdt(domain.getId());
			List<PjOperation> pjOperQos = pjOperationDaoImpl.getPJOperationQosListByMesIdt(domain.getId());

			List<PieceJointeDto> pjRt = new ArrayList<PieceJointeDto>();
			MesRt mesRtValide = mesRtDaoImpl.getLastMesRtValidByMes(domain.getId());
			if(mesRtValide != null){
				if(mesRtValide.getRt() != null){
					List<PjDocuments> pjRtList = pjDocumentsDaoImpl.getPjRtByRtIdt(mesRtValide.getRt().getId());
					dto.setRt(mesRtValide.getRt().getReference());
					if(pjRtList != null && pjRtList.size() > 0){
						for(PjDocuments pj: pjRtList){
							if(pj.getPieceJointe() != null){
								// pjRt.add(toPjDto(pj.getPieceJointe())); // On doit Afficher toutes les pieces jointes RT
							}
						}
					}
				}
			}

			List<PieceJointeDto> pjExploitation = new ArrayList<PieceJointeDto>();
			if(pjOperExploitation != null && pjOperExploitation.size() > 0){
				for(PjOperation pj: pjOperExploitation){
					if(pj.getPieceJointe() != null){
						pjExploitation.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}

			List<PieceJointeDto> pjOptim = new ArrayList<PieceJointeDto>();
			if(pjOperOptim != null && pjOperOptim.size() > 0){
				for(PjOperation pj: pjOperOptim){
					if(pj.getPieceJointe() != null){
						pjOptim.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}
			List<PieceJointeDto> pjDcc = new ArrayList<PieceJointeDto>();
			if(pjOperDcc != null && pjOperDcc.size() > 0){
				for(PjOperation pj: pjOperDcc){
					if(pj.getPieceJointe() != null){
						pjDcc.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}

			List<PieceJointeDto> pjDccEnv = new ArrayList<PieceJointeDto>();
			if(pjOperDccEnv != null && pjOperDccEnv.size() > 0){
				for(PjOperation pj: pjOperDccEnv){
					if(pj.getPieceJointe() != null){
						pjDccEnv.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}

			List<PieceJointeDto> pjQos = new ArrayList<PieceJointeDto>();
			if(pjOperQos != null && pjOperQos.size() > 0){
				for(PjOperation pj: pjOperQos){
					if(pj.getPieceJointe() != null){
						pjQos.add(toPjDto(pj.getPieceJointe()));
					}
				}
			}


			List<MesRt> mesRtList = mesRtDaoImpl.getListMesRtByMes(domain.getId());
			List<RtStatutEditionDto> listRt = new ArrayList<RtStatutEditionDto>();
			if(mesRtList != null && mesRtList.size() > 0){
				for(MesRt mRt: mesRtList){
					RtStatutEditionDto rtStatutEditionDto = new RtStatutEditionDto();
					if(mRt.getRt() != null){
						rtStatutEditionDto.setIdt(mRt.getRt().getId());
						rtStatutEditionDto.setReference(mRt.getRt().getReference());
						if(mRt.getRt().getStatutEdition() != null){
							rtStatutEditionDto.setStatutEditionIdt(mRt.getRt().getStatutEdition().getId());
							rtStatutEditionDto.setStatutEditionLabel(mRt.getRt().getStatutEdition().getLabel());
						}
						listRt.add(rtStatutEditionDto);

						//PJ RT
						List<PjDocuments> pjRtList = pjDocumentsDaoImpl.getPjRtByRtIdt(mRt.getRt().getId());
						if(pjRtList != null && pjRtList.size() > 0){
							for(PjDocuments pj: pjRtList){
								if(pj.getPieceJointe() != null){
									pjRt.add(toPjDto(pj.getPieceJointe())); // On doit Afficher toutes les pieces jointes RT
								}
							}
						}
					}
				}
				dto.setListRt(listRt);
			}
			dto.setPjExploitation(pjExploitation);
			dto.setPjOptim(pjOptim);
			dto.setPjDcc(pjDcc);
			dto.setPjRt(pjRt);
			dto.setPjPvr(pjPvr);
			dto.setPjDccEnv(pjDccEnv);
			dto.setPjQos(pjQos);
			return dto;
		}
		return null;
	} 
	private PieceJointeDto toPjDto(PieceJointe pj) {
		PieceJointeDto pjDto = new PieceJointeDto();
		pjDto.setIdt(pj.getId());
		pjDto.setFileName(pj.getFileName());
		pjDto.setOriginFileName(pj.getOriginFileName() == null || pj.getOriginFileName().equals("") ? pj.getFileName():pj.getOriginFileName());
		return pjDto;
	}
	public List<MesRemiseDto> toDtos(List<Mes> domains){
		List<MesRemiseDto> dtos = null;
		if(domains != null && domains.size()>0){
			dtos = new ArrayList<MesRemiseDto>();
			for(Mes domain : domains){
				dtos.add(toDto(domain));
			}
			return dtos;
		}
		return null;		
	}

}
