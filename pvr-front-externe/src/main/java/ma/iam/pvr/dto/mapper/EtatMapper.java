/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.dao.interfaces.IEtatDao;
import ma.iam.pvr.domain.Etat;
import ma.iam.pvr.dto.dtos.EtatDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
public class EtatMapper {
	@Autowired 
	private IEtatDao etatDaoImpl;
	
	public Etat toDomain(EtatDto dto){
		Etat domain=null;
		if(dto != null){
			if(dto.getIdt() == null){
				domain = new Etat();
				domain.setId(dto.getIdt());
			
			}else{
				domain = etatDaoImpl.get(dto.getIdt());
			}
			
			domain.setLabel(dto.getLabel());			
			return domain;
		}
		return null;
	}	
	public EtatDto toDto(Etat domain){
		EtatDto dto = null;
		if(domain != null){
			dto = new EtatDto();
			dto.setIdt(domain.getId());
			dto.setLabel(domain.getLabel());	
			return dto;
		}
		return null;
	}    
    public List<EtatDto> toDtos(List<Etat> domains){
    	List<EtatDto> dtos = null;
    	if(domains != null && domains.size()>0){
    			dtos = new ArrayList<EtatDto>();
        	for(Etat domain : domains){
        		dtos.add(toDto(domain));
        	}
    		return dtos;
    	}
		return null;		
	}

}
