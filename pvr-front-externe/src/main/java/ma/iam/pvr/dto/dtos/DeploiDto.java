package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class DeploiDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 230916616033186942L;

	private Long idt;

	private String fournisseur;	
   
	private String contrat;
   
	private String site;   
   
	private String dr;
   
	private String etat;
   
	private String numOperation;
	   
	private String date;	 
	   
	private String projet;
	private String workflow;
	
	private String qualifOptim;	
	private String qualifExploi;	
	private String qualifDcc;
	private String qualifQos;
	
	private List<ReserveDto> reserves; 
	private Long typeProjetIdt;	
	private String typeProjetLabel;
	
	private List<PieceJointeDto> pjExploitation;   
	private List<PieceJointeDto> pjOptim;   
	private List<PieceJointeDto> pjDcc;
	private List<PieceJointeDto> pjDccEnv;
	private List<PieceJointeDto> pjQos;
	
	private List<PieceJointeDto> pjQualifDcc;
	private List<PieceJointeDto> pjQualifQos;
	private List<PieceJointeDto> pjQualifOptim;
	private List<PieceJointeDto> pjQualifExp;
	
	private String commentaireDcc;
	private String commentaireQos;
	private String commentaireOptim;
	private String commentaireExp;
	
	private Long idtEtat;
   
	public DeploiDto() {}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public String getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(String fournisseur) {
		this.fournisseur = fournisseur;
	}

	public String getContrat() {
		return contrat;
	}

	public void setContrat(String contrat) {
		this.contrat = contrat;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getDr() {
		return dr;
	}

	public void setDr(String dr) {
		this.dr = dr;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getNumOperation() {
		return numOperation;
	}

	public void setNumOperation(String numOperation) {
		this.numOperation = numOperation;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getProjet() {
		return projet;
	}

	public void setProjet(String projet) {
		this.projet = projet;
	}

	public String getWorkflow() {
		return workflow;
	}

	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}

	public String getQualifOptim() {
		return qualifOptim;
	}

	public void setQualifOptim(String qualifOptim) {
		this.qualifOptim = qualifOptim;
	}

	public String getQualifExploi() {
		return qualifExploi;
	}

	public void setQualifExploi(String qualifExploi) {
		this.qualifExploi = qualifExploi;
	}

	public String getQualifDcc() {
		return qualifDcc;
	}

	public void setQualifDcc(String qualifDcc) {
		this.qualifDcc = qualifDcc;
	}

	public String getQualifQos() {
		return qualifQos;
	}

	public void setQualifQos(String qualifQos) {
		this.qualifQos = qualifQos;
	}

	public List<ReserveDto> getReserves() {
		return reserves;
	}

	public void setReserves(List<ReserveDto> reserves) {
		this.reserves = reserves;
	}

	public Long getTypeProjetIdt() {
		return typeProjetIdt;
	}

	public void setTypeProjetIdt(Long typeProjetIdt) {
		this.typeProjetIdt = typeProjetIdt;
	}

	public String getTypeProjetLabel() {
		return typeProjetLabel;
	}

	public void setTypeProjetLabel(String typeProjetLabel) {
		this.typeProjetLabel = typeProjetLabel;
	}

	public List<PieceJointeDto> getPjExploitation() {
		return pjExploitation;
	}

	public void setPjExploitation(List<PieceJointeDto> pjExploitation) {
		this.pjExploitation = pjExploitation;
	}

	public List<PieceJointeDto> getPjOptim() {
		return pjOptim;
	}

	public void setPjOptim(List<PieceJointeDto> pjOptim) {
		this.pjOptim = pjOptim;
	}

	public List<PieceJointeDto> getPjDcc() {
		return pjDcc;
	}

	public void setPjDcc(List<PieceJointeDto> pjDcc) {
		this.pjDcc = pjDcc;
	}

	public List<PieceJointeDto> getPjDccEnv() {
		return pjDccEnv;
	}

	public void setPjDccEnv(List<PieceJointeDto> pjDccEnv) {
		this.pjDccEnv = pjDccEnv;
	}

	public List<PieceJointeDto> getPjQos() {
		return pjQos;
	}

	public void setPjQos(List<PieceJointeDto> pjQos) {
		this.pjQos = pjQos;
	}

	public List<PieceJointeDto> getPjQualifDcc() {
		return pjQualifDcc;
	}

	public void setPjQualifDcc(List<PieceJointeDto> pjQualifDcc) {
		this.pjQualifDcc = pjQualifDcc;
	}

	public List<PieceJointeDto> getPjQualifQos() {
		return pjQualifQos;
	}

	public void setPjQualifQos(List<PieceJointeDto> pjQualifQos) {
		this.pjQualifQos = pjQualifQos;
	}

	public List<PieceJointeDto> getPjQualifOptim() {
		return pjQualifOptim;
	}

	public void setPjQualifOptim(List<PieceJointeDto> pjQualifOptim) {
		this.pjQualifOptim = pjQualifOptim;
	}

	public List<PieceJointeDto> getPjQualifExp() {
		return pjQualifExp;
	}

	public void setPjQualifExp(List<PieceJointeDto> pjQualifExp) {
		this.pjQualifExp = pjQualifExp;
	}

	public String getCommentaireDcc() {
		return commentaireDcc;
	}

	public void setCommentaireDcc(String commentaireDcc) {
		this.commentaireDcc = commentaireDcc;
	}

	public String getCommentaireQos() {
		return commentaireQos;
	}

	public void setCommentaireQos(String commentaireQos) {
		this.commentaireQos = commentaireQos;
	}

	public String getCommentaireOptim() {
		return commentaireOptim;
	}

	public void setCommentaireOptim(String commentaireOptim) {
		this.commentaireOptim = commentaireOptim;
	}

	public String getCommentaireExp() {
		return commentaireExp;
	}

	public void setCommentaireExp(String commentaireExp) {
		this.commentaireExp = commentaireExp;
	}

	public Long getIdtEtat() {
		return idtEtat;
	}

	public void setIdtEtat(Long idtEtat) {
		this.idtEtat = idtEtat;
	}

}
