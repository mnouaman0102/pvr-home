package ma.iam.pvr.dto.dtos;

/**
 * @author Z.BELGHAOUTI
 *
 */
public class RemiseCriDto {

	private String codeEtat;
	private Long idtRemise;

	public String getCodeEtat() {
		return codeEtat;
	}

	public void setCodeEtat(String codeEtat) {
		this.codeEtat = codeEtat;
	}

	public Long getIdtRemise() {
		return idtRemise;
	}

	public void setIdtRemise(Long idtRemise) {
		this.idtRemise = idtRemise;
	}

}
