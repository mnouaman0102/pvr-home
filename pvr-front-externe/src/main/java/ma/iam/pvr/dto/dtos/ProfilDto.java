package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class ProfilDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6419597579809189348L;
	private Long idt;
	private String label;
	
	public Long getIdt() {
		return idt;
	}
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
}
