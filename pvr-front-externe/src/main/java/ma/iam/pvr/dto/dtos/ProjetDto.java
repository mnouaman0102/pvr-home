/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class ProjetDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7632081505870061843L;
	private Long idt;	
	private String label;
	private String workflow;
	/**
	 * 
	 */
	public ProjetDto() {
	}
	/**
	 * @return the idt
	 */
	public Long getIdt() {
		return idt;
	}
	/**
	 * @param idt the idt to set
	 */
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	public String getWorkflow() {
		return workflow;
	}
	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}
	
}
