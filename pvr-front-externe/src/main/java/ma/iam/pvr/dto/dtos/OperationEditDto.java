/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;

/**
 * @author K.ELBAGUARI
 *
 */
public class OperationEditDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3156316253677906230L;
	private Long idt; 
	private Long idtContrat;
	private Long idtFournisseur;
	private Long idtSite;	
	private Long idtDr;
	private Long idtProjet;
	private Long idtTypeProjet;
	
	private List<PjDto> pjExploi ;
	private List<PjDto> pjDcc ;	
	private List<PjDto> pjOptim ;
	private List<PjDto> pjDccEnv ;
	private List<PjDto> pjQos ;
	
	private String comment;
	
	public Long getIdt() {
		return idt;
	}
	public void setIdt(Long idt) {
		this.idt = idt;
	}
	public Long getIdtContrat() {
		return idtContrat;
	}
	public void setIdtContrat(Long idtContrat) {
		this.idtContrat = idtContrat;
	}
	public Long getIdtFournisseur() {
		return idtFournisseur;
	}
	public void setIdtFournisseur(Long idtFournisseur) {
		this.idtFournisseur = idtFournisseur;
	}
	public Long getIdtSite() {
		return idtSite;
	}
	public void setIdtSite(Long idtSite) {
		this.idtSite = idtSite;
	}
	public Long getIdtDr() {
		return idtDr;
	}
	public void setIdtDr(Long idtDr) {
		this.idtDr = idtDr;
	}
	public Long getIdtProjet() {
		return idtProjet;
	}
	public void setIdtProjet(Long idtProjet) {
		this.idtProjet = idtProjet;
	}

	public Long getIdtTypeProjet() {
		return idtTypeProjet;
	}
	public void setIdtTypeProjet(Long idtTypeProjet) {
		this.idtTypeProjet = idtTypeProjet;
	}
	public List<PjDto> getPjExploi() {
		return pjExploi;
	}
	public void setPjExploi(List<PjDto> pjExploi) {
		this.pjExploi = pjExploi;
	}
	public List<PjDto> getPjDcc() {
		return pjDcc;
	}
	public void setPjDcc(List<PjDto> pjDcc) {
		this.pjDcc = pjDcc;
	}
	public List<PjDto> getPjOptim() {
		return pjOptim;
	}
	public void setPjOptim(List<PjDto> pjOptim) {
		this.pjOptim = pjOptim;
	}
	public List<PjDto> getPjDccEnv() {
		return pjDccEnv;
	}
	public void setPjDccEnv(List<PjDto> pjDccEnv) {
		this.pjDccEnv = pjDccEnv;
	}
	public List<PjDto> getPjQos() {
		return pjQos;
	}
	public void setPjQos(List<PjDto> pjQos) {
		this.pjQos = pjQos;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
