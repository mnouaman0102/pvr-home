/**
 * 
 */
package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class PjDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4623803956370393414L;

	private Long idt;
	
	private String fileNameSaved;

	private String originFileName;

	public PjDto() {
	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public String getFileNameSaved() {
		return fileNameSaved;
	}

	public void setFileNameSaved(String fileNameSaved) {
		this.fileNameSaved = fileNameSaved;
	}

	public String getOriginFileName() {
		return originFileName;
	}

	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}

}
