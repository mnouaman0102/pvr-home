package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class RemiseSaveDto implements Serializable {

	private static final long serialVersionUID = -5207242822725449361L;

	private Long idt;
	private String titre;
	private String solution;
	private String dateCreation;
	private List<Long> listMesToUpdate;
	private List<PjDto> pjRemise;
	// For Edit
	private List<MesRemiseDto> mesList;

	public RemiseSaveDto() {

	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<Long> getListMesToUpdate() {
		return listMesToUpdate;
	}

	public void setListMesToUpdate(List<Long> listMesToUpdate) {
		this.listMesToUpdate = listMesToUpdate;
	}

	public List<PjDto> getPjRemise() {
		return pjRemise;
	}

	public void setPjRemise(List<PjDto> pjRemise) {
		this.pjRemise = pjRemise;
	}

	public List<MesRemiseDto> getMesList() {
		return mesList;
	}

	public void setMesList(List<MesRemiseDto> mesList) {
		this.mesList = mesList;
	}

}
