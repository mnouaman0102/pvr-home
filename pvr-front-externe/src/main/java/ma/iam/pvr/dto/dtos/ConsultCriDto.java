package ma.iam.pvr.dto.dtos;

import java.io.Serializable;
import java.util.List;


/**
 * @author K.ELBAGUARI
 *
 */
public class ConsultCriDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3629506711901900440L;

	private List<Long> idtsProjet;

	private List<Long> idtsContrat;

	private List<Long> idtsEtat;

	private List<Long> idtsDr;

	private List<Long> idtsSite;

	/**
	 * 
	 */
	public ConsultCriDto() {
	}
	
	public List<Long> getIdtsProjet() {
		return idtsProjet;
	}

	public void setIdtsProjet(List<Long> idtsProjet) {
		this.idtsProjet = idtsProjet;
	}

	public List<Long> getIdtsContrat() {
		return idtsContrat;
	}

	public void setIdtsContrat(List<Long> idtsContrat) {
		this.idtsContrat = idtsContrat;
	}

	public List<Long> getIdtsEtat() {
		return idtsEtat;
	}

	public void setIdtsEtat(List<Long> idtsEtat) {
		this.idtsEtat = idtsEtat;
	}

	public List<Long> getIdtsDr() {
		return idtsDr;
	}

	public void setIdtsDr(List<Long> idtsDr) {
		this.idtsDr = idtsDr;
	}

	public List<Long> getIdtsSite() {
		return idtsSite;
	}

	public void setIdtsSite(List<Long> idtsSite) {
		this.idtsSite = idtsSite;
	}
	
}
