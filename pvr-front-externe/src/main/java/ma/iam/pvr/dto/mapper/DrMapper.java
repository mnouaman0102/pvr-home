/**
 * 
 */
package ma.iam.pvr.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.domain.Dr;
import ma.iam.pvr.dto.dtos.DrDto;

import org.springframework.stereotype.Service;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
public class DrMapper {
	public DrDto toDto(Dr domain){
		DrDto dto = null;
		if(domain != null){
			dto = new DrDto();
			dto.setIdt(domain.getId());
			dto.setCode(domain.getCode());
			dto.setLabel(domain.getLabel());	
			return dto;
		}
		return null;
	}    
    public List<DrDto> toDtos(List<Dr> domains){
    	List<DrDto> dtos = null;
    	if(domains != null && domains.size()>0){
    			dtos = new ArrayList<DrDto>();
        	for(Dr domain : domains){
        		dtos.add(toDto(domain));
        	}
    		return dtos;
    	}
		return null;		
	}

}
