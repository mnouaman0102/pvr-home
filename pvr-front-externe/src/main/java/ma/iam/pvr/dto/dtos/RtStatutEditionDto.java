package ma.iam.pvr.dto.dtos;

import java.io.Serializable;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public class RtStatutEditionDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idt;
	private String reference;
	private String statutEditionLabel;
	private Long statutEditionIdt;
	private String workFlowRt;
	
	public RtStatutEditionDto() {
		
	}

	public Long getIdt() {
		return idt;
	}

	public void setIdt(Long idt) {
		this.idt = idt;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getStatutEditionLabel() {
		return statutEditionLabel;
	}

	public void setStatutEditionLabel(String statutEditionLabel) {
		this.statutEditionLabel = statutEditionLabel;
	}

	public Long getStatutEditionIdt() {
		return statutEditionIdt;
	}

	public void setStatutEditionIdt(Long statutEditionIdt) {
		this.statutEditionIdt = statutEditionIdt;
	}

	public String getWorkFlowRt() {
		return workFlowRt;
	}

	public void setWorkFlowRt(String workFlowRt) {
		this.workFlowRt = workFlowRt;
	}
	
}
