package ma.iam.pvr.services.implementation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.mysql.cj.util.StringUtils;
import ma.iam.pvr.codeError.MyAppErrorCode;
import ma.iam.pvr.dao.interfaces.IContratDao;
import ma.iam.pvr.dao.interfaces.IDrDao;
import ma.iam.pvr.dao.interfaces.IEmailDao;
import ma.iam.pvr.dao.interfaces.IEntiteDao;
import ma.iam.pvr.dao.interfaces.IEtatRemiseControleDao;
import ma.iam.pvr.dao.interfaces.IFournisseurDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IPieceJointeDao;
import ma.iam.pvr.dao.interfaces.IPjDocumentsDao;
import ma.iam.pvr.dao.interfaces.IProjetDao;
import ma.iam.pvr.dao.interfaces.IRemiseControleDao;
import ma.iam.pvr.dao.interfaces.IRemiseMesDao;
import ma.iam.pvr.dao.interfaces.ISiteDao;
import ma.iam.pvr.dao.interfaces.IStatutEditionDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Contrat;
import ma.iam.pvr.domain.Dr;
import ma.iam.pvr.domain.Email;
import ma.iam.pvr.domain.Entite;
import ma.iam.pvr.domain.EtatRemiseControle;
import ma.iam.pvr.domain.Fournisseur;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjDocuments;
import ma.iam.pvr.domain.Projet;
import ma.iam.pvr.domain.RemiseControle;
import ma.iam.pvr.domain.RemiseMes;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.domain.StatutEdition;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.dto.dtos.CodeLabelDto;
import ma.iam.pvr.dto.dtos.DrDto;
import ma.iam.pvr.dto.dtos.MesCriDto;
import ma.iam.pvr.dto.dtos.MesRemiseDto;
import ma.iam.pvr.dto.dtos.PjDto;
import ma.iam.pvr.dto.dtos.ProjetDto;
import ma.iam.pvr.dto.dtos.RemiseControleDto;
import ma.iam.pvr.dto.dtos.RemiseCriDto;
import ma.iam.pvr.dto.dtos.RemiseFrmDto;
import ma.iam.pvr.dto.dtos.RemiseSaveDto;
import ma.iam.pvr.dto.dtos.SiteDto;
import ma.iam.pvr.dto.mapper.EmailMapper;
import ma.iam.pvr.dto.mapper.MesRemiseMapper;
import ma.iam.pvr.dto.mapper.RemiseControleMapper;
import ma.iam.pvr.exception.MyAppException;
import ma.iam.pvr.services.interfaces.IActionService;
import ma.iam.pvr.services.interfaces.IRemiseControleService;
import ma.iam.pvr.utils.ActionCode;
import ma.iam.pvr.utils.Constants;
import ma.iam.pvr.utils.DateUtils;
import ma.iam.pvr.utils.Utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Service
@Path("/services/remise-controle")
@Produces("application/json")
@Transactional(rollbackFor = Exception.class)
public class RemiseControleServiceImpl implements IRemiseControleService {

	private final Logger logger = LoggerFactory.getLogger(RemiseControleServiceImpl.class);

	@Autowired private IContratDao contratDaoImpl;
	@Autowired private ISiteDao siteDaoImpl;
	@Autowired private IMesDao mesDaoImpl;
	@Autowired private IFournisseurDao fournisseurDaoImpl;
	@Autowired private MesRemiseMapper mesRemiseMapper;
	@Autowired private RemiseControleMapper remiseControleMapper;
	@Autowired private IProjetDao projetDaoImpl;
	@Autowired private IDrDao drDaoImpl;
	@Autowired private IUtilisateurDao utilisateurDaoImpl;
	@Autowired private IRemiseControleDao remiseControleDaoImpl;
	@Autowired private IEtatRemiseControleDao etatRemiseControleDaoImpl;
	@Autowired private IPieceJointeDao pieceJointeDaoImpl;
	@Autowired private IRemiseMesDao remiseMesDaoImpl;
	@Autowired private IPjDocumentsDao pjDocumentsDaoImpl;
	@Autowired private IActionService actionServiceImpl;
	@Autowired private EmailMapper emailMapper;
	@Autowired private IEntiteDao entiteDaoImpl;
	@Autowired private IEmailDao emailDaoImpl;
	
	@Autowired private IStatutEditionDao statutEditionDaoImpl;
	
	private String fileUploadDir;
	
	private String projectInterne;

	public void setFileUploadDir(String fileUploadDir) {
		this.fileUploadDir = fileUploadDir;
	}
	
	public void setProjectInterne(String projectInterne) {
		this.projectInterne = projectInterne;
	}


	/**
	 * Upload File
	 */
	@POST
	@Path("/uploadFile")
	@Consumes("multipart/form-data")
	public List<PjDto> uploadFile(MultipartFormDataInput input) {

		final String FILE_EXP = "REMISE";
		final String EXT_EXP = "extRemise";
		final String FILE_RT_ORIGINE = "originRemise";
		
		final String FILE_EXP_LOG = "REMISE_LOG";
		final String EXT_EXP_LOG = "extRemiseLog";
		final String FILE_RT_ORIGINE_LOG = "originRemiseLog";

		Map<String, List<InputPart>> formParts = input.getFormDataMap();
		List<PjDto> pjRemise = new ArrayList<PjDto>();

		String fileNameSaved = "";
		String originFileName = "";
		
		String fileNameSavedLog = "";
		String originFileNameLog = "";

		if(formParts.size() > 0){
			String fileToGet = "";
			String fileToGetOrigin = "";
			String extToGet = "";
			String fileName = "";
			String extention = "";
			
			String fileToGetLog = "";
			String fileToGetOriginLog = "";
			String extToGetLog = "";
			String fileNameLog = "";
			String extentionLog = "";

			int j = 0 ;
			int x = 0 ;
			boolean isNoMoreFiles = false;
			boolean isNoMoreFilesLog = false;
			String fileNameToSave = "";
			String fileNameToSaveLog = "";
			while(!isNoMoreFiles){
				j++;
				fileNameToSave = FILE_EXP;
				fileToGet = FILE_EXP + j;
				extToGet  = EXT_EXP + j;
				fileToGetOrigin = FILE_RT_ORIGINE + j;

				if(formParts.get(fileToGet) != null && formParts.get(extToGet) != null){							
					List<InputPart> inPart = formParts.get(fileToGet);
					List<InputPart> inPart2 = formParts.get(extToGet);
					List<InputPart> inPart3 = formParts.get(fileToGetOrigin);

					for(InputPart inputPart : inPart2){
						try {
							InputStream istream = inputPart.getBody(InputStream.class,null);
							extention = IOUtils.toString(istream);	
							break;
						} catch (IOException e) {

						}
					}
					for(InputPart inputPart : inPart3){
						try {
							InputStream istream = inputPart.getBody(InputStream.class,null);
							originFileName = IOUtils.toString(istream);	
							break;
						} catch (IOException e) {

						}
					}
					PjDto pjDto = new PjDto();
					for (InputPart inputPart : inPart) {

						try {
							MultivaluedMap<String, String> headers = inputPart.getHeaders();
							fileName = parseFileName(headers);
							InputStream istream = inputPart.getBody(InputStream.class,null);
							fileNameSaved = fileNameToSave + "-" + String.valueOf(new Date().getTime()) + "." + extention;
							fileName = fileUploadDir + fileNameSaved;
							byte [] bytes = IOUtils.toByteArray(istream);
							writeFile(bytes,fileName);
							if(isFileExistInFS(fileName)){
								pjDto.setFileNameSaved(fileNameSaved);
								pjDto.setOriginFileName(originFileName);
								pjRemise.add(pjDto);
							}


						} catch (IOException e) {
							e.printStackTrace();
						}

					}
				}else{
					isNoMoreFiles = true;
				}
				while(!isNoMoreFilesLog){
					x++;
					fileNameToSaveLog = FILE_EXP_LOG;
					fileToGetLog = FILE_EXP_LOG + x;
					extToGetLog  = EXT_EXP_LOG + x;
					fileToGetOriginLog = FILE_RT_ORIGINE_LOG + x;

					if(formParts.get(fileToGetLog) != null && formParts.get(extToGetLog) != null){							
						List<InputPart> inPart = formParts.get(fileToGetLog);
						List<InputPart> inPart2 = formParts.get(extToGetLog);
						List<InputPart> inPart3 = formParts.get(fileToGetOriginLog);

						for(InputPart inputPart : inPart2){
							try {
								InputStream istream = inputPart.getBody(InputStream.class,null);
								extentionLog = IOUtils.toString(istream);	
								break;
							} catch (IOException e) {

							}
						}
						for(InputPart inputPart : inPart3){
							try {
								InputStream istream = inputPart.getBody(InputStream.class,null);
								originFileNameLog = IOUtils.toString(istream);	
								break;
							} catch (IOException e) {

							}
						}
						PjDto pjDto = new PjDto();
						for (InputPart inputPart : inPart) {

							try {
								MultivaluedMap<String, String> headers = inputPart.getHeaders();
								fileNameLog = parseFileName(headers);
								InputStream istream = inputPart.getBody(InputStream.class,null);
								fileNameSavedLog = fileNameToSaveLog + "-" + String.valueOf(new Date().getTime()) + "." + extentionLog;
								fileNameLog = fileUploadDir + fileNameSavedLog;
								byte [] bytes = IOUtils.toByteArray(istream);
								writeFile(bytes,fileNameLog);
								if(isFileExistInFS(fileNameLog)){
									pjDto.setFileNameSaved(fileNameSavedLog);
									pjDto.setOriginFileName(originFileNameLog);
									pjRemise.add(pjDto);
								}


							} catch (IOException e) {
								e.printStackTrace();
							}

						}
					}else{
						isNoMoreFilesLog = true;
					}
				}
				if(isNoMoreFiles && isNoMoreFilesLog){
					isNoMoreFiles = false;
					isNoMoreFilesLog = false;
					break; // go for next i
				}
			}

		}
		return pjRemise;
	}

	private String parseFileName(MultivaluedMap<String, String> headers) {
		String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");
		for (String name : contentDispositionHeader) {
			if ((name.trim().startsWith("filename"))) {
				String[] tmp = name.split("=");
				String fileName = tmp[1].trim().replaceAll("\"","");
				return fileName;
			}
		}
		return "randomName";
	}

	private void writeFile(byte[] content, String filename) throws IOException {

		File file;
		FileOutputStream fop=null;;
		try {
			file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}
			fop = new FileOutputStream(file);
			fop.write(content);
			fop.flush();
			fop.close();
		} catch (Exception e) {

		}
		finally{
			closeFile(fop);
		}
	}

	private void closeFile(FileOutputStream fop) throws IOException{
		fop.close();
	}

	//To make sure that File exists in the Directory of Server
	private boolean isFileExistInFS(String filename){
		File file = new File(filename);
		if (file.exists()) {
			return true;
		}
		return false;
	}

	/**
	 * GET sites Id by their labels
	 */
	private List<Long> getIdSites(String siteLabels){

		if(siteLabels != null){
			String[] sitesLabels = siteLabels.split(";");
			List<String> listLabels = new ArrayList<String>();
			for(String lbl : sitesLabels){
				listLabels.add(lbl.trim());
			}
			List<Long> listIds = siteDaoImpl.getSitesByLabels(listLabels);
			logger.info(Utils.getLogParam() + "Recuperation Liste des sites à partir de leurs libelles");
			return listIds;
		}
		return null;
	}
	
	@POST
	@Path("/getMesByCriteresMultiSites")
	@Produces("application/json")
	public List<MesRemiseDto> getMesByCriteresMultiSites(MesCriDto dto, @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize){

		List<Long> siteIds = getIdSites(dto.getSites());
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user != null && user.getFournisseur() != null) {
			List<Object> listMesIdt = mesDaoImpl.getListMesIdtMultiSitesForRemise(user.getFournisseur().getId(), 
					dto.getIdtContrat(), dto.getIdtProjet(), dto.getDateDebut(), dto.getDateFin(),
					dto.getSelectedMesDto(), dto.getIdtDr(), siteIds);
			Fournisseur fournisseur = fournisseurDaoImpl.get(user.getFournisseur().getId());
			Contrat contrat = contratDaoImpl.get(dto.getIdtContrat());
			Dr dr = drDaoImpl.get(dto.getIdtDr());
			Projet projet = projetDaoImpl.get(dto.getIdtProjet());
			String fournisseurLabel = "";
			if(fournisseur != null){
				fournisseurLabel = fournisseur.getLabel();
			}
			String contratLabel = "";
			if(contrat != null){
				contratLabel = contrat.getLabel();
			}
			String drLabel = "";
			if(dr != null){
				drLabel = dr.getLabel();
			}
			String sitesLabel = "";
			if(dto.getSites() != null){
				sitesLabel = dto.getSites();
			}
			String projetLabel = "";
			if(projet != null){
				projetLabel = projet.getLabel();
			}
			List<Long> idsMES = new ArrayList<Long>();
			if(listMesIdt != null && listMesIdt.size() > 0){
				for(Object obj: listMesIdt){
					idsMES.add(((BigInteger)obj).longValue());
				}
				
				List<Mes> domains = mesDaoImpl.getMesByCriteresPagination(idsMES, pageNumber, pageSize);
				logger.info(Utils.getLogParam() + "Recuperation Liste MES pour Remise au controle avec les parametres suivantes => Fournisseur : " + fournisseurLabel
						+ " et Contrat : " + contratLabel 
						+ " et Dr : " + drLabel + " et Sites : " + sitesLabel
						 + " et Projet : " + projetLabel
						);
				return mesRemiseMapper.toDtos(domains);
			}
		}
		
		return null;
	}

	/**
	 * GET ids MES With Etat mes
	 */
	@POST
	@Path("/getIdMesByCriteres")
	@Produces("application/json")
	public List<Long> getIdMesByCriteres(MesCriDto dto){
		
		List<Long> siteIds = getIdSites(dto.getSites());
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user != null && user.getFournisseur() != null) {
			List<Object> listMesIdt = mesDaoImpl.getListMesIdtMultiSitesForRemise(user.getFournisseur().getId(), 
					dto.getIdtContrat(), dto.getIdtProjet(), dto.getDateDebut(), dto.getDateFin(),
					dto.getSelectedMesDto(), dto.getIdtDr(), siteIds);
			Fournisseur fournisseur = fournisseurDaoImpl.get(user.getFournisseur().getId());
			Contrat contrat = contratDaoImpl.get(dto.getIdtContrat());
			Dr dr = drDaoImpl.get(dto.getIdtDr());
			Projet projet = projetDaoImpl.get(dto.getIdtProjet());
			String fournisseurLabel = "";
			if(fournisseur != null){
				fournisseurLabel = fournisseur.getLabel();
			}
			String contratLabel = "";
			if(contrat != null){
				contratLabel = contrat.getLabel();
			}
			String drLabel = "";
			if(dr != null){
				drLabel = dr.getLabel();
			}
			String sitesLabel = "";
			if(dto.getSites() != null){
				sitesLabel = dto.getSites();
			}
			String projetLabel = "";
			if(projet != null){
				projetLabel = projet.getLabel();
			}
			List<Long> idsMES = new ArrayList<Long>();
			if(listMesIdt != null && listMesIdt.size() > 0){
				for(Object obj: listMesIdt){
					idsMES.add(((BigInteger)obj).longValue());
				}
				
				logger.info(Utils.getLogParam() + "Recuperation des id MES pour Nouveau RT RADIO avec les parametres suivantes => Fournisseur : " + fournisseurLabel
						+ " et Contrat : " + contratLabel 
						+ " et Dr : " + drLabel + " et Sites : (" + sitesLabel
						 + ") et Projet : " + projetLabel
						);
				return idsMES;
			}
		}
		
		return null;
	}
	
	/**
	 * validate Sites Filter
	 */
	@POST
	@Path("/validSitesCriteres")
	@Produces("application/json")
	public List<String> validSitesCriteres(MesCriDto dto){
		String sites = dto.getSites();
		List<String> errors = new ArrayList<String>();
		if(!StringUtils.isNullOrEmpty(sites)){
			String[] sitesLabels = dto.getSites().split(";");
			for(String lbl : sitesLabels){
				String siteLabel = lbl.trim();
				if(!StringUtils.isNullOrEmpty(siteLabel)){
					Long idSite = siteDaoImpl.getSiteIdByLabel(siteLabel);
					if(idSite == null){
						errors.add(siteLabel);
					}
				}
			}
		}
		return errors;
	}
	
	/**
	 * COUNT SEARCHED MES Multi Sites
	 */
	@POST
	@Path("/getTotalMesByCriteresMultiSites")
	@Produces("application/json")
	public int getTotalMesByCriteresMultiSites(MesCriDto dto){

		List<Long> siteIds = getIdSites(dto.getSites());
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user != null && user.getFournisseur() != null) {
			List<Object> listMesIdt = mesDaoImpl.getListMesIdtMultiSitesForRemise(user.getFournisseur().getId(), 
					dto.getIdtContrat(), dto.getIdtProjet(), dto.getDateDebut(), dto.getDateFin(),
					dto.getSelectedMesDto(), dto.getIdtDr(), siteIds);
			
			List<Long> idsMES = new ArrayList<Long>();
			if(listMesIdt != null && listMesIdt.size() > 0){
				for(Object obj: listMesIdt){
					idsMES.add(((BigInteger)obj).longValue());
				}
				int total = mesDaoImpl.getTotalMesByCriteres(idsMES);
				logger.info(Utils.getLogParam() + "Recuperation Total : " + total + " M.E.S pour RT Radio");
				return total;
			}
		}
		return 0;
	}

	/**
	 * Save Remise controle
	 * @throws ParseException 
	 */
	@POST
	@Path("/saveRemiseControle")
	public boolean saveRemiseControle(RemiseSaveDto dto) throws ParseException {	
		if(dto != null){
			RemiseControle remise = remiseControleMapper.toDomain(dto);
			if(remise != null) {
				Long idtRemise = remiseControleDaoImpl.save(remise).getId();
				saveRemiseAttachments(idtRemise, dto);
				saveRemiseMes(idtRemise, dto);
//				sendEmail(remise);
				saveRemiseTrace(idtRemise);
				return true;
			}
		}
		return false;
	}
	private void saveRemiseAttachments(Long idtRemise, RemiseSaveDto remiseDto) {
		RemiseControle remise = remiseControleDaoImpl.get(idtRemise);			
		if(remiseDto.getPjRemise() != null && remiseDto.getPjRemise().size() > 0){
			for(PjDto pj : remiseDto.getPjRemise()){
				if(pj != null){
					PieceJointe pieceJointe = new PieceJointe();
					pieceJointe.setFileName(pj.getFileNameSaved());
					pieceJointe.setOriginFileName(pj.getOriginFileName());
					Long idtPj = pieceJointeDaoImpl.save(pieceJointe).getId();
					PieceJointe savedPj = pieceJointeDaoImpl.get(idtPj);
					if(savedPj != null){
						PjDocuments pjDocuments= new PjDocuments();
						pjDocuments.setPieceJointe(savedPj);
						pjDocuments.setRemiseControle(remise);
						pjDocumentsDaoImpl.save(pjDocuments);
					}
				}
			}
		}
	}
		
	private void saveRemiseMes(Long idtRemise,RemiseSaveDto remiseDto) {
		RemiseControle remise = remiseControleDaoImpl.get(idtRemise);	
		if(remiseDto.getListMesToUpdate() != null && remiseDto.getListMesToUpdate().size() > 0){
			for(Long mesIdt: remiseDto.getListMesToUpdate()){
				if(mesIdt != null){
					Mes mes = mesDaoImpl.get(mesIdt);
					if(mes != null){
						RemiseMes remiseMes = new RemiseMes();
						remiseMes.setMes(mes);
						remiseMes.setRemiseControle(remise);
						remiseMesDaoImpl.save(remiseMes);
					}
				}
			}
		}	
	}
	
	private void sendEmail(RemiseControle remise) {
		if(remise != null) {
			Entite entite = entiteDaoImpl.get(1L);
			Email email = new Email();
			email.setContenu("");
			email.setEntite(entite);
			email.setTemplate("create-remise");
			email.setEnvoi(false);
			email.setDateCreation(new Date());
			email.setRemiseControle(remise);
			email.setFournisseur(remise.getFournisseur());
			if(projectInterne != null && !projectInterne.equals("")){
				email.setLink(projectInterne+"/index.html#/remise-controle?remise=" + remise.getId());
			}
			Utilisateur utilisateur = utilisateurDaoImpl.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
			if (utilisateur != null) {
				email.setUtilisateur(utilisateur);
			}

			emailDaoImpl.save(email);
			logger.info(Utils.getLogParam() + "Sauvegarde Email de validation a l'entité deploiement ");
		}
	}
	
	private void saveRemiseTrace(Long idtRemise) {
		RemiseControle remiseTrace = remiseControleDaoImpl.get(idtRemise);
		String titre = "";
		String solution = "";
		String pjs = "";
		String remiseMess = "";	
		int totalMes = 0;
		if(remiseTrace != null){
			titre = remiseTrace.getTitre();
			solution = remiseTrace.getSolution();
			List<PjDocuments> pjsDocuments = pjDocumentsDaoImpl.getPjRemiseByRemiseIdt(remiseTrace.getId());
			if(pjsDocuments != null && pjsDocuments.size() > 0){
				for(PjDocuments pjDocuments : pjsDocuments){
					if(pjDocuments.getPieceJointe() != null){
						pjs += pjDocuments.getPieceJointe().getOriginFileName() + ", ";
					}
				}
			}
			if(pjs.length() > 0){
				pjs = pjs.substring(0, pjs.length() -2);
			}else{
				pjs = "Pas de piece jointes";
			}
			
			List<RemiseMes> messRemise = remiseMesDaoImpl.getListRemiseMesByRemise(remiseTrace.getId());
			if(messRemise != null && messRemise.size() > 0){
				for(RemiseMes mesRemise: messRemise){
					if(mesRemise.getMes() != null && mesRemise.getMes().getSite() != null && mesRemise.getMes().getSite().getDr() != null){
						totalMes++;
						remiseMess += " Mes ayant Idt : " + mesRemise.getMes().getId() 
								+ " et Numero : " + mesRemise.getMes().getNumOperation()
								+ " et Dr : " + mesRemise.getMes().getSite().getDr().getLabel()
								+ " et Site : " + mesRemise.getMes().getSite().getLabel()
								+ ", "; 
					}
				}
			}
			if(remiseMess.length() > 0){
				remiseMess = remiseMess.substring(0, remiseMess.length() -2);
			}else{
				remiseMess = "Pas de mes";
			}
		}
		actionServiceImpl.saveAction("Création d'une nouvelle Remise au controle ayant Idt : " + remiseTrace.getId()
				+ " et Titre : " + titre
				+ ", Solution : " + solution
				+ ", Pieces Jointes : " + pjs
				+ " et l'etat Draft "
				+ " avec Total : " + totalMes + " Mes (s)"
				+ " : " + remiseMess
				, ActionCode.ADD.value());
		logger.info(Utils.getLogParam() + "Création d'une nouvelle Remise au controle ayant Idt : " + remiseTrace.getId()
				+ " et Titre : " + titre
				+ " et Solution : " + solution
				+ " et Pieces Jointes : " + pjs
				+ " et l'etat Draft "
				+ " avec Total : " + totalMes + " Mes (s)"
				+ " : " + remiseMess);
	}

	/**
	 * GET List MES Panier By List IDT
	 */
	@GET
	@Path("/getMesByIDT")
	@Produces("application/json")
	public MesRemiseDto getMesByIDT(@QueryParam("idtMes") Long idtMes){		
		Mes mes = mesDaoImpl.get(idtMes);
		if(mes != null){
			logger.info(Utils.getLogParam() + "Recuperation de la MES by IDT pour Remise controle");
			return mesRemiseMapper.toDto(mes);
		}
		return null;

	}
	
	@GET
	@Path("/getAllContratsByFounisseur")
	public List<Contrat> getAllContratsByFounisseur(){
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user != null && user.getFournisseur() != null) {
			List<Contrat> contrats = contratDaoImpl.getAllContactsByFounisseur(user.getFournisseur().getId());
			Fournisseur fournisseur = fournisseurDaoImpl.get(user.getFournisseur().getId());
			String fournisseurLabel = "";
			if(fournisseur != null){
				fournisseurLabel = fournisseur.getLabel();
			}
			if(contrats != null && contrats.size() >0){
				//actionServiceImpl.saveAction("Recuperation Liste des Contrats par Fournisseur", ActionCode.CONSULTATION.value());	
				logger.info(Utils.getLogParam() + "Recuperation Liste des Contrats par Fournisseur : " + fournisseurLabel);
				return contrats;	
			}
		}
		return null;
	}
	
	/**
	 * GET List DR
	 */
	@GET
	@Path("/getAllDr")
	public List<DrDto> getAllDr() {
		List<DrDto> dtos = new ArrayList<DrDto>();
		List<Dr> domains = drDaoImpl.findAll();
		if(domains != null && domains.size()>0) {
			for(Dr dr: domains){
				DrDto dto = new DrDto();
				dto.setIdt(dr.getId());
				dto.setCode(dr.getCode());
				dto.setLabel(dr.getLabel());
				dtos.add(dto);
			}
		}	
		if(dtos != null && dtos.size() > 0)	{
			logger.info(Utils.getLogParam() + "Recuperation Liste des Drs");
			return dtos;	
		}
		return dtos;		
	}
	
	/**
	 * GET LIST SITES BY DR
	 */
	@GET
	@Path("/getAllSitesByDr")
	public List<SiteDto> getAllSiteByDr(@QueryParam("idtDr") Long idtDr) {
		List<Site> domains = siteDaoImpl.getAllSitesByDr(idtDr);
		Dr dr = null;
		String drLabel = "";
		if (idtDr != null)
			dr = drDaoImpl.get(idtDr);
		if (dr != null)
			drLabel = dr.getLabel();
		List<SiteDto> dtos = new ArrayList<SiteDto>();
		if (domains != null && domains.size() > 0) {
			for(Site s: domains) {
				SiteDto dto = new SiteDto();
				dto.setIdt(s.getId());
				dto.setLabel(s.getLabel());
				dtos.add(dto);
			}
		}
		if (dtos != null && dtos.size() > 0) {
			logger.info(Utils.getLogParam() + "Recuperation da la liste des Sites par Dr : " + drLabel);
			return dtos;
		}
		return null;
	}
	
	@GET
	@Path("/getAllProjects")
	public List<ProjetDto> getAllProjects() {
		List<ProjetDto> dtos = new ArrayList<ProjetDto>();
		List<Projet> projets = projetDaoImpl.findAll();
		if(projets != null && projets.size() > 0){
			for(Projet p: projets){
				ProjetDto dto = new ProjetDto();
				dto.setIdt(p.getId());
				dto.setLabel(p.getLabel());
				dto.setWorkflow(p.getWorkflow());
				dtos.add(dto);
			}
		}
		logger.info(Utils.getLogParam() + "Recuperation Liste des Projets");
		return dtos;
	}
	
	@GET
	@Path("/getAllEtatRemise")
	public List<CodeLabelDto> getAllEtatRemise() {
		List<CodeLabelDto> dtos = new ArrayList<CodeLabelDto>();
		List<EtatRemiseControle> etats = etatRemiseControleDaoImpl.findAll();
		if(etats != null && etats.size() > 0){
			for(EtatRemiseControle e: etats){
				CodeLabelDto dto = new CodeLabelDto();
				dto.setIdt(e.getId());
				dto.setLabel(e.getLabel());
				dto.setCode(e.getCode());
				dtos.add(dto);
			}
		}
		logger.info(Utils.getLogParam() + "Recuperation Liste des etats remise");
		return dtos;
	}

	/**
	 * GET LIST REMISE CONTROLE
	 */
	@POST
	@Path("/getListRemiseControle")
	@Produces("application/json")
	public List<RemiseControleDto> getListRemiseControle(RemiseCriDto dto, @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) {
		List<RemiseControleDto> listRtDto = null;
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user != null && user.getFournisseur() != null) {
			String codeEtat = (dto.getCodeEtat() != null && !dto.getCodeEtat().equalsIgnoreCase("Tous")) ? dto.getCodeEtat() : "";
			List<RemiseControle> listRemise = remiseControleDaoImpl
					.getListRemiseControle(codeEtat, user.getFournisseur().getId(), dto.getIdtRemise(), pageNumber, pageSize);
			if (listRemise != null && listRemise.size() > 0) {
				listRtDto = remiseControleMapper.toDtos(listRemise);
				logger.info(Utils.getLogParam() + "Recuperation Liste Remise controle");
			}
		}
		
		return listRtDto;
	}
	
	/**
	 * COUNT REMISE CONTROLE
	 */
	@POST
	@Path("/getTotalRemiseControle")
	public Integer getTotalRemiseControle(RemiseCriDto dto) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		int total = 0;
		if(user != null && user.getFournisseur() != null) {
			String codeEtat = (dto.getCodeEtat() != null && !dto.getCodeEtat().equalsIgnoreCase("Tous")) ? dto.getCodeEtat() : "";
			total = remiseControleDaoImpl.getTotalRemiseControle(codeEtat, user.getFournisseur().getId(), dto.getIdtRemise());
			logger.info(Utils.getLogParam() + "Recuperation Total Remise controle");
		}
		return total;
	}
	
	/**
	 * DOWNLOAD PIECES JOINTES FOR REMISE
	 */
	@GET
	@Path("/getPjsRemise")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getPjsRemise(@QueryParam("fileName") String fileName){
		String path = fileUploadDir + fileName;
		File file = new File(path);
		if(file.exists()){
			String extension = FilenameUtils.getExtension(file.getName()).equals("") ? "" : "."+FilenameUtils.getExtension(file.getName());
			String originFileName = file.getName();
			PieceJointe pj = pieceJointeDaoImpl.getOriginFileName(fileName);
			if(pj != null && pj.getOriginFileName()!= null && !pj.getOriginFileName().equals("")){
				originFileName = pj.getOriginFileName() + extension;
			}
			logger.info(Utils.getLogParam() + "Telechargement Piece jointe : " + originFileName + "dans menu Remise controle");
			return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
					.header("Content-Disposition", "attachment; filename=\"" + originFileName + "\"" )
					.build();
		}		
		return null;
	}
	
	/**
	 * GET List MES By List IDT
	 */
	@POST
	@Path("/getListMesByIDTS")
	@Produces("application/json")
	public List<MesRemiseDto> getListMesByIDTS(List<Long> list_Mes){		
		List<Mes> listMes = mesDaoImpl.getListMesByIDT(list_Mes);
		if(listMes != null && listMes.size() > 0){
			logger.info(Utils.getLogParam() + "Recuperation Liste MES pour Nouvelle remise au controle");
			return mesRemiseMapper.toDtos(listMes);
		}
		return null;
	}
	
	/**
	 * GET List MES By Remise
	 */
	@GET
	@Path("/getListMesByRemise")
	@Produces("application/json")
	public List<MesRemiseDto> getListMesByRemise(@QueryParam("idtRemise") Long idtRemise){		
		List<Mes> listMes = mesDaoImpl.getMesListByRemiseId(idtRemise);
		if(listMes != null && listMes.size() > 0){
			logger.info(Utils.getLogParam() + "Recuperation Liste MES par remise controle");
			return mesRemiseMapper.toDtos(listMes);
		}
		return null;
	}
	
	/**
	 * DELETE REMISE
	 * @throws MyAppException 
	 */
	@GET
	@Path("/deleteRemise")
	@Produces("application/json")
	public boolean deleteRemise(@QueryParam("idtRemise") Long idtRemise) throws MyAppException{		
		RemiseControle remise = remiseControleDaoImpl.get(idtRemise);
		if (remise == null) {
			logger.error(Utils.getLogParam() + Constants.errorRemiseInexistante);
			throw new MyAppException(MyAppErrorCode.REMISE_INEXISTANT, Constants.errorRemiseInexistante);
		}
		
		if(remise.getEtatRemiseControle() != null 
				&& !"INSTANCE".equals(remise.getEtatRemiseControle().getCode())) {
			logger.error(Utils.getLogParam() + Constants.errorRemiseNonInstance);
			throw new MyAppException(MyAppErrorCode.REMISE_NON_INSTANCE, Constants.errorRemiseNonInstance);
		}
		
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user != null && user.getFournisseur() != null
				&& !user.getFournisseur().getId().equals(remise.getFournisseur().getId())) {
			logger.error(Utils.getLogParam() + Constants.errorDeleteRemise);
			throw new MyAppException(MyAppErrorCode.REMISE_DELETE, Constants.errorDeleteRemise);
		}
			
		List<RemiseMes> remiseMes = remiseMesDaoImpl.getListRemiseMesByRemise(remise.getId());
		if (remiseMes != null && remiseMes.size() > 0) {
			for(RemiseMes rm: remiseMes) {
				remiseMesDaoImpl.delete(rm);
			}
		}
		logger.info(Utils.getLogParam() 
				+ "Suppression des MES de la remise au controle ayant IDT: " + remise.getId());
		List<PjDocuments> pjDocuments = pjDocumentsDaoImpl.getPjRemiseByRemiseIdt(remise.getId());
		if (pjDocuments != null && pjDocuments.size() > 0) {
			for(PjDocuments pj: pjDocuments) {
				pjDocumentsDaoImpl.delete(pj);
			}
		}
		logger.info(Utils.getLogParam() 
				+ "Suppression des PJ de la remise au controle ayant IDT: " + remise.getId());
		List<Email> emails = emailDaoImpl.getListEmailByRemise(remise.getId());
		if (emails != null && emails.size() > 0) {
			for(Email e: emails) {
				emailDaoImpl.delete(e);
			}
		}
		logger.info(Utils.getLogParam() 
				+ "Suppression des Emails associes à la remise au controle ayant IDT: " + remise.getId());
		actionServiceImpl.saveAction("Suppresion de la Remise au contrôle ayant Idt : " + remise.getId()
				+ " et Titre : " + remise.getTitre()
				+ " et Solution : " + remise.getSolution()
				, ActionCode.DELETE.value());
		remiseControleDaoImpl.delete(remise);
		
		return true;
	}
	
	/**
	 * GET DRAFT REMISE BY IDT
	 */
	@GET
	@Path("/getRemiseDraftByIdt")
	public RemiseFrmDto getRemiseDraftByIdt(@QueryParam("idtRemise") Long idtRemise) throws MyAppException {
		RemiseControle remise = remiseControleDaoImpl.get(idtRemise);
		RemiseFrmDto dto = new RemiseFrmDto();
		if(remise == null){
			logger.info(Utils.getLogParam() + " Remise Inexistante");
			throw new MyAppException(MyAppErrorCode.REMISE_INEXISTANT, Constants.errorRemiseInexistante);
		}
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user != null && user.getFournisseur() != null
				&& !user.getFournisseur().getId().equals(remise.getFournisseur().getId())) {
			logger.error(Utils.getLogParam() + Constants.errorRemiseEdit);
			throw new MyAppException(MyAppErrorCode.REMISE_EDIT, Constants.errorRemiseEdit);
		}

		if(remise.getStatutEdition() != null){

			if(remise.getStatutEdition().getId() != 1L){ // Valid / Refus
				logger.info(Utils.getLogParam() + " Remise au controle n'est pas en mode draft");
				throw new MyAppException(MyAppErrorCode.REMISE_NOT_DRAFT, Constants.errorRemiseNonDraft);
			}else{

				dto.setIdt(remise.getId());
				dto.setTitre(remise.getTitre());
				dto.setSolution(remise.getSolution());
				
				if(remise.getFournisseur() != null){
					dto.setIdtFournisseur(remise.getFournisseur().getId());
					dto.setFournisseurLabel(remise.getFournisseur().getLabel());
				}
				if(remise.getDateCreation() != null)
					dto.setDateCreation(DateUtils.dateTimeToStringMinutes(remise.getDateCreation()));
				
				if(remise.getEtatRemiseControle() != null){
					dto.setIdtEtatRemise(remise.getEtatRemiseControle().getId());
					dto.setCodeEtatRemise(remise.getEtatRemiseControle().getCode());
					dto.setEtatRemise(remise.getEtatRemiseControle().getLabel());
				}
				
				if(remise.getStatutEdition() != null) {
					dto.setIdtStatutEdition(remise.getStatutEdition().getId());
					dto.setStatutEdition(remise.getStatutEdition().getLabel());
				}
				
				if(remise.getUtilisateur() != null) {
					dto.setCreerPar(remise.getUtilisateur().getLogin());
				}
				if(remise.getDateValidation() != null)
					dto.setDateValidation(DateUtils.dateTimeToStringMinutes(remise.getDateValidation()));
				
				List<MesRemiseDto> mesList = new ArrayList<MesRemiseDto>();
				List<Long> listMesToUpdate = new ArrayList<Long>();
				List<RemiseMes> listMesRemise = remiseMesDaoImpl.getListRemiseMesByRemise(remise.getId());
				if(listMesRemise != null && listMesRemise.size() > 0){
					for(RemiseMes mesRemise:listMesRemise){
						if(mesRemise.getMes()!= null){
							listMesToUpdate.add(mesRemise.getMes().getId());
							mesList.add(mesRemiseMapper.toDto(mesRemise.getMes()));
						}
					}
					logger.info(Utils.getLogParam() + "MES_REMISE sont supprimes");
				}
				dto.setListMesToUpdate(listMesToUpdate);
				dto.setMesList(mesList);

				List<PjDto> listPjDto = new ArrayList<PjDto>();
				List<PjDocuments> listPjRemises = pjDocumentsDaoImpl.getPjRemiseByRemiseIdt(remise.getId());
				if(listPjRemises != null && listPjRemises.size() > 0){
					for(PjDocuments p: listPjRemises){
						if(p.getPieceJointe() != null){
							PjDto pjDto = new PjDto();
							pjDto.setIdt(p.getId());
							pjDto.setFileNameSaved(p.getPieceJointe().getFileName());
							pjDto.setOriginFileName(p.getPieceJointe().getOriginFileName());
							listPjDto.add(pjDto);
						}
					}
				}
				dto.setPjRemise(listPjDto);
			}
		}
		
		actionServiceImpl.saveAction("Recuperation Remise au controle ayant idt : " 
		+ remise.getId() + " et titre : " + remise.getTitre() + " pour modification"
				, ActionCode.MODIF.value());
		logger.info(Utils.getLogParam() + "Recuperation Remise au controle ayant idt : " 
		+ remise.getId() + " et titre : " + remise.getTitre() + " pour modification");
		return dto;
	}
	
	/**
	 * Modifier Remise draft
	 */
	@POST
	@Path("/updateRemiseDraft")
	@Produces("application/json")
	public Long updateRemiseDraft(RemiseFrmDto remiseDto) throws ParseException, MyAppException {
		logger.info(Utils.getLogParam() + "Debut modification du Remise draft");
		String oldTitre = "";
		String oldSolution = "";
		String oldDateCreation = "";
		String newTitre = "";
		String newSolution = "";
		String newDateCreation = "";
		String oldRemiseMes = "";
		String newRemiseMes = "";
		if(remiseDto != null){
			Long idtRemiseToUpdate = remiseDto.getIdt();
			RemiseControle domain = remiseControleDaoImpl.get(idtRemiseToUpdate);
			if(domain != null){
				oldTitre = domain.getTitre();
				oldSolution = domain.getSolution();
				if(domain.getDateCreation() != null){
					oldDateCreation = DateUtils.dateTimeToStringMinutes(domain.getDateCreation());
				}
				if(domain.getStatutEdition().getId() == 1L){ // RT doit etre en mode draft
					
					domain.setDateModification(new Date());
					String login = SecurityContextHolder.getContext().getAuthentication().getName();
					Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
					domain.setModifierPar(user);
					if(domain.getStatutEdition() == null)  {
						StatutEdition se = statutEditionDaoImpl.get(1L);
						domain.setStatutEdition(se);
					}
					
					domain.setTitre(remiseDto.getTitre());
					domain.setSolution(remiseDto.getSolution());
					if(remiseDto.getDateCreation() != null && !remiseDto.getDateCreation().equals(""))
						domain.setDateCreation(DateUtils.stringToDateTimeMinute(remiseDto.getDateCreation()));
					

					// Fournisseur et Contrat a ne pas modifier

					remiseControleDaoImpl.update(domain);
					newTitre = domain.getTitre();
					newSolution = domain.getSolution();
					
					// ajouter les nouveau PJ
					if(remiseDto.getPjRemise() != null && remiseDto.getPjRemise().size() > 0){
						for(PjDto pj : remiseDto.getPjRemise()){
							if(pj != null && pj.getFileNameSaved() != null && pj.getOriginFileName() != null){				
								PieceJointe pieceJointe = new PieceJointe();
								pieceJointe.setFileName(pj.getFileNameSaved());
								pieceJointe.setOriginFileName(pj.getOriginFileName());
								Long idtPj = pieceJointeDaoImpl.save(pieceJointe).getId();
								PieceJointe savedPj = pieceJointeDaoImpl.get(idtPj);
								if(savedPj != null){
									PjDocuments pjDocuments= new PjDocuments();
									pjDocuments.setPieceJointe(savedPj);
									pjDocuments.setRemiseControle(domain);
									pjDocumentsDaoImpl.save(pjDocuments);
								}
							}
						}
					}
					// Supprimer PjRt si l'utilisateur a retirer des PJ deja existantes
					if(remiseDto.getPjRemise() != null && remiseDto.getPjRemise().size() > 0){
						List<Long> listPjRtToRemove = new ArrayList<Long>();
						for(PjDto pj : remiseDto.getPjRemise()){
							if(pj != null && pj.getFileNameSaved() == null && pj.getOriginFileName() == null){
								listPjRtToRemove.add(pj.getIdt());
							}
						}
						List<PjDocuments> listPjRts = pjDocumentsDaoImpl.getPjRemiseByRemiseIdt(domain.getId());
						if(listPjRts != null && listPjRts.size() > 0){
							for(PjDocuments p: listPjRts){
								if(listPjRtToRemove.contains(p.getId())){
									pjDocumentsDaoImpl.delete(p);
								}
							}
						}
					}


					// il s'agit d'ecraser toutes les mes en relation avec rt puis reinserer a nouveau les mes de mise a jour
					List<RemiseMes> listMesRemise = remiseMesDaoImpl.getListRemiseMesByRemise(idtRemiseToUpdate);
					if(listMesRemise != null && listMesRemise.size() > 0){
						for(RemiseMes mesRemise:listMesRemise){
							oldRemiseMes = informationMes(oldRemiseMes, mesRemise);
							remiseMesDaoImpl.delete(mesRemise);
						}
					}
					if(remiseDto.getListMesToUpdate() != null && remiseDto.getListMesToUpdate().size() > 0){
						for(Long mesIdt: remiseDto.getListMesToUpdate()){
							if(mesIdt != null){
								Mes mes = mesDaoImpl.get(mesIdt);
								if(mes != null){
									RemiseMes mesRemise = new RemiseMes();
									mesRemise.setMes(mes);
									mesRemise.setRemiseControle(domain);
									remiseMesDaoImpl.save(mesRemise);
									newRemiseMes = informationMes(newRemiseMes, mesRemise);
								}
							}
						}
					}
					
					updateRemiseDraftSaveTrace(oldTitre, oldSolution, oldDateCreation, oldRemiseMes,
							newTitre, newSolution, newDateCreation, newRemiseMes,domain);
					return idtRemiseToUpdate;
				}else{
					logger.info(Utils.getLogParam() + " Erreur update => Remise au controle n\'est pas en mode draft");
					throw new MyAppException(MyAppErrorCode.REMISE_NOT_DRAFT, Constants.errorRemiseNonDraft);
				}
			}	
		}

		return null;
	}
	
	private String informationMes(String mesDeleted, RemiseMes mesRt) {
		if(mesRt.getMes() != null &&  mesRt.getMes().getSite() != null &&  mesRt.getMes().getSite().getDr() != null && mesRt.getMes().getProjet() != null){
			mesDeleted +=   " MES ayant Idt : "+ mesRt.getMes().getId() 
					+ " et Numero : " + mesRt.getMes().getNumOperation()
					+ " et Dr : " +  mesRt.getMes().getSite().getDr().getLabel()
					+ " et Site : " +  mesRt.getMes().getSite().getLabel()
					+ " et Projet : " +  mesRt.getMes().getProjet().getLabel() 
					+ ", ";
		}else{
			mesDeleted = "Manque informations MES , ";
		}
		return mesDeleted;
	}
	
	private void updateRemiseDraftSaveTrace(String oldTitre, String oldSolution, String oldDateCreation,
			String oldRemiseMes, String newTitre, String newSolution, String newDateCreation,
			String newRemiseMes,
			RemiseControle domain) {
		if(oldRemiseMes.length() >0){
			oldRemiseMes = oldRemiseMes.substring(0, oldRemiseMes.length() -2);
		}else{
			oldRemiseMes = "Pas de MES";
		}
		if(newRemiseMes.length() >0){
			newRemiseMes = newRemiseMes.substring(0, newRemiseMes.length() -2);
		}else{
			newRemiseMes = "Pas de MES";
		}
		actionServiceImpl.saveAction("Modification Remise au controle Draft ayant IDT : " + domain.getId()  
				+ ", Titre : " + oldTitre
				+ ", Solution : " + oldSolution
				+ ", Etat: DRAFT"
				+ " et MES: " + oldRemiseMes
				+ " avec nouveaux donnees : "
				+ " Titre : " + newTitre
				+ ", Solution : " + newSolution
				+ " et MES: " + newRemiseMes
				, ActionCode.MODIF.value());
		logger.info(Utils.getLogParam() + "Modification Remise au controle Draft ayant IDT : " + domain.getId()  
				+ ", Titre : " + oldTitre
				+ ", Solution : " + oldSolution
				+ " et MES: " + oldRemiseMes
				+ " avec nouveaux donnees : "
				+ " Titre : " + newTitre
				+ ", Solution : " + newSolution
				+ " et MES: " + newRemiseMes
				);
	}
	
	/**
	 * VALIDER REMISE DRAFT
	 */
	@GET
	@Path("/validerRemiseDraft")
	@Produces("application/json")
	public boolean validerRemiseDraft(@QueryParam("idtRemise") Long idtRemise) {
		if(idtRemise != null) {
			RemiseControle remiseControle = remiseControleDaoImpl.get(idtRemise);
			if(remiseControle != null) {
				StatutEdition sd = statutEditionDaoImpl.get(2L);
				remiseControle.setStatutEdition(sd);
				remiseControleDaoImpl.save(remiseControle);
				sendEmail(remiseControle);
				actionServiceImpl.saveAction("Validation de la Remise au controle DRAFT ayant IDT : " + remiseControle.getId()  
						+ " et Titre : " + remiseControle.getTitre()
						, ActionCode.MODIF.value());
				logger.info(Utils.getLogParam() + "Validation de la Remise au controle DRAFT ayant IDT : " + remiseControle.getId()  
						+ " et Titre : " + remiseControle.getTitre());
				return true;
			}
			logger.info(Utils.getLogParam() + "Aucune Remise au controle existe avec IDT : " + idtRemise);
		}
		return false;
	}
	

}
