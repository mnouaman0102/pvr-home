package ma.iam.pvr.services.implementation;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


import javax.ws.rs.QueryParam;
import ma.iam.pvr.dao.interfaces.IContratDao;
import ma.iam.pvr.dao.interfaces.IDrDao;
import ma.iam.pvr.dao.interfaces.IEtatDao;
import ma.iam.pvr.dao.interfaces.IFournisseurDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IPieceJointeDao;
import ma.iam.pvr.dao.interfaces.IProjetDao;
import ma.iam.pvr.dao.interfaces.ISiteDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Fournisseur;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.domain.utils.DataHelper;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.dto.dtos.ConsultCriDto;
import ma.iam.pvr.dto.dtos.ConsultDto;
import ma.iam.pvr.dto.mapper.ConsultMapper;
import ma.iam.pvr.services.interfaces.IActionService;
import ma.iam.pvr.services.interfaces.IConsultationService;
import ma.iam.pvr.utils.ActionCode;
import ma.iam.pvr.utils.Constants;


import ma.iam.pvr.utils.Utils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Service
@RequestMapping("/services/consultation")
@Transactional(rollbackFor = Exception.class)
public class ConsultationServiceImpl implements IConsultationService {
	private final Logger logger = LoggerFactory.getLogger(ConsultationServiceImpl.class);

	@Autowired
	private IMesDao mesDaoImpl;
	@Autowired
	private ConsultMapper consultMapper;
	@Autowired
	private IFournisseurDao fournisseurDaoImpl;	
	@Autowired
	private IContratDao contratDaoImpl;
	@Autowired
	private IProjetDao projetDaoImpl;
	@Autowired
	private IEtatDao etatDaoImpl;
	@Autowired
	private IDrDao drDaoImpl;
	@Autowired
	private ISiteDao siteDaoImpl;
	@Autowired
	private IPieceJointeDao pieceJointeDaoImpl;
	@Autowired 
	private IActionService actionServiceImpl;
	@Autowired 
	private IUtilisateurDao utilisateurDaoImpl;

	private String fileUploadDir;
	/**
	 * @param fileUploadDir the fileUploadDir to set
	 */
	public void setFileUploadDir(String fileUploadDir) {
		this.fileUploadDir = fileUploadDir;
	}

	
	@PostMapping("/consultMesByCriteresPagination")
	public List<ConsultDto> consultMesByCriteresPagination(ConsultCriDto dto, @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize){
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		List<Long> idtsFournisseur = new ArrayList<Long>();
		idtsFournisseur.add(fournisseur.getId());
		String idtFournisseur = StringUtil.joinListLong(idtsFournisseur, ",");
		String idtContrat = StringUtil.joinListLong(dto.getIdtsContrat(), ",");
		String idtProjet = StringUtil.joinListLong(dto.getIdtsProjet(), ",");
		String idtEtat = StringUtil.joinListLong(dto.getIdtsEtat(), ",");
		String idtDr = StringUtil.joinListLong(dto.getIdtsDr(), ",");
		String idtSite = StringUtil.joinListLong(dto.getIdtsSite(), ",");
		
		List<Object> listMesIdt = mesDaoImpl.consultMesIdtByCriteres(
				idtFournisseur, idtContrat, idtProjet, idtEtat, idtDr, idtSite, null);
		
		String fournisseurLabel = DataHelper.joinListLabel(idtsFournisseur, ", ", fournisseurDaoImpl);
		String contratLabel = DataHelper.joinListLabel(dto.getIdtsContrat(), ", ", contratDaoImpl);
		String drLabel = DataHelper.joinListLabel(dto.getIdtsDr(), ", ", drDaoImpl);
		String siteLabel = DataHelper.joinListLabel(dto.getIdtsSite(), ", ", siteDaoImpl);
		String projetLabel = DataHelper.joinListLabel(dto.getIdtsProjet(), ", ", projetDaoImpl);
		String etatLabel = DataHelper.joinListLabel(dto.getIdtsEtat(), ", ", etatDaoImpl);
		
		List<Long> idsMES = new ArrayList<Long>();
		if(listMesIdt != null && listMesIdt.size() > 0){
			for(Object obj: listMesIdt){
				idsMES.add(((BigInteger)obj).longValue());
			}
			List<Mes> domains = mesDaoImpl.getMesByCriteresPagination(idsMES, pageNumber, pageSize);
			if(domains != null && domains.size() > 0){
				List<ConsultDto> dtos = consultMapper.toDtos(domains);
				if(dtos != null && dtos.size() > 0){			
					actionServiceImpl.saveAction("Module Fournisseur : Recuperation Liste M.E.S dans Menu Consultation avec pagination avec les parametres suivants => "
							+ "[Fournisseur] : " + fournisseurLabel +
							" et [Contrat] : " + contratLabel +
							" et [Projet] : " + projetLabel +
							" et [Dr] : " + drLabel +
							" et [Site] : " + siteLabel +
							" et [Etat] : " + etatLabel , ActionCode.CONSULTATION.value());
					logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Liste M.E.S dans Menu Consultation avec pagination avec les parametres suivants => "
							+ "[Fournisseur] : " + fournisseurLabel +
							" et [Contrat] : " + contratLabel +
							" et [Projet] : " + projetLabel +
							" et [Dr] : " + drLabel +
							" et [Site] : " + siteLabel +
							" et [Etat] : " + etatLabel );									
					return dtos;
				}
			}
		}

		return null;

	}

	@PostMapping("/consultTotalMesByCriteres")
	public int consultTotalMesByCriteres(ConsultCriDto dto){
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return 0;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return 0;
		List<Long> idtsFournisseur = new ArrayList<Long>();
		idtsFournisseur.add(fournisseur.getId());
		String idtFournisseur = StringUtil.joinListLong(idtsFournisseur, ",");
		String idtContrat = StringUtil.joinListLong(dto.getIdtsContrat(), ",");
		String idtProjet = StringUtil.joinListLong(dto.getIdtsProjet(), ",");
		String idtEtat = StringUtil.joinListLong(dto.getIdtsEtat(), ",");
		String idtDr = StringUtil.joinListLong(dto.getIdtsDr(), ",");
		String idtSite = StringUtil.joinListLong(dto.getIdtsSite(), ",");
		
		List<Object> listMesIdt = mesDaoImpl.consultMesIdtByCriteres(
				idtFournisseur, idtContrat, idtProjet, idtEtat, idtDr, idtSite, null);
		
		List<Long> idsMES = new ArrayList<Long>();
		if(listMesIdt != null && listMesIdt.size() > 0){
			for(Object obj: listMesIdt){
				idsMES.add(((BigInteger)obj).longValue());
			}
			int total = mesDaoImpl.consultTotalMesByCriteres(idsMES);
			actionServiceImpl.saveAction("Module Fournisseur : Recuperation Total : " + total + " M.E.S dans Menu Consultation", ActionCode.CONSULTATION.value());
			logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Total : " + total + " M.E.S dans Menu Consultation");
			return total;
		}

		return 0;
	}

	@GetMapping("/getPjs")
	public ResponseEntity<Resource> getPjs(@RequestParam("fileName") String fileName) {
		if (Utils.validePathAndFileName(fileName)) {
			String path = fileUploadDir + fileName;
			File file = new File(path);

			if (file.exists()) {
				String extension = FilenameUtils.getExtension(file.getName()).equals("") ? "" : "." + FilenameUtils.getExtension(file.getName());
				String originFileName = file.getName();
				PieceJointe pj = pieceJointeDaoImpl.getOriginFileName(fileName);

				if (pj != null && pj.getOriginFileName() != null && !pj.getOriginFileName().equals("")) {
					originFileName = pj.getOriginFileName() + extension;
				}

				logger.info(Utils.getLogParam() + "Module Fournisseur : Telechargement Piece jointe : " + originFileName + " dans menu Consultation");

				HttpHeaders headers = new HttpHeaders();
				headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + originFileName + "\"");
				headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

				Resource resource = new FileSystemResource(file);
				return ResponseEntity.ok()
						.headers(headers)
						.body(resource);
			}
		} else {
			logger.info(Utils.getLogParam() + Constants.ERROR_PATH_FILNAME_LABEL);
		}

		logger.info(Utils.getLogParam() + "Module Fournisseur : Piece jointe : " + fileName + " pour le menu Consultation Inexistante dans le Serveur");
		return ResponseEntity.notFound().build();
	}



}
