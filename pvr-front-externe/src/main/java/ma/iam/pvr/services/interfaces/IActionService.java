package ma.iam.pvr.services.interfaces;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IActionService {

	void saveAction(String description, String typOperation);
}
