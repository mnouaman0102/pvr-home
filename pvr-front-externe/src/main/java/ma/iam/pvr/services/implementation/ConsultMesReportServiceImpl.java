package ma.iam.pvr.services.implementation;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.dao.interfaces.IContratDao;
import ma.iam.pvr.dao.interfaces.IDrDao;
import ma.iam.pvr.dao.interfaces.IEtatDao;
import ma.iam.pvr.dao.interfaces.IFournisseurDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IProjetDao;
import ma.iam.pvr.dao.interfaces.ISiteDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Contrat;
import ma.iam.pvr.domain.Dr;
import ma.iam.pvr.domain.Etat;
import ma.iam.pvr.domain.Fournisseur;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.Projet;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.domain.utils.DataHelper;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.dto.dtos.ConsultMesReportDto;
import ma.iam.pvr.dto.mapper.ConsultMesReportMapper;
//import ma.iam.pvr.security.SecurityContextHelper;
import ma.iam.pvr.services.interfaces.IActionService;
import ma.iam.pvr.services.interfaces.IConsultMesReportService;
import ma.iam.pvr.utils.ActionCode;
import ma.iam.pvr.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author K.ELBAGUARI
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class ConsultMesReportServiceImpl implements IConsultMesReportService {
	private final Logger logger = LoggerFactory.getLogger(ConsultMesReportServiceImpl.class);
	@Autowired
	private IMesDao mesDaoImpl;	
	@Autowired
	private IFournisseurDao fournisseurDaoImpl;	
	@Autowired
	private IContratDao contratDaoImpl;
	@Autowired
	private IProjetDao projetDaoImpl;
	@Autowired
	private IEtatDao etatDaoImpl;
	@Autowired
	private IDrDao drDaoImpl;
	@Autowired
	private ISiteDao siteDaoImpl;
	@Autowired
	private ConsultMesReportMapper consultMesReportMapper;
	@Autowired 
	private IActionService actionServiceImpl;
	@Autowired 
	private IUtilisateurDao utilisateurDaoImpl;
	
	public List<ConsultMesReportDto> consultMesByCriteres(String idtsContrat, String idtsProjet, String idtsEtat, String idtsDr, String idtsSite) throws Exception {
		//if (/*!SecurityContextHelper.isFournisseur()*/true)
			//throw new Exception(SecurityContextHelper.getUserName() + "Utilisateur non autorise exporter le rapport !");
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		List<Long> idtsFournisseur = new ArrayList<Long>();
		idtsFournisseur.add(fournisseur.getId());
		String idtsFournisseurStr = StringUtil.joinListLong(idtsFournisseur, ",");
		List<Object> listMesIdt = mesDaoImpl.consultMesIdtByCriteres(idtsFournisseurStr, idtsContrat, 
				idtsProjet, idtsEtat, idtsDr, idtsSite, null);
		
		List<Long> idtFournisseur = StringUtil.toListLong(idtsFournisseurStr, ",");
		List<Long> idtContrat = StringUtil.toListLong(idtsContrat, ",");
		List<Long> idtProjet = StringUtil.toListLong(idtsProjet, ",");
		List<Long> idtEtat = StringUtil.toListLong(idtsEtat, ",");
		List<Long> idtDr = StringUtil.toListLong(idtsDr, ",");
		List<Long> idtSite = StringUtil.toListLong(idtsSite, ",");
		
		String fournisseurLabel = DataHelper.joinListLabel(idtFournisseur, ", ", fournisseurDaoImpl);
		String contratLabel = DataHelper.joinListLabel(idtContrat, ", ", contratDaoImpl);
		String drLabel = DataHelper.joinListLabel(idtDr, ", ", drDaoImpl);
		String siteLabel = DataHelper.joinListLabel(idtSite, ", ", siteDaoImpl);
		String projetLabel = DataHelper.joinListLabel(idtProjet, ", ", projetDaoImpl);
		String etatLabel = DataHelper.joinListLabel(idtEtat, ", ", etatDaoImpl);

		List<Long> idsMES = new ArrayList<Long>();
		if(listMesIdt != null && listMesIdt.size() > 0){
			for(Object obj: listMesIdt){
				idsMES.add(((BigInteger)obj).longValue());
			}
			List<Mes> domains = mesDaoImpl.getMesByCriteres(idsMES);
			if(domains != null && domains.size() > 0){				
				List<ConsultMesReportDto> dtos = consultMesReportMapper.toDtos(domains);	
				if(dtos != null && dtos.size() > 0){
					actionServiceImpl.saveAction("Module Fournisseur : Exporter le rapport de Liste M.E.S dans Menu Consultation avec les parametres suivants : "
							+ "[Fournisseur] : " + fournisseurLabel +
							" [Contrat] : " + contratLabel +
							" [Projet] : " + projetLabel +
							" [Dr] : " + drLabel +
							" [Site] : " + siteLabel +
							" [Etat] : " + etatLabel , ActionCode.EXPORT.value());
					logger.info(Utils.getLogParam() + "Module Fournisseur : Exporter le rapport de Liste M.E.S dans Menu Consultation avec les parametres suivants : "
							+ "[Fournisseur] : " + fournisseurLabel +
							" [Contrat] : " + contratLabel +
							" [Projet] : " + projetLabel +
							" [Dr] : " + drLabel +
							" [Site] : " + siteLabel +
							" [Etat] : " + etatLabel );									
					return dtos;
				}
			}
		}
		return null;			

	}


	public Fournisseur getFournisseurByIdt(Long idtFournisseur) {

		return fournisseurDaoImpl.get(idtFournisseur);
	}


	public Contrat getContratByIdt(Long idtContrat) {

		return contratDaoImpl.get(idtContrat);
	}


	public Projet getProjetByIdt(Long idtProjet) {

		return projetDaoImpl.get(idtProjet);
	}


	public Etat getEtatByIdt(Long idtEtat) {

		return etatDaoImpl.get(idtEtat);
	}

	public Dr getDrByIdt(Long idtDr) {
		return drDaoImpl.get(idtDr);
	}


	public Site getSiteByIdt(Long idtSite) {
		return siteDaoImpl.get(idtSite);
	}

}

