package ma.iam.pvr.services.interfaces;

import java.util.List;

import ma.iam.pvr.dto.dtos.EmailDto;



/**
 * 
 * @author Z.BELGHAOUTI && K.ELBAGUARI
 *
 */
public interface IEmailService{

    Long sendEmails(List<EmailDto> emailDtos);   
    void sendEmailRequalifByFournisseur(EmailDto emailDto);
}
