package ma.iam.pvr.services.interfaces;

import java.io.FileNotFoundException;
import java.util.List;

import javax.ws.rs.core.Response;

import ma.iam.pvr.domain.Contrat;
import ma.iam.pvr.dto.dtos.DeploiDto;
import ma.iam.pvr.dto.dtos.EmailDto;
import ma.iam.pvr.dto.dtos.FourniCriMultipleDto;
import ma.iam.pvr.dto.dtos.OperationEditDto;
import ma.iam.pvr.dto.dtos.OperationPjUpdateDto;
import ma.iam.pvr.dto.dtos.OperationSaveDto;
import ma.iam.pvr.dto.dtos.OperationUpdateDto;
import ma.iam.pvr.exception.MyAppException;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

public interface IFourniService {
	List<Contrat> getListContratByFournisseur();
	int getTotalMesByCriteres(FourniCriMultipleDto dto);
	List<DeploiDto> getListMesIdtForDeploi(FourniCriMultipleDto dto, int pageNumber, int pageSize);
	List<EmailDto> requalifOperation(Long idtMes, Long idtEntite);
	ResponseEntity<Resource> getFileDeploi(String fileName) throws FileNotFoundException;
	OperationSaveDto  uploadFile(MultipartFormDataInput input);
	Long addMorePj(OperationSaveDto dto) throws MyAppException;
	OperationEditDto getOperationByIdt(Long idtOperation) throws MyAppException;
	List<EmailDto> updateOperation(OperationUpdateDto dto) throws MyAppException;
	OperationPjUpdateDto  uploadPjsUpdateOperation(MultipartFormDataInput input);
	Long sendEmails(List<EmailDto> dtos);
}
