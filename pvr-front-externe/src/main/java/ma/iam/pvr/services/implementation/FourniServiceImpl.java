package ma.iam.pvr.services.implementation;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import ma.iam.pvr.codeError.MyAppErrorCode;
import ma.iam.pvr.dao.interfaces.IContratDao;
import ma.iam.pvr.dao.interfaces.IDrDao;
import ma.iam.pvr.dao.interfaces.IEmailDao;
import ma.iam.pvr.dao.interfaces.IEntiteDao;
import ma.iam.pvr.dao.interfaces.IFournisseurDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IPieceJointeDao;
import ma.iam.pvr.dao.interfaces.IPjOperationDao;
import ma.iam.pvr.dao.interfaces.IProjetDao;
import ma.iam.pvr.dao.interfaces.IQualificationDao;
import ma.iam.pvr.dao.interfaces.ISiteDao;
import ma.iam.pvr.dao.interfaces.IStatutOperationDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Contrat;
import ma.iam.pvr.domain.Dr;
import ma.iam.pvr.domain.Entite;
import ma.iam.pvr.domain.Fournisseur;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.PieceJointe;
import ma.iam.pvr.domain.PjOperation;
import ma.iam.pvr.domain.Projet;
import ma.iam.pvr.domain.Qualification;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.domain.StatutOperation;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.dto.dtos.DeploiDto;
import ma.iam.pvr.dto.dtos.EmailDto;
import ma.iam.pvr.dto.dtos.FourniCriMultipleDto;
import ma.iam.pvr.dto.dtos.OperationEditDto;
import ma.iam.pvr.dto.dtos.OperationPjUpdateDto;
import ma.iam.pvr.dto.dtos.OperationSaveDto;
import ma.iam.pvr.dto.dtos.OperationUpdateDto;
import ma.iam.pvr.dto.dtos.PjDto;
import ma.iam.pvr.dto.mapper.EmailMapper;
import ma.iam.pvr.dto.mapper.MesDeploiMapper;
import ma.iam.pvr.dto.mapper.OperationEditMapper;
import ma.iam.pvr.dto.mapper.OperationSaveMapper;
import ma.iam.pvr.exception.MyAppException;
import ma.iam.pvr.services.interfaces.IActionService;
import ma.iam.pvr.services.interfaces.IEmailService;
import ma.iam.pvr.services.interfaces.IFourniService;
import ma.iam.pvr.utils.ActionCode;
import ma.iam.pvr.utils.Constants;
import ma.iam.pvr.utils.Utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.ws.rs.core.MultivaluedMap;

/**
 * @author K.ELBAGUARI
 *
 */
@Service
@RequestMapping("/services/fourni")
@Transactional(rollbackFor = Exception.class)
public class FourniServiceImpl implements IFourniService {

	private final Logger logger = LoggerFactory.getLogger(FourniServiceImpl.class);

	@Autowired 
	private IMesDao mesDaoImpl;
	@Autowired 
	private IQualificationDao qualificationDaoImpl;
	@Autowired 
	private IStatutOperationDao statutOperationDaoImpl;
	@Autowired 
	private IEntiteDao entiteDaoImpl;
	@Autowired 
	private MesDeploiMapper mesDeploiMapper;
	@Autowired 
	private IPjOperationDao pjOperationDaoImpl;	
	@Autowired
	private IEmailDao emailDaoImpl;	
	@Autowired
	private IEmailService emailServiceImpl;
	@Autowired 
	private IActionService actionServiceImpl;	
	@Autowired
	private IUtilisateurDao utilisateurDaoImpl;	
	@Autowired
	private IPieceJointeDao pieceJointeDaoImpl;
	@Autowired
	private IContratDao contratDaoImpl;
	@Autowired
	private OperationSaveMapper operationSaveMapper;
	@Autowired
	private EmailMapper emailMapper;
	@Autowired
	private IFournisseurDao fournisseurDaoImpl;
	@Autowired
	private IProjetDao projetDaoImpl;
	@Autowired
	private IDrDao drDaoImpl;
	@Autowired
	private ISiteDao siteDaoImpl;
	@Autowired
	private OperationEditMapper operationEditMapper;

	private String fileUploadDir;

	public void setFileUploadDir(String fileUploadDir) {
		this.fileUploadDir = fileUploadDir;
	}

	/**
	 * GET CONTRACTS'LIST FOR AUHENTIFIED PROVIDER
	 * @return
	 */

	@GetMapping("/getListContratByFournisseur")
	public List<Contrat> getListContratByFournisseur() {	
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		List<Contrat> contrats = new ArrayList<Contrat>();
		contrats = contratDaoImpl.getAllContactsByFounisseur(fournisseur.getId());

		logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Total de : "+ contrats.size() +
				" Liste Contrats pour Fournisseur : " + fournisseur.getLabel());
		actionServiceImpl.saveAction("Module Fournisseur : Recuperation Total de : "+ contrats.size() +
				" Liste Contrats pour Fournisseur : " + fournisseur.getLabel(), ActionCode.CONSULTATION.value());
		return contrats;
	}

	/**
	 * GET Total Mes By Criteres
	 */

	@PostMapping("/getTotalMesByCriteres")
	public int getTotalMesByCriteres(FourniCriMultipleDto dto) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return 0;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return 0;
		
		String idtContrat = StringUtil.joinListLong(dto.getIdtsContrat(), ",");
		String idtProjet = StringUtil.joinListLong(dto.getIdtsProjet(), ",");
		String idtEtat = StringUtil.joinListLong(dto.getIdtsEtat(), ",");
		String idtDr = StringUtil.joinListLong(dto.getIdtsDr(), ",");
		String idtSite = StringUtil.joinListLong(dto.getIdtsSite(), ",");

		List<Object> listMesIdt = mesDaoImpl.getListMesIdtForFournisseurMultipleValues(fournisseur.getId(), idtContrat, 
				idtProjet, idtDr, idtSite, idtEtat , dto.getNumOperation());
		List<Long> idsMES = new ArrayList<Long>();
		if(listMesIdt != null && listMesIdt.size() > 0){
			for(Object obj: listMesIdt){
				idsMES.add(((BigInteger)obj).longValue());
			}
			int total = mesDaoImpl.getTotalMesByCriteres(idsMES);
			logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Total : " + total 
					+ " Operation(s) ayant Etat En cours de qualification pour Fournisseur : " + fournisseur.getLabel());
			return total;
		}

		return 0;
	}

	/**
	 * Search Multi critere with pagination
	 */

	@PostMapping("/getListMesIdtForDeploi")
	public List<DeploiDto> getListMesIdtForDeploi(FourniCriMultipleDto dto, @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		
		String idtContrat = StringUtil.joinListLong(dto.getIdtsContrat(), ",");
		String idtProjet = StringUtil.joinListLong(dto.getIdtsProjet(), ",");
		String idtEtat = StringUtil.joinListLong(dto.getIdtsEtat(), ",");
		String idtDr = StringUtil.joinListLong(dto.getIdtsDr(), ",");
		String idtSite = StringUtil.joinListLong(dto.getIdtsSite(), ",");
		List<DeploiDto> dtos = new ArrayList<DeploiDto>();
		List<Object> listMesIdt = mesDaoImpl.getListMesIdtForFournisseurMultipleValues(fournisseur.getId(), idtContrat, 
				idtProjet, idtDr, idtSite, idtEtat , dto.getNumOperation());
		List<Long> idsMES = new ArrayList<Long>();
		if(listMesIdt != null && listMesIdt.size() > 0){
			for(Object obj: listMesIdt){
				idsMES.add(((BigInteger)obj).longValue());
			}
			List<Mes> domains = mesDaoImpl.getMesByCriteresPagination(idsMES, pageNumber, pageSize);
			dtos =  mesDeploiMapper.toDtos(domains);
		}
		logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Liste de : "+ dtos.size() +
				" Operations pour Fournisseur : " + fournisseur.getLabel());

		actionServiceImpl.saveAction("Module Fournisseur : Recuperation Liste de : "+ dtos.size() +
				" Operations pour Fournisseur : " + fournisseur.getLabel(), ActionCode.CONSULTATION.value());
		return dtos;
	}

	/**
	 * Change Statut Operation (Qualif table)
	 */

	@GetMapping("/requalifOperation")
	public List<EmailDto> requalifOperation(@QueryParam("idtMes") Long idtMes,@QueryParam("idtEntite")  Long idtEntite) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		List<EmailDto> emailDtos = new ArrayList<EmailDto>();
		Mes mes = null;
		final String pourQualif = "pour qualification";
		final String pourReQualif = "pour requalification";
		Qualification qualification = null;
		StatutOperation statutOperation = null;
		Entite entite = entiteDaoImpl.get(idtEntite);
		mes = mesDaoImpl.get(idtMes);
		if(mes != null && entite != null){
			qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(idtMes,idtEntite);	
			statutOperation = statutOperationDaoImpl.get(1L);
			if(qualification != null){
				if(statutOperation !=null){
					if(qualification.getStatutOperation().equals(statutOperation)){
						// jamais qualifie
						// donc il faut d'abord qualifier
						qualification.setActionType(pourQualif);
					}else{
						// au moins une qualification est deja faite
						// donc il faut requalifier
						qualification.setActionType(pourReQualif);
					}
					qualification.setStatutOperation(statutOperation);
					qualification.setCommentaire(null);
					qualificationDaoImpl.save(qualification);
					EmailDto emailDto=emailMapper.toDto(mes, entite, true);
					if(emailDto!=null)
						emailDtos.add(emailDto);
				}					
			}else{
				Qualification qualif = new Qualification();
				qualif.setStatutOperation(statutOperation);
				qualif.setCommentaire(null);
				qualif.setEntite(entite);
				qualif.setMes(mes);
				qualif.setDate(new Date());
				qualif.setActionType(pourQualif);
				qualificationDaoImpl.save(qualif);
				EmailDto emailDto = emailMapper.toDto(mes, entite, false);
				if(emailDto != null)
					emailDtos.add(emailDto);
			}
			actionServiceImpl.saveAction("Module Fournisseur : Envoi de Demande de Requalification Operation ayant idt : " + mes.getId() 
					+ " et numero : " + mes.getNumOperation() 
					+ " pour Entite : " + entite.getLabel()
					+ " par Fournisseur : " + fournisseur.getLabel() 
					, ActionCode.MODIF.value());
			logger.info(Utils.getLogParam() + "Module Fournisseur : Envoi de Demande de Requalification Operation ayant idt : " + mes.getId() 
					+ " et numero : " + mes.getNumOperation() 
					+ " pour Entite : " + entite.getLabel()
					+ " par Fournisseur : " + fournisseur.getLabel());
			return emailDtos;

		}

		return null;
	}

	/**
	 * GET PJ
	 */
	@GetMapping("/getPjDeploi")
	public ResponseEntity<Resource> getFileDeploi(@RequestParam("fileName") String fileName) throws FileNotFoundException {
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);

		if (user == null) {
			return ResponseEntity.notFound().build();
		}

		Fournisseur fournisseur = user.getFournisseur();

		if (fournisseur == null) {
			return ResponseEntity.notFound().build();
		}

		if (Utils.validePathAndFileName(fileName)) {
			String path = fileUploadDir + fileName;
			File file = new File(path);

			if (file.exists()) {
				String extension = FilenameUtils.getExtension(file.getName());
				String originFileName = file.getName();
				PieceJointe pj = pieceJointeDaoImpl.getOriginFileName(fileName);

				if (pj != null && pj.getOriginFileName() != null && !pj.getOriginFileName().equals("")) {
					originFileName = pj.getOriginFileName() + "." + extension;
				}

				logger.info(Utils.getLogParam() + "Module Fournisseur: Telechargement Piece jointe: " + originFileName);
				actionServiceImpl.saveAction("Module Fournisseur : Telechargement Piece jointe : " + originFileName
						+ " par Fournisseur : " + fournisseur.getLabel(), ActionCode.DOWNLOAD.value());

				HttpHeaders headers = new HttpHeaders();
				headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + originFileName + "\"");
				headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

				FileInputStream fileInputStream = new FileInputStream(file);
				InputStreamResource resource = new InputStreamResource(fileInputStream);

				return ResponseEntity.ok()
						.headers(headers)
						.contentLength(file.length())
						.contentType(MediaType.APPLICATION_OCTET_STREAM)
						.body(resource);
			}
		} else {
			logger.info(Utils.getLogParam() + Constants.ERROR_PATH_FILNAME_LABEL);
		}

		logger.info(Utils.getLogParam() + "Module Fournisseur: Piece jointe : " + fileName + " Inexistante dans le Serveur");
		return ResponseEntity.notFound().build();
	}

	/**
	 * UPLOAD FILE
	 */

	@PostMapping("/uploadFile")
	@Consumes("multipart/form-data")
	public OperationSaveDto  uploadFile(MultipartFormDataInput input) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		OperationSaveDto dto = new OperationSaveDto();

		final String FILE_EXP = "Exp";
		final String EXT_EXP = "extExp";
		final String FILE_EXP_ORIGINE = "originExp";

		final String FILE_OPT = "Opt";
		final String EXT_OPT = "extOpt";
		final String FILE_OPT_ORIGINE = "originOpt";

		final String FILE_DCC = "Dcc";
		final String EXT_DCC = "extDcc";
		final String FILE_DCC_ORIGINE = "originDcc";

		final String FILE_DCC_ENV = "DccEnv";
		final String EXT_DCC_ENV = "extDccEnv";
		final String FILE_DCC_ENV_ORIGINE = "originDccEnv";

		final String FILE_QOS = "Qos";
		final String EXT_QOS = "extQos";
		final String FILE_QOS_ORIGINE = "originQos";

		Map<String, List<InputPart>> formParts = input.getFormDataMap();
		List<PjDto> pjOptim = new ArrayList<PjDto>();
		List<PjDto> pjExploi = new ArrayList<PjDto>();
		List<PjDto> pjDcc = new ArrayList<PjDto>();
		List<PjDto> pjDccEnv = new ArrayList<PjDto>();
		List<PjDto> pjQos = new ArrayList<PjDto>();
		String fileNameSaved = "";
		String originFileName = "";

		if(formParts.size() > 0){
			String fileToGet = "";
			String fileToGetOrigin = "";
			String extToGet = "";
			String fileName = "";
			String extention = "";

			int j = 0 ;
			boolean isNoMoreFiles = false;
			String fileNameToSave = "";

			for(int i=0 ; i<5 ; i++){					
				j = 0;
				while(!isNoMoreFiles){
					if(i==0){
						//Exploitation
						j++;
						fileNameToSave = FILE_EXP;
						fileToGet = FILE_EXP + j;
						extToGet  = EXT_EXP + j;
						fileToGetOrigin = FILE_EXP_ORIGINE + j;
					}else if(i==1){
						//OPTIM
						j++;
						fileNameToSave = FILE_OPT;
						fileToGet = FILE_OPT + j;
						extToGet  = EXT_OPT + j;
						fileToGetOrigin = FILE_OPT_ORIGINE + j;
					}else if(i==2){
						//DCC RADIO
						j++;
						fileNameToSave = FILE_DCC;
						fileToGet = FILE_DCC + j;
						extToGet  = EXT_DCC + j;
						fileToGetOrigin = FILE_DCC_ORIGINE + j;
					}else if(i==3){
						//DCC ENV
						j++;
						fileNameToSave = FILE_DCC_ENV;
						fileToGet = FILE_DCC_ENV + j;
						extToGet  = EXT_DCC_ENV + j;
						fileToGetOrigin = FILE_DCC_ENV_ORIGINE + j;
					}else if(i==4){
						//QOS
						j++;
						fileNameToSave = FILE_QOS;
						fileToGet = FILE_QOS + j;
						extToGet  = EXT_QOS + j;
						fileToGetOrigin = FILE_QOS_ORIGINE + j;
					}

					if(formParts.get(fileToGet) != null && formParts.get(extToGet) != null){							
						List<InputPart> inPart = formParts.get(fileToGet);
						List<InputPart> inPart2 = formParts.get(extToGet);
						List<InputPart> inPart3 = formParts.get(fileToGetOrigin);

						for(InputPart inputPart : inPart2){
							try {
								InputStream istream = inputPart.getBody(InputStream.class,null);
								extention = IOUtils.toString(istream);	
								break;
							} catch (IOException e) {
								e.getMessage();
							}
						}
						for(InputPart inputPart : inPart3){
							try {
								InputStream istream = inputPart.getBody(InputStream.class,null);
								originFileName = IOUtils.toString(istream);	
								break;
							} catch (IOException e) {
								e.getMessage();
							}
						}

						PjDto pjDto = new PjDto();
						for (InputPart inputPart : inPart) {

							try {
								MultivaluedMap<String, String> headers = inputPart.getHeaders();
								fileName = parseFileName(headers);
								InputStream istream = inputPart.getBody(InputStream.class,null);
								fileNameSaved = fileNameToSave + String.valueOf(new Date().getTime()) + "." + extention;
								fileName = fileUploadDir + fileNameSaved;
								byte [] bytes = IOUtils.toByteArray(istream);
								writeFile(bytes,fileName);
								switch (i) {
								case 0:
									if(isFileExistInFS(fileName)){
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjExploi.add(pjDto);
										break;
									}
								case 1:
									if(isFileExistInFS(fileName)){
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjOptim.add(pjDto);
										break;
									}
								case 2:
									if(isFileExistInFS(fileName)){
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjDcc.add(pjDto);
										break;
									}

								case 3:
									if(isFileExistInFS(fileName)){
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjDccEnv.add(pjDto);
										break;
									}

								case 4:
									if(isFileExistInFS(fileName)){
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjQos.add(pjDto);
										break;
									}

								}

							} catch (IOException e) {
								e.getMessage();
							}

						}
					}else{
						isNoMoreFiles = true;
					}
					if(isNoMoreFiles){
						isNoMoreFiles = false;
						break; // go for next i
					}
				}

			}
			dto.setPjExploi(pjExploi);
			dto.setPjOptim(pjOptim);
			dto.setPjQos(pjQos);
			dto.setPjDcc(pjDcc);
			dto.setPjDccEnv(pjDccEnv);
		}

		if(pjExploi != null && pjExploi.size() > 0){			
			dto.setPjExploi(pjExploi);
		}
		if(pjOptim != null && pjOptim.size() > 0){
			dto.setPjOptim(pjOptim);
		}
		if(pjDcc != null && pjDcc.size() > 0){			
			dto.setPjDcc(pjDcc);
		}
		if(pjDccEnv != null && pjDccEnv.size() > 0){
			dto.setPjDccEnv(pjDccEnv);
		}
		if(pjQos != null && pjQos.size() > 0){			
			dto.setPjQos(pjQos);
		}
		return dto;
	}

	private String parseFileName(MultivaluedMap<String, String> headers) {
		String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");
		for (String name : contentDispositionHeader) {
			if ((name.trim().startsWith("filename"))) {
				String[] tmp = name.split("=");
				String fileName = tmp[1].trim().replaceAll("\"","");
				return fileName;
			}
		}
		return "randomName";
	}
	private void writeFile(byte[] content, String filename) throws IOException {

		File file;
		FileOutputStream fop=null;;
		try {
			file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}
			fop = new FileOutputStream(file);
			fop.write(content);
			fop.flush();
			fop.close();
		} catch (Exception e) {
			e.getMessage();
		}
		finally{
			closeFile(fop);
		}
	}
	private void closeFile(FileOutputStream fop) throws IOException{
		fop.close();
	}

	//To make sure that File exists in the Directory of Server
	private boolean isFileExistInFS(String filename){
		File file = new File(filename);
		if (file.exists()) {
			return true;
		}
		return false;
	}


	@PostMapping("/addMorePj")
	public Long addMorePj(OperationSaveDto dto) throws MyAppException {	
		Mes	mes = operationSaveMapper.addPj(dto);		
		if(mes != null){
			addMorePjTrace(dto, mes);
			return mes.getId();
		}
		return null;
	}

	/**
	 * Tracer l'action de joindres des pieces par l'entite fournisseur
	 * @param dto
	 * @param mes
	 */
	private void addMorePjTrace(OperationSaveDto dto, Mes mes) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return ;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return ;
		String entite = "";
		String pjs = "";
		String operationType = "";	
		if(mes.getTypeProjet() != null){
			operationType = mes.getTypeProjet().getLabel();
		}
		if(dto.getPjExploi() != null && dto.getPjExploi().size() > 0){
			entite = "Exploitation";
			for(PjDto pj: dto.getPjExploi()){
				pjs += pj.getOriginFileName() + ", ";
			}
		}
		if(dto.getPjOptim() != null && dto.getPjOptim().size() > 0){
			entite = "Optim";
			for(PjDto pj: dto.getPjOptim()){
				pjs += pj.getOriginFileName() + ", ";
			}
		}
		if(dto.getPjDcc() != null && dto.getPjDcc().size() > 0){
			entite = "Dcc Radio";
			for(PjDto pj: dto.getPjDcc()){
				pjs += pj.getOriginFileName() + ", ";
			}
		}
		if(dto.getPjDccEnv() != null && dto.getPjDccEnv().size() > 0){
			entite = "Dcc Env";
			for(PjDto pj: dto.getPjDccEnv()){
				pjs += pj.getOriginFileName() + ", ";
			}
		}
		if(dto.getPjQos() != null && dto.getPjQos().size() > 0){
			entite = "Qos";
			for(PjDto pj: dto.getPjQos()){
				pjs += pj.getOriginFileName() + ", ";
			}
		}
		if(pjs.length() > 0){
			pjs = pjs.substring(0, pjs.length() -2);
		}
		actionServiceImpl.saveAction("Module Fournisseur : Ajout plus de Pieces Jointes pour Operation " + operationType + " ayant idt : " + mes.getId()
				+ " et Numero : " + mes.getNumOperation()
				+ " pour Entite : " + entite
				+ " Pieces Jointes : " + pjs
				+ " Par Fournisseur : " + fournisseur.getLabel()
				, ActionCode.MODIF.value());
		logger.info(Utils.getLogParam() + "Module Fournisseur : Ajout plus de Pieces Jointes pour Operation " + operationType + " ayant idt : " + mes.getId()
				+ " et Numero : " + mes.getNumOperation()
				+ " pour Entite : " + entite
				+ " Pieces Jointes : " + pjs
				+ " Par Fournisseur : " + fournisseur.getLabel()
				);
	}
	/**
	 * GET OPERATION BY IDT
	 */
	@GetMapping("/getOperationByIdt")
	public OperationEditDto getOperationByIdt(@QueryParam("idtOperation") Long idtOperation) throws MyAppException {
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null ;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null ;
		Mes domain = mesDaoImpl.get(idtOperation);
		OperationEditDto dto = new OperationEditDto();
		if (domain == null) {
			logger.info(Utils.getLogParam() + "Operation Inexistante");
			throw new MyAppException(MyAppErrorCode.OPERATION_INEXISTANTE, Constants.errorOperationInexistante);
		}
		if (domain.getEtat() != null) {
			if (domain.getEtat().getId() != 1L) { // En cours de qualification
				logger.info(Utils.getLogParam() + "Site deja mis en service");
				throw new MyAppException(MyAppErrorCode.SITE_ALREADY_MES, Constants.errorSiteMES);
			} else {
				logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation operation ayant idt : " + idtOperation
						+ " Par Fournisseur : " + fournisseur.getLabel());
				actionServiceImpl.saveAction("Module Fournisseur : Recuperation operation ayant idt : " + idtOperation
						+ " Par Fournisseur : " + fournisseur.getLabel()
						, ActionCode.CONSULTATION.value());
				dto = operationEditMapper.toDto(domain);
			}
		}

		return dto;
	}

	@PostMapping("/updateOperation")
	public List<EmailDto> updateOperation(OperationUpdateDto dto) throws MyAppException {
		List<EmailDto> emailDtos=new ArrayList<EmailDto>();
		String operationNum = "";
		String operationContrat = "";
		String operationFournisseur = "";
		String operationSite = "";
		String operationDr = "";
		String operationProjet = "";
		String operationType = "";
		String pjOperExp = "";
		String pjOperOpt = "";
		String pjOperQos = "";
		String pjOperDcc = "";
		String pjOperDccEnv = "";
		String pjOperationEntite = "";

		String mesContrat = "";
		String mesFournisseur = "";
		String mesSite = "";
		String mesDr = "";
		String mesProjet = "";
		String pjMesEntite = "";
		String pjMesExp = "";
		String pjMesOpt = "";
		String pjMesQos = "";
		String pjMesDcc = "";
		String pjMesDccEnv = "";
		if (dto != null && dto.getIdt() > 0) {
			Mes operation = mesDaoImpl.get(dto.getIdt());
			if (operation != null) {
				operationNum = operation.getNumOperation();
				if (operation.getContrat() != null) {
					Contrat contrat = contratDaoImpl.get(operation.getContrat().getId());
					if (contrat != null) {
						operationContrat = contrat.getLabel();
						if (contrat.getFournisseur() != null) {
							Fournisseur fournisseur = fournisseurDaoImpl.get(contrat.getFournisseur().getId());
							if (fournisseur != null) {
								operationFournisseur = fournisseur.getLabel();
							}
						}
					}
				}
				if (operation.getSite() != null) {
					Site site = siteDaoImpl.get(operation.getSite().getId());
					if (site != null) {
						operationSite = site.getLabel();
						if (site.getDr() != null) {
							Dr dr = drDaoImpl.get(site.getDr().getId());
							if (dr != null) {
								operationDr = dr.getLabel();
							}
						}
					}
				}else{
					operationSite = operation.getCommentaireOperation();
				}
				if (operation.getProjet() != null) {
					Projet projet = projetDaoImpl.get(operation.getProjet().getId());
					if (projet != null) {
						operationProjet = projet.getLabel();
					}
				}
				if (operation.getTypeProjet() != null) {
					operationType = operation.getTypeProjet().getLabel();
				}

				List<PjOperation> pjsOperations = pjOperationDaoImpl.getPJsOperationByMesIdt(operation.getId());

				if (pjsOperations != null && pjsOperations.size() > 0) {
					for (PjOperation pjOperation : pjsOperations) {
						if (pjOperation.getPieceJointe() != null && pjOperation.getEntite() != null) {
							Entite entite = pjOperation.getEntite();
							PieceJointe pj = pjOperation.getPieceJointe();
							if (entite.getId() == 2L) {
								if (pjOperExp.equals("")) {
									pjOperExp += "Exploitation : " + pj.getOriginFileName() + ", ";
								} else {
									pjOperExp += pj.getOriginFileName() + ", ";
								}
							}
							if (entite.getId() == 3L) {
								if (pjOperOpt.equals("")) {
									pjOperOpt += "Optim : " + pj.getOriginFileName() + ", ";
								} else {
									pjOperOpt += pj.getOriginFileName() + ", ";
								}
							}
							if (entite.getId() == 4L) {
								if (pjOperDcc.equals("")) {
									pjOperDcc += "Dcc Radio : " + pj.getOriginFileName() + ", ";
								} else {
									pjOperDcc += pj.getOriginFileName() + ", ";
								}
							}
							if (entite.getId() == 5L) {
								if (pjOperDccEnv.equals("")) {
									pjOperDccEnv += "Dcc Env : " + pj.getOriginFileName() + ", ";
								} else {
									pjOperDccEnv += pj.getOriginFileName() + ", ";
								}
							}
							if (entite.getId() == 6L) {
								if (pjOperQos.equals("")) {
									pjOperQos += "Qos : " + pj.getOriginFileName() + ", ";
								} else {
									pjOperQos += pj.getOriginFileName() + ", ";
								}
							}
						}
					}

					if (pjOperExp.length() > 0) {
						pjOperExp = pjOperExp.substring(0, pjOperExp.length() - 2);
					}
					if (pjOperOpt.length() > 0) {
						pjOperOpt = pjOperOpt.substring(0, pjOperOpt.length() - 2);
					}
					if (pjOperDcc.length() > 0) {
						pjOperDcc = pjOperDcc.substring(0, pjOperDcc.length() - 2);
					}
					if (pjOperDccEnv.length() > 0) {
						pjOperDccEnv = pjOperDccEnv.substring(0, pjOperDccEnv.length() - 2);
					}
					if (pjOperQos.length() > 0) {
						pjOperQos = pjOperQos.substring(0, pjOperQos.length() - 2);
					}

					pjOperationEntite += pjOperExp + " " + pjOperOpt + " " + pjOperDcc + " " + pjOperDccEnv + " "
							+ pjOperQos;

				}
				if (pjOperationEntite.length() > 0) {
					pjOperationEntite = pjOperationEntite + "";
				} else {
					pjOperationEntite = "Pas de pieces jointes";
				}
			}
		}
		Mes mes = operationEditMapper.toDomain(dto);
		if (mes != null) {
			if (mes.getContrat() != null) {
				Contrat contrat = contratDaoImpl.get(mes.getContrat().getId());
				if (contrat != null) {
					mesContrat = contrat.getLabel();
					if (contrat.getFournisseur() != null) {
						Fournisseur fournisseur = fournisseurDaoImpl.get(contrat.getFournisseur().getId());
						if (fournisseur != null) {
							mesFournisseur = fournisseur.getLabel();
						}
					}
				}
			}
			if (mes.getSite() != null) {
				Site site = siteDaoImpl.get(mes.getSite().getId());
				if (site != null) {
					mesSite = site.getLabel();
					if (site.getDr() != null) {
						Dr dr = drDaoImpl.get(site.getDr().getId());
						if (dr != null) {
							mesDr = dr.getLabel();
						}
					}
				}
			}else{
				mesSite = mes.getCommentaireOperation();
			}
			if (mes.getProjet() != null) {
				Projet projet = projetDaoImpl.get(mes.getProjet().getId());
				if (projet != null) {
					mesProjet = projet.getLabel();
				}
			}
			List<PjOperation> pjsMes = pjOperationDaoImpl.getPJsOperationByMesIdt(mes.getId());
			if (pjsMes != null && pjsMes.size() > 0) {
				for (PjOperation pjMes : pjsMes) {
					if (pjMes.getPieceJointe() != null && pjMes.getEntite() != null) {
						Entite entite = pjMes.getEntite();
						PieceJointe pj = pjMes.getPieceJointe();
						if (entite.getId() == 2L) {
							if (pjMesExp.equals("")) {
								pjMesExp += "Exploitation : " + pj.getOriginFileName() + ", ";
							} else {
								pjMesExp += pj.getOriginFileName() + ", ";
							}
						}
						if (entite.getId() == 3L) {
							if (pjMesOpt.equals("")) {
								pjMesOpt += "Optim : " + pj.getOriginFileName() + ", ";
							} else {
								pjMesOpt += pj.getOriginFileName() + ", ";
							}
						}
						if (entite.getId() == 4L) {
							if (pjMesDcc.equals("")) {
								pjMesDcc += "Dcc Radio : " + pj.getOriginFileName() + ", ";
							} else {
								pjMesDcc += pj.getOriginFileName() + ", ";
							}
						}
						if (entite.getId() == 5L) {
							if (pjMesDccEnv.equals("")) {
								pjMesDccEnv += "Dcc Env : " + pj.getOriginFileName() + ", ";
							} else {
								pjMesDccEnv += pj.getOriginFileName() + ", ";
							}
						}
						if (entite.getId() == 6L) {
							if (pjMesQos.equals("")) {
								pjMesQos += "Qos : " + pj.getOriginFileName() + ", ";
							} else {
								pjMesQos += pj.getOriginFileName() + ", ";
							}
						}
					}
				}

				if (pjMesExp.length() > 0) {
					pjMesExp = pjMesExp.substring(0, pjMesExp.length() - 2);
				}
				if (pjMesOpt.length() > 0) {
					pjMesOpt = pjMesOpt.substring(0, pjMesOpt.length() - 2);
				}
				if (pjMesDcc.length() > 0) {
					pjMesDcc = pjMesDcc.substring(0, pjMesDcc.length() - 2);
				}
				if (pjMesDccEnv.length() > 0) {
					pjMesDccEnv = pjMesDccEnv.substring(0, pjMesDccEnv.length() - 2);
				}
				if (pjMesQos.length() > 0) {
					pjMesQos = pjMesQos.substring(0, pjMesQos.length() - 2);
				}

				pjMesEntite += pjMesExp + " " + pjMesOpt + " " + pjMesDcc + " " + pjMesDccEnv + " " + pjMesQos;

			}
			if (pjMesEntite.length() > 0) {
				pjMesEntite = pjMesEntite + "";
			} else {
				pjMesEntite = "Pas de pieces jointes";
			}

			if (mes.getTypeProjet().getId() == 1L) {
				List<Entite> entites = entiteDaoImpl.findAll();
				if (entites != null && entites.size() > 0) {
					for (Entite e : entites) {
						if (e.getId() != null && !e.getId().equals(1L) && !e.getId().equals(5L)) {
							Qualification qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(mes.getId(),
									e.getId());
							if (qualification != null) {
								EmailDto emailDto = emailMapper.toDto(mes, e,"UPDATE_RADIO");
								if(emailDto!=null)
									emailDtos.add(emailDto);
							}

						}
					}
				}
			}
			if (mes.getTypeProjet().getId() == 2L) {
				Entite entite = entiteDaoImpl.get(5L);
				if (entite != null) {
					Qualification qualification = qualificationDaoImpl.getQualifByMesIdAndEntite(mes.getId(),
							entite.getId());
					if (qualification != null) {
						EmailDto emailDto=emailMapper.toDto(mes, entite,"UPDATE_ENV");
						if(emailDto!=null)
							emailDtos.add(emailDto);
					}

				}
			}

		}
		actionServiceImpl.saveAction(
				"Module Fournisseur MAJ Pieces Jointes pour Operation " + operationType + " ayant idt : " + mes.getId() + " et Numero : "
						+ operationNum + " et Fournisseur : " + operationFournisseur + " et Contrat : "
						+ operationContrat + " et Dr : " + operationDr + " et Site : " + operationSite + " et Projet : "
						+ operationProjet + " et Pieces Jointes : " + pjOperationEntite + " avec nouveaux donnees : "
						+ " Fournisseur : " + mesFournisseur + " Contrat : " + mesContrat + " Dr : " + mesDr
						+ " Site : " + mesSite + " Projet : " + mesProjet + " Pieces Jointes : " + pjMesEntite,
						ActionCode.MODIF.value());
		logger.info(Utils.getLogParam() + "Module Fournisseur MAJ Pieces Jointes pour Operation " + operationType + " ayant idt : " + mes.getId()
				+ " et Numero : " + operationNum + " et Fournisseur : " + operationFournisseur + " et Contrat : "
				+ operationContrat + " et Dr : " + operationDr + " et Site : " + operationSite + " et Projet : "
				+ operationProjet + " et Pieces Jointes : " + pjOperationEntite + " avec nouveaux donnees : "
				+ " Fournisseur : " + mesFournisseur + " Contrat : " + mesContrat + " Dr : " + mesDr + " Site : "
				+ mesSite + " Projet : " + mesProjet + " Pieces Jointes : " + pjMesEntite);
		emailServiceImpl.sendEmails(emailDtos);
		return emailDtos;
	}

	@PostMapping("/uploadPjsUpdateOperation")
	@Consumes("multipart/form-data")
	public OperationPjUpdateDto uploadPjsUpdateOperation(MultipartFormDataInput input) {
		OperationPjUpdateDto dto = new OperationPjUpdateDto();

		final String FILE_EXP = "Exp";
		final String EXT_EXP = "extExp";
		final String FILE_EXP_ORIGINE = "originExp";

		final String FILE_OPT = "Opt";
		final String EXT_OPT = "extOpt";
		final String FILE_OPT_ORIGINE = "originOpt";

		final String FILE_DCC = "Dcc";
		final String EXT_DCC = "extDcc";
		final String FILE_DCC_ORIGINE = "originDcc";

		final String FILE_DCC_ENV = "DccEnv";
		final String EXT_DCC_ENV = "extDccEnv";
		final String FILE_DCC_ENV_ORIGINE = "originDccEnv";

		final String FILE_QOS = "Qos";
		final String EXT_QOS = "extQos";
		final String FILE_QOS_ORIGINE = "originQos";

		Map<String, List<InputPart>> formParts = input.getFormDataMap();
		List<PjDto> pjOptim = new ArrayList<PjDto>();
		List<PjDto> pjExploi = new ArrayList<PjDto>();
		List<PjDto> pjDcc = new ArrayList<PjDto>();
		List<PjDto> pjDccEnv = new ArrayList<PjDto>();
		List<PjDto> pjQos = new ArrayList<PjDto>();
		String fileNameSaved = "";
		String originFileName = "";

		if (formParts.size() > 0) {
			String fileToGet = "";
			String fileToGetOrigin = "";
			String extToGet = "";
			String fileName = "";
			String extention = "";

			int j = 0;
			boolean isNoMoreFiles = false;
			String fileNameToSave = "";

			for (int i = 0; i < 5; i++) {
				j = 0;
				while (!isNoMoreFiles) {
					if (i == 0) {
						// Exploitation
						j++;
						fileNameToSave = FILE_EXP;
						fileToGet = FILE_EXP + j;
						extToGet = EXT_EXP + j;
						fileToGetOrigin = FILE_EXP_ORIGINE + j;
					} else if (i == 1) {
						// OPTIM
						j++;
						fileNameToSave = FILE_OPT;
						fileToGet = FILE_OPT + j;
						extToGet = EXT_OPT + j;
						fileToGetOrigin = FILE_OPT_ORIGINE + j;
					} else if (i == 2) {
						// DCC RADIO
						j++;
						fileNameToSave = FILE_DCC;
						fileToGet = FILE_DCC + j;
						extToGet = EXT_DCC + j;
						fileToGetOrigin = FILE_DCC_ORIGINE + j;
					} else if (i == 3) {
						// DCC ENV
						j++;
						fileNameToSave = FILE_DCC_ENV;
						fileToGet = FILE_DCC_ENV + j;
						extToGet = EXT_DCC_ENV + j;
						fileToGetOrigin = FILE_DCC_ENV_ORIGINE + j;
					} else if (i == 4) {
						// QOS
						j++;
						fileNameToSave = FILE_QOS;
						fileToGet = FILE_QOS + j;
						extToGet = EXT_QOS + j;
						fileToGetOrigin = FILE_QOS_ORIGINE + j;
					}

					if (formParts.get(fileToGet) != null && formParts.get(extToGet) != null) {
						List<InputPart> inPart = formParts.get(fileToGet);
						List<InputPart> inPart2 = formParts.get(extToGet);
						List<InputPart> inPart3 = formParts.get(fileToGetOrigin);

						for (InputPart inputPart : inPart2) {
							try {
								InputStream istream = inputPart.getBody(InputStream.class, null);
								extention = IOUtils.toString(istream);
								break;
							} catch (IOException e) {
								e.getMessage();
							}
						}
						for (InputPart inputPart : inPart3) {
							try {
								InputStream istream = inputPart.getBody(InputStream.class, null);
								originFileName = IOUtils.toString(istream);
								break;
							} catch (IOException e) {
								e.getMessage();
							}
						}

						PjDto pjDto = new PjDto();
						for (InputPart inputPart : inPart) {

							try {
								MultivaluedMap<String, String> headers = inputPart.getHeaders();
								fileName = parseFileName(headers);
								InputStream istream = inputPart.getBody(InputStream.class, null);
								fileNameSaved = fileNameToSave + String.valueOf(new Date().getTime()) + "." + extention;
								fileName = fileUploadDir + fileNameSaved;
								byte[] bytes = IOUtils.toByteArray(istream);
								writeFile(bytes, fileName);
								switch (i) {
								case 0:
									if (isFileExistInFS(fileName)) {
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjExploi.add(pjDto);
										break;
									}
								case 1:
									if (isFileExistInFS(fileName)) {
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjOptim.add(pjDto);
										break;
									}
								case 2:
									if (isFileExistInFS(fileName)) {
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjDcc.add(pjDto);
										break;
									}

								case 3:
									if (isFileExistInFS(fileName)) {
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjDccEnv.add(pjDto);
										break;
									}

								case 4:
									if (isFileExistInFS(fileName)) {
										pjDto.setFileNameSaved(fileNameSaved);
										pjDto.setOriginFileName(originFileName);
										if(pjDto != null)
											pjQos.add(pjDto);
										break;
									}

								}

							} catch (IOException e) {
								e.getMessage();
							}

						}
					} else {
						isNoMoreFiles = true;
					}
					if (isNoMoreFiles) {
						isNoMoreFiles = false;
						break; // go for next i
					}
				}

			}
			dto.setPjExploi(pjExploi);
			dto.setPjOptim(pjOptim);
			dto.setPjQos(pjQos);
			dto.setPjDcc(pjDcc);
			dto.setPjDccEnv(pjDccEnv);
		}

		if (pjExploi != null && pjExploi.size() > 0) {
			dto.setPjExploi(pjExploi);
		}
		if (pjOptim != null && pjOptim.size() > 0) {
			dto.setPjOptim(pjOptim);
		}
		if (pjDcc != null && pjDcc.size() > 0) {
			dto.setPjDcc(pjDcc);
		}
		if (pjDccEnv != null && pjDccEnv.size() > 0) {
			dto.setPjDccEnv(pjDccEnv);
		}
		if (pjQos != null && pjQos.size() > 0) {
			dto.setPjQos(pjQos);
		}
		return dto;
	}

	@PostMapping("/sendEmails")
	public Long sendEmails(List<EmailDto> dtos) {
		if(dtos != null && dtos.size() > 0) {
			for(EmailDto dto:dtos) {
				emailServiceImpl.sendEmailRequalifByFournisseur(dto);
			}
			return 1L;
		}
		return 0L;
	}	
}
