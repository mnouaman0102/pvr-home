package ma.iam.pvr.services.implementation;

import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.services.interfaces.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class UserServiceImpl implements IUserService {

    private IUtilisateurDao utilisateurDao;

    @Override
    public Utilisateur getUserByUsername(String username) {
        return utilisateurDao.getUserByLogin(username);
    }

    @Resource
    public void setUtilisateurDao(IUtilisateurDao utilisateurDao) {
        this.utilisateurDao = utilisateurDao;
    }

}
