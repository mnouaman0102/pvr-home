package ma.iam.pvr.services.implementation;


import java.util.List;

import ma.iam.pvr.codeError.MyAppErrorCode;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IRemiseControleDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Fournisseur;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.RemiseControle;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.dto.dtos.ConsultMesReportDto;
import ma.iam.pvr.dto.mapper.ConsultMesReportMapper;
import ma.iam.pvr.exception.MyAppException;
import ma.iam.pvr.services.interfaces.IActionService;
import ma.iam.pvr.services.interfaces.IRemiseMesReportService;
import ma.iam.pvr.utils.ActionCode;
import ma.iam.pvr.utils.Constants;
import ma.iam.pvr.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Z.BELGHAOUTI
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class RemiseMesReportServiceImpl implements IRemiseMesReportService {
	
	private final Logger logger = LoggerFactory.getLogger(RemiseMesReportServiceImpl.class);
	
	@Autowired
	private ConsultMesReportMapper consultMesReportMapper;
	
	@Autowired
	private IActionService actionServiceImpl;
	
	@Autowired
	private IUtilisateurDao utilisateurDaoImpl;
	
	@Autowired
	private IRemiseControleDao remiseControleDaoImpl;
	
	@Autowired
	private IMesDao mesDaoImpl;
	
	public List<ConsultMesReportDto> getRemiseMesByCriteres(Long idtRemise) throws Exception {
		//if (!SecurityContextHelper.isFournisseur())
			//throw new Exception(SecurityContextHelper.getUserName() + "Utilisateur non autorise exporter le rapport !");
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		if(idtRemise == null)
			return null;
		RemiseControle remise = remiseControleDaoImpl.get(idtRemise);
		if(remise != null) {
			if(!fournisseur.getId().equals(remise.getFournisseur().getId())) {
				logger.error(Utils.getLogParam() + Constants.errorRemiseEdit);
				throw new MyAppException(MyAppErrorCode.REMISE_EXPORT, Constants.errorRemiseEdit);
			}
			
			List<Mes> domains = mesDaoImpl.getMesListByRemiseId(idtRemise);
			if(domains != null && domains.size() > 0){				
				List<ConsultMesReportDto> dtos = consultMesReportMapper.toDtos(domains);	
				if(dtos != null && dtos.size() > 0){
					actionServiceImpl.saveAction("Module Fournisseur : Exporter le rapport de "
							+ "la Liste Remise MES de taille : " + dtos.size() + " associées à la remise au contrôle ayant IDT : " + idtRemise
							, ActionCode.EXPORT.value());
					logger.info(Utils.getLogParam() + "Module Fournisseur : Exporter le rapport de "
							+ "la Liste Remise MES de taille : " + dtos.size() + " associées à la remise au contrôle ayant IDT : " + idtRemise);									
					return dtos;
				}
			}
		}
		return null;
	}

}

