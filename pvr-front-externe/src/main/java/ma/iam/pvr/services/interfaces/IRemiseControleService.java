package ma.iam.pvr.services.interfaces;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.core.Response;

import ma.iam.pvr.domain.Contrat;
import ma.iam.pvr.dto.dtos.CodeLabelDto;
import ma.iam.pvr.dto.dtos.DrDto;
import ma.iam.pvr.dto.dtos.MesCriDto;
import ma.iam.pvr.dto.dtos.MesRemiseDto;
import ma.iam.pvr.dto.dtos.PjDto;
import ma.iam.pvr.dto.dtos.ProjetDto;
import ma.iam.pvr.dto.dtos.RemiseControleDto;
import ma.iam.pvr.dto.dtos.RemiseCriDto;
import ma.iam.pvr.dto.dtos.RemiseFrmDto;
import ma.iam.pvr.dto.dtos.RemiseSaveDto;
import ma.iam.pvr.dto.dtos.SiteDto;
import ma.iam.pvr.exception.MyAppException;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;


/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IRemiseControleService {
	
	List<PjDto> uploadFile(MultipartFormDataInput input);

	List<MesRemiseDto> getMesByCriteresMultiSites(MesCriDto dto, int pageNumber, int pageSize);
	
	List<Long> getIdMesByCriteres(MesCriDto dto);

	List<String> validSitesCriteres(MesCriDto dto);
	
	int getTotalMesByCriteresMultiSites(MesCriDto dto);
	
	boolean saveRemiseControle(RemiseSaveDto dto) throws ParseException;
	
	MesRemiseDto getMesByIDT(Long idtMes);
	
	List<Contrat> getAllContratsByFounisseur();
	
	List<DrDto> getAllDr();
	
	List<SiteDto> getAllSiteByDr(Long idtDr);
	
	List<ProjetDto> getAllProjects();
	
	List<CodeLabelDto> getAllEtatRemise();
	
	List<RemiseControleDto> getListRemiseControle(RemiseCriDto dto, 
			int pageNumber, int pageSize);
	
	Integer getTotalRemiseControle(RemiseCriDto dto);
	
	Response getPjsRemise(String fileName);
	
	List<MesRemiseDto> getListMesByIDTS(List<Long> list_Mes);
	
	List<MesRemiseDto> getListMesByRemise(Long idtRemise);
	
	boolean deleteRemise(Long idtRemise) throws MyAppException;
	
	RemiseFrmDto getRemiseDraftByIdt(Long idtRemise) throws MyAppException;
	
	Long updateRemiseDraft(RemiseFrmDto remiseDto) throws ParseException, MyAppException;
	
	boolean validerRemiseDraft(Long idtRemise);
}
