package ma.iam.pvr.services.implementation;


import java.util.Date;

import ma.iam.pvr.dao.interfaces.IActionParamDao;
import ma.iam.pvr.domain.ActionParam;
import ma.iam.pvr.services.interfaces.IActionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ActionServiceImpl implements IActionService {
	
	@Autowired
	private IActionParamDao actionParamDaoImpl;

	public void saveAction(String description, String typeOperation) {		
		String user = SecurityContextHolder.getContext().getAuthentication().getName();
		ActionParam ap = new ActionParam();
		if(user != null){
			ap.setUtilisateur(user);
		}
		ap.setDate(new Date());		
		ap.setDescription(description);
		ap.setTypeOperation(typeOperation);
		actionParamDaoImpl.save(ap);		
	}
}
