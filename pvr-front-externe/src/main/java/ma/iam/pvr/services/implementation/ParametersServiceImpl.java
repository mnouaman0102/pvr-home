package ma.iam.pvr.services.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ma.iam.pvr.dao.interfaces.IDrDao;
import ma.iam.pvr.dao.interfaces.IEtatDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IProjetDao;
import ma.iam.pvr.dao.interfaces.ISiteDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Dr;
import ma.iam.pvr.domain.Etat;
import ma.iam.pvr.domain.Fournisseur;
import ma.iam.pvr.domain.Projet;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.dto.dtos.DrDto;
import ma.iam.pvr.dto.dtos.EtatDto;
import ma.iam.pvr.dto.dtos.NumOperationDto;
import ma.iam.pvr.dto.dtos.ProjetDto;
import ma.iam.pvr.dto.dtos.SiteDto;
import ma.iam.pvr.dto.mapper.DrMapper;
import ma.iam.pvr.dto.mapper.EtatMapper;
import ma.iam.pvr.dto.mapper.SiteMapper;
import ma.iam.pvr.services.interfaces.IActionService;
import ma.iam.pvr.services.interfaces.IParametersService;
import ma.iam.pvr.utils.ActionCode;
import ma.iam.pvr.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * @author K.ELBAGUARI
 *
 */
@Service
@Path("/services/parameters")
@Produces("application/json")
@Transactional(rollbackFor = Exception.class)
public class ParametersServiceImpl implements IParametersService {

	private final Logger logger = LoggerFactory.getLogger(ParametersServiceImpl.class);

	@Autowired
	private IDrDao drDaoImpl;
	@Autowired
	private DrMapper drMapper;
	@Autowired
	private ISiteDao siteDaoImpl;
	@Autowired 
	private SiteMapper siteMapper;
	@Autowired
	private IProjetDao projetDaoImpl;
	@Autowired
	private IUtilisateurDao utilisateurDaoImpl;
	@Autowired 
	private IMesDao mesDaoImpl;
	@Autowired 
	private IActionService actionServiceImpl;
	@Autowired
	private IEtatDao etatDaoImpl;
	@Autowired 
	private EtatMapper etatMapper;
	/**
	 * GET LIST PROJECTS
	 */
	@GET
	@Path("/getAllProjects")
	public List<ProjetDto> getAllProjects() {
		List<ProjetDto> dtos = new ArrayList<ProjetDto>();
		List<Projet> projets = projetDaoImpl.findAll();
		if(projets != null && projets.size() > 0){
			for(Projet p: projets){
				ProjetDto dto = new ProjetDto();
				dto.setIdt(p.getId());
				dto.setLabel(p.getLabel());
				dto.setWorkflow(p.getWorkflow());
				dtos.add(dto);
			}
		}
		logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Liste de : " + dtos.size() + " Projets");
		actionServiceImpl.saveAction("Module Fournisseur : Recuperation Liste de : " + dtos.size() + " Projets", ActionCode.CONSULTATION.value());
		return dtos;
	}

	/**
	 * GET List DR
	 */
	@GET
	@Path("/getAllDr")
	public List<DrDto> getAllDr() {
		List<DrDto> dtos = new ArrayList<DrDto>();
		List<Dr> domains = null;
		domains = drDaoImpl.findAll();
		if(domains != null && domains.size() > 0)
			dtos = drMapper.toDtos(domains);	
		logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Liste de : " + dtos.size() + " Drs");
		actionServiceImpl.saveAction("Module Fournisseur : Recuperation Liste de : " + dtos.size() + " Drs", ActionCode.CONSULTATION.value());
		return dtos;	
	}

	/**
	 * GET LIST SITES BY DR	
	 */
	@GET
	@Path("/getAllSitesByDr")
	public List<SiteDto> getAllSiteByDr(@QueryParam("idtDr") Long idtDr) {		 			
		List<Site> domains = siteDaoImpl.getAllSitesByDr(idtDr);
		List<SiteDto> dtos = new ArrayList<SiteDto>();
		String drLabel = "";
		Dr dr = drDaoImpl.get(idtDr);
		if(dr != null)
			drLabel = dr.getLabel();
		if(domains != null && domains.size() > 0)
			dtos = siteMapper.toDtos(domains);
		logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation da la liste de: " + dtos.size() + " Sites par Dr : " + drLabel);
		actionServiceImpl.saveAction("Module Fournisseur : Recuperation da la liste de: " + dtos.size() + " Sites par Dr : " + drLabel, ActionCode.CONSULTATION.value());
		return dtos;
	}	

	/**
	 * GET ALL NUM OPERATIONS FOR FOURNISSEUR
	 */
	@GET
	@Path("/getAllNumOperationsForDeploi")
	public List<NumOperationDto> getAllNumOperations(){
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		Utilisateur user = utilisateurDaoImpl.getUserByLogin(login);
		if(user == null)
			return null;
		Fournisseur fournisseur = user.getFournisseur();
		if(fournisseur == null)
			return null;
		List<Object> numOperations = mesDaoImpl.findAllNumOperationsForFournisseur(fournisseur.getId());
		List<NumOperationDto> numOperList = new ArrayList<NumOperationDto>();
		long i = 0L;
		if(numOperations != null && numOperations.size() > 0){
			for(Object obj: numOperations){
				NumOperationDto dto = new NumOperationDto();
				if(obj != null && !obj.equals("")){
					i++;
					dto.setIdt(i);
					dto.setNumOperation((String) obj);
					numOperList.add(dto);
				}
			}
		}
		logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Liste de : " + numOperList.size() +
				" Numero Operation pour Fournisseur : " + fournisseur.getLabel());
		actionServiceImpl.saveAction("Module Fournisseur : Recuperation Liste de : " + numOperList.size() +
				" Numero Operation pour Fournisseur : " + fournisseur.getLabel(), ActionCode.CONSULTATION.value());
		return numOperList;
	}
	@GET
	@Path("/getAllEtat")
	public List<EtatDto> getAllEtat() {
		List<EtatDto> dtos = null;
		List<Etat> domains = null;
		domains = etatDaoImpl.findAll();
		if(domains != null && domains.size()>0){
			dtos = etatMapper.toDtos(domains);	
			if(dtos != null && dtos.size() > 0)	{
				actionServiceImpl.saveAction("Module Fournisseur : Recuperation Liste des Etats", ActionCode.CONSULTATION.value());
				logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Liste des Etats");
				return dtos;
			}
		}

		return dtos;		
	}
	
	@GET
	@Path("/getOperMesRtEtats")
	public List<EtatDto> getOperMesRtEtat() {
		List<EtatDto> dtos = null;
		List<Etat> domains = null;
		List<Long> idts = new ArrayList<Long>();
		idts.add(1L);
		idts.add(2L);
		idts.add(3L);
		domains = etatDaoImpl.getListEtatsByIdts(idts);
		if(domains != null && domains.size()>0){
			dtos = etatMapper.toDtos(domains);	
			if(dtos != null && dtos.size() > 0)	{
				actionServiceImpl.saveAction("Module Fournisseur : Recuperation Liste des Etats", ActionCode.CONSULTATION.value());
				logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation Liste des Etats");
				return dtos;
			}
		}

		return dtos;		
	}

	@GET
	@Path("/getAllSitesByListDr")
	public List<SiteDto> getAllSiteByListDr(@QueryParam("idtsDr") String idtsDr) {

		if(StringUtil.isNullOrEmpty(idtsDr)) {
			return new ArrayList<SiteDto>();
		}

		List<Long> listIdtDrs = StringUtil.toListLong(idtsDr, ",");
		List<Site> domains = siteDaoImpl.getAllSitesByListDr(listIdtDrs);
		Dr dr = null;
		StringBuilder drLabel = new StringBuilder();
		if (listIdtDrs != null) {
			for(Long idtDr : listIdtDrs) {
				dr = drDaoImpl.get(idtDr);
				if (dr != null) {
					if(drLabel.length() > 0) {
						drLabel.append(", ");
					}
					drLabel.append(dr.getLabel());
				}
			}
		}
		if (domains != null && domains.size() > 0) {
			List<SiteDto> dtos = siteMapper.toDtos(domains);
			if (dtos != null && dtos.size() > 0) {
				logger.info(Utils.getLogParam() + "Module Fournisseur : Recuperation da la liste des Sites par liste des Dr : " + drLabel.toString());
				return dtos;
			}

		}
		return null;
	}
}
