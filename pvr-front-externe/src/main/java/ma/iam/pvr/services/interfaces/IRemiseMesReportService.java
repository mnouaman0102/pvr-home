/**
 * 
 */
package ma.iam.pvr.services.interfaces;

import java.util.List;

import ma.iam.pvr.dto.dtos.ConsultMesReportDto;


/**
 * @author Z.BELGHAOUTI
 *
 */
public interface IRemiseMesReportService {
	
	List<ConsultMesReportDto> getRemiseMesByCriteres(Long idtRemise) throws Exception;

}
