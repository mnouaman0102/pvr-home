package ma.iam.pvr.services.interfaces;

import java.util.List;

import ma.iam.pvr.dto.dtos.DrDto;
import ma.iam.pvr.dto.dtos.EtatDto;
import ma.iam.pvr.dto.dtos.NumOperationDto;
import ma.iam.pvr.dto.dtos.ProjetDto;
import ma.iam.pvr.dto.dtos.SiteDto;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IParametersService {
	List<ProjetDto> getAllProjects();
	List<DrDto> getAllDr() ;
	List<SiteDto> getAllSiteByDr(Long idtDr);
	List<NumOperationDto> getAllNumOperations();
	List<EtatDto> getAllEtat();
	List<EtatDto> getOperMesRtEtat();	
	List<SiteDto> getAllSiteByListDr(String idtsDr);
	}

