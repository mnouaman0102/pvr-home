/**
 * 
 */
package ma.iam.pvr.services.interfaces;

import java.util.List;

import ma.iam.pvr.dto.dtos.ConsultCriDto;
import ma.iam.pvr.dto.dtos.ConsultDto;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

/**
 * @author K.ELBAGUARI
 *
 */
public interface IConsultationService {
	
	List<ConsultDto> consultMesByCriteresPagination(ConsultCriDto dto, int pageNumber,  int pageSize);
	int consultTotalMesByCriteres(ConsultCriDto dto);	
	ResponseEntity<Resource> getPjs(String fileName);
	
	
}
