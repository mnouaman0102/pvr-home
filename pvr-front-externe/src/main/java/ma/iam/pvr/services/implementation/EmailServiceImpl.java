package ma.iam.pvr.services.implementation;

import java.util.Date;
import java.util.List;

import ma.iam.pvr.dao.interfaces.IEmailDao;
import ma.iam.pvr.dao.interfaces.IEntiteDao;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Email;
import ma.iam.pvr.domain.Entite;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.Utilisateur;
import ma.iam.pvr.dto.dtos.EmailDto;
import ma.iam.pvr.services.interfaces.IActionService;
import ma.iam.pvr.services.interfaces.IEmailService;
import ma.iam.pvr.utils.ActionCode;
import ma.iam.pvr.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Z.BELGHAOUTI && K.ELBAGUARI
 *
 */

@Service
public class EmailServiceImpl implements IEmailService {

	private final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
	@Autowired
	private IMesDao mesDaoImpl;
	@Autowired
	private IEmailDao emailDaoImpl;
	@Autowired
	private IEntiteDao entiteDaoImpl;
	@Autowired
	private IUtilisateurDao utilisateurDaoImpl;
	@Autowired 
	private IActionService actionServiceImpl;

	private String pathInterneProject;

	public void setPathInterneProject(String pathInterneProject) {
		this.pathInterneProject = pathInterneProject;
	}

	public Long sendEmails(List<EmailDto> dtos) {
		if (dtos != null && dtos.size() > 0) {
			for (EmailDto dto : dtos) {
				sendEmail(dto);
			}
			return 1L;
		}
		return 0L;
	}

	public void sendEmailRequalifByFournisseur(EmailDto emailDto ){
		if(emailDto != null) {
			Mes mes = mesDaoImpl.get(emailDto.getIdtMes());
			Entite entite = entiteDaoImpl.get(emailDto.getIdtEntite());
			Boolean isQualif = emailDto.isQualif();
			if( mes!=null && entite!=null && isQualif != null ) {
				Email email = new Email();
				email.setContenu("");
				email.setEntite(entite);
				email.setMes(mes);
				email.setTemplate("requalif");
				email.setEnvoi(false);
				email.setDateCreation(new Date());
				String pathProject = pathInterneProject;
				if(pathProject != null && !pathProject.equals("")){
					if(entite.getId() != null && entite.getId().equals(2L)){
						email.setLink(pathProject+"/index.html#/exploitation?numOperation=" + mes.getNumOperation());
					}

					if(entite.getId() != null && entite.getId().equals(3L)){
						email.setLink(pathProject+"/index.html#/optim?numOperation=" + mes.getNumOperation());
					}

					if(entite.getId() != null && entite.getId().equals(4L)){
						email.setLink(pathProject+"/index.html#/dcc?numOperation=" + mes.getNumOperation());
					}

					if(entite.getId() != null && entite.getId().equals(5L)){
						email.setLink(pathProject+"/index.html#/dcc-env?numOperation=" + mes.getNumOperation());
					}

					if(entite.getId() != null && entite.getId().equals(6L)){
						email.setLink(pathProject+"/index.html#/qos?numOperation=" + mes.getNumOperation());
					}

				}
				Utilisateur utilisateur = utilisateurDaoImpl.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
				if(utilisateur == null)
					return ;
				if(utilisateur.getFournisseur() == null)
					return;

				email.setUtilisateur(utilisateur);
				emailDaoImpl.save(email);
				logger.info(Utils.getLogParam() + "Module Fournisseur : Sauvegarde Email de requalification pour l'entite : " + entite.getLabel() +
						" par Fournisseur : " + utilisateur.getFournisseur().getLabel());

				actionServiceImpl.saveAction("Module Fournisseur : Sauvegarde Email de requalification pour l'entite : " + entite.getLabel() +
						" par Fournisseur : " + utilisateur.getFournisseur().getLabel(), ActionCode.ADD.value());
			}
		}
	}
	private void sendEmail(EmailDto dto) {
		if (dto != null &&  dto.getIdtMes() != null) {
			Mes mes = mesDaoImpl.get(dto.getIdtMes());
			Entite entite = entiteDaoImpl.get(dto.getIdtEntite());
			if ("UPDATE_RADIO".equals(dto.getTypeEmail()))
				this.sendEmailUpdateOperationRadioByFournisseur(mes, entite);
			else if ("UPDATE_ENV".equals(dto.getTypeEmail()))
				this.sendEmailUpdateOperationEnvByFournisseur(mes, entite);
		}
	}

	private void sendEmailUpdateOperationRadioByFournisseur(Mes mes, Entite e) {

		if(mes != null && e != null){
			Email email = new Email();
			email.setContenu("");
			email.setEntite(e);
			email.setTemplate("update");
			email.setMes(mes);
			email.setEnvoi(false);
			email.setDateCreation(new Date());
			Utilisateur utilisateur = utilisateurDaoImpl.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
			if(utilisateur == null)
				return ;
			if(utilisateur.getFournisseur() == null)
				return;

			email.setUtilisateur(utilisateur);

			String pathProject = pathInterneProject;
			if (pathProject != null && !pathProject.equals("")) {
				if (e.getId() != null && e.getId().equals(2L)){
					email.setLink(pathProject + "/index.html#/exploitation?numOperation=" + mes.getNumOperation());
				}

				if (e.getId() != null && e.getId().equals(3L)){
					email.setLink(pathProject + "/index.html#/optim?numOperation=" + mes.getNumOperation());
				}

				if (e.getId() != null && e.getId().equals(4L)){
					email.setLink(pathProject + "/index.html#/dcc?numOperation=" + mes.getNumOperation());
				}

				if (e.getId() != null && e.getId().equals(6L)){
					email.setLink(pathProject + "/index.html#/qos?numOperation=" + mes.getNumOperation());
				}

			}
			emailDaoImpl.save(email);
			logger.info(Utils.getLogParam() + "Module Fournisseur : Sauvegarde Email de la modification de l'operation ayant IDT: " + mes.getId() +
					" a l'entite: " + e.getLabel() +
					" par Fournisseur : " + utilisateur.getFournisseur().getLabel());

			actionServiceImpl.saveAction("Module Fournisseur : Sauvegarde Email de la modification de l'operation ayant IDT: " + mes.getId() +
					" a l'entite: " + e.getLabel() +
					" par Fournisseur : " + utilisateur.getFournisseur().getLabel(), ActionCode.ADD.value());
		}

	}

	private void sendEmailUpdateOperationEnvByFournisseur(Mes mes, Entite entite) {

		if(mes != null && entite != null){
			Email email = new Email();
			email.setContenu("");
			email.setEntite(entite);
			email.setTemplate("update");
			email.setMes(mes);
			email.setEnvoi(false);
			email.setDateCreation(new Date());
			Utilisateur utilisateur = utilisateurDaoImpl.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
			if(utilisateur == null)
				return ;
			if(utilisateur.getFournisseur() == null)
				return;

			email.setUtilisateur(utilisateur);

			String pathProject = pathInterneProject;
			if (pathProject != null && !pathProject.equals("")) {
				email.setLink(pathProject + "/index.html#/dcc-env?numOperation=" + mes.getNumOperation());
			}
			emailDaoImpl.save(email);
			logger.info(Utils.getLogParam() + "Module Fournisseur : Sauvegarde Email de la modification de l'operation ayant IDT: " + mes.getId() + 
					" a l'entite DCC ENV " +
					" par Fournisseur : " + utilisateur.getFournisseur().getLabel());

			actionServiceImpl.saveAction("Module Fournisseur : Sauvegarde Email de la modification de l'operation ayant IDT: " + mes.getId() + 
					" a l'entite DCC ENV " +
					" par Fournisseur : " + utilisateur.getFournisseur().getLabel(), ActionCode.ADD.value());
		}

	}

}
