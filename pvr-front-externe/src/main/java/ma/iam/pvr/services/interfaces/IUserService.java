package ma.iam.pvr.services.interfaces;

import ma.iam.pvr.domain.Utilisateur;

public interface IUserService {
    Utilisateur getUserByUsername(String username);
}
