/**
 * 
 */
package ma.iam.pvr.services.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Contrat;
import ma.iam.pvr.domain.Dr;
import ma.iam.pvr.domain.Etat;
import ma.iam.pvr.domain.Fournisseur;
import ma.iam.pvr.domain.Projet;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.dto.dtos.ConsultMesReportDto;


/**
 * @author K.ELBAGUARI
 *
 */
public interface IConsultMesReportService {
	
	List<ConsultMesReportDto> consultMesByCriteres(String idtsContrat, String idtsProjet, String idtsEtat, String idtsDr, String idtsSite) throws Exception;
	
	Fournisseur getFournisseurByIdt(Long idtFournisseur);
	
	Contrat getContratByIdt(Long idtContrat);	
	
	Projet getProjetByIdt(Long Projet);
	
	Etat getEtatByIdt(Long idtEtat);

	Dr getDrByIdt(Long idtDr);

	Site getSiteByIdt(Long idtSite);

}
