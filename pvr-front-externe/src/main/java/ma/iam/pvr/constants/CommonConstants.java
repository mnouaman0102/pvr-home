package ma.iam.pvr.constants;

public final class CommonConstants {
    public static class Numbers {
        public final static int SEVEN = 7;
    }
    public static class Authentication{
        public final static String AUTHORIZATION= "Authorization";
        public final static String BEARER_KEY= "Bearer ";
        public static final String SECRET_KEY = "HereYourPersonalSecretKeyForSmartLocateAccount256558";
        public static final long EXPIRATION_TIME = 864_000_000; // 24 HOURS
    }
}
