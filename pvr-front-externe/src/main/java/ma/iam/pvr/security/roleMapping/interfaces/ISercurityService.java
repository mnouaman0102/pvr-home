package ma.iam.pvr.security.roleMapping.interfaces;

import java.util.List;

/**
 * Retourne le rôle actuellement conencté
 * @author marwane
 */
public interface ISercurityService {
	/**
	 * Permet de récupérer les rôles de l'utilisateur courant
	 * 
	 * @return
	 */
	public List<String> getAuthentifiedRoles();
	
	/**
	 * Permet de récupérer le login de l'utilisateur courant
	 * 
	 * @return
	 */
	public String getCurrentLogin();
}
