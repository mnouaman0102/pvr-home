//package ma.iam.pvr.security;
//
//import ma.iam.pvr.domain.Profil;
//
//import org.springframework.security.GrantedAuthorityImpl;
//
//public class AppGrantedAuthority extends GrantedAuthorityImpl {
//
//	private static final long serialVersionUID = 1603087460533012780L;
//	/**
//	 *
//	 * @param owns
//	 */
//	public AppGrantedAuthority(String owns) {
//		super(owns);
//	}
//	/**
//	 *
//	 * @param profil
//	 */
//	public AppGrantedAuthority(Profil profil) {
//		super("ROLE_" + profil.getLabel().replaceAll("\\s+", "_").toUpperCase());
//	}
//
//}
