//
//package ma.iam.pvr.security.sessionManagement;
//
//import java.util.Iterator;
//
//import javax.naming.directory.DirContext;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.dao.DataAccessException;
//import org.springframework.ldap.NamingException;
//import org.springframework.ldap.core.ContextSource;
//import org.springframework.ldap.core.DirContextOperations;
//import org.springframework.ldap.core.DistinguishedName;
//import org.springframework.ldap.core.support.LdapContextSource;
//
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.ldap.SpringSecurityLdapTemplate;
//
//import org.springframework.security.ldap.authentication.BindAuthenticator;
//import org.springframework.security.web.authentication.WebAuthenticationDetails;
//import org.springframework.util.Assert;
//
///**
// *
// * @author rabbah && K.ELBAGUARI
// */
//public class IamBindAuthenticator extends BindAuthenticator {
//
//    private static final Log logger = LogFactory.getLog(IamBindAuthenticator.class);
//
//    private SessionTracker sessionTracker;
//
//    public IamBindAuthenticator(LdapContextSource contextSource) {
//        super(contextSource);
//    }
//
//    @Override
//    public DirContextOperations authenticate(Authentication authentication) {
//        DirContextOperations user = null;
//        Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class, authentication,
//                "Can only process UsernamePasswordAuthenticationToken objects");
//
//        String username = authentication.getName();
//        String password = (String)authentication.getCredentials();
//
//        // If DN patterns are configured, try authenticating with them directly
//        Iterator dns = getUserDns(username).iterator();
//
//        while (dns.hasNext() && user == null) {
//            user = bindWithDn((String) dns.next(), username, password);
//        }
//
//        // Otherwise use the configured locator to find the user
//        // and authenticate with the returned DN.
//        if (user == null && getUserSearch() != null) {
//            DirContextOperations userFromSearch = getUserSearch().searchForUser(username);
//            user = bindWithDn(userFromSearch.getDn().toString(), username, password);
//        }
//
//        if (user == null) {
//            throw new BadCredentialsException(
//                    messages.getMessage("BindAuthenticator.badCredentials", "Bad credentials"));
//        }
//
//        Object o = authentication.getDetails();
//        if(o instanceof WebAuthenticationDetails) {
//            WebAuthenticationDetails wad = (WebAuthenticationDetails) o;
//            boolean allowed = sessionTracker.isUserAllowedToConnect(username,
//                    wad.getRemoteAddress(), wad.getSessionId());
//            /*if(!allowed) {
//                throw new MultipleSessionException("User not allowed to have multiple sessions, please disconnect first");
//            }*/
//        }
//
//
//        return user;
//    }
//
//    private DirContextOperations bindWithDn(String userDn, String username, String password) {
//        SpringSecurityLdapTemplate template = new SpringSecurityLdapTemplate(
//                new IamBindAuthenticator.BindWithSpecificDnContextSource((LdapContextSource) getContextSource(), userDn, password));
//
//        try {
//            return template.retrieveEntry(userDn, getUserAttributes());
//
//        } catch (BadCredentialsException e) {
//            // This will be thrown if an invalid user name is used and the method may
//            // be called multiple times to try different names, so we trap the exception
//            // unless a subclass wishes to implement more specialized behaviour.
//            handleBindException(userDn, username, e.getCause());
//        }
//
//        return null;
//    }
//
//    /**
//     * Allows subclasses to inspect the exception thrown by an attempt to bind with a particular DN.
//     * The default implementation just reports the failure to the debug log.
//     */
//    protected void handleBindException(String userDn, String username, Throwable cause) {
//        if (logger.isDebugEnabled()) {
//            logger.debug("Failed to bind as " + userDn + ": " + cause);
//        }
//    }
//
//    private class BindWithSpecificDnContextSource implements ContextSource {
//        private LdapContextSource ctxFactory;
//        DistinguishedName userDn;
//        private String password;
//
//        public BindWithSpecificDnContextSource(LdapContextSource ctxFactory, String userDn, String password) {
//            this.ctxFactory = ctxFactory;
//            this.userDn = new DistinguishedName(userDn);
//            this.userDn.prepend(ctxFactory.getBaseLdapPath());
//            this.password = password;
//        }
//
//        public DirContext getReadOnlyContext() throws DataAccessException {
//            return ctxFactory.getReadWriteContext();
//        }
//
//        public DirContext getReadWriteContext() throws DataAccessException {
//            return getReadOnlyContext();
//        }
//
//        @Override
//        public DirContext getContext(String s, String s1) throws NamingException {
//            return null;
//        }
//
//    }
//
//    public void setSessionTracker(SessionTracker sessionTracker) {
//        this.sessionTracker = sessionTracker;
//    }
//
//}
