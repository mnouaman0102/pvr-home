///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package ma.iam.pvr.security.sessionManagement;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.CopyOnWriteArrayList;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.security.concurrent.SessionRegistryImpl;
//
///**
// *
// * @author rabbah && K.ELBAGUARI
// */
//public class SessionTracker {
//
//    private final Map<String, String> sessions
//            = new ConcurrentHashMap<String, String>();
//    private final Map<String, List<Session>> usersSessions
//            = new ConcurrentHashMap<String, List<Session>>();
//
//    private boolean disableMultipleSessionsSameHost;
//    private boolean disableMultipleSessionsMultiHosts;
//    private long sessionTimeout;
//
//    private SessionRegistryImpl sessionRegistry;
//
//    private static final Log logger = LogFactory.getLog(SessionTracker.class);
//
//    public boolean isUserAllowedToConnect(String login, String host, String sessionId) {
//        if(!disableMultipleSessionsSameHost && !disableMultipleSessionsMultiHosts) {
//            return true;
//        }
//        long now = new Date().getTime();
//        List<Session> ss = usersSessions.get(login);
//        if(ss == null) {
//            logger.info("First session for user " + login);
//            ss = new CopyOnWriteArrayList<Session>();
//            usersSessions.put(login, ss);
//        }
//        /**
//         * sessionId != null => Test For Mobile
//         */
//        if(ss.isEmpty() && sessionId != null) {
//            logger.info("No session for user " + login);
//            Session session = new Session(login, sessionId, now, host);
//            ss.add(session);
//            sessions.put(sessionId, login);
//            return true;
//        }
//
//        for (Session s : ss) {
//            long timeFromLastInteraction = now - s.getLastInteractionTime();
//            if(timeFromLastInteraction > sessionTimeout) {
//                logger.info("Session" + s.getSessionId() + " expired for user " + login);
//                ss.remove(s);
//                sessions.remove(s.getSessionId());
//            } else {
//                if(disableMultipleSessionsMultiHosts &&
//                        !s.getRemoteHost().equals(host)) {
//                    logger.info("User " + login + " try to connect from host " +
//                            host + " but not allowed to have multiples sessions old session id"
//                            + s.getSessionId());
//
//                    s.setExpired(true);
//                    //return false;
//                } else if(disableMultipleSessionsSameHost  &&
//                        s.getRemoteHost().equals(host)){
//                    logger.info("User " + login + " try to connect from host " +
//                            host + " but not allowed to have multiples sessions old session id"
//                            + s.getSessionId());
//
//                    s.setExpired(true);
//                }
//            }
//        }
//        /**
//         * sessionId != null => Test For Mobile
//         */
//        if(sessionId != null){
//            logger.info("adding the new session " + sessionId + " for user " + login + " from " + host);
//            Session session = new Session(login, sessionId, now, host);
//            ss.add(session);
//            sessions.put(sessionId, login);
//        }
//
//        return true;
//    }
//
//    public void refreshSession(String sessionId) throws MultipleSessionException {
//        /**
//         * sessionId != null => Test For Mobile
//         */
//        if(sessionId != null){
//            logger.info("Refreshing session " + sessionId);
//            long now = new Date().getTime();
//            String login = sessions.get(sessionId);
//            if(login != null) {
//                List<Session> ss = usersSessions.get(login);
//                if(ss != null) {
//                    for (Session session : ss) {
//                        if(session.getSessionId().equals(sessionId)) {
//                            session.setLastInteractionTime(now);
//                            if(session.isExpired()) {
//                                ss.remove(session);
//                                sessions.remove(session.getSessionId());
//                                throw new MultipleSessionException("Session was opened in another location, this session will be close");
//                            }
//                            break;
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public void disconnectSession(String sessionId) {
//        /**
//         * sessionId != null => Test For Mobile
//         */
//        if(sessionId != null){
//            logger.info("disconnection session " + sessionId);
//            String login = sessions.get(sessionId);
//            if(login != null) {
//                logger.info("disconnection user " + login +  " from session " + sessionId);
//                List<Session> ss = usersSessions.get(login);
//
//                if (ss != null) {
//                    Session s = null;
//                    for (Session session : ss) {
//                        if (session.getSessionId().equals(sessionId)) {
//                            s = session;
//                            break;
//                        }
//                    }
//                    if (s != null) {
//                        ss.remove(s);
//                    }
//                }
//                sessions.remove(sessionId);
//            }
//        }
//
//    }
//
//    public void setDisableMultipleSessionsSameHost(boolean disableMultipleSessionsSameHost) {
//        this.disableMultipleSessionsSameHost = disableMultipleSessionsSameHost;
//    }
//
//    public void setDisableMultipleSessionsMultiHosts(boolean disableMultipleSessionsMultiHosts) {
//        this.disableMultipleSessionsMultiHosts = disableMultipleSessionsMultiHosts;
//    }
//
//    public void setSessionTimeout(long sessionTimeout) {
//        this.sessionTimeout = sessionTimeout;
//    }
//
//    public void setSessionRegistry(SessionRegistryImpl sessionRegistry) {
//        this.sessionRegistry = sessionRegistry;
//    }
//
//}
