//package ma.iam.pvr.security;
//
//
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.dao.DataAccessException;
//import org.springframework.security.providers.dao.DaoAuthenticationProvider;
//import org.springframework.security.userdetails.UserDetails;
//import org.springframework.security.userdetails.UsernameNotFoundException;
//import org.springframework.transaction.annotation.Transactional;
///**
// * for spring-security xml file
// *
// */
//@Transactional(rollbackFor = {Exception.class})
//public class AppUserDetailsServiceImpl implements org.springframework.security.userdetails.UserDetailsService {
//	private final Logger logger = LoggerFactory.getLogger(getClass());
//    /**
//     * Locates the user based on the username. In the actual implementation, the search may possibly be case
//     * insensitive, or case insensitive depending on how the implementaion instance is configured. In this case, the
//     * <code>UserDetails</code> object that comes back may have a username that is of a different case than what was
//     * actually requested..
//     *
//     * @param username the username presented to the {@link DaoAuthenticationProvider}
//     * @return a fully populated user record (never <code>null</code>)
//     * @throws UsernameNotFoundException if the user could not be found or the user has no GrantedAuthority
//     * @throws DataAccessException                        if user could not be found for a repository-specific reason
//     */
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
//    	logger.info("******* in AppUserDetailsServiceImpl.loadUserByUsername");
//        return null;
//    }
//}
