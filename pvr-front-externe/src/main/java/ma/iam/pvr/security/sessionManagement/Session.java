package ma.iam.pvr.security.sessionManagement;

/**
 *
 * @author rabbah && K.ELBAGUARI
 */
public class Session {
    private String login;
    private String sessionId;
    private long lastInteractionTime;
    private String remoteHost;
    private boolean expired;

    public Session() {
    }

    
    public Session(String login, String sessionId, long lastInteractionTime, String remoteHost) {
        this.login = login;
        this.sessionId = sessionId;
        this.lastInteractionTime = lastInteractionTime;
        this.remoteHost = remoteHost;
        expired = false;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getLastInteractionTime() {
        return lastInteractionTime;
    }

    public void setLastInteractionTime(long lastInteractionTime) {
        this.lastInteractionTime = lastInteractionTime;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }
    
    
}
