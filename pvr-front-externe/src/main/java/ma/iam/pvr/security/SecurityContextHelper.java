//package ma.iam.pvr.security;
//
//import org.springframework.security.GrantedAuthority;
//import org.springframework.security.context.SecurityContextHolder;
//
//
///**
// *
// * @author K.ELBAGUARI
// *
// */
//public class SecurityContextHelper {
//
//	public final static String FOURNISSEUR = "ROLE_FOURNISSEUR";
//
//	public static String getUserName() {
//		return SecurityContextHolder.getContext().getAuthentication().getName();
//	}
//
//	public static boolean isFournisseur() {
//		GrantedAuthority[] profils = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
//		for (GrantedAuthority g : profils) {
//			if (FOURNISSEUR.equals(g.getAuthority())) {
//				return true;
//			}
//		}
//		return false;
//	}
//}
