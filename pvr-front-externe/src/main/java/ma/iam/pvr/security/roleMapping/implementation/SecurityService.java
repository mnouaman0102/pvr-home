package ma.iam.pvr.security.roleMapping.implementation;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.security.roleMapping.interfaces.ISercurityService;
import ma.iam.pvr.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Service
@Transactional(rollbackFor = {Exception.class})
@RequestMapping("services/security")
public class SecurityService implements ISercurityService {
	private final Logger logger = LoggerFactory.getLogger(SecurityService.class);	
    /**
     * pour loginService
     * @return 
     */
    @GetMapping("/getAuthentifiedRoles")
    public List<String> getAuthentifiedRoles() {
        GrantedAuthority[] listAuth = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray(new GrantedAuthority[0]);
        List<String> retour = new ArrayList<String>();
        
        for(GrantedAuthority au:listAuth){
            retour.add(au.toString());
        }
        logger.info(Utils.getLogParam() + "L'utilisateur courant a ete authentifie avec les roles : " + retour );
      return retour;
    }
    /**
     * pour indexController et ajaxService
     */
    @GetMapping("/getCurrentLogin")
	public String getCurrentLogin() {
    	 String login = SecurityContextHolder.getContext().getAuthentication().getName();    	
    	 logger.info(Utils.getLogParam() + "L'utilisateur courant a ete authentifie avec le login : " + login);
		return login;
	}
    
}
