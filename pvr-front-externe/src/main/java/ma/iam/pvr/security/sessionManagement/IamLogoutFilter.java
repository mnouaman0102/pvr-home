//package ma.iam.pvr.security.sessionManagement;
//
//import java.io.IOException;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletResponseWrapper;
//
///**
// *
// * @author rabbah && K.ELBAGUARI
// */
//public class IamLogoutFilter extends SpringSecurityFilter {
//
//    private final String filterProcessesUrl = "/j_spring_security_logout";
//    private SessionTracker sessionTracker;
//
//    @Override
//    public void doFilterHttp(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
//            ServletException {
//
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        if(auth != null) {
//            Object o = auth.getDetails();
//            if (o instanceof WebAuthenticationDetails) {
//                WebAuthenticationDetails wad = (WebAuthenticationDetails) o;
//                if (request != null && requiresLogout(request)) {
//                    sessionTracker.disconnectSession(wad.getSessionId());
//                } else {
//                    try {
//                        sessionTracker.refreshSession(wad.getSessionId());
//                    } catch (MultipleSessionException multipleSessionException) {
//                        logger.error("User already connected in a new session will be logout");
//                        sessionTracker.disconnectSession(wad.getSessionId());
//                        disconnect(request, response);
//                        return;
//
//                        // throw multipleSessionException;
//                    }
//                }
//            }
//        }
//
//        chain.doFilter(request, response);
//    }
//
//    private void disconnect(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            request.getSession().invalidate();
//            response = new HttpServletResponseWrapper(response);
//            String encodedRedirectURL = response.encodeRedirectURL(request.getContextPath() + "/login.html");
//            response.setHeader("Server", "IAM SERVER");
//            response.setHeader("MULTISESSIONEXCEPTION", "Une autre session est en cours d'utilisation, cette session va être fermée");
//            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//            response.sendRedirect(encodedRedirectURL);
//        } catch (IOException ex) {
//            logger.error(ex.getMessage());
//        }
//    }
//    protected boolean requiresLogout(HttpServletRequest request) {
//        String uri = request.getRequestURI();
//        int pathParamIndex = uri.indexOf(';');
//
//        if (pathParamIndex > 0) {
//            // strip everything from the first semi-colon
//            uri = uri.substring(0, pathParamIndex);
//        }
//
//        int queryParamIndex = uri.indexOf('?');
//
//        if (queryParamIndex > 0) {
//            // strip everything from the first question mark
//            uri = uri.substring(0, queryParamIndex);
//        }
//
//        if ("".equals(request.getContextPath())) {
//            return uri.endsWith(filterProcessesUrl);
//        }
//
//        return uri.endsWith(request.getContextPath() + filterProcessesUrl);
//    }
//
// //   @Override
//    public int getOrder() {
//        return FilterChainOrder.LOGOUT_FILTER - 10;
//    }
//
//    public void setSessionTracker(SessionTracker sessionTracker) {
//        this.sessionTracker = sessionTracker;
//    }
//
//
//}
