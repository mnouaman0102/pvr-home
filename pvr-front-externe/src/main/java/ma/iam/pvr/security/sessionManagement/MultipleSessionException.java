///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package ma.iam.pvr.security.sessionManagement;
//
//import org.springframework.security.AuthenticationException;
//
///**
// *
// * @author rabbah && K.ELBAGUARI
// */
//public class MultipleSessionException extends AuthenticationException {
//
//    /**
//	 *
//	 */
//	private static final long serialVersionUID = 1167442421767714812L;
//
//	/**
//     * Constructs a <code>BadCredentialsException</code> with the specified
//     * message.
//     *
//     * @param msg the detail message
//     */
//    public MultipleSessionException(String msg) {
//        super(msg);
//    }
//
//    public MultipleSessionException(String msg, Object extraInformation) {
//        super(msg, extraInformation);
//    }
//
//    /**
//     * Constructs a <code>BadCredentialsException</code> with the specified
//     * message and root cause.
//     *
//     * @param msg the detail message
//     * @param t root cause
//     */
//    public MultipleSessionException(String msg, Throwable t) {
//        super(msg, t);
//    }
//}
