//package ma.iam.pvr.security;
//
//import java.io.IOException;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletResponseWrapper;
//import javax.servlet.http.HttpSession;
//import javax.ws.rs.core.MediaType;
//
//import ma.iam.pvr.exception.CompteSuspenduException;
//import ma.iam.pvr.security.sessionManagement.MultipleSessionException;
//
//import org.springframework.security.AuthenticationException;
//import org.springframework.security.BadCredentialsException;
//import org.springframework.security.CredentialsExpiredException;
//import org.springframework.security.DisabledException;
//import org.springframework.security.GrantedAuthority;
//import org.springframework.security.LockedException;
//import org.springframework.security.context.SecurityContextHolder;
//import org.springframework.security.ui.webapp.AuthenticationProcessingFilter;
///**
// * for spring-security xml file
// *
// */
//public class SecurityFilter extends AuthenticationProcessingFilter {
//
//	private static StringBuilder responseContent;
//
//	@Override
//	protected void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
//		response = new HttpServletResponseWrapper(response);
//
//		response.setHeader("Content-Type", "application/json;charset=UTF-8");
//		response.setCharacterEncoding("UTF-8");
//		response.setContentType("application/json;charset=UTF-8");
//		response.setHeader("Server", "IAM SERVER");
//
//		if (url.equals(this.getDefaultTargetUrl())) {
//			StringBuilder builder = new StringBuilder("{\"profiles\":[\"\"");
//			GrantedAuthority[] listAuth = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
//			for (GrantedAuthority au : listAuth) {
//				String row = ",\"" + au + "\"";
//				builder.append(row);
//			}
//			builder.append("]}");
//
//			response.getWriter().print(builder.toString());
//			response.getWriter().flush();
//
//			response.setStatus(HttpServletResponse.SC_OK);
//		} else if (url.equals(this.getAuthenticationFailureUrl())) {
//			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//
//			if (responseContent != null) {
//
//				response.getWriter().print(responseContent.toString());
//			} else {
//				response.getWriter().print("");
//			}
//			response.flushBuffer();
//
//		} else {
//
//			super.sendRedirect(request, response, url);
//		}
//	}
//
//	@Override
//	protected void unsuccessfulAuthentication(HttpServletRequest request,
//			HttpServletResponse response, AuthenticationException failed)
//					throws IOException {
//		System.setProperty( "file.encoding", "UTF-8" );
//		logger.error("******* in SecurityFilter.unsuccessfulAuthentication:" + failed.getMessage());
//		SecurityContextHolder.getContext().setAuthentication(null);
//
//		logger.debug("Unsuccessful Login");
//		if (logger.isDebugEnabled()) {
//			logger.debug("Updated SecurityContextHolder to contain null Authentication");
//			logger.info("Updated SecurityContextHolder to contain null Authentication");
//		}
//
//		if (logger.isDebugEnabled()) {
//			logger.debug("Authentication request failed: " + failed.getMessage());
//			logger.info("Authentication request failed: " + failed.getMessage());
//		}
//		try {
//
//			HttpSession session = request.getSession(false);
//			if (session != null || getAllowSessionCreation()) {
//				request.getSession().setAttribute(
//						SPRING_SECURITY_LAST_EXCEPTION_KEY, failed);
//			}
//
//		} catch (Exception ignored) {
//			logger.info("HttpSession failed: " + ignored.getMessage());
//		}
//
//		onUnsuccessfulAuthentication(request, response, failed);
//		getRememberMeServices().loginFail(request, response);
//		response.setContentType(MediaType.APPLICATION_JSON);
//
//		if (failed instanceof BadCredentialsException) {
//			responseContent = new StringBuilder("{\"code\":\"F001\", \"description\":\"Les paramètres de connexion sont erronés\"}");
//
//		} else if (failed instanceof DisabledException) {
//			responseContent = new StringBuilder("{\"code\":\"F002\", \"description\":\"Compte désactivé\"}");
//
//		} else if (failed instanceof LockedException) {
//			responseContent = new StringBuilder("{\"code\":\"F003\", \"description\":\"Compte verrouillé\"}");
//
//		} else if (failed instanceof CredentialsExpiredException) {
//			responseContent = new StringBuilder("{\"code\":\"F004\", \"description\":\"Mot de passe expiré\"}");
//
//		} else if (failed instanceof CompteSuspenduException) {
//			responseContent = new StringBuilder("{\"code\":\"F005\", \"description\":\"Compte Suspendu\"}");
//
//		} else if (failed instanceof MultipleSessionException) {
//            responseContent = new StringBuilder("{\"code\":\"F006\", \"description\":\"Une autre session est en cours d'utilisation, cette session va être fermée\"}");
//
//        }
//
//		this.sendRedirect(request, response, this.getAuthenticationFailureUrl());
//	}
//
//}
