package ma.iam.pvr.security.roleMapping.implementation;


import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import ma.iam.pvr.dto.dtos.UtilisateurDto;
import ma.iam.pvr.security.roleMapping.interfaces.ISearchService;

import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.AbstractContextMapper;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.stereotype.Service;
/**
 * for spring-security xml file
 *
 */
@Service
public class SearchServiceImpl implements ISearchService {
	private String userBaseDn;
	private DefaultSpringSecurityContextSource contextSource;

	protected ContextMapper getContextMapper() {
		return new PersonContextMapper();
	}

	protected static class PersonContextMapper extends AbstractContextMapper {
		public Object doMapFromContext(DirContextOperations context) {
			UtilisateurDto person = new UtilisateurDto();
			person.setNom(context.getStringAttribute("givenName"));
//			person.setNom(context.getStringAttribute("cn"));
			person.setPrenom(context.getStringAttribute("sn"));
			person.setEmail(context.getStringAttribute("mail"));
			person.setMatricule(context.getStringAttribute("employeeID"));
			person.setLogin(context.getStringAttribute("sAMAccountName"));
//			person.setLogin(context.getStringAttribute("uid"));
			return person;
		}
	}

	public String getUserBaseDn() {
		return userBaseDn;
	}

	public void setUserBaseDn(String userBaseDn) {
		this.userBaseDn = userBaseDn;
	}

	public DefaultSpringSecurityContextSource getContextSource() {
		return contextSource;
	}

	public void setContextSource(DefaultSpringSecurityContextSource contextSource) {
		this.contextSource = contextSource;
	}
}
