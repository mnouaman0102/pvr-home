//package ma.iam.pvr.security;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.security.AuthenticationCredentialsNotFoundException;
//import org.springframework.security.AuthenticationException;
//import org.springframework.security.InsufficientAuthenticationException;
//import org.springframework.security.ui.webapp.AuthenticationProcessingFilterEntryPoint;
///**
// * for spring-security xml file
// *
// */
//public class CustomEntryPointProvider extends
//		AuthenticationProcessingFilterEntryPoint {
//
//	@Override
//	public void commence(ServletRequest request, ServletResponse response, AuthenticationException authException) throws IOException, ServletException {
//            HttpServletRequest httpRequest = (HttpServletRequest)request;
//            HttpServletResponse httpResponse = (HttpServletResponse)response;
//
//            String url = httpRequest.getRequestURI().toString();
//
//            String contextPath = httpRequest.getContextPath();
//            String targetUrl = url.replace(contextPath, "");
//
//            if("/".equals(targetUrl) || "/index.html".equals(targetUrl) ){
//                httpResponse.sendRedirect(httpResponse.encodeRedirectURL(contextPath + "/login.html") );
//                return;
//            }
//
//            if (authException instanceof InsufficientAuthenticationException) {
//			((HttpServletResponse) response).sendError(401);
//		} else if (authException instanceof AuthenticationCredentialsNotFoundException){
//			((HttpServletResponse) response).sendError(401);
//		}else {
//			super.commence(request, response, authException);
//		}
//	}
//}