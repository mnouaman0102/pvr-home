//package ma.iam.pvr.security;
//
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import ma.iam.pvr.security.sessionManagement.IamBindAuthenticator;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.security.AccountExpiredException;
//import org.springframework.security.BadCredentialsException;
//import org.springframework.security.CredentialsExpiredException;
//import org.springframework.security.DisabledException;
//import org.springframework.security.LockedException;
//import org.springframework.security.ldap.SpringSecurityContextSource;
//import org.springframework.security.userdetails.UsernameNotFoundException;
//
///**
// * Special purpose LDAP Authenticator tailored towards ActiveDirectory.  Understands how to
// * extract AD-specific return codes from exceptions in order to throw the correct exception.
// */
///**
// * for spring-security xml file
// *
// */
//public class ActiveDirectoryBindAuthenticator extends IamBindAuthenticator  {
//    Pattern authenticationSubcodeRegexp = Pattern.compile(".* data\\s(\\d+).*" );
//
//    Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    public ActiveDirectoryBindAuthenticator(final SpringSecurityContextSource contextSource) {
//        super(contextSource);
//    }
//
//    @Override
//    protected void handleBindException(final String userDn, final String username, final Throwable cause) {
//    	System.setProperty( "file.encoding", "UTF-8" );
//    	logger.info("******* inhandleBindException:" + cause.getMessage());
//        int subcode = getAuthenticationExceptionSubcode( cause );
//        switch( subcode ) {
//
//        	case 52:
//        		logger.info("{} : Le mot de passe est erronn�",username);
//        		throw new BadCredentialsException( "Le mot de passe est erronn�" );
//            case 525:
//            	logger.info("Le login de l'utilisateur {} est introuvable sur le LDAP",username);
//                throw new UsernameNotFoundException( "Votre login est introuvable sur le LDAP" );
//            case 532:
//            	logger.info("Le mot de passe de l'utilisateur {} a expir�",username);
//                throw new CredentialsExpiredException( "Votre mot de passe a exipr�" );
//            case 533:
//            	logger.info("Le compte de l'utilisateur {} est d�sactive",username);
//            	throw new DisabledException( "Votre compte est d�sactiv�" );
//            case 701:
//            	logger.info("Le compte de l'utilisateur {} a expir�",username);
//                throw new AccountExpiredException( "Votre compte a exipr�" );
//            case 773:
//            	logger.info("Le mot de passe de l'utilisateur {} doit etre reinitialis�",username);
//                throw new CredentialsExpiredException( "Vous devez r�initialiser votre mot de passe" );
//            case 775:
//            	logger.info("Le compte de l'utilisateur {} est verouill�",username);
//                throw new LockedException( "Votre compte est verouill�" );
//        }
//        // 525 & 52e fall through to here.
//        super.handleBindException(userDn, username, cause);
//    }
//
//    /**
//     * Attemps to extract the reason for the authentication error so we can translate to
//     * the correct exception.  Unfortunately JNDI isn't nice enough to propagate the parsed
//     * exception, so we need to extract it from the message text.
//     *
//     * @param e Exception to get error code for
//     * @return Sub-code for error, 0 if not specified.
//     */
//    protected int getAuthenticationExceptionSubcode( Throwable e ) {
//        Matcher m = authenticationSubcodeRegexp.matcher( e.getMessage() );
//        if( m.matches() ) {
//            String subcode = m.group( 1 );
//            return Integer.parseInt( subcode );
//        }
//        return 0;
//    }
//
//}
