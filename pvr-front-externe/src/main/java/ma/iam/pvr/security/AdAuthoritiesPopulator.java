//package ma.iam.pvr.security;
//
//import java.sql.Timestamp;
//import java.util.Collection;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.List;
//
//import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
//import ma.iam.pvr.domain.Profil;
//import ma.iam.pvr.domain.Utilisateur;
//import ma.iam.pvr.exception.CompteSuspenduException;
//import ma.iam.pvr.utils.SecurityUtils;
//import ma.iam.pvr.utils.Utils;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ldap.core.DirContextOperations;
//import org.springframework.security.GrantedAuthority;
//import org.springframework.security.GrantedAuthorityImpl;
//import org.springframework.security.ldap.LdapAuthoritiesPopulator;
//import org.springframework.transaction.annotation.Transactional;
//
///**
// * @author le.sahel && K.ELBAGUARI
// *
// */
///**
// * for spring-security xml file
// *
// */
//@Transactional
//public class AdAuthoritiesPopulator implements LdapAuthoritiesPopulator {
//
//	@Autowired
//	IUtilisateurDao utilisateurDaoImpl;
//
//	private final Logger logger = LoggerFactory.getLogger(AdAuthoritiesPopulator.class);
//
//	public GrantedAuthority[] getGrantedAuthorities(DirContextOperations userData, String username) {
//
//		Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
//		String userLogin = userData.getStringAttribute("sAMAccountName");
////		String userLogin = userData.getStringAttribute("uid");
//		logger.info(Utils.getLogParam() + "Tentative de connexion de l'utilisateur ayant le compte ldap : " + userLogin);
//
//		Utilisateur currentUser = utilisateurDaoImpl.getUserByLogin(userLogin);
//		if (currentUser == null) {
//			logger.info(Utils.getLogParam() + "Connexion echoue pour l'utilisateur ayant le compte ldap : " + userLogin);
//			return authorities.toArray(new GrantedAuthorityImpl[authorities.size()]);
//		}
//
//		if (SecurityUtils.isLocked(currentUser)) {
//			logger.info("Connexion echouee pour l'utilisateur ayant le login: "	+ userLogin + " => Compte Suspendu");
//			throw new CompteSuspenduException("Compte Suspendu");
//		}
//		List<Profil> profils = currentUser.getListProfils();
//
//		for (Profil profil : profils) {
//			authorities
//					.add(new GrantedAuthorityImpl("ROLE_" + profil.getLabel().toUpperCase().replaceAll("\\s+", "_")));
//		}
//
//		currentUser.setDateLastConnection(new Timestamp(new Date().getTime()));
//		utilisateurDaoImpl.update(currentUser);
//		logger.info("Mise a jour date derniere connexion pour l'utilisateur ayant le login: " + userLogin);
//
//		logger.info(Utils.getLogParam() + "Connexion reussie pour l'utilisateur ayant le compte ldap : " + userLogin);
//		return authorities.toArray(new GrantedAuthorityImpl[authorities.size()]);
//	}
//}
