package ma.iam.pvr.config;


import ma.iam.pvr.domain.Utilisateur;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Objects;
import java.util.Properties;
@Configuration
@EnableTransactionManagement
@EntityScan(basePackages = {"ma.iam.pvr.domain"})
public class HibernateUtil {

    @Resource
    private Environment env;

    /**
     * Create Hibernate Session Factory
     *
     * @return the configured sessionFactory
     */
    @Bean(name="sessionFactory")
    public LocalSessionFactoryBean sessionFactory() {
        final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "ma.iam.pvr.domain" });
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }

    /**
     * Configure Database source
     * @return the configured datasource
     */
    @Bean
    public DataSource dataSource() {
        final BasicDataSource dataSource = new BasicDataSource();
        Assert.notNull(env.getProperty("jdbc.driverClassName"),"DB driver should not be null");
        Assert.notNull(env.getProperty("jdbc.url"),"DB url should not be null");
        Assert.notNull(env.getProperty("jdbc.user"),"DB username should not be null");
        Assert.notNull(env.getProperty("jdbc.pass"),"DB password should not be null");

        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.pass"));

        return dataSource;
    }

    /**
     * Creates and configures a HibernateTransactionManager for managing transactions in a Hibernate-based application.
     *
     * @return HibernateTransactionManager
     */
//    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        final HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    /**
     * Creates and returns a Properties object containing Hibernate-specific configuration properties.
     *
     * @return Hibernate Properties
     */
    private final Properties hibernateProperties() {
        final Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        hibernateProperties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
        hibernateProperties.setProperty("hibernate.show_sql",env.getProperty("hibernate.show_sql"));
        hibernateProperties.setProperty("hibernate.format_sql", env.getProperty("hibernate.format_sql"));

        return hibernateProperties;
    }
}

