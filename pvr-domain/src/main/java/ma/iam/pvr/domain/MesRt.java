/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_MES_RT")
public class MesRt extends BaseEntity{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_MES")
	private Mes mes;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RT")
	private Rt rt;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the mes
	 */
	public Mes getMes() {
		return mes;
	}

	/**
	 * @param mes the mes to set
	 */
	public void setMes(Mes mes) {
		this.mes = mes;
	}

	/**
	 * @return the rt
	 */
	public Rt getRt() {
		return rt;
	}

	/**
	 * @param rt the rt to set
	 */
	public void setRt(Rt rt) {
		this.rt = rt;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
