package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_PROJET")
public class Projet extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7370418782175092111L;

	@Column(name = "WORKFLOW")
	private String workflow ;

	@Column(name = "ETAT_SITE")
	private String etatSite ;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the workflow
	 */
	public String getWorkflow() {
		return workflow;
	}

	/**
	 * @param workflow the workflow to set
	 */
	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}

	public String getEtatSite() {
		return etatSite;
	}

	public void setEtatSite(String etatSite) {
		this.etatSite = etatSite;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
