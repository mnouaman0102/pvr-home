/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_PVR_OPERATION_INFRA")
public class PvrOperationInfra extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_OPERATION_INFRA")
	private OperationInfra operationInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVR_INFRA")
	private PvrInfra pvrInfra;
	@Id
	@GeneratedValue
	private Long id;

	public OperationInfra getOperationInfra() {
		return operationInfra;
	}

	public void setOperationInfra(OperationInfra operationInfra) {
		this.operationInfra = operationInfra;
	}

	public PvrInfra getPvrInfra() {
		return pvrInfra;
	}

	public void setPvrInfra(PvrInfra pvrInfra) {
		this.pvrInfra = pvrInfra;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
