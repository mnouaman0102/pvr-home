package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import ma.iam.pvr.domain.BaseEntityLabel;
/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_TYPE_ALIMENTATION")
public class TypeAlimentation extends BaseEntityLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3990304438671507496L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}