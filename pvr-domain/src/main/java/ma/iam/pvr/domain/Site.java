package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI / O.SlAAOUITER
 *
 */
@Entity
@Table(name = "PVR_SITE")
public class Site extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4607049725164555011L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_DR")
	private Dr dr;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_DC")
	private Dc dc;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_REGION")
	private Region region;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PROVINCE")
	private Province province;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_COMMUNE")
	private Commune commune;

	@Column(name = "PLAQUE")
	private String plaque;

	@Column(name = "SOUS_PLAQUE")
	private String sousPlaque;

	@Column(name = "SEGMENT")
	private String segment;

	@Column(name = "ANTENNE")
	private String antenne;

	@Column(name = "TYPE_ANTENNE")
	private String typeAntenne;

	@Column(name = "LATITUDE")
	private Double latitude;

	@Column(name = "LONGITUDE")
	private Double longitude;

	@Column(name = "TYPOLOGIE")
	// Infrastructure (Indoor , outdoor, desserter)
	private String typologie;

	@Column(name = "LIEU_INSTALLATION")
	private String lieuInstallation;

	@Column(name = "SITE_PACTE")
	private Boolean sitePacte;

	@Column(name = "PARTAGE")
	private String partage;

	@Column(name = "OP_PARTAGE")
	private String opPartage;

	@Column(name = "TYPE_ALIMENTATION")
	private String typeAlimentation;

	@Column(name = "TYPE_TRANSMISSION")
	private String typeTransmission;

	@Column(name = "TYPE_SITE")
	private String typeSite;

	@Column(name = "MICRO_MACRO")
	private String microMacro;

	@Column(name = "DATE_MES")
	private Date dateMES;

	@Column(name = "CONFIGURATION_RESEAU")
	// 2G , 3G, 4G
	private String configurationReseau;

	@Column(name = "BANDES_4G")
	private String bandes4g;

	@Column(name = "GARDIENNAGE")
	private Boolean gardiennage;

	@Column(name = "FRAIS_MENSUEL_GARDIENNAGE")
	private Double fraisMensuelGardiennage;

	@Column(name = "FRAIS_MENSUEL_LOCATION")
	private Double fraisMensuelLocation;

	@Column(name = "CONSOMMATION_ENERGIE")
	private Double consommationEnergie;

	@Column(name = "SENSIBILITE")
	private String sensibilite;

	@Column(name = "RESP_MAINTENANCE_1")
	private String respMaintenance1;

	@Column(name = "RESP_MAINTENANCE_2")
	private String respMaintenance2;

	@Column(name = "RESP_MAINTENANCE_3")
	private String respMaintenance3;

	@Column(name = "GENERATION")
	// SRAN, RAN
	private String generation;

	@Column(name = "NOM_SITE_2G")
	private String nomSite2G;

	@Column(name = "NOM_SITE_3G")
	private String nomSite3G;

	@Column(name = "NOM_SITE_4G")
	private String nomSite4G;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_TECHNOLOGIE")
	private Fournisseur fournisseur;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ETAT_SITE")
	// 'En service, En cours d’intégration, Sarf, Démonté'
	private EtatSite etatSite;

	@Column(name = "X_CGPS")
	private Double xCGPS;

	@Column(name = "Y_CGPS")
	private Double yCGPS;

	@Column(name = "HAUTEUR_SUPPORT")
	private Double hauteurSupport;

	@Column(name = "HBA")
	private Double hBA;

	@Column(name = "AZIMUT", columnDefinition = "TEXT")
	private String azimut;

	@Column(name = "TILT_MECANIQUE", columnDefinition = "TEXT")
	private String tiltMecanique;

	@Column(name = "TILT_ELECTRIQUE", columnDefinition = "TEXT")
	private String tiltElectrique;

	@Column(name = "DATE_CANDIDATURE")
	private Date dateCandidature;

	@Column(name = "DATE_DERNIERE_MODIFICATION")
	private Date dateLastModification;

	@Column(name = "NUM_CIL")
	private String numCil;

	@Column(name = "GROUPE_SUPPORT_L1")
	private String groupeSupportL1;

	@Column(name = "ETAT_BATTERIE")
	private String etatBatterie;

	@Column(name = "SURVEILLANCE_CAMERA")
	private String surveillanceCamera;

	@Column(name = "CENTRE_RATTACHEMENT")
	private String centreRattachement;
	
	@Column(name = "PROPRIETE_IAM")	
	private String proprieteIAM;
	
	@Column(name = "TYPE_PROPRIETE")	
	private String typePropriete;
	
	@Column(name = "DATE_ACQUISITION")	
	private Date dateAcquisition;
	
	@Column(name = "ETAT_INFRA")	
	private String etatInfra;

	@Column(name = "TYPE_SUPPORT") //ANTENNE ??
	private String typeSupport;
	
	@Column(name = "ETAT_PREREQUIS")	
	private String etatPrerequis;
	@Id
	@GeneratedValue
	private Long id;


	public Dr getDr() {
		return dr;
	}

	public void setDr(Dr dr) {
		this.dr = dr;
	}

	public Dc getDc() {
		return dc;
	}

	public void setDc(Dc dc) {
		this.dc = dc;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	public String getPlaque() {
		return plaque;
	}

	public void setPlaque(String plaque) {
		this.plaque = plaque;
	}

	public String getSousPlaque() {
		return sousPlaque;
	}

	public void setSousPlaque(String sousPlaque) {
		this.sousPlaque = sousPlaque;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getAntenne() {
		return antenne;
	}

	public void setAntenne(String antenne) {
		this.antenne = antenne;
	}

	public String getTypeAntenne() {
		return typeAntenne;
	}

	public void setTypeAntenne(String typeAntenne) {
		this.typeAntenne = typeAntenne;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public EtatSite getEtatSite() {
		return etatSite;
	}

	public void setEtatSite(EtatSite etatSite) {
		this.etatSite = etatSite;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getTypologie() {
		return typologie;
	}

	public void setTypologie(String typologie) {
		this.typologie = typologie;
	}

	public String getLieuInstallation() {
		return lieuInstallation;
	}

	public void setLieuInstallation(String lieuInstallation) {
		this.lieuInstallation = lieuInstallation;
	}

	public Boolean getSitePacte() {
		return sitePacte;
	}

	public void setSitePacte(Boolean sitePacte) {
		this.sitePacte = sitePacte;
	}

	public String getPartage() {
		return partage;
	}

	public void setPartage(String partage) {
		this.partage = partage;
	}

	public String getOpPartage() {
		return opPartage;
	}

	public void setOpPartage(String opPartage) {
		this.opPartage = opPartage;
	}

	public String getTypeAlimentation() {
		return typeAlimentation;
	}

	public void setTypeAlimentation(String typeAlimentation) {
		this.typeAlimentation = typeAlimentation;
	}

	public String getTypeTransmission() {
		return typeTransmission;
	}

	public void setTypeTransmission(String typeTransmission) {
		this.typeTransmission = typeTransmission;
	}

	public String getTypeSite() {
		return typeSite;
	}

	public void setTypeSite(String typeSite) {
		this.typeSite = typeSite;
	}

	public String getMicroMacro() {
		return microMacro;
	}

	public void setMicroMacro(String microMacro) {
		this.microMacro = microMacro;
	}

	public Date getDateMES() {
		return dateMES;
	}

	public void setDateMES(Date dateMES) {
		this.dateMES = dateMES;
	}

	public String getConfigurationReseau() {
		return configurationReseau;
	}

	public void setConfigurationReseau(String configurationReseau) {
		this.configurationReseau = configurationReseau;
	}

	public String getBandes4g() {
		return bandes4g;
	}

	public void setBandes4g(String bandes4g) {
		this.bandes4g = bandes4g;
	}

	public Boolean getGardiennage() {
		return gardiennage;
	}

	public void setGardiennage(Boolean gardiennage) {
		this.gardiennage = gardiennage;
	}

	public Double getFraisMensuelGardiennage() {
		return fraisMensuelGardiennage;
	}

	public void setFraisMensuelGardiennage(Double fraisMensuelGardiennage) {
		this.fraisMensuelGardiennage = fraisMensuelGardiennage;
	}

	public Double getFraisMensuelLocation() {
		return fraisMensuelLocation;
	}

	public void setFraisMensuelLocation(Double fraisMensuelLocation) {
		this.fraisMensuelLocation = fraisMensuelLocation;
	}

	public Double getConsommationEnergie() {
		return consommationEnergie;
	}

	public void setConsommationEnergie(Double consommationEnergie) {
		this.consommationEnergie = consommationEnergie;
	}

	public String getSensibilite() {
		return sensibilite;
	}

	public void setSensibilite(String sensibilite) {
		this.sensibilite = sensibilite;
	}

	public String getRespMaintenance1() {
		return respMaintenance1;
	}

	public void setRespMaintenance1(String respMaintenance1) {
		this.respMaintenance1 = respMaintenance1;
	}

	public String getRespMaintenance2() {
		return respMaintenance2;
	}

	public void setRespMaintenance2(String respMaintenance2) {
		this.respMaintenance2 = respMaintenance2;
	}

	public String getRespMaintenance3() {
		return respMaintenance3;
	}

	public void setRespMaintenance3(String respMaintenance3) {
		this.respMaintenance3 = respMaintenance3;
	}

	public String getGeneration() {
		return generation;
	}

	public void setGeneration(String generation) {
		this.generation = generation;
	}

	public String getNomSite2G() {
		return nomSite2G;
	}

	public void setNomSite2G(String nomSite2G) {
		this.nomSite2G = nomSite2G;
	}

	public String getNomSite3G() {
		return nomSite3G;
	}

	public void setNomSite3G(String nomSite3G) {
		this.nomSite3G = nomSite3G;
	}

	public String getNomSite4G() {
		return nomSite4G;
	}

	public void setNomSite4G(String nomSite4G) {
		this.nomSite4G = nomSite4G;
	}

	public Double getxCGPS() {
		return xCGPS;
	}

	public void setxCGPS(Double xCGPS) {
		this.xCGPS = xCGPS;
	}

	public Double getyCGPS() {
		return yCGPS;
	}

	public void setyCGPS(Double yCGPS) {
		this.yCGPS = yCGPS;
	}

	public Double getHauteurSupport() {
		return hauteurSupport;
	}

	public void setHauteurSupport(Double hauteurSupport) {
		this.hauteurSupport = hauteurSupport;
	}

	public Double gethBA() {
		return hBA;
	}

	public void sethBA(Double hBA) {
		this.hBA = hBA;
	}

	public String getAzimut() {
		return azimut;
	}

	public void setAzimut(String azimut) {
		this.azimut = azimut;
	}

	public String getTiltMecanique() {
		return tiltMecanique;
	}

	public void setTiltMecanique(String tiltMecanique) {
		this.tiltMecanique = tiltMecanique;
	}

	public String getTiltElectrique() {
		return tiltElectrique;
	}

	public void setTiltElectrique(String tiltElectrique) {
		this.tiltElectrique = tiltElectrique;
	}

	public Date getDateCandidature() {
		return dateCandidature;
	}

	public void setDateCandidature(Date dateCandidature) {
		this.dateCandidature = dateCandidature;
	}

	public Date getDateLastModification() {
		return dateLastModification;
	}

	public void setDateLastModification(Date dateLastModification) {
		this.dateLastModification = dateLastModification;
	}

	public String getNumCil() {
		return numCil;
	}

	public void setNumCil(String numCil) {
		this.numCil = numCil;
	}

	public String getGroupeSupportL1() {
		return groupeSupportL1;
	}

	public void setGroupeSupportL1(String groupeSupportL1) {
		this.groupeSupportL1 = groupeSupportL1;
	}

	public String getEtatBatterie() {
		return etatBatterie;
	}

	public void setEtatBatterie(String etatBatterie) {
		this.etatBatterie = etatBatterie;
	}

	public String getSurveillanceCamera() {
		return surveillanceCamera;
	}

	public void setSurveillanceCamera(String surveillanceCamera) {
		this.surveillanceCamera = surveillanceCamera;
	}

	public String getCentreRattachement() {
		return centreRattachement;
	}

	public void setCentreRattachement(String centreRattachement) {
		this.centreRattachement = centreRattachement;
	}

	public String getProprieteIAM() {
		return proprieteIAM;
	}

	public void setProprieteIAM(String proprieteIAM) {
		this.proprieteIAM = proprieteIAM;
	}

	public String getTypePropriete() {
		return typePropriete;
	}

	public void setTypePropriete(String typePropriete) {
		this.typePropriete = typePropriete;
	}

	public Date getDateAcquisition() {
		return dateAcquisition;
	}

	public void setDateAcquisition(Date dateAcquisition) {
		this.dateAcquisition = dateAcquisition;
	}

	public String getEtatInfra() {
		return etatInfra;
	}

	public void setEtatInfra(String etatInfra) {
		this.etatInfra = etatInfra;
	}

	public String getEtatPrerequis() {
		return etatPrerequis;
	}

	public void setEtatPrerequis(String etatPrerequis) {
		this.etatPrerequis = etatPrerequis;
	}

	public String getTypeSupport() {
		return typeSupport;
	}

	public void setTypeSupport(String typeSupport) {
		this.typeSupport = typeSupport;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
