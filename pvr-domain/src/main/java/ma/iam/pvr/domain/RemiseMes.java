/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_REMISE_MES")
public class RemiseMes extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_MES")
	private Mes mes;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_REMISE_CONTROLE")
	private RemiseControle remiseControle;
	@Id
	@GeneratedValue
	private Long id;

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public RemiseControle getRemiseControle() {
		return remiseControle;
	}

	public void setRemiseControle(RemiseControle remiseControle) {
		this.remiseControle = remiseControle;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
