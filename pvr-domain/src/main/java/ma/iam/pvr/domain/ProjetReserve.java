/**
 * 
 */
package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_PROJET_RESERVE")
public class ProjetReserve extends BaseEntity{		

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_RESERVE")
	private Reserve reserve;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PROJET")
	private Projet projet;	
	
	@Column(name = "RESPONSABILITE")
	private String responsabilite;
	
	@Column(name = "SEVERITE")
	private String severite;
	
	@Column(name = "CHAPITRE")
	private String chapitre;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the reserve
	 */
	public Reserve getReserve() {
		return reserve;
	}

	/**
	 * @param reserve the reserve to set
	 */
	public void setReserve(Reserve reserve) {
		this.reserve = reserve;
	}

	/**
	 * @return the projet
	 */
	public Projet getProjet() {
		return projet;
	}

	/**
	 * @param projet the projet to set
	 */
	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	/**
	 * @return the responsabilite
	 */
	public String getResponsabilite() {
		return responsabilite;
	}

	/**
	 * @param responsabilite the responsabilite to set
	 */
	public void setResponsabilite(String responsabilite) {
		this.responsabilite = responsabilite;
	}

	/**
	 * @return the severite
	 */
	public String getSeverite() {
		return severite;
	}

	/**
	 * @param severite the severite to set
	 */
	public void setSeverite(String severite) {
		this.severite = severite;
	}

	/**
	 * @return the chapitre
	 */
	public String getChapitre() {
		return chapitre;
	}

	/**
	 * @param chapitre the chapitre to set
	 */
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
