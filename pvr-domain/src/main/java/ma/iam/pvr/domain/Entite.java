package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_ENTITE")
public class Entite extends BaseEntityCodeLabel {

	
	private static final long serialVersionUID = 1635047007770467625L;
	private String hierarchie;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "CC")
	private String cc;
	
	@Column(name = "HIERARCHIE")


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getHierarchie() {
		return hierarchie;
	}

	public void setHierarchie(String hierarchie) {
		this.hierarchie = hierarchie;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
