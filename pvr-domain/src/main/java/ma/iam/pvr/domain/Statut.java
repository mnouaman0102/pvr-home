/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_STATUT")
public class Statut extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5231603321626677714L;
	@Id
	@GeneratedValue
	private Long id;


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
