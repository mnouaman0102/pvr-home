/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_PJ_OPERATION_INFRA")
public class PjOperationInfra extends BaseEntity{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1502655053809316230L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_OPERATION_INFRA")
	private OperationInfra operationInfra;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ENTITE")
	private Entite entite;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_DR")
	private Dr dr;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PIECE_JOINTE")
	private PieceJointe pieceJointe;	
	
	@Column(name = "ETAPE")
	private String etape = "oper";
	@Id
	@GeneratedValue
	private Long id;

	public OperationInfra getOperationInfra() {
		return operationInfra;
	}

	public void setOperationInfra(OperationInfra operationInfra) {
		this.operationInfra = operationInfra;
	}

	/**
	 * @return the entite
	 */
	public Entite getEntite() {
		return entite;
	}

	/**
	 * @param entite the entite to set
	 */
	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	/**
	 * @return the pieceJointe
	 */
	public PieceJointe getPieceJointe() {
		return pieceJointe;
	}

	/**
	 * @param pieceJointe the pieceJointe to set
	 */
	public void setPieceJointe(PieceJointe pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	public String getEtape() {
		return etape;
	}

	public void setEtape(String etape) {
		this.etape = etape;
	}

	public Dr getDr() {
		return dr;
	}

	public void setDr(Dr dr) {
		this.dr = dr;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
