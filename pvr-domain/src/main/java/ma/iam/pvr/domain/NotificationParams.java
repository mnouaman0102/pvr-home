/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_NOTIFICATION_PARAMS")
public class NotificationParams extends BaseEntity{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2965932081884283753L;

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ENTITE")
	private Entite entite;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_DR")
	private Dr dr;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_FOURNISSEUR")
	private Fournisseur technologie;	
	
	@Column(name = "EMAIL")
	private String email;
	@Id
	@GeneratedValue
	private Long id;

	public Entite getEntite() {
		return entite;
	}

	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	public Dr getDr() {
		return dr;
	}

	public void setDr(Dr dr) {
		this.dr = dr;
	}

	public Fournisseur getTechnologie() {
		return technologie;
	}

	public void setTechnologie(Fournisseur technologie) {
		this.technologie = technologie;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
