package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_RT_INFRA")
public class RtInfra extends BaseEntity {

	private static final long serialVersionUID = -6596624334965704197L;

	@Column(name = "DATE", nullable = false)
	private Date date;

	@Column(name = "REFERENCE")
	private String reference;

	@Column(name = "OBJET")
	private String objet;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_FOURNISSEUR")
	private Fournisseur fournisseur;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_CONTRAT")
	private Contrat contrat;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_STATUT_EDITION")
	private StatutEdition statutEdition;

	@Column(name = "COMMENTAIRE")
	private String commentaire;

	@Column(name = "WORKFLOW")
	private String workflow;

	@Column(name = "DATE_VALIDATION")
	private Date dateValidation;
	@Id
	@GeneratedValue
	private Long id;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getObjet() {
		return objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	public StatutEdition getStatutEdition() {
		return statutEdition;
	}

	public void setStatutEdition(StatutEdition statutEdition) {
		this.statutEdition = statutEdition;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public String getWorkflow() {
		return workflow;
	}

	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}

	public Date getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
