/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_PJ_QUALIFICATIONS")
public class PjQualifications extends BaseEntity{		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5397000180418494401L;


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_QUALIFICATION")
	private Qualification qualification;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PIECE_JOINTE")
	private PieceJointe pieceJointe;
	@Id
	@GeneratedValue
	private Long id;


	public Qualification getQualification() {
		return qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public PieceJointe getPieceJointe() {
		return pieceJointe;
	}
	
	public void setPieceJointe(PieceJointe pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
