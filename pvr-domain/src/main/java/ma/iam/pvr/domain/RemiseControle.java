package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_REMISE_CONTROLE")
public class RemiseControle extends BaseEntity {

	private static final long serialVersionUID = 1635047007770467625L;

	@Column(name = "TITRE")
	private String titre;

	@Column(name = "SOLUTION", columnDefinition = "TEXT")
	private String solution;

	@Column(name = "DATE_CREATION")
	private Date dateCreation;

	@Column(name = "DATE_CREATION_REELLE")
	private Date dateCreationReelle;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ETAT_REMISE_CONTROLE")
	private EtatRemiseControle etatRemiseControle;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_FOURNISSEUR")
	private Fournisseur fournisseur;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_CREER_PAR")
	private Utilisateur utilisateur;

	@Column(name = "DATE_VALIDATION")
	private Date dateValidation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_STATUT_EDITION")
	private StatutEdition statutEdition;

	@Column(name = "DATE_MODIFICATION")
	private Date dateModification;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_MODIFIER_PAR")
	private Utilisateur modifierPar;
	@Id
	@GeneratedValue
	private Long id;

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateCreationReelle() {
		return dateCreationReelle;
	}

	public void setDateCreationReelle(Date dateCreationReelle) {
		this.dateCreationReelle = dateCreationReelle;
	}

	public EtatRemiseControle getEtatRemiseControle() {
		return etatRemiseControle;
	}

	public void setEtatRemiseControle(EtatRemiseControle etatRemiseControle) {
		this.etatRemiseControle = etatRemiseControle;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Date getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	public StatutEdition getStatutEdition() {
		return statutEdition;
	}

	public void setStatutEdition(StatutEdition statutEdition) {
		this.statutEdition = statutEdition;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Utilisateur getModifierPar() {
		return modifierPar;
	}

	public void setModifierPar(Utilisateur modifierPar) {
		this.modifierPar = modifierPar;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
