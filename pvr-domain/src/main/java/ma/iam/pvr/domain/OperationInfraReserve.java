/**
 * 
 */
package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_OPERATION_INFRA_RESERVE")
public class OperationInfraReserve extends BaseEntity {

	private static final long serialVersionUID = 7956394177523709489L;

	@Column(name = "DATE")
	private Date date;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_OPERATION_INFRA")
	private OperationInfra operationInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RESERVE_INFRA")
	private ReserveInfra reserveInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_STATUT")
	private Statut statut;

	@Column(name = "COMMENTAIRE", columnDefinition = "TEXT")
	private String commentaire;
	@Id
	@GeneratedValue
	private Long id;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public OperationInfra getOperationInfra() {
		return operationInfra;
	}

	public void setOperationInfra(OperationInfra operationInfra) {
		this.operationInfra = operationInfra;
	}
	
	public ReserveInfra getReserveInfra() {
		return reserveInfra;
	}

	public void setReserveInfra(ReserveInfra reserveInfra) {
		this.reserveInfra = reserveInfra;
	}

	public Statut getStatut() {
		return statut;
	}

	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
