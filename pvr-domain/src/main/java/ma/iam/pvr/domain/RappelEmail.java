package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_RAPPEL_EMAIL")
public class RappelEmail extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7665726506664239363L;
	
	@Column(name = "DATE_ENVOI")
	private Date dateEnvoi;
	
	@Column(name = "ENVOI")
	private Boolean envoi;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_EMAIL")
	private Email email;
	
	@Column(name = "NOMBRE_ENVOI")
	private Integer nombreEnvoi;
	@Id
	@GeneratedValue
	private Long id;

	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public Boolean getEnvoi() {
		return envoi;
	}

	public void setEnvoi(Boolean envoi) {
		this.envoi = envoi;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Integer getNombreEnvoi() {
		return nombreEnvoi;
	}

	public void setNombreEnvoi(Integer nombreEnvoi) {
		this.nombreEnvoi = nombreEnvoi;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
