package ma.iam.pvr.domain;

import javax.persistence.*;

@Entity
@Table(name = "PVR_PROVINCE")
public class Province extends BaseEntityLabel {
	private static final long serialVersionUID = 1635047706770467625L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_REGION")
	private Region region;
	@Id
	@GeneratedValue
	private Long id;

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
