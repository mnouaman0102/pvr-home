package ma.iam.pvr.domain;


import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_MES")
public class Mes extends BaseEntity {

	
	private static final long serialVersionUID = 4254778491585532964L;
	
	@Column(name = "DATE", nullable = false)
	private Date date;
	
	@Column(name = "NUMOPERATION")
	private String numOperation;
	
	@Column(name = "COMMANDE")
	private String commande;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVR")
	private Pvr pvr;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_SITE")
	private Site site;

	@Column(name = "RESERVE")
	private String reserve;
	
	@Column(name = "STATUT")
	private String statut;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ETAT")
	private Etat etat;
	
	@Column(name = "COMMENTAIRE")
	private String commentaire;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVD")
	private Pvd pvd;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_CONTRAT")
	private Contrat contrat;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PROJET")
	private Projet projet;
	
	@Column(name = "DATE_MES")
	private Date dateMES;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_TYPE_PROJET")
	private TypeProjet typeProjet;
	
	@Column(name = "DATE_MES_REELLE")
	private Date dateMESReelle;
	
	@Column(name = "COMMENTAIRE_OPERATION", columnDefinition="TEXT")
	private String commentaireOperation;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}


	/**
	 * @return the commande
	 */
	public String getCommande() {
		return commande;
	}

	/**
	 * @param commande the commande to set
	 */
	public void setCommande(String commande) {
		this.commande = commande;
	}

	/**
	 * @return the pvr
	 */
	public Pvr getPvr() {
		return pvr;
	}

	/**
	 * @param pvr the pvr to set
	 */
	public void setPvr(Pvr pvr) {
		this.pvr = pvr;
	}
	

	/**
	 * @return the site
	 */
	public Site getSite() {
		return site;
	}

	/**
	 * @param site the site to set
	 */
	public void setSite(Site site) {
		this.site = site;
	}

	/**
	 * @return the reserve
	 */
	public String getReserve() {
		return reserve;
	}

	/**
	 * @param reserve the reserve to set
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

	/**
	 * @return the statut
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * @param statut the statut to set
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * @return the etat
	 */
	public Etat getEtat() {
		return etat;
	}

	/**
	 * @param etat the etat to set
	 */
	public void setEtat(Etat etat) {
		this.etat = etat;
	}

	public Pvd getPvd() {
		return pvd;
	}

	public void setPvd(Pvd pvd) {
		this.pvd = pvd;
	}

	/**
	 * @return the contrat
	 */
	public Contrat getContrat() {
		return contrat;
	}

	/**
	 * @param contrat the contrat to set
	 */
	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	/**
	 * @return the numOperation
	 */
	public String getNumOperation() {
		return numOperation;
	}

	/**
	 * @param numOperation the numOperation to set
	 */
	public void setNumOperation(String numOperation) {
		this.numOperation = numOperation;
	}

	public Date getDateMES() {
		return dateMES;
	}

	public void setDateMES(Date dateMES) {
		this.dateMES = dateMES;
	}

	/**
	 * @return the typeProjet
	 */
	public TypeProjet getTypeProjet() {
		return typeProjet;
	}

	/**
	 * @param typeProjet the typeProjet to set
	 */
	public void setTypeProjet(TypeProjet typeProjet) {
		this.typeProjet = typeProjet;
	}

	public Date getDateMESReelle() {
		return dateMESReelle;
	}

	public void setDateMESReelle(Date dateMESReelle) {
		this.dateMESReelle = dateMESReelle;
	}

	public String getCommentaireOperation() {
		return commentaireOperation;
	}

	public void setCommentaireOperation(String commentaireOperation) {
		this.commentaireOperation = commentaireOperation;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
