package ma.iam.pvr.domain;

import javax.persistence.*;

@Entity
@Table(name = "PVR_COMMUNE")
public class Commune extends BaseEntityLabel {
	private static final long serialVersionUID = 1635047006660467625L;
	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PROVINCE")
	private Province province;

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}
	
}
