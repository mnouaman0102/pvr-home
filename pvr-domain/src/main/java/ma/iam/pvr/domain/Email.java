package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_EMAIL")
public class Email extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7665726506664239363L;

	@Column(name = "CONTENU")
	private String contenu;

	@Column(name = "DATE_ENVOI")
	private Date dateEnvoi;

	@Column(name = "ENVOI")
	private Boolean envoi;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ENTITE")
	private Entite entite;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_MES")
	private Mes mes;

	@Column(name = "LINK")
	private String link;

	@Column(name = "TEMPLATE")
	private String template;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_FOURNISSEUR")
	private Fournisseur fournisseur;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_DR")
	private Dr dr;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CREER_PAR")
	private Utilisateur utilisateur;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ENTITE_SEND")
	private Entite entiteSend;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RT")
	private Rt rt;

	@Column(name = "DATE_CREATION")
	private Date dateCreation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_REMISE_CONTROLE")
	private RemiseControle remiseControle;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_SITE")
	private Site site;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_SITE_CANDIDAT_HIS")
	private SiteCandidatHistory siteCandidatHistory;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_OPERATION_INFRA")
	private OperationInfra operationInfra;
	@Id
	@GeneratedValue
	private Long id;

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public Boolean getEnvoi() {
		return envoi;
	}

	public void setEnvoi(Boolean envoi) {
		this.envoi = envoi;
	}

	public Entite getEntite() {
		return entite;
	}

	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Dr getDr() {
		return dr;
	}

	public void setDr(Dr dr) {
		this.dr = dr;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Entite getEntiteSend() {
		return entiteSend;
	}

	public void setEntiteSend(Entite entiteSend) {
		this.entiteSend = entiteSend;
	}

	public Rt getRt() {
		return rt;
	}

	public void setRt(Rt rt) {
		this.rt = rt;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public RemiseControle getRemiseControle() {
		return remiseControle;
	}

	public void setRemiseControle(RemiseControle remiseControle) {
		this.remiseControle = remiseControle;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public SiteCandidatHistory getSiteCandidatHistory() {
		return siteCandidatHistory;
	}

	public void setSiteCandidatHistory(SiteCandidatHistory siteCandidatHistory) {
		this.siteCandidatHistory = siteCandidatHistory;
	}

	public OperationInfra getOperationInfra() {
		return operationInfra;
	}

	public void setOperationInfra(OperationInfra operationInfra) {
		this.operationInfra = operationInfra;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
