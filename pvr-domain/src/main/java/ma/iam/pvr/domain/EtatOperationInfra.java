package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_ETAT_OPERATION_INFRA")
public class EtatOperationInfra extends BaseEntityCodeLabel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3067313834006583069L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
