package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_RESERVE_INFRA")
public class ReserveInfra extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3608950486392843752L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PROJET_INFRA")
	private ProjetInfra projetInfra;	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_TYPE_PROJET_INFRA")
	private TypeProjetInfra typeProjetInfra;	
	
	@Column(name = "RESPONSABILITE")
	private String responsabilite;
	
	@Column(name = "SEVERITE")
	private String severite;
	
	@Column(name = "CHAPITRE")
	private String chapitre;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the responsabilite
	 */
	public String getResponsabilite() {
		return responsabilite;
	}

	/**
	 * @param responsabilite the responsabilite to set
	 */
	public void setResponsabilite(String responsabilite) {
		this.responsabilite = responsabilite;
	}

	/**
	 * @return the severite
	 */
	public String getSeverite() {
		return severite;
	}

	/**
	 * @param severite the severite to set
	 */
	public void setSeverite(String severite) {
		this.severite = severite;
	}

	/**
	 * @return the chapitre
	 */
	public String getChapitre() {
		return chapitre;
	}

	/**
	 * @param chapitre the chapitre to set
	 */
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}

	public ProjetInfra getProjetInfra() {
		return projetInfra;
	}

	public void setProjetInfra(ProjetInfra projetInfra) {
		this.projetInfra = projetInfra;
	}

	public TypeProjetInfra getTypeProjetInfra() {
		return typeProjetInfra;
	}

	public void setTypeProjetInfra(TypeProjetInfra typeProjetInfra) {
		this.typeProjetInfra = typeProjetInfra;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
