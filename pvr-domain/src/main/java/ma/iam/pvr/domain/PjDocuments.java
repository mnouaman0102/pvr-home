/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_PJ_DOCUMENTS")
public class PjDocuments extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5397000180418494401L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RT")
	private Rt rt;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PIECE_JOINTE")
	private PieceJointe pieceJointe;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVR")
	private Pvr pvr;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVD")
	private Pvd pvd;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_REMISE_CONTROLE")
	private RemiseControle remiseControle;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RT_INFRA")
	private RtInfra rtInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVR_INFRA")
	private PvrInfra pvrInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVD_INFRA")
	private PvdInfra pvdInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_CANDIDAT_SITE")
	private Site candidatSite;
	@Id
	@GeneratedValue
	private Long id;

	public Rt getRt() {
		return rt;
	}

	public void setRt(Rt rt) {
		this.rt = rt;
	}

	public PieceJointe getPieceJointe() {
		return pieceJointe;
	}

	public void setPieceJointe(PieceJointe pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	public Pvr getPvr() {
		return pvr;
	}

	public void setPvr(Pvr pvr) {
		this.pvr = pvr;
	}

	public Pvd getPvd() {
		return pvd;
	}

	public void setPvd(Pvd pvd) {
		this.pvd = pvd;
	}

	public RemiseControle getRemiseControle() {
		return remiseControle;
	}

	public void setRemiseControle(RemiseControle remiseControle) {
		this.remiseControle = remiseControle;
	}

	public RtInfra getRtInfra() {
		return rtInfra;
	}

	public void setRtInfra(RtInfra rtInfra) {
		this.rtInfra = rtInfra;
	}

	public PvrInfra getPvrInfra() {
		return pvrInfra;
	}

	public void setPvrInfra(PvrInfra pvrInfra) {
		this.pvrInfra = pvrInfra;
	}

	public PvdInfra getPvdInfra() {
		return pvdInfra;
	}

	public void setPvdInfra(PvdInfra pvdInfra) {
		this.pvdInfra = pvdInfra;
	}

	public Site getCandidatSite() {
		return candidatSite;
	}

	public void setCandidatSite(Site candidatSite) {
		this.candidatSite = candidatSite;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
