/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_PJ_OPERATION")
public class PjOperation extends BaseEntity{

	private static final long serialVersionUID = 7956394177523709489L;	
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_MES")
	private Mes mes;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ENTITE")
	private Entite entite;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PIECE_JOINTE")
	private PieceJointe pieceJointe;	
	
	@Column(name = "ETAPE")
	private String etape = "oper";
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the mes
	 */
	public Mes getMes() {
		return mes;
	}

	/**
	 * @param mes the mes to set
	 */
	public void setMes(Mes mes) {
		this.mes = mes;
	}

	/**
	 * @return the entite
	 */
	public Entite getEntite() {
		return entite;
	}

	/**
	 * @param entite the entite to set
	 */
	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	/**
	 * @return the pieceJointe
	 */
	public PieceJointe getPieceJointe() {
		return pieceJointe;
	}

	/**
	 * @param pieceJointe the pieceJointe to set
	 */
	public void setPieceJointe(PieceJointe pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	public String getEtape() {
		return etape;
	}

	public void setEtape(String etape) {
		this.etape = etape;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
