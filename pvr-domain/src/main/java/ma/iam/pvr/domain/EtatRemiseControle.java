package ma.iam.pvr.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_ETAT_REMISE_CONTROLE")
public class EtatRemiseControle extends BaseEntityCodeLabel {

	
	private static final long serialVersionUID = 1635047007770467625L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
