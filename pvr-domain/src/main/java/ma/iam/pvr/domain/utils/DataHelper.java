package ma.iam.pvr.domain.utils;

import java.util.List;

import ma.iam.pvr.dao.interfaces.IGenericDao;
import ma.iam.pvr.domain.BaseEntityCodeLabel;
import ma.iam.pvr.domain.BaseEntityLabel;

/**
 * @author H.ELKHATEB
 *
 */
public class DataHelper {
	
	public static String joinListLabel(List<Long> list, String delim, IGenericDao<? extends BaseEntityCodeLabel, Long> dao) {
		StringBuilder listLabels = new StringBuilder();
		if(list != null && !list.isEmpty()){
			for(Long idt : list) {
				BaseEntityLabel bel = dao.get(idt);
				if(bel != null) {
					if(listLabels.length() == 0) {
						listLabels.append(", ");
					}
					listLabels.append(bel.getLabel());
				}
			}
		}else{
			listLabels.append("Tous");
		}
		return listLabels.toString();
	}
}
