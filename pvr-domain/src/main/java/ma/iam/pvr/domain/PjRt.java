/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_PJ_RT")
public class PjRt extends BaseEntity{		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5397000180418494401L;


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RT")
	private Rt rt;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PIECE_JOINTE")
	private PieceJointe pieceJointe;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the pieceJointe
	 */
	public PieceJointe getPieceJointe() {
		return pieceJointe;
	}

	/**
	 * @param pieceJointe the pieceJointe to set
	 */
	public void setPieceJointe(PieceJointe pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	/**
	 * @return the rt
	 */
	public Rt getRt() {
		return rt;
	}

	/**
	 * @param rt the rt to set
	 */
	public void setRt(Rt rt) {
		this.rt = rt;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
