/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_MES_PVR")
public class MesPvr extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_MES")
	private Mes mes;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVR")
	private Pvr pvr;
	@Id
	@GeneratedValue
	private Long id;

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public Pvr getPvr() {
		return pvr;
	}

	public void setPvr(Pvr pvr) {
		this.pvr = pvr;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
