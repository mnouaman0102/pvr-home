package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_OPERATION_INFRA")
public class OperationInfra extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4795757981169858545L;

	@Column(name = "DATE", nullable = false)
	private Date date;

	@Column(name = "NUMOPERATION")
	private String numOperation;

	@Column(name = "DATE_DISPONIBLE")
	private Date dateDisponible;

	@Column(name = "DATE_REELLE_DISPONIBLE")
	private Date dateReelleDisponible;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_SITE")
	private Site site;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_CONTRAT")
	private Contrat contrat;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PROJET_INFRA")
	private ProjetInfra projetInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_TYPE_PROJET_INFRA")
	private TypeProjetInfra typeProjetInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ETAT_OPERATION_INFRA")
	private EtatOperationInfra etatOperationInfra;

	@Column(name = "STATUT")
	private String statut; // Selon les reserves

	@Column(name = "COMMENTAIRE_OPERATION", columnDefinition = "TEXT")
	private String commentaireOperation; // lors de la creation

	@Column(name = "COMMENTAIRE", columnDefinition = "TEXT")
	private String commentaire; // etat disponible

	@Column(name = "OS", columnDefinition = "TEXT")
	private String os; // etat disponible

	@Column(name = "TYPE_SUPPORT", columnDefinition = "TEXT")
	private String typeSupport;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_TYPE_SUPPORT")
	private TypeSupport typeSup;

	@Column(name = "DATE_REMISE_CONTROLE")
	private Date dateRemiseControle;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVR_INFRA")
	private PvrInfra pvrInfra;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PVD_INFRA")
	private PvdInfra pvdInfra;
	@Id
	@GeneratedValue
	private Long id;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNumOperation() {
		return numOperation;
	}

	public void setNumOperation(String numOperation) {
		this.numOperation = numOperation;
	}

	public Date getDateDisponible() {
		return dateDisponible;
	}

	public void setDateDisponible(Date dateDisponible) {
		this.dateDisponible = dateDisponible;
	}

	public Date getDateReelleDisponible() {
		return dateReelleDisponible;
	}

	public void setDateReelleDisponible(Date dateReelleDisponible) {
		this.dateReelleDisponible = dateReelleDisponible;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	public ProjetInfra getProjetInfra() {
		return projetInfra;
	}

	public void setProjetInfra(ProjetInfra projetInfra) {
		this.projetInfra = projetInfra;
	}

	public TypeProjetInfra getTypeProjetInfra() {
		return typeProjetInfra;
	}

	public void setTypeProjetInfra(TypeProjetInfra typeProjetInfra) {
		this.typeProjetInfra = typeProjetInfra;
	}

	public EtatOperationInfra getEtatOperationInfra() {
		return etatOperationInfra;
	}

	public void setEtatOperationInfra(EtatOperationInfra etatOperationInfra) {
		this.etatOperationInfra = etatOperationInfra;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getCommentaireOperation() {
		return commentaireOperation;
	}

	public void setCommentaireOperation(String commentaireOperation) {
		this.commentaireOperation = commentaireOperation;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getTypeSupport() {
		return typeSupport;
	}

	public void setTypeSupport(String typeSupport) {
		this.typeSupport = typeSupport;
	}

	public TypeSupport getTypeSup() {
		return typeSup;
	}

	public void setTypeSup(TypeSupport typeSup) {
		this.typeSup = typeSup;
	}

	public Date getDateRemiseControle() {
		return dateRemiseControle;
	}

	public void setDateRemiseControle(Date dateRemiseControle) {
		this.dateRemiseControle = dateRemiseControle;
	}

	public PvrInfra getPvrInfra() {
		return pvrInfra;
	}

	public void setPvrInfra(PvrInfra pvrInfra) {
		this.pvrInfra = pvrInfra;
	}

	public PvdInfra getPvdInfra() {
		return pvdInfra;
	}

	public void setPvdInfra(PvdInfra pvdInfra) {
		this.pvdInfra = pvdInfra;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
