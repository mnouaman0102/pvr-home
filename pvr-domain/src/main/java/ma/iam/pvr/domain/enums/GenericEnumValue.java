package ma.iam.pvr.domain.enums;

import java.io.Serializable;

public interface GenericEnumValue extends Serializable {
    String getCode();

}
