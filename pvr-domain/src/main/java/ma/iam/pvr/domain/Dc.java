package ma.iam.pvr.domain;

import javax.persistence.*;

@Entity
@Table(name = "PVR_DC")
public class Dc extends BaseEntityCodeLabel  {
	private static final long serialVersionUID = 1635047006770467625L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_DR")
	private Dr dr;
	@Id
	@GeneratedValue
	private Long id;

	public Dr getDr() {
		return dr;
	}

	public void setDr(Dr dr) {
		this.dr = dr;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
