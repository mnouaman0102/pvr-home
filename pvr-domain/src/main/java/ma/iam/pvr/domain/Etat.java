/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_ETAT")
public class Etat extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6600545341893484341L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
