package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
public class Fournisseur extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7370418782175092111L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "EMAIL")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
