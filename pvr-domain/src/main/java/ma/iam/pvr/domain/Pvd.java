package ma.iam.pvr.domain;


import java.util.Date;

import javax.persistence.*;


/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_PVD")
public class Pvd extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4787040581256533367L;

	@Column(name = "DATE", nullable = false)
	private Date date;
	
	@Column(name = "REFERENCE")
	private String reference;
	
	@Column(name = "OBJET")
	private String objet;
	
	@Column(name = "PIECE_JOINTE")
	private String pieceJointe;
	
	@Column(name = "ORIGIN_FILE_NAME")
	private String originFileName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_FOURNISSEUR")
	private Fournisseur fournisseur;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_STATUT_EDITION")
	private StatutEdition statutEdition;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the objet
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param objet the objet to set
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * @return the pieceJointe
	 */
	public String getPieceJointe() {
		return pieceJointe;
	}

	/**
	 * @param pieceJointe the pieceJointe to set
	 */
	public void setPieceJointe(String pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	/**
	 * @return the fournisseur
	 */
	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	/**
	 * @param fournisseur the fournisseur to set
	 */
	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	/**
	 * @return the statutEdition
	 */
	public StatutEdition getStatutEdition() {
		return statutEdition;
	}

	/**
	 * @param statutEdition the statutEdition to set
	 */
	public void setStatutEdition(StatutEdition statutEdition) {
		this.statutEdition = statutEdition;
	}

	/**
	 * @return the originFileName
	 */
	public String getOriginFileName() {
		return originFileName;
	}

	/**
	 * @param originFileName the originFileName to set
	 */
	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
