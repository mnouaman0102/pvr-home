package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_EMAIL_RT")
public class EmailRt extends BaseEntity {

	private static final long serialVersionUID = 7665726506664300363L;
	
	@Column(name = "email")
	private String email;
	@Id
	@GeneratedValue
	private Long id;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
