/**
 * 
 */
package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_MES_RESERVE")
public class MesReserve extends BaseEntity{

	private static final long serialVersionUID = 7956394177523709489L;	
	
	
	@Column(name = "DATE")
	private Date date;	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_MES")
	private Mes mes;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RESERVE")
	private Reserve reserve;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_STATUT")
	private Statut statut;	
	
	@Column(name = "COMMENTAIRE", columnDefinition="TEXT")
	private String commentaire;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the mes
	 */
	public Mes getMes() {
		return mes;
	}

	/**
	 * @param mes the mes to set
	 */
	public void setMes(Mes mes) {
		this.mes = mes;
	}

	/**
	 * @return the reserve
	 */
	public Reserve getReserve() {
		return reserve;
	}

	/**
	 * @param reserve the reserve to set
	 */
	public void setReserve(Reserve reserve) {
		this.reserve = reserve;
	}

	/**
	 * @return the statut
	 */
	public Statut getStatut() {
		return statut;
	}

	/**
	 * @param statut the statut to set
	 */
	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
