package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 */
@Entity
@Table(name = "PVR_CRYPT_DECRYPT_DATA")
public class CryptDecryptData extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7665726596664239363L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	@Column(name = "COMPTE")
	private String compte;
	
	@Column(name = "LABEL_CRYPT")
	private String labelCrypt;
	
	@Column(name = "DATE_MODIFICATION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateModification;

	/**
	 * @return the compte
	 */
	public String getCompte() {
		return compte;
	}

	/**
	 * @param compte the compte to set
	 */
	public void setCompte(String compte) {
		this.compte = compte;
	}

	/**
	 * @return the labelCrypt
	 */
	public String getLabelCrypt() {
		return labelCrypt;
	}

	/**
	 * @param labelCrypt the labelCrypt to set
	 */
	public void setLabelCrypt(String labelCrypt) {
		this.labelCrypt = labelCrypt;
	}

	/**
	 * @return the dateModification
	 */
	public Date getDateModification() {
		return dateModification;
	}

	/**
	 * @param dateModification the dateModification to set
	 */
	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}


}