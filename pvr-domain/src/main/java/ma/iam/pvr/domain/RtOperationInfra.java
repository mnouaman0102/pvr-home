/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_RT_OPERATION_INFRA")
public class RtOperationInfra extends BaseEntity{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_OPERATION_INFRA")
	private OperationInfra operationInfra;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_RT_INFRA")
	private RtInfra rtInfra;
	@Id
	@GeneratedValue
	private Long id;

	public OperationInfra getOperationInfra() {
		return operationInfra;
	}

	public void setOperationInfra(OperationInfra operationInfra) {
		this.operationInfra = operationInfra;
	}

	public RtInfra getRtInfra() {
		return rtInfra;
	}

	public void setRtInfra(RtInfra rtInfra) {
		this.rtInfra = rtInfra;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
