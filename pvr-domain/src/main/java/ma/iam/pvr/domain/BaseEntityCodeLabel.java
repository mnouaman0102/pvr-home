package ma.iam.pvr.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


/**
 * Entité de base pour les objets (code, label)
 * 
 * @author Karami
 */
@MappedSuperclass
public abstract class BaseEntityCodeLabel extends BaseEntityLabel {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = -6950843390793197841L;
	@Column(name = "CODE")
	protected String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}