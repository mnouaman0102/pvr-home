package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import ma.iam.pvr.domain.BaseEntityLabel;

@Entity
@Table(name = "PVR_TYPE_ANTENNE")
public class TypeAntenne extends BaseEntityLabel {
	private static final long serialVersionUID = 1635047009990467690L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}