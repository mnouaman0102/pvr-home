package ma.iam.pvr.domain.utils;

/**
 * @author H.ELKHATEB
 *
 */
public class Constants {

	public final static String TYPE_PROJET_RADIO = "radio";
	public final static String TYPE_PROJET_ENV = "env";

	public final static Long TYPE_PROJET_ID_RADIO = 1L;
	public final static Long TYPE_PROJET_ID_ENV = 2L;

	public final static Long STATUT_EDITION_ID_DRAFT = 1L;
	public final static Long STATUT_EDITION_ID_VALID = 2L;
	
}
