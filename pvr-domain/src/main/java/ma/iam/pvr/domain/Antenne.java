package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PVR_ANTENNE")
public class Antenne extends BaseEntityLabel {
	private static final long serialVersionUID = 1635047005660467690L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
