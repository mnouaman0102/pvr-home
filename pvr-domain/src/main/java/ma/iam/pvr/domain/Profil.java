package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_PROFIL")
public class Profil extends BaseEntityLabel {

		private static final long serialVersionUID = 2799493742171532611L;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
