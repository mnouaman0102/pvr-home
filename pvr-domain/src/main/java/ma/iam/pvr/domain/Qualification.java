/**
 * 
 */
package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_QUALIFICATION")
public class Qualification extends BaseEntity {

	private static final long serialVersionUID = 7956394177523709489L;

	@Column(name = "DATE")
	private Date date;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_MES")
	private Mes mes;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_ENTITE")
	private Entite entite;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_STATUT_OPERATION")
	private StatutOperation statutOperation;

	@Column(name = "COMMENTAIRE", columnDefinition = "TEXT")
	private String commentaire;

	@Column(name = "TYPE_ACTION")
	private String actionType;

	@Column(name = "DATE_REPONSE")
	private Date dateReponse;

	@Column(name = "QUALIFIER_PAR")
	private String qualifierPar;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the mes
	 */
	public Mes getMes() {
		return mes;
	}

	/**
	 * @param mes
	 *            the mes to set
	 */
	public void setMes(Mes mes) {
		this.mes = mes;
	}

	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @param commentaire
	 *            the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * @return the entite
	 */
	public Entite getEntite() {
		return entite;
	}

	/**
	 * @param entite
	 *            the entite to set
	 */
	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	/**
	 * @return the statutOperation
	 */
	public StatutOperation getStatutOperation() {
		return statutOperation;
	}

	/**
	 * @param statutOperation
	 *            the statutOperation to set
	 */
	public void setStatutOperation(StatutOperation statutOperation) {
		this.statutOperation = statutOperation;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public Date getDateReponse() {
		return dateReponse;
	}

	public void setDateReponse(Date dateReponse) {
		this.dateReponse = dateReponse;
	}

	public String getQualifierPar() {
		return qualifierPar;
	}

	public void setQualifierPar(String qualifierPar) {
		this.qualifierPar = qualifierPar;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
