package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_DR")
public class Dr extends BaseEntityCodeLabel {

	
	private static final long serialVersionUID = 1635047007770467625L;
	
	@Column(name = "EMAIL")
	private String email;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
