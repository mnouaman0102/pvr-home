package ma.iam.pvr.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Entité de base pour les objets (code, label)
 * 
 * @author Karami
 */
@MappedSuperclass
public abstract class BaseEntityLabel extends BaseEntity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7299178223003335808L;
	@Column(name = "LABEL")
	protected String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}