package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_PIECE_JOINTE")
public class PieceJointe extends BaseEntity {

	
	private static final long serialVersionUID = 1635047007770467625L;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Column(name = "ORIGIN_FILE_NAME")
	private String originFileName;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the originFileName
	 */
	public String getOriginFileName() {
		return originFileName;
	}

	/**
	 * @param originFileName the originFileName to set
	 */
	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
