package ma.iam.pvr.domain.utils;

import java.util.List;

/**
 * @author H.ELKHATEB
 *
 */
public class CollectionUtils {

	public static boolean isEmpty(List<?> list) {
		if(list == null) {
			return true;
		}
		if(list.isEmpty()) {
			return true;
		}
		return false;
	}
	public static boolean isNotEmpty(List<?> list) {
		return !isEmpty(list);
	}
}
