package ma.iam.pvr.domain.utils;


/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class Utils {
	
public static Boolean containInjectChar(String str){
		if(str.contains(">") || str.contains("<") || str.contains("%") ||
			str.contains(";") || str.contains("=") || str.contains("'")){
			return true;
		}
		return false;
	}
}
