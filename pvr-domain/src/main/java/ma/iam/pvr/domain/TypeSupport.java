package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_TYPE_SUPPORT")
public class TypeSupport extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9181338497264577078L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
