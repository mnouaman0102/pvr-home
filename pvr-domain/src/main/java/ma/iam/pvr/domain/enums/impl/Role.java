package ma.iam.pvr.domain.enums.impl;

import ma.iam.pvr.domain.enums.GenericEnumValue;

public enum Role implements GenericEnumValue {
    FOURNISSEUR("FOURNISSEUR"),
    ADMIN("ADMINISTRATEUR")
    ;

    private final String code;
    Role(String code){

        this.code = code.intern();
    }
    @Override
    public String getCode() {
        return code;
    }


}
