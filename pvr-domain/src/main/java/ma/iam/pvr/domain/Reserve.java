package ma.iam.pvr.domain;


import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_RESERVE")
public class Reserve extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8737389659401130856L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PROJET")
	private Projet projet;	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_TYPE_PROJET")
	private TypeProjet typeProjet;	
	
	@Column(name = "RESPONSABILITE")
	private String responsabilite;
	
	@Column(name = "SEVERITE")
	private String severite;
	
	@Column(name = "CHAPITRE")
	private String chapitre;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the responsabilite
	 */
	public String getResponsabilite() {
		return responsabilite;
	}

	/**
	 * @param responsabilite the responsabilite to set
	 */
	public void setResponsabilite(String responsabilite) {
		this.responsabilite = responsabilite;
	}

	/**
	 * @return the severite
	 */
	public String getSeverite() {
		return severite;
	}

	/**
	 * @param severite the severite to set
	 */
	public void setSeverite(String severite) {
		this.severite = severite;
	}

	/**
	 * @return the chapitre
	 */
	public String getChapitre() {
		return chapitre;
	}

	/**
	 * @param chapitre the chapitre to set
	 */
	public void setChapitre(String chapitre) {
		this.chapitre = chapitre;
	}

	/**
	 * @return the projet
	 */
	public Projet getProjet() {
		return projet;
	}

	/**
	 * @param projet the projet to set
	 */
	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	/**
	 * @return 
	 */
	public TypeProjet getTypeProjet() {
		return typeProjet;
	}

	/**
	 * @param typeProjet
	 */
	public void setTypeProjet(TypeProjet typeProjet) {
		this.typeProjet = typeProjet;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
