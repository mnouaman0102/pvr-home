/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

import java.util.Date;



/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_ACTION_PARAM")
public class ActionParam extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 287113699378178905L;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "UTILISATEUR", columnDefinition="TEXT")
	private String utilisateur;
	
	@Column(name = "DESCRIPTION", columnDefinition="LONGTEXT")
	private String description;

	@Column(name = "DATE_OPERATION", nullable = false)
	private Date date;
	
	@Column(name = "TYPE_OPERATION", columnDefinition="TEXT")
	private String typeOperation;

	public String getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTypeOperation() {
		return typeOperation;
	}

	public void setTypeOperation(String typeOperation) {
		this.typeOperation = typeOperation;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
