/**
 * 
 */
package ma.iam.pvr.domain;

import javax.persistence.*;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Entity
@Table(name = "PVR_PJ_SITE_CANDIDAT_HIS")
public class PjSiteCandidat extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6510786600299212674L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_SITE_CANDIDAT_HIS")
	private SiteCandidatHistory siteCandidatHistory;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_PIECE_JOINTE")
	private PieceJointe pieceJointe;
	@Id
	@GeneratedValue
	private Long id;

	public SiteCandidatHistory getSiteCandidatHistory() {
		return siteCandidatHistory;
	}

	public void setSiteCandidatHistory(SiteCandidatHistory siteCandidatHistory) {
		this.siteCandidatHistory = siteCandidatHistory;
	}

	public PieceJointe getPieceJointe() {
		return pieceJointe;
	}

	public void setPieceJointe(PieceJointe pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
