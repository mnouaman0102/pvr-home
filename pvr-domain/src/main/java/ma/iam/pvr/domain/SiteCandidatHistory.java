/**
 * 
 */
package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * @author Z.BELGHAOUTI
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_SITE_CANDIDAT_HISTORY")
public class SiteCandidatHistory extends BaseEntity {

	private static final long serialVersionUID = 7956394177523709489L;

	@Column(name = "DATE_VALIDATION")
	private Date dateValidation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_SITE")
	private Site site;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_ENTITE")
	private Entite entite;

	@Column(name = "STATUT_CANDIDAT")
	private String statutCandidat;

	@Column(name = "COMMENTAIRE", columnDefinition = "TEXT")
	private String commentaire;

	@Column(name = "X_CGPS")
	private Double xCGPS;

	@Column(name = "Y_CGPS")
	private Double yCGPS;

	@Column(name = "HBA")
	private Double hBA;

	@Column(name = "AZIMUT", columnDefinition = "TEXT")
	private String azimut;

	@Column(name = "TILT_MECANIQUE", columnDefinition = "TEXT")
	private String tiltMecanique;

	@Column(name = "TILT_ELECTRIQUE", columnDefinition = "TEXT")
	private String tiltElectrique;

	@Column(name = "ANTENNE")
	private String antenne;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FAIT_PAR")
	private Utilisateur utilisateur;

	@Column(name = "ETAPE_SOUMISSION")
	private String etapeSoumission;

	@Column(name = "PARTAGE")
	private String partage;

	@Column(name = "OP_PARTAGE")
	private String opPartage;

	@Column(name = "LIEU_INSTALLATION")
	private String lieuInstallation;
	
	@Column(name = "TYPE_PROPRIETE")	
	private String typePropriete;
	
	@Column(name = "DATE_ACQUISITION")	
	private Date dateAcquisition;
	
	@Column(name = "ETAT_INFRA")	
	private String etatInfra;

	@Column(name = "HAUTEUR_SUPPORT")
	private Double hauteurSupport;

	@Column(name = "TYPE_SUPPORT")
	private String typeSupport;
	
	@Column(name = "ETAT_PREREQUIS")	
	private String etatPrerequis;

	@Column(name = "TYPE_TRANSMISSION")
	private String typeTransmission;

	@Column(name = "TYPE_ALIMENTATION")
	private String typeAlimentation;

	@Column(name = "NUM_CIL")
	private String numCil;

	@Column(name = "CENTRE_RATTACHEMENT")
	private String centreRattachement;

	@Column(name = "ETAT_BATTERIE")
	private String etatBatterie;

	@Column(name = "SURVEILLANCE_CAMERA")
	private String surveillanceCamera;

	@Column(name = "PHASE")
	private String phase;
	@Id
	@GeneratedValue
	private Long id;

	public Date getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public Entite getEntite() {
		return entite;
	}

	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	public String getStatutCandidat() {
		return statutCandidat;
	}

	public void setStatutCandidat(String statutCandidat) {
		this.statutCandidat = statutCandidat;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Double getxCGPS() {
		return xCGPS;
	}

	public void setxCGPS(Double xCGPS) {
		this.xCGPS = xCGPS;
	}

	public Double getyCGPS() {
		return yCGPS;
	}

	public void setyCGPS(Double yCGPS) {
		this.yCGPS = yCGPS;
	}

	public Double getHauteurSupport() {
		return hauteurSupport;
	}

	public void setHauteurSupport(Double hauteurSupport) {
		this.hauteurSupport = hauteurSupport;
	}

	public Double gethBA() {
		return hBA;
	}

	public void sethBA(Double hBA) {
		this.hBA = hBA;
	}

	public String getAzimut() {
		return azimut;
	}

	public void setAzimut(String azimut) {
		this.azimut = azimut;
	}

	public String getTiltMecanique() {
		return tiltMecanique;
	}

	public void setTiltMecanique(String tiltMecanique) {
		this.tiltMecanique = tiltMecanique;
	}

	public String getTiltElectrique() {
		return tiltElectrique;
	}

	public void setTiltElectrique(String tiltElectrique) {
		this.tiltElectrique = tiltElectrique;
	}

	public String getAntenne() {
		return antenne;
	}

	public void setAntenne(String antenne) {
		this.antenne = antenne;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public String getEtapeSoumission() {
		return etapeSoumission;
	}

	public void setEtapeSoumission(String etapeSoumission) {
		this.etapeSoumission = etapeSoumission;
	}

	public String getPartage() {
		return partage;
	}

	public void setPartage(String partage) {
		this.partage = partage;
	}

	public String getOpPartage() {
		return opPartage;
	}

	public void setOpPartage(String opPartage) {
		this.opPartage = opPartage;
	}

	public String getLieuInstallation() {
		return lieuInstallation;
	}

	public void setLieuInstallation(String lieuInstallation) {
		this.lieuInstallation = lieuInstallation;
	}

	public String getTypePropriete() {
		return typePropriete;
	}

	public void setTypePropriete(String typePropriete) {
		this.typePropriete = typePropriete;
	}

	public Date getDateAcquisition() {
		return dateAcquisition;
	}

	public void setDateAcquisition(Date dateAcquisition) {
		this.dateAcquisition = dateAcquisition;
	}

	public String getEtatInfra() {
		return etatInfra;
	}

	public void setEtatInfra(String etatInfra) {
		this.etatInfra = etatInfra;
	}

	public String getEtatPrerequis() {
		return etatPrerequis;
	}

	public void setEtatPrerequis(String etatPrerequis) {
		this.etatPrerequis = etatPrerequis;
	}

	public String getTypeSupport() {
		return typeSupport;
	}

	public void setTypeSupport(String typeSupport) {
		this.typeSupport = typeSupport;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getTypeAlimentation() {
		return typeAlimentation;
	}

	public void setTypeAlimentation(String typeAlimentation) {
		this.typeAlimentation = typeAlimentation;
	}

	public String getTypeTransmission() {
		return typeTransmission;
	}

	public void setTypeTransmission(String typeTransmission) {
		this.typeTransmission = typeTransmission;
	}

	public String getCentreRattachement() {
		return centreRattachement;
	}

	public void setCentreRattachement(String centreRattachement) {
		this.centreRattachement = centreRattachement;
	}

	public String getNumCil() {
		return numCil;
	}

	public void setNumCil(String numCil) {
		this.numCil = numCil;
	}

	public String getEtatBatterie() {
		return etatBatterie;
	}

	public void setEtatBatterie(String etatBatterie) {
		this.etatBatterie = etatBatterie;
	}

	public String getSurveillanceCamera() {
		return surveillanceCamera;
	}

	public void setSurveillanceCamera(String surveillanceCamera) {
		this.surveillanceCamera = surveillanceCamera;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
