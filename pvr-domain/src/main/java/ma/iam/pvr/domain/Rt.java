package ma.iam.pvr.domain;

import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_RT")
public class Rt extends BaseEntity {

	private static final long serialVersionUID = -6596624334965704197L;

	@Column(name = "DATE", nullable = false)
	private Date date;

	@Column(name = "REFERENCE")
	private String reference;

	@Column(name = "OBJET")
	private String objet;

	@Column(name = "PIECE_JOINTE")
	private String pieceJointe;

	@Column(name = "ORIGIN_FILE_NAME")
	private String originFileName;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_FOURNISSEUR")
	private Fournisseur fournisseur;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDT_CONTRAT")
	private Contrat contrat;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_STATUT_EDITION")
	private StatutEdition statutEdition;

	@Column(name = "COMMENTAIRE")
	private String commentaire;

	@Column(name = "WORKFLOW")
	private String workflow;

	@Column(name = "DATE_VALIDATION")
	private Date dateValidation;

	@Column(name = "TITRE_REMISE_CONTROLE")
	private String titreRemiseControle;

	@Column(name = "FROM_REMISE_CONTROLE")
	private Boolean fromRemise;
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference
	 *            the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the objet
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param objet
	 *            the objet to set
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * @return the fournisseur
	 */
	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	/**
	 * @param fournisseur
	 *            the fournisseur to set
	 */
	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	/**
	 * @return the contrat
	 */
	public Contrat getContrat() {
		return contrat;
	}

	/**
	 * @param contrat
	 *            the contrat to set
	 */
	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	/**
	 * @return the pieceJointe
	 */
	public String getPieceJointe() {
		return pieceJointe;
	}

	/**
	 * @param pieceJointe
	 *            the pieceJointe to set
	 */
	public void setPieceJointe(String pieceJointe) {
		this.pieceJointe = pieceJointe;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * @return the statutEdition
	 */
	public StatutEdition getStatutEdition() {
		return statutEdition;
	}

	/**
	 * @param statutEdition
	 *            the statutEdition to set
	 */
	public void setStatutEdition(StatutEdition statutEdition) {
		this.statutEdition = statutEdition;
	}

	/**
	 * @return the originFileName
	 */
	public String getOriginFileName() {
		return originFileName;
	}

	/**
	 * @param originFileName
	 *            the originFileName to set
	 */
	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}

	public String getWorkflow() {
		return workflow;
	}

	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}

	public Date getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	public String getTitreRemiseControle() {
		return titreRemiseControle;
	}

	public void setTitreRemiseControle(String titreRemiseControle) {
		this.titreRemiseControle = titreRemiseControle;
	}

	public Boolean getFromRemise() {
		return fromRemise;
	}

	public void setFromRemise(Boolean fromRemise) {
		this.fromRemise = fromRemise;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
