package ma.iam.pvr.domain;

import javax.persistence.*;
import ma.iam.pvr.domain.enums.impl.Role;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
public class Utilisateur extends BaseEntity {	/**
	 * 
	 */
	private static final long serialVersionUID = -4981438598518341400L;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToMany(targetEntity = Profil.class, fetch = FetchType.LAZY)
	@JoinTable(name = "PVR_PROFIL_USER", joinColumns = @JoinColumn(name = "IDT_USER"), inverseJoinColumns = @JoinColumn(name = "IDT_PROFIL"))
	private List<Profil> listProfils;

	
	@Column(name = "LOGIN",unique = true)
	private String login;
	@Column(name = "password")
	private String password;

	@Column(name = "NOM")
	private String nom;

	@Column(name = "PRENOM")
	private String prenom;

	@Column(name = "MATRICULE")
	private Integer matricule;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_ENTITE")
	private Entite entite;
	
	@Column(name = "EMAIL")
	private String email;
	@ElementCollection(targetClass = Role.class,fetch = FetchType.EAGER)
	@CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
	@Enumerated
	private List<Role> roles;

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_FOURNISSEUR")
	private Fournisseur fournisseur;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDT_DR")
	private Dr dr;
	
    @Column(name = "DATE_DERNIERE_CONNEXION")
    private Date dateLastConnection;


	public Utilisateur() {
	}


	public String getLogin() {
		return this.login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the matricule
	 */
	public Integer getMatricule() {
		return matricule;
	}

	/**
	 * @param matricule
	 *            the matricule to set
	 */
	public void setMatricule(Integer matricule) {
		this.matricule = matricule;
	}

	public List<Profil> getListProfils() {
		return listProfils;
	}

	public void setListProfils(List<Profil> profilsUser) {
		this.listProfils = profilsUser;
	}

	/**
	 * @return the entite
	 */
	public Entite getEntite() {
		return entite;
	}

	/**
	 * @param entite the entite to set
	 */
	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public Fournisseur getFournisseur() {
		return fournisseur;
	}
	
	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Dr getDr() {
		return dr;
	}
	public void setDr(Dr dr) {
		this.dr = dr;
	}

	public Date getDateLastConnection() {
		return dateLastConnection;
	}

	public void setDateLastConnection(Date dateLastConnection) {
		this.dateLastConnection = dateLastConnection;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
