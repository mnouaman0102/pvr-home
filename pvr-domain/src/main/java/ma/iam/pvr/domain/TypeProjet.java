package ma.iam.pvr.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Entity
@Table(name = "PVR_TYPE_PROJET")
public class TypeProjet extends BaseEntityCodeLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1261080910145067843L;
	@Id
	@GeneratedValue
	private Long id;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
