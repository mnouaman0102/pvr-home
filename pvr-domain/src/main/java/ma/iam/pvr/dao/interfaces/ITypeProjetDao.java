package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.TypeProjet;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface ITypeProjetDao extends IGenericDao<TypeProjet, Long> {

	
}
