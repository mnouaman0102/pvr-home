package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.EtatRemiseControle;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IEtatRemiseControleDao extends IGenericDao<EtatRemiseControle, Long> {
	EtatRemiseControle getByCode(String code);
}
