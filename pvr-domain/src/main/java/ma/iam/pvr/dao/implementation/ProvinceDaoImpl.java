package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.IProvinceDao;
import ma.iam.pvr.domain.Province;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ProvinceDaoImpl extends DefaultGenericDao<Province ,Long> implements IProvinceDao {

	public Province getProvinceByRegionAndLabel(Long idtRegion, String label) {
		Criteria cr = this.getSession().createCriteria(Province.class, "province");
		cr.createAlias("province.region", "region");
		cr.add(Restrictions.eq("region.idt", idtRegion));
		cr.add(Restrictions.eq("province.label", label));
		cr.setMaxResults(1);
		return (Province) cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Province> getProvinceByRegion(Long idtRegion) {
		Criteria cr = this.getSession().createCriteria(Province.class, "province");
		cr.createAlias("province.region", "region");
		cr.add(Restrictions.eq("region.idt", idtRegion));
		return  cr.list();
	}
}
