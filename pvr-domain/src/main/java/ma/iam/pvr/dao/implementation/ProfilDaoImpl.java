package ma.iam.pvr.dao.implementation;


import ma.iam.pvr.dao.interfaces.IProfilDao;
import ma.iam.pvr.domain.Profil;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class ProfilDaoImpl extends DefaultGenericDao<Profil, Long> implements IProfilDao {

	public Profil getProfilByName(String label) {
		Criteria cr = this.getSession().createCriteria(Profil.class);
		cr.add(Restrictions.eq("label", label).ignoreCase());
		return (Profil) cr.uniqueResult();
	}

}
