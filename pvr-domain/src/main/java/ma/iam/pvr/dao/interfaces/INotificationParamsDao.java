package ma.iam.pvr.dao.interfaces;


import ma.iam.pvr.domain.NotificationParams;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface INotificationParamsDao extends IGenericDao<NotificationParams, Long> {

	NotificationParams getNotificationParamsByDrAndEntiteAndFournisseur(Long idtDr, Long idtEntite, Long idtFournisseur);
	
}
