package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.ProjetReserve;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IProjetReserveDao extends IGenericDao<ProjetReserve, Long> {


}
