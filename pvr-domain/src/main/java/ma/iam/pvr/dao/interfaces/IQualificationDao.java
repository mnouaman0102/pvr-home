package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Qualification;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IQualificationDao extends IGenericDao<Qualification, Long> {
	
	Qualification getQualificationExploitationByMesId(Long idtMes);
	Qualification getQualificationOptimByMesId(Long idtMes);
	Qualification getQualificationDccByMesId(Long idtMes);	
	List<Qualification> getAllQualifsForDeploiByMesId(Long idtMes);
	Qualification getQualifByMesIdAndEntite(Long idtMes, Long idtEntite);
	Qualification getQualifDccByMesId(Long idtMes);
	Qualification getQualifDccEnvByMesId(Long idtMes);
	Qualification getQualificationDccEnvByMesId(Long idtMes);
	Qualification getQualificationQosByMesId(Long idtMes);
	List<Qualification> getQualifsByMesIdt(Long idtMes);
}
