package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Reserve;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IReserveDao extends IGenericDao<Reserve, Long> {

	Reserve getByLabel(String reserve);

	int findTotalReserves(Long idtProjet, String label, boolean isRadio);

	List<Reserve> findAllReservesPagination(Long idtProjet, String label, boolean isRadio, int pageNumber, int pageSize);

	List<Reserve> getByProjet(Long idtProjet);
	
	List<Reserve> getByCriteria(Long idtProjet, Long idtTypeProjet);

	List<String> findAllLabels();

	List<Reserve> findAllReserves();		
	
}
