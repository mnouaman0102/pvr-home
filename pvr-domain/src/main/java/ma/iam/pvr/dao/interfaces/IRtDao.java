package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Rt;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IRtDao extends IGenericDao<Rt, Long> {

	List<Rt> getListRtByContrat(Long idtContrat);
	
	List<Rt> getAllRt(Integer pageNumber, Integer pageSize, String workflow);
	
	Integer countAllRt(String workflow);
	
	List<Rt> getListRtDraft(Integer pageNumber, Integer pageSize, String workflow);
	
	Integer countListRtDraft(String workflow);
	
	List<Rt> getListRtValid(Integer pageNumber, Integer pageSize, String workflow);
	
	Integer countListRtValid(String workflow);
}
