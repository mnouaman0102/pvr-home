package ma.iam.pvr.dao.implementation;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ma.iam.pvr.dao.interfaces.IResponsabiliteDao;
import ma.iam.pvr.domain.Responsabilite;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

public class ResponsabiliteDaoImpl extends DefaultGenericDao<Responsabilite, Long> implements IResponsabiliteDao{

	public Responsabilite getResponsabiliteByLabel(String label){
		Criteria cr = this.getSession().createCriteria(Responsabilite.class, "responsabilite");
		cr.add(Restrictions.eq("responsabilite.label", label));
		cr.setMaxResults(1);
		return (Responsabilite) cr.uniqueResult();
	}
}
