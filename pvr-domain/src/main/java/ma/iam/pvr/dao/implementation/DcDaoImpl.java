package ma.iam.pvr.dao.implementation;

import java.util.List;

import ma.iam.pvr.dao.interfaces.IDcDao;
import ma.iam.pvr.domain.Dc;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class DcDaoImpl extends DefaultGenericDao<Dc, Long> implements IDcDao {

	public Dc getDcByLabel(String label) {
		Criteria cr = this.getSession().createCriteria(Dc.class, "dc");
		cr.add(Restrictions.eq("dc.label", label));
		cr.setMaxResults(1);
		return (Dc) cr.uniqueResult();
	}

	public Dc getDcByDrAndLabel(Long idtDr, String label) {
		Criteria cr = this.getSession().createCriteria(Dc.class, "dc");
		cr.createAlias("dc.dr", "dr");
		cr.add(Restrictions.eq("dr.idt", idtDr));
		cr.add(Restrictions.eq("dc.label", label));
		cr.setMaxResults(1);
		return (Dc) cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Dc> getDcByDr(Long idtDr) {
		Criteria cr = this.getSession().createCriteria(Dc.class, "dc");
		cr.createAlias("dc.dr", "dr");
		cr.add(Restrictions.eq("dr.idt", idtDr));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<Dc> getDcByDrs(List<Long> listIdtsDr) {
		Criteria cr = this.getSession().createCriteria(Dc.class, "dc");
		cr.createAlias("dc.dr", "dr");
		if(listIdtsDr != null && listIdtsDr.size() > 0){
			cr.add(Restrictions.in("dr.idt", listIdtsDr));
		}		
		return (List<Dc>) cr.list();
	}

}
