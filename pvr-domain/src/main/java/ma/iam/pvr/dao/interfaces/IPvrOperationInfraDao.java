package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.PvrOperationInfra;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IPvrOperationInfraDao extends IGenericDao<PvrOperationInfra, Long> {

	PvrOperationInfra getMesPvrByMes(Long idtMes);
	List<PvrOperationInfra> getListMesPvrByMes(Long idtMes);
	List<PvrOperationInfra> getListMesPvrByPvr(Long idtPvr);
	PvrOperationInfra getLastMesPvrValidByMes(Long idtMes);
	List<PvrOperationInfra> getListMesPvrValidByMes(Long idtMes);
}
