package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.MesReserve;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IMesReserveDao extends IGenericDao<MesReserve, Long> {

	List<MesReserve> getReservesByMesId(Long idtMes);
	Long deleteMesReserveByMesIdt(Long idtMes);
	List<MesReserve> getReservesNotNormaliseByMesId(Long idtMes);
}
