package ma.iam.pvr.dao.implementation;

import java.util.List;

import ma.iam.pvr.dao.interfaces.IPvrInfraDao;
import ma.iam.pvr.domain.PvrInfra;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class PvrInfraDaoImpl extends DefaultGenericDao<PvrInfra, Long> implements IPvrInfraDao {

	@SuppressWarnings("unchecked")
	public List<PvrInfra> getListPvrByContrat(Long idtContrat) {
		Criteria cr = this.getSession().createCriteria(PvrInfra.class,"pvr");
		cr.createAlias("pvr.contrat", "contrat");
		cr.add(Restrictions.eq("contrat.idt", idtContrat));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PvrInfra> getAllPvr(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(PvrInfra.class, "pvr");
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvr.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.addOrder(Order.desc("pvr.idt"));
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	public Integer countAllPvr() {
		Criteria cr = this.getSession().createCriteria(PvrInfra.class, "pvr");
		cr.setProjection(Projections.rowCount());
		return (Integer) cr.uniqueResult();
	}
	
	public Integer countListPvrDraft() {
		Criteria cr = this.getSession().createCriteria(PvrInfra.class, "pvr");
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<PvrInfra> getListPvrDraft(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(PvrInfra.class, "pvr");
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvr.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("pvr.idt"));
			return cr.list();
		}
		return null;
	}
	
	public Integer countListPvrValid() {
		Criteria cr = this.getSession().createCriteria(PvrInfra.class, "pvr");
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<PvrInfra> getListPvrValid(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(PvrInfra.class, "pvr");
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvr.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("pvr.idt"));
			return cr.list();
		}
		return null;
	}
	
}
