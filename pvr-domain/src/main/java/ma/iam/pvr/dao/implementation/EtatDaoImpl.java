package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IEtatDao;
import ma.iam.pvr.domain.Etat;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class EtatDaoImpl extends DefaultGenericDao<Etat, Long> implements IEtatDao {

	

	@SuppressWarnings("unchecked")
	public List<Etat> getListEtatsByIdts(List<Long> idts) {
		Criteria cr = this.getSession().createCriteria(Etat.class, "etat");
		cr.add(Restrictions.in("etat.idt", idts));
		return cr.list();
	}
}

