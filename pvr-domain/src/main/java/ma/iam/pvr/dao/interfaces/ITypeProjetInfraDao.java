package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.TypeProjetInfra;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface ITypeProjetInfraDao extends IGenericDao<TypeProjetInfra, Long> {

}
