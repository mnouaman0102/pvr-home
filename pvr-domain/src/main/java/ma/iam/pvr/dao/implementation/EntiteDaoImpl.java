package ma.iam.pvr.dao.implementation;


import ma.iam.pvr.dao.interfaces.IEntiteDao;
import ma.iam.pvr.domain.Entite;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class EntiteDaoImpl extends DefaultGenericDao<Entite, Long> implements IEntiteDao {


	public Entite getEntiteByLabel(String label) {
		Criteria cr = this.getSession().createCriteria(Entite.class, "entite");
		cr.add(Restrictions.eq("entite.label", label));
		return (Entite) cr.uniqueResult();
	}
	
	public Entite getEntiteByCode(String code) {
		Criteria cr = this.getSession().createCriteria(Entite.class, "entite");
		cr.add(Restrictions.eq("entite.code", code));
		cr.setMaxResults(1);
		return (Entite) cr.uniqueResult();
	}
	
}
