package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.IStatutEditionDao;
import ma.iam.pvr.domain.StatutEdition;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class StatutEditionDaoImpl extends DefaultGenericDao<StatutEdition, Long> implements IStatutEditionDao {



	
}
