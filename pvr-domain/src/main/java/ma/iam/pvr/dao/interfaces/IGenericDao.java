package ma.iam.pvr.dao.interfaces;
import java.io.Serializable;
import java.util.List;

public interface IGenericDao<T extends Serializable,ID extends Serializable>{
    void setClazz(Class< T > clazzToSet);

    T get(final ID id);

    List<T> findAll();

    T save(final T entity);

    T update(final T entity);

    void delete(final T entity);

    void deleteById(final ID entityId);

}
