package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.Fournisseur;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IFournisseurDao extends IGenericDao<Fournisseur, Long> {
	
	Fournisseur getFournisseurByLabel(String label);
}
