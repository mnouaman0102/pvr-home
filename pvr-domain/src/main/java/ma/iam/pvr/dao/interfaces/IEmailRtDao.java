package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.EmailRt;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI,K.ELBAGUARI
 *
 */
public interface IEmailRtDao extends IGenericDao<EmailRt, Long> {
}
