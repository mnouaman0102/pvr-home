package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.EtatOperationInfra;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IEtatOperationInfraDao extends IGenericDao<EtatOperationInfra, Long>  {

	List<EtatOperationInfra> getListEtatsByIdts(List<Long> idts);

}
