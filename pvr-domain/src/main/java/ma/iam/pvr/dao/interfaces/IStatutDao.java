package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.Statut;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IStatutDao extends IGenericDao<Statut, Long> {

	Statut getByLabel(String statut);
	
}
