package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Province;
//import ma.iam.socle.service.GenericDao;

public interface IProvinceDao extends IGenericDao<Province,Long> {

	Province getProvinceByRegionAndLabel(Long idtRegion, String label);
	List<Province>  getProvinceByRegion(Long idtRegion);
}
