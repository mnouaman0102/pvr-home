package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IMesPvrDao;
import ma.iam.pvr.domain.MesPvr;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class MesPvrDaoImpl extends DefaultGenericDao<MesPvr, Long> implements IMesPvrDao {


	public MesPvr getMesPvrByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesPvr.class,"mes_pvr");
		cr.createAlias("mes_pvr.mes", "mes");
		cr.createAlias("mes_pvr.pvr", "pvr");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		cr.setMaxResults(1);
		return (MesPvr) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<MesPvr> getListMesPvrByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesPvr.class,"mes_pvr");
		cr.createAlias("mes_pvr.mes", "mes");
		cr.createAlias("mes_pvr.pvr", "pvr");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		return  cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<MesPvr> getListMesPvrByPvr(Long idtPvr) {
		Criteria cr = this.getSession().createCriteria(MesPvr.class,"mes_pvr");
		cr.createAlias("mes_pvr.pvr", "pvr");
		cr.add(Restrictions.eq("pvr.idt", idtPvr));
		return  cr.list();
	}
	
	public MesPvr getLastMesPvrValidByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesPvr.class,"mes_pvr");
		cr.createAlias("mes_pvr.mes", "mes");
		cr.createAlias("mes_pvr.pvr", "pvr");
		
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		cr.setMaxResults(1);
		return (MesPvr) cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<MesPvr> getListMesPvrValidByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesPvr.class,"mes_pvr");
		cr.createAlias("mes_pvr.mes", "mes");
		cr.createAlias("mes_pvr.pvr", "pvr");
		
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		return  cr.list();
	}

}

