package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IEmailDao;
import ma.iam.pvr.domain.Email;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Z.BELGHAOUTI,K.ELBAGUARI
 *
 */
@Repository
public class EmailDaoImpl extends DefaultGenericDao<Email, Long> implements IEmailDao {


	@SuppressWarnings("unchecked")
	public List<Email> getListEmailByEntite(Long idtEntite) {
		Criteria cr = this.getSession().createCriteria(Email.class,"email");
		cr.createAlias("email.entite", "entite");
		cr.add(Restrictions.eq("entite.idt", idtEntite));
		return cr.list();
	}


	@SuppressWarnings("unchecked")
	public List<Email> getListEmailByMesId(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(Email.class,"email");
		cr.createAlias("email.mes", "mes");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Email> getListEmailByRemise(Long idtRemise) {
		Criteria cr = this.getSession().createCriteria(Email.class,"email");
		cr.createAlias("email.remiseControle", "remiseControle");
		cr.add(Restrictions.eq("remiseControle.idt", idtRemise));
		return cr.list();
	}
}
