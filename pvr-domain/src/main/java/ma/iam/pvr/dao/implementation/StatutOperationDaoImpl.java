package ma.iam.pvr.dao.implementation;


import ma.iam.pvr.dao.interfaces.IStatutOperationDao;
import ma.iam.pvr.domain.StatutOperation;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class StatutOperationDaoImpl extends DefaultGenericDao<StatutOperation, Long> implements IStatutOperationDao {


}
