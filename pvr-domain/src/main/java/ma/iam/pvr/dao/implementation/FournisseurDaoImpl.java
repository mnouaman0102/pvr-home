package ma.iam.pvr.dao.implementation;



import ma.iam.pvr.dao.interfaces.IFournisseurDao;
import ma.iam.pvr.domain.Fournisseur;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class FournisseurDaoImpl extends DefaultGenericDao<Fournisseur, Long> implements IFournisseurDao {

	public Fournisseur getFournisseurByLabel(String label) {
		Criteria cr = this.getSession().createCriteria(Fournisseur.class, "fournisseur");
		
		cr.add(Restrictions.eq("fournisseur.label", label));
		cr.setMaxResults(1);
		return (Fournisseur) cr.uniqueResult();
	}
}
