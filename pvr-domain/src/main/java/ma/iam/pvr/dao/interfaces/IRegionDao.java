package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.Region;
//import ma.iam.socle.service.GenericDao;

public interface IRegionDao  extends IGenericDao<Region,Long>{
	Region getRegionByLabel(String label);
}
