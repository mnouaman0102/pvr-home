package ma.iam.pvr.dao.implementation;

import java.util.List;

import ma.iam.pvr.dao.interfaces.IMesReserveDao;
import ma.iam.pvr.domain.MesReserve;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class MesReserveDaoImpl extends DefaultGenericDao<MesReserve, Long> implements IMesReserveDao {


	@SuppressWarnings("unchecked")
	public List<MesReserve> getReservesByMesId(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesReserve.class, "mesReserve");
		cr.createAlias("mesReserve.mes", "mes");
		cr.createAlias("mesReserve.reserve", "reserve");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("reserve.responsabilite"));
		return cr.list();
	}
	
	public Long deleteMesReserveByMesIdt(Long idtMes) {
		Session session = this.getSession();  
		String hql = "delete from MesReserve where mes.idt= :id"; 
		session.createQuery(hql).setLong("id", idtMes).executeUpdate();
		return idtMes;
	}

	@SuppressWarnings("unchecked")
	public List<MesReserve> getReservesNotNormaliseByMesId(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesReserve.class, "mesReserve");
		cr.createAlias("mesReserve.mes", "mes");
		cr.createAlias("mesReserve.reserve", "reserve");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("mesReserve.statut.idt", 1L));
		cr.addOrder(Order.desc("reserve.responsabilite"));
		return cr.list();
	}
}

