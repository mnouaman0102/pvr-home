package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.PieceJointe;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IPieceJointeDao extends IGenericDao<PieceJointe, Long> {

	PieceJointe getOriginFileName(String fileName);
	
}
