package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.OperationInfraReserve;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IOperationInfraReserveDao extends IGenericDao<OperationInfraReserve, Long> {

	List<OperationInfraReserve> getReservesByOperationInfraId(Long idtMes);
	Long deleteOperationInfraReserveByOperationInfraIdt(Long idtMes);
	List<OperationInfraReserve> getReservesNotNormaliseByOperationInfraId(Long idtMes);
}
