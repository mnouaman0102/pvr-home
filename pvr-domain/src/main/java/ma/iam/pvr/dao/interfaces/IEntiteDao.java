package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.Entite;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IEntiteDao extends IGenericDao<Entite, Long> {
	
	Entite getEntiteByLabel(String label);

	Entite getEntiteByCode(String code);
}
