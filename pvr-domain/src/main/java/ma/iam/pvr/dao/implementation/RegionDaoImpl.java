package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.IRegionDao;
import ma.iam.pvr.domain.Region;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class RegionDaoImpl extends DefaultGenericDao<Region,Long> implements IRegionDao {

	public Region getRegionByLabel(String label) {
		Criteria cr = this.getSession().createCriteria(Region.class, "region");
		cr.add(Restrictions.eq("region.label", label));
		cr.setMaxResults(1);
		return (Region) cr.uniqueResult();
	}
}
