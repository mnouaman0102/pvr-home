package ma.iam.pvr.dao.implementation;


import ma.iam.pvr.dao.interfaces.IEtatRemiseControleDao;
import ma.iam.pvr.domain.EtatRemiseControle;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class EtatRemiseControleDaoImpl extends DefaultGenericDao<EtatRemiseControle, Long> implements IEtatRemiseControleDao {


	public EtatRemiseControle getByCode(String code) {
		Criteria cr = this.getSession().createCriteria(EtatRemiseControle.class, "etatRemiseControle");
		cr.add(Restrictions.eq("etatRemiseControle.code", code).ignoreCase());
		return (EtatRemiseControle) cr.uniqueResult();
	}
}
