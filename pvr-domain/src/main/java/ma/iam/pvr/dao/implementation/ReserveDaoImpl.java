package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IReserveDao;
import ma.iam.pvr.domain.Reserve;
import ma.iam.pvr.domain.utils.Constants;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class ReserveDaoImpl extends DefaultGenericDao<Reserve, Long> implements IReserveDao {


	public Reserve getByLabel(String reserve) {
		Criteria cr = this.getSession().createCriteria(Reserve.class);
		cr.add(Restrictions.eq("label", reserve).ignoreCase());
		return (Reserve) cr.uniqueResult();
	}

	public int findTotalReserves(Long idtProjet, String label, boolean isRadio) {
		Criteria cr = this.getSession().createCriteria(Reserve.class, "reserve");
		cr.createAlias("reserve.projet", "projet");
		cr.createAlias("reserve.typeProjet", "typeProjet");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projet.idt", idtProjet));		
		}
		if(label != null && !label.equals("")){
			cr.add(Restrictions.eq("reserve.label", label));
		}
		if(isRadio){
			cr.add(Restrictions.eq("typeProjet.idt", Constants.TYPE_PROJET_ID_RADIO));
		}
		else{
			cr.add(Restrictions.eq("typeProjet.idt", Constants.TYPE_PROJET_ID_ENV));
		}
		cr.addOrder(Order.desc("reserve.idt"));
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();

	}

	@SuppressWarnings("unchecked")
	public List<Reserve> findAllReservesPagination(Long idtProjet, String label, boolean isRadio, int pageNumber, int pageSize) {
		Criteria cr = this.getSession().createCriteria(Reserve.class, "reserve");
		cr.createAlias("reserve.projet", "projet");
		cr.createAlias("reserve.typeProjet", "typeProjet");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projet.idt", idtProjet));		
		}
		if(label != null && !label.equals("")){
			cr.add(Restrictions.eq("reserve.label", label));
		}
		if(isRadio){
			cr.add(Restrictions.eq("typeProjet.idt", Constants.TYPE_PROJET_ID_RADIO));
		}
		else{
			cr.add(Restrictions.eq("typeProjet.idt", Constants.TYPE_PROJET_ID_ENV));
		}
		//Pagination
		if(pageNumber>0 && pageSize>0){
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
		}else{
			cr.setFirstResult(0);
		}
		cr.addOrder(Order.desc("reserve.idt"));
		return  cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<Reserve> getByProjet(Long idtProjet) {
		Criteria cr = this.getSession().createCriteria(Reserve.class, "reserve");
		cr.createAlias("reserve.projet", "projet");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projet.idt", idtProjet));		
		}			
		return  cr.list();
	}
	@SuppressWarnings("unchecked")
	public List<Reserve> getByCriteria(Long idtProjet, Long idtTypeProjet) {
		Criteria cr = this.getSession().createCriteria(Reserve.class, "reserve");
		cr.createAlias("reserve.projet", "projet");
		cr.createAlias("reserve.typeProjet", "typeProjet");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projet.idt", idtProjet));		
		}
		if(idtTypeProjet != 0) {
			cr.add(Restrictions.eq("typeProjet.idt", idtTypeProjet));
		}
		return  cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<String> findAllLabels() {
		Criteria cr = this.getSession().createCriteria(Reserve.class)
			    .setProjection(Projections.projectionList()
			    .add(Projections.property("label"), "label"));
		cr.addOrder(Order.asc("label"));
		return (List<String>) cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<Reserve> findAllReserves() {
		Criteria cr = this.getSession().createCriteria(Reserve.class);
		cr.addOrder(Order.asc("label"));
		return cr.list();
	}

}
