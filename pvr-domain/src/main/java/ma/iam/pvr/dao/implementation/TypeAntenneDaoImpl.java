package ma.iam.pvr.dao.implementation;
import ma.iam.pvr.dao.interfaces.ITypeAntenneDao;
import ma.iam.pvr.domain.TypeAntenne;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class TypeAntenneDaoImpl extends DefaultGenericDao<TypeAntenne, Long> implements ITypeAntenneDao {
	

	public TypeAntenne getTypeAntenneByLabel(String label){
		Criteria cr = this.getSession().createCriteria(TypeAntenne.class, "typeAntenne");
		cr.add(Restrictions.eq("typeAntenne.label", label));
		cr.setMaxResults(1);
		return (TypeAntenne) cr.uniqueResult();
	}
}
