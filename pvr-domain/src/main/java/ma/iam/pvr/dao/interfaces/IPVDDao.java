package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Pvd;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IPVDDao extends IGenericDao<Pvd, Long> {
	
	List<Pvd> getAllPvd(Integer pageNumber, Integer pageSize);
	Integer countAllPvd();
	Integer countListPvdDraft();
	List<Pvd> getListPvdDraft(Integer pageNumber, Integer pageSize);
	Integer countListPvdValid();
	List<Pvd> getListPvdValid(Integer pageNumber, Integer pageSize);
}
