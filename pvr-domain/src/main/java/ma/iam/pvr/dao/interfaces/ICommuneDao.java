package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Commune;
import ma.iam.pvr.domain.Province;
//import ma.iam.socle.service.GenericDao;

public interface ICommuneDao extends IGenericDao<Commune, Long> {

	Commune getCommuneByProvinceAndLabel(Long idProvince, String label);
	List<Commune> getCommuneByProvince(Long idProvince);

}
