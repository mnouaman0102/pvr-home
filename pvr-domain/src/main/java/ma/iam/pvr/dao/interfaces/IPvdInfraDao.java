package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.PvdInfra;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IPvdInfraDao extends IGenericDao<PvdInfra, Long> {
	
	List<PvdInfra> getAllPvd(Integer pageNumber, Integer pageSize);
	Integer countAllPvd();
	Integer countListPvdDraft();
	List<PvdInfra> getListPvdDraft(Integer pageNumber, Integer pageSize);
	Integer countListPvdValid();
	List<PvdInfra> getListPvdValid(Integer pageNumber, Integer pageSize);
}
