package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.MesRt;
import ma.iam.pvr.domain.Rt;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IMesRtDao extends IGenericDao<MesRt, Long> {

	MesRt getMesRtByMes(Long idMes);
	List<MesRt> getListMesRtByMes(Long idMes);
	List<MesRt> getListMesRtByRt(Long idRt);
	MesRt getLastMesRtValidByMes(Long idMes);
	MesRt getLastMesRtRadioEnvValidByMes(Long idtMes, String workFlow);
	List<MesRt> getListMesRtValidByMes(Long idMes);
}
