package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Entite;
import ma.iam.pvr.domain.Site;
import ma.iam.pvr.domain.SiteCandidatHistory;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface ISiteCandidatHistoryDao extends IGenericDao<SiteCandidatHistory, Long> {

	List<SiteCandidatHistory> getHistoriquesCandidatSite(Long idtSite, String statut);
	List<SiteCandidatHistory> getHistoriquesCandidatBySiteAndEntite(Long idSite, Long idEntite);
	List<SiteCandidatHistory> getHistoriquesCandidatSiteBySite(Long idSite);
	List<SiteCandidatHistory> getHistoriquesCandidatBySiteAndPhase(Long idSite, String phase);
}
