package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.Antenne;
//import ma.iam.socle.service.GenericDao;

public interface IAntenneDao extends IGenericDao<Antenne, Long> {

	Antenne getAntenneByLabel(String label);
}
