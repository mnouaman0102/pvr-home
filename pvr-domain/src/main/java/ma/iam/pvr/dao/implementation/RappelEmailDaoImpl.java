package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IRappelEmailDao;
import ma.iam.pvr.domain.RappelEmail;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class RappelEmailDaoImpl extends DefaultGenericDao<RappelEmail, Long> implements IRappelEmailDao {

	@SuppressWarnings("unchecked")
	public List<RappelEmail> getRappelsByEmailId(Long idtEmail) {
		Criteria cr = this.getSession().createCriteria(RappelEmail.class,"rappelEmail");
		cr.createAlias("rappelEmail.email", "email");
		cr.add(Restrictions.eq("email.idt", idtEmail));
		return cr.list();
	}

}
