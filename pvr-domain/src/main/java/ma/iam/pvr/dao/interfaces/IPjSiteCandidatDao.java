package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.PjSiteCandidat;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IPjSiteCandidatDao extends IGenericDao<PjSiteCandidat, Long> {

	List<PjSiteCandidat> getPjSiteCandidatByHistoryIdt(Long idtSiteCandidat);
		
}
