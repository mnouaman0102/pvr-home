package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Contrat;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IContratDao extends IGenericDao<Contrat, Long> {

	List<Contrat> getAllContactsByFounisseur(Long idtFournisseur);
	List<Contrat> getAllContactsByFounisseurs(List<Long> idtsFournisseur);
	Contrat getContratByLabelAndFournisseur(String label, Long idtFournisseur);
}
