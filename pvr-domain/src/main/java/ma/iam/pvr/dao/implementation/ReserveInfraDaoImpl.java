package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IReserveInfraDao;
import ma.iam.pvr.domain.ReserveInfra;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class ReserveInfraDaoImpl extends DefaultGenericDao<ReserveInfra, Long> implements IReserveInfraDao {


	public ReserveInfra getByLabel(String reserveInfra) {
		Criteria cr = this.getSession().createCriteria(ReserveInfra.class);
		cr.add(Restrictions.eq("label", reserveInfra).ignoreCase());
		return (ReserveInfra) cr.uniqueResult();
	}

	public int findTotalReservesInfra(Long idtProjet, String label, Long idtTypeProjet) {
		Criteria cr = this.getSession().createCriteria(ReserveInfra.class, "reserveInfra");
		cr.createAlias("reserveInfra.projetInfra", "projetInfra");
		cr.createAlias("reserveInfra.typeProjetInfra", "typeProjetInfra");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projetInfra.idt", idtProjet));		
		}
		if(idtTypeProjet != 0){
			cr.add(Restrictions.eq("typeProjetInfra.idt", idtTypeProjet));		
		}
		if(label != null && !label.equals("")){
			cr.add(Restrictions.eq("reserveInfra.label", label));
		}
		cr.addOrder(Order.desc("reserveInfra.idt"));
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();

	}

	@SuppressWarnings("unchecked")
	public List<ReserveInfra> findAllReservesInfraPagination(Long idtProjet, String label, Long idtTypeProjet, int pageNumber, int pageSize) {
		Criteria cr = this.getSession().createCriteria(ReserveInfra.class, "reserveInfra");
		cr.createAlias("reserveInfra.projetInfra", "projetInfra");
		cr.createAlias("reserveInfra.typeProjetInfra", "typeProjetInfra");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projetInfra.idt", idtProjet));		
		}
		if(idtTypeProjet != 0){
			cr.add(Restrictions.eq("typeProjetInfra.idt", idtTypeProjet));		
		}
		if(label != null && !label.equals("")){
			cr.add(Restrictions.eq("reserveInfra.label", label));
		}
		//Pagination
		if(pageNumber>0 && pageSize>0){
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
		}else{
			cr.setFirstResult(0);
		}
		cr.addOrder(Order.desc("reserveInfra.idt"));
		return  cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<ReserveInfra> getByProjet(Long idtProjet) {
		Criteria cr = this.getSession().createCriteria(ReserveInfra.class, "reserveInfra");
		cr.createAlias("reserveInfra.projetInfra", "projetInfra");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projetInfra.idt", idtProjet));		
		}			
		return  cr.list();
	}
	@SuppressWarnings("unchecked")
	public List<ReserveInfra> getByCriteria(Long idtProjet, Long idtTypeProjet) {
		Criteria cr = this.getSession().createCriteria(ReserveInfra.class, "reserveInfra");
		cr.createAlias("reserveInfra.projetInfra", "projetInfra");
		cr.createAlias("reserveInfra.typeProjetInfra", "typeProjetInfra");
		if(idtProjet != 0){
			cr.add(Restrictions.eq("projetInfra.idt", idtProjet));		
		}
		if(idtTypeProjet != 0) {
			cr.add(Restrictions.eq("typeProjetInfra.idt", idtTypeProjet));
		}
		return  cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<String> findAllLabels() {
		Criteria cr = this.getSession().createCriteria(ReserveInfra.class)
			    .setProjection(Projections.projectionList()
			    .add(Projections.property("label"), "label"));
		cr.addOrder(Order.asc("label"));
		return (List<String>) cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<ReserveInfra> findAllReservesInfra() {
		Criteria cr = this.getSession().createCriteria(ReserveInfra.class);
		cr.addOrder(Order.asc("label"));
		return cr.list();
	}

}
