package ma.iam.pvr.dao.implementation;
import ma.iam.pvr.dao.interfaces.ITypeAlimentationDao;
import ma.iam.pvr.domain.TypeAlimentation;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
/**
 * @author K.ELBAGUARI
 *
 */
import org.springframework.stereotype.Repository;
/**
 * @author K.ELBAGUARI
 *
 */
@Repository
public class TypeAlimentationDaoImpl extends DefaultGenericDao<TypeAlimentation, Long> implements ITypeAlimentationDao {
	

	public TypeAlimentation getTypeAlimentationByLabel(String label){
		Criteria cr = this.getSession().createCriteria(TypeAlimentation.class, "typeAlimentation");
		cr.add(Restrictions.eq("typeAlimentation.label", label));
		cr.setMaxResults(1);
		return (TypeAlimentation) cr.uniqueResult();
	}
}
