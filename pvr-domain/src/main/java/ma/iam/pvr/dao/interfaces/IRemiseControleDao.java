package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.RemiseControle;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IRemiseControleDao extends IGenericDao<RemiseControle, Long> {
	List<RemiseControle> getListRemiseControle(String codeEtat, Long idtFournisseur, Long idtRemise, Integer pageNumber, 
			Integer pageSize);
	Integer getTotalRemiseControle(String codeEtat, Long idtFournisseur, Long idtRemise);
	
	List<Object> getListRemiseIdsControleNative(String idtsFournisseur, String idtsContrat, 
			String idtsProjet, String idtsDr, String idtsSite, String codesEtat, Long idtRemise);
	List<RemiseControle> getRemiseControleByCriteresPagination(List<Long> idsRemise,
			int pageNumber, int pageSize);
	
	int getTotalRemiseControleByCriteres(List<Long> idsRemise);
}
