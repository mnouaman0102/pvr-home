package ma.iam.pvr.dao.implementation;



import java.util.List;

import ma.iam.pvr.dao.interfaces.IPvdInfraDao;
import ma.iam.pvr.domain.PvdInfra;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class PvdInfraDaoImpl extends DefaultGenericDao<PvdInfra, Long> implements IPvdInfraDao {


	@SuppressWarnings("unchecked")
	public List<PvdInfra> getAllPvd(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(PvdInfra.class, "pvd");
		if(pageNumber == null || pageSize == null || pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			cr.setFirstResult((pageNumber-1) * pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	public Integer countAllPvd() {
		Criteria cr = this.getSession().createCriteria(PvdInfra.class, "pvd");
		cr.setProjection(Projections.rowCount());
		return (Integer) cr.uniqueResult();
	}
	
	public Integer countListPvdDraft() {
		Criteria cr = this.getSession().createCriteria(PvdInfra.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<PvdInfra> getListPvdDraft(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(PvdInfra.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		if(pageNumber == null || pageSize == null || pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1) * pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}
		return null;
	}
	
	public Integer countListPvdValid() {
		Criteria cr = this.getSession().createCriteria(PvdInfra.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<PvdInfra> getListPvdValid(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(PvdInfra.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		if(pageNumber == null || pageSize == null || pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}
		return null;
	}
}
