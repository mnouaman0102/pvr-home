package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Dc;
import ma.iam.pvr.domain.Dr;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IDcDao extends IGenericDao<Dc, Long>  {
	Dc getDcByLabel(String label);
	Dc getDcByDrAndLabel(Long idDr, String label);
	List<Dc> getDcByDr(Long idDr);
	List<Dc> getDcByDrs(List<Long> listIdtsDr);
}
