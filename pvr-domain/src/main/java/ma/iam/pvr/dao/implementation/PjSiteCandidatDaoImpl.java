package ma.iam.pvr.dao.implementation;

import java.util.List;

import ma.iam.pvr.dao.interfaces.IPjSiteCandidatDao;
import ma.iam.pvr.domain.PjSiteCandidat;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class PjSiteCandidatDaoImpl extends DefaultGenericDao<PjSiteCandidat, Long> implements IPjSiteCandidatDao {

	@SuppressWarnings("unchecked")
	public List<PjSiteCandidat> getPjSiteCandidatByHistoryIdt(Long idtSiteCandidat) {
		Criteria cr = this.getSession().createCriteria(PjSiteCandidat.class, "pjSiteCandidat");
		cr.createAlias("pjSiteCandidat.siteCandidatHistory", "siteCandidatHistory");
		
		cr.add(Restrictions.eq("siteCandidatHistory.idt", idtSiteCandidat));
		
		return cr.list();
	}
	
}
