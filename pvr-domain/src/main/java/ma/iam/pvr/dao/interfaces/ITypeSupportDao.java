package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.TypeSupport;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface ITypeSupportDao extends IGenericDao<TypeSupport, Long> {

}
