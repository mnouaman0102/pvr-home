package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.IProjetReserveDao;
import ma.iam.pvr.domain.ProjetReserve;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class ProjetReserveDaoImpl extends DefaultGenericDao<ProjetReserve, Long> implements IProjetReserveDao {



}

