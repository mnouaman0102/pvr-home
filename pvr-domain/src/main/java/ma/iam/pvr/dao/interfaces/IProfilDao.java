package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.Profil;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IProfilDao extends IGenericDao<Profil, Long> {

	public Profil getProfilByName(String label);
	
}
