package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.ITypeProjetInfraDao;
import ma.iam.pvr.domain.TypeProjetInfra;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class TypeProjetInfraDaoImpl extends DefaultGenericDao<TypeProjetInfra, Long> implements ITypeProjetInfraDao {

}
