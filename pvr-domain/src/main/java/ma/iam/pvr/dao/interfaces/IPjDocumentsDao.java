package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.PjDocuments;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IPjDocumentsDao extends IGenericDao<PjDocuments, Long> {

	List<PjDocuments> getPjRtByRtIdt(Long idt);
	List<PjDocuments> getPjPvrByPvrIdt(Long idt);
	List<PjDocuments> getPjPvdByPvdIdt(Long idt);
	List<PjDocuments> getPjRtRadioEnvByRtIdt(Long idt, String workFlow);
	List<PjDocuments> getPjRemiseByRemiseIdt(Long idt);
	List<PjDocuments> getPjRtInfraByRtIdt(Long idt);
	List<PjDocuments> getPjPvrInfraByPvrIdt(Long idt);
	List<PjDocuments> getPjPvdInfraByPvdIdt(Long idt);
	List<PjDocuments> getPjCandidatSiteVR(Long idtSite);
}
