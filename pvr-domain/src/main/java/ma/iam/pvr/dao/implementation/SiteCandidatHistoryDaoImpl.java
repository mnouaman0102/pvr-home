package ma.iam.pvr.dao.implementation;

import java.util.List;

import io.micrometer.core.instrument.util.StringUtils;
import ma.iam.pvr.dao.interfaces.ISiteCandidatHistoryDao;
import ma.iam.pvr.domain.SiteCandidatHistory;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 * @author K.ELBAGUARI
 *
 */
@Repository
public class SiteCandidatHistoryDaoImpl extends DefaultGenericDao<SiteCandidatHistory, Long> implements ISiteCandidatHistoryDao {


	
	@SuppressWarnings("unchecked")
	public List<SiteCandidatHistory> getHistoriquesCandidatSite(Long idtSite, String statut) {
		Criteria cr = this.getSession().createCriteria(SiteCandidatHistory.class, "siteCandidatHistory");
		cr.createAlias("siteCandidatHistory.site", "site");
		if(idtSite != null && idtSite != 0){
			cr.add(Restrictions.eq("site.idt", idtSite));
		}
		if(statut != null && !"".equals(statut)){
			cr.add(Restrictions.eq("siteCandidatHistory.statutCandidat", statut));
		}
		cr.addOrder(Order.desc("siteCandidatHistory.dateValidation"));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<SiteCandidatHistory> getHistoriquesCandidatBySiteAndEntite(Long idtSite, Long idtEntite) {
		Criteria cr = this.getSession().createCriteria(SiteCandidatHistory.class,
				"siteCandidatHistory");
		cr.createAlias("siteCandidatHistory.site", "site");
		cr.createAlias("siteCandidatHistory.entite", "entite");
		if(idtSite != null && idtSite != 0L){
			cr.add(Restrictions.eq("site.idt", idtSite));
		}
		if(idtEntite != null && idtEntite != 0L){
			cr.add(Restrictions.eq("entite.idt", idtEntite));
		}
		cr.addOrder(Order.asc("siteCandidatHistory.idt"));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<SiteCandidatHistory> getHistoriquesCandidatSiteBySite(Long idtSite) {
		Criteria cr = this.getSession().createCriteria(SiteCandidatHistory.class, "siteCandidatHistory");
		cr.createAlias("siteCandidatHistory.site", "site");
		if(idtSite != null && idtSite != 0){
			cr.add(Restrictions.eq("site.idt", idtSite));
		}
		cr.addOrder(Order.desc("siteCandidatHistory.dateValidation"));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<SiteCandidatHistory> getHistoriquesCandidatBySiteAndPhase( Long idtSite, String phase) {
		Criteria cr = this.getSession().createCriteria(SiteCandidatHistory.class, "siteCandidatHistory");
		cr.createAlias("siteCandidatHistory.site", "site");
		if(idtSite != null && idtSite != 0L){
			cr.add(Restrictions.eq("site.idt", idtSite));
		}
		if(!StringUtils.isEmpty(phase)){
			cr.add(Restrictions.eq("siteCandidatHistory.phase", phase));
		}
		
		cr.addOrder(Order.asc("siteCandidatHistory.idt"));
		return cr.list();
	}
}
