package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.CryptDecryptData;
//import ma.iam.socle.service.GenericDao;

public interface ICryptDecryptDataDao extends IGenericDao<CryptDecryptData, Long> {

	public CryptDecryptData findByCompte(String compte);
}
