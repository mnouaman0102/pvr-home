package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IRtDao;
import ma.iam.pvr.domain.Rt;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class RtDaoImpl extends DefaultGenericDao<Rt, Long> implements IRtDao {

	@SuppressWarnings("unchecked")
	public List<Rt> getListRtByContrat(Long idtContrat) {
		Criteria cr = this.getSession().createCriteria(Rt.class,"rt");
		cr.createAlias("rt.contrat", "contrat");
		cr.add(Restrictions.eq("contrat.idt", idtContrat));
		return cr.list();
	}
	
	public Integer countAllRt(String workflow) {
		Criteria cr = this.getSession().createCriteria(Rt.class,"rt");
		cr.add(Restrictions.eq("rt.workflow", workflow));
		cr.setProjection(Projections.rowCount());
		return (Integer) cr.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<Rt> getAllRt(Integer pageNumber, Integer pageSize, String workflow) {
		Criteria cr = this.getSession().createCriteria(Rt.class,"rt");
		cr.add(Restrictions.eq("rt.workflow", workflow));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.addOrder(Order.desc("rt.idt"));
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}

	public Integer countListRtDraft(String workflow) {
		Criteria cr = this.getSession().createCriteria(Rt.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<Rt> getListRtDraft(Integer pageNumber, Integer pageSize, String workflow) {
		Criteria cr = this.getSession().createCriteria(Rt.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}
		return null;
	}
	
	public Integer countListRtValid(String workflow) {
		
		Criteria cr = this.getSession().createCriteria(Rt.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<Rt> getListRtValid(Integer pageNumber, Integer pageSize, String workflow) {
		Criteria cr = this.getSession().createCriteria(Rt.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}
		return null;
	}
	
}
