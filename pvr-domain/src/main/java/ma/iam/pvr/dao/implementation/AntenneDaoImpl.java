package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.IAntenneDao;
import ma.iam.pvr.domain.Antenne;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class AntenneDaoImpl extends DefaultGenericDao<Antenne, Long> implements IAntenneDao{

	public Antenne getAntenneByLabel(String label){
		Criteria cr = this.getSession().createCriteria(Antenne.class, "antenne");
		cr.add(Restrictions.eq("antenne.label", label));
		cr.setMaxResults(1);
		return (Antenne) cr.uniqueResult();

	}
}
