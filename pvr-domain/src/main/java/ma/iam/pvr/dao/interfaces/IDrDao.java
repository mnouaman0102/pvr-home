package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Dr;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IDrDao extends IGenericDao<Dr, Long> {
	
	Dr getDrByLabel(String label);
	List<Dr> getAllDrLabelAsc();
}
