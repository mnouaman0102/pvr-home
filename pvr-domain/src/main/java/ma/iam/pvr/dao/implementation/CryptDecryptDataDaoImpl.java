package ma.iam.pvr.dao.implementation;


import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import ma.iam.pvr.dao.interfaces.ICryptDecryptDataDao;
import ma.iam.pvr.domain.CryptDecryptData;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

@Repository
public class CryptDecryptDataDaoImpl extends DefaultGenericDao<CryptDecryptData, Long> implements ICryptDecryptDataDao {
	

	public CryptDecryptData findByCompte(String compte) {
		Criteria cr = this.getSession().createCriteria(CryptDecryptData.class, "data");
		cr.add(Restrictions.eq("data.compte", compte));
		return (CryptDecryptData) cr.uniqueResult();
	}
}

