package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IPjDocumentsDao;
import ma.iam.pvr.domain.PjDocuments;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class PjDocumentsDaoImpl extends DefaultGenericDao<PjDocuments, Long> implements IPjDocumentsDao {

	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjRtByRtIdt(Long idt) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.rt", "rt");
		
		cr.add(Restrictions.eq("rt.idt", idt));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjPvrByPvrIdt(Long idt) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.pvr", "pvr");
		
		cr.add(Restrictions.eq("pvr.idt", idt));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjPvdByPvdIdt(Long idt) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.pvd", "pvd");
		
		cr.add(Restrictions.eq("pvd.idt", idt));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjRtRadioEnvByRtIdt(Long idt, String workFlow) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.rt", "rt");
		cr.add(Restrictions.eq("rt.idt", idt));
		cr.add(Restrictions.eq("rt.workflow", workFlow));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjRemiseByRemiseIdt(Long idt) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.remiseControle", "remiseControle");
		
		cr.add(Restrictions.eq("remiseControle.idt", idt));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjRtInfraByRtIdt(Long idt) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.rtInfra", "rtInfra");
		
		cr.add(Restrictions.eq("rtInfra.idt", idt));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjPvrInfraByPvrIdt(Long idt) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.pvrInfra", "pvr");
		cr.add(Restrictions.eq("pvr.idt", idt));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjPvdInfraByPvdIdt(Long idt) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.pvdInfra", "pvd");
		cr.add(Restrictions.eq("pvd.idt", idt));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjDocuments> getPjCandidatSiteVR(Long idtSite) {
		Criteria cr = this.getSession().createCriteria(PjDocuments.class, "pjDocuments");
		cr.createAlias("pjDocuments.candidatSite", "candidatSite");
		cr.add(Restrictions.eq("candidatSite.idt", idtSite));
		return cr.list();
	}
	
}
