package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.ITypeSupportDao;
import ma.iam.pvr.domain.TypeSupport;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class TypeSupportDaoImpl extends DefaultGenericDao<TypeSupport, Long> implements ITypeSupportDao {

}
