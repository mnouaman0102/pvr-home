package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.ICommuneDao;
import ma.iam.pvr.domain.Commune;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class CommuneDaoImpl extends DefaultGenericDao<Commune, Long> implements ICommuneDao {
  @Override
	public Commune getCommuneByProvinceAndLabel(Long idtProvince, String label) {
		Criteria cr = this.getSession().createCriteria(Commune.class, "commune");
		cr.createAlias("commune.province", "province");
		cr.add(Restrictions.eq("province.idt", idtProvince));
		cr.add(Restrictions.eq("commune.label", label));
		cr.setMaxResults(1);
		return (Commune) cr.uniqueResult();
	}

	@Override
	public List<Commune> getCommuneByProvince(Long idtProvince) {
		Criteria cr = this.getSession().createCriteria(Commune.class, "commune");
		cr.createAlias("commune.province", "province");
		cr.add(Restrictions.eq("province.idt", idtProvince));
		return cr.list();
	}
}

