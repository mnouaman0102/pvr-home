package ma.iam.pvr.dao.implementation;

import java.util.List;

import ma.iam.pvr.dao.interfaces.IOperationInfraReserveDao;
import ma.iam.pvr.domain.OperationInfraReserve;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


/**
 * 
 * @author Z.BELGHAOUTI && K.ELBAGUARI
 *
 */
@Repository
public class OperationInfraReserveDaoImpl extends DefaultGenericDao<OperationInfraReserve, Long> implements IOperationInfraReserveDao {


	
	@SuppressWarnings("unchecked")
	public List<OperationInfraReserve> getReservesByOperationInfraId(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(OperationInfraReserve.class, "operReserve");
		cr.createAlias("operReserve.operationInfra", "operationInfra");
		cr.createAlias("operReserve.reserveInfra", "reserveInfra");
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.addOrder(Order.desc("reserveInfra.responsabilite"));
		return cr.list();
	}
	
	public Long deleteOperationInfraReserveByOperationInfraIdt(Long idtMes) {
		Session session = this.getSession();  
		String hql = "delete from OperationInfraReserve where operationInfra.idt= :id"; 
		session.createQuery(hql).setLong("id", idtMes).executeUpdate();
		return idtMes;
	}

	@SuppressWarnings("unchecked")
	public List<OperationInfraReserve> getReservesNotNormaliseByOperationInfraId(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(OperationInfraReserve.class, "operReserve");
		cr.createAlias("operReserve.operationInfra", "operationInfra");
		cr.createAlias("operReserve.reserveInfra", "reserveInfra");
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.add(Restrictions.eq("operReserve.statut.idt", 1L));
		cr.addOrder(Order.desc("reserveInfra.responsabilite"));
		return cr.list();
	}
}

