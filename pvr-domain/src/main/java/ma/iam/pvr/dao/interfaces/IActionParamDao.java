package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.ActionParam;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IActionParamDao extends IGenericDao<ActionParam, Long> {
}
