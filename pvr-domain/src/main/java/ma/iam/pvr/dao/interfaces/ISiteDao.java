package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Site;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface ISiteDao extends IGenericDao<Site, Long> {

	List<Site> getSiteByRtId(Long idtRt);

	List<Site> getAllSitesByDr(Long idtDr);
	
	List<Site> getAllSitesByListDr(List<Long> idtDrs);
	
	List<Site> getSitesList(Long idtDr, Long idtSite, Long idtEtatSite, Integer pageNumber, Integer pageSize);
	
	List<Site> getSitesList(Long idtDr, Long idtSite, Integer pageNumber, Integer pageSize);

	int getTotalSitesList(Long idtDr, Long idtSite, Long idtEtatSite);
	
	int getTotalSites(Long idtDr, Long idtSite);

	List<Site> getSitesExceptMesList(Long idtDr, Long idtSite, Integer pageNumber, Integer pageSize);
	
	int getTotalSitesExceptMesList(Long idtDr, Long idtSite);


	List<Site> getSitesListByDr(Long idtDr);
	
	List<Site> getSitesExceptMesListByDr(Long idtDr);
	
	boolean isSiteDrAlreadyExist(String label, Long idtDr);
	
	Site getSiteByLabelAndDr(String label, Long idtDr);
	
	Long getSiteIdByLabel(String label);
	
	List<Long> getSitesByLabels(List<String> labels);
	
	Site getSiteByCode(String code);
	
	List<Site> getAllSiteByDrAndListEtats(Long idtDr, List<Long> idtEtats);
	
	List<Site> getSitesByCritere(List<Long> idsSites);
	
	List<Site> getSitesCandidaturesByCriteres(Long idtDr, Long idtSite, String code, String label, 
			List<Long> idtEtatSite, Integer pageNumber, Integer pageSize);
	
	int getTotalSitesCandidaturesByCriteres(Long idtDr, Long idtSite, String code, String label, 
			List<Long> idtEtatSite);

	List<Object> findAllCentreRattachementForBdSite();

	List<Object> findAllCentreRattachementByDrs(String idtsDr);

	List<Site> getSitesListByDrs(List<Long> idtsDr);

	int getTotalSitesByCriteres(List<Long> idtsDr, List<Long> idtsDc,
			List<Long> idtsSite, List<String> centresRattachement);

	List<Site> getSitesByCriteres(List<Long> idtsDr, List<Long> idtsDc,
	List<Long> idtsSite, List<String> centresRattachement, Integer pageNumber, Integer pageSize);

	int getTotalSitesByCriteresForExploi(Long idtDr, Long idtSite, String code, String label, List<Long> idtEtatsSite);

	List<Site> getSitesByCriteresForExploi(Long idtDr, Long idtSite,
			String code, String label, List<Long> idtEtatsSite, Integer pageNumber,
			Integer pageSize);

	int getTotalSitesByCriteresForMaj(Long idtDr, Long idtSite, List<Long> idtEtatsSite);

	List<Site> getSitesByCriteresForMaj(Long idtDr, Long idtSite,
			List<Long> idtEtatsSite, Integer pageNumber, Integer pageSize);

	List<Site> getSitesForMajByDr(Long idtDr, List<Long> idtEtatsSite);
	
}
