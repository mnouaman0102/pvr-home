package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.ReserveInfra;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IReserveInfraDao extends IGenericDao<ReserveInfra, Long> {

	ReserveInfra getByLabel(String reserve);

	int findTotalReservesInfra(Long idtProjet, String label, Long idtTypeProjet);

	List<ReserveInfra> findAllReservesInfraPagination(Long idtProjet, String label, Long idtTypeProjet, int pageNumber, int pageSize);

	List<ReserveInfra> getByProjet(Long idtProjet);
	
	List<ReserveInfra> getByCriteria(Long idtProjet, Long idtTypeProjet);

	List<String> findAllLabels();

	List<ReserveInfra> findAllReservesInfra();
	
}
