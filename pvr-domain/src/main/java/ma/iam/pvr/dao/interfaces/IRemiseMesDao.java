package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.RemiseMes;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IRemiseMesDao extends IGenericDao<RemiseMes, Long> {

	RemiseMes getRemiseMesByMes(Long idMes);
	List<RemiseMes> getListRemiseMesByMes(Long idMes);
	List<RemiseMes> getListRemiseMesByRemise(Long idtRemise);
	List<RemiseMes> getListRemiseMesValidByMes(Long idMes);
}
