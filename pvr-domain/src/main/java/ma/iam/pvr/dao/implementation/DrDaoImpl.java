package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IDrDao;
import ma.iam.pvr.domain.Dr;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class DrDaoImpl extends DefaultGenericDao<Dr, Long> implements IDrDao {

	public Dr getDrByLabel(String label) {
		Criteria cr = this.getSession().createCriteria(Dr.class, "dr");
		cr.add(Restrictions.eq("dr.label", label));
		cr.setMaxResults(1);
		return (Dr) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Dr> getAllDrLabelAsc() {
		Criteria cr = this.getSession().createCriteria(Dr.class, "dr");
		cr.addOrder(Order.asc("dr.label"));
		return cr.list();
	}
	
	
}
