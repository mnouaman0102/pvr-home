package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.IGenericDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;


public class DefaultGenericDao<T extends Serializable,ID extends Serializable> implements IGenericDao<T,ID> {
    private Class<T> clazz;

    protected SessionFactory sessionFactory;

    public void setClazz(final Class<T> clazzToSet) {
        clazz = clazzToSet;
    }

    public T get(final ID id) {
        return (T) getSession().get(clazz, id);
    }

    public List<T> findAll() {
        return getSession().createQuery("from " + clazz.getName()).list();
    }

    public T save(final T entity) {
        getSession().saveOrUpdate(entity);
        return entity;
    }

    public T update(final T entity) {
        return (T) getSession().merge(entity);
    }

    public void delete(final T entity) {
        getSession().delete(entity);
    }

    public void deleteById(final ID entityId) {
        final T entity = get(entityId);
        delete(entity);
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
     @Resource
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
