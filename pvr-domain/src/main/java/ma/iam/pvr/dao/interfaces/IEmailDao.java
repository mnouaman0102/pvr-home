package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Email;
import ma.iam.pvr.domain.Entite;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI,K.ELBAGUARI
 *
 */
public interface IEmailDao extends IGenericDao<Email, Long> {
	
	List<Email> getListEmailByEntite(Long IdEntite);
	List<Email> getListEmailByMesId(Long idt);
	List<Email> getListEmailByRemise(Long idtRemise);
}
