package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IPjOperationDao;
import ma.iam.pvr.domain.PjOperation;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class PjOperationDaoImpl extends DefaultGenericDao<PjOperation, Long> implements IPjOperationDao {

	
	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJOperationExploitationListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 2L));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJOperationOptimListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 3L));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJOperationDccListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));		
		cr.add(Restrictions.eq("entite.idt", 4L));
		cr.add(Restrictions.eq("pjOperation.etape", "oper"));
		
		return cr.list();
	}

	public PjOperation getPJOperationDeploiByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 1L));
		cr.addOrder(Order.desc("pjOperation.idt"));
		cr.setMaxResults(1);
		return (PjOperation) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJOperationDccEnvListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 5L));
		cr.add(Restrictions.eq("pjOperation.etape", "oper"));
		
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJOperationQosListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 6L));
		
		return cr.list();
	}


	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJsOperationByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.asc("entite.idt"));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJsOperationByMesIdtAndEntiteIdt(Long idtMes, Long idtEntite) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", idtEntite));
		return cr.list();
	}


	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJOperationDccSuiviListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));		
		cr.add(Restrictions.eq("entite.idt", 4L));
		cr.add(Restrictions.eq("pjOperation.etape", "suivi"));
		
		return cr.list();
	}


	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJOperationDccEnvSuiviListByMesIdt(Long idtMes) {

		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 5L));
		cr.add(Restrictions.eq("pjOperation.etape", "suivi"));
		
		return cr.list();
	}


	@SuppressWarnings("unchecked")
	public List<PjOperation> getPJDeploiByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperation.class, "pjOperation");
		cr.createAlias("pjOperation.mes", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 1L));
		
		return cr.list();
	}
	
}
