package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IPjOperationInfraDao;
import ma.iam.pvr.domain.PjOperationInfra;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class PjOperationInfraDaoImpl extends DefaultGenericDao<PjOperationInfra, Long> implements IPjOperationInfraDao {

	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJOperationDccListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "operationInfra");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));		
		cr.add(Restrictions.eq("entite.idt", 4L));
		cr.add(Restrictions.eq("pjOperation.etape", "oper"));
		
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJOperationDeploiByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "operationInfra");
		cr.createAlias("pjOperation.entite", "entite");
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 9L));
		cr.addOrder(Order.desc("pjOperation.idt"));
		return cr.list();
	}


	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJsOperationByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "operationInfra");
		cr.createAlias("pjOperation.entite", "entite");
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.addOrder(Order.asc("entite.idt"));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJsOperationByMesIdtAndEntiteIdt(Long idtMes, Long idtEntite) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "operationInfra");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", idtEntite));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJOperationByMesIdtAndDrIdt(Long idtMes,Long idtDr) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "operationInfra");
		cr.createAlias("pjOperation.dr", "dr");
		
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.add(Restrictions.eq("dr.idt", idtDr));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJsOperationByOperIdt(Long idtOper) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "operationInfra");
		cr.add(Restrictions.eq("operationInfra.idt", idtOper));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJOperationDccSuiviListByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "mes");
		cr.createAlias("pjOperation.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));		
		cr.add(Restrictions.eq("entite.idt", 4L));
		cr.add(Restrictions.eq("pjOperation.etape", "suivi"));
		
		return cr.list();
	}
	
}
