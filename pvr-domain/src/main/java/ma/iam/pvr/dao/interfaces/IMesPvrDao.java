package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.MesPvr;
import ma.iam.pvr.domain.Pvr;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IMesPvrDao extends IGenericDao<MesPvr, Long> {

	MesPvr getMesPvrByMes(Long IdMes);
	List<MesPvr> getListMesPvrByMes(Long idMes);
	List<MesPvr> getListMesPvrByPvr(Long idPvr);
	MesPvr getLastMesPvrValidByMes(Long idMes);
	List<MesPvr> getListMesPvrValidByMes(Long idMes);
}
