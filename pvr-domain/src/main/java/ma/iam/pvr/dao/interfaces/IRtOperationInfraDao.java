package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.RtOperationInfra;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IRtOperationInfraDao extends IGenericDao<RtOperationInfra, Long> {

	RtOperationInfra getRtOperationInfraByOper(Long idtOper);
	List<RtOperationInfra> getListRtOperationInfraByOper(Long idtOper);
	List<RtOperationInfra> getListRtOperationInfraByRt(Long idtRt);
	RtOperationInfra getLastRtOperationInfraValidByOper(Long idtOper);
	RtOperationInfra getLastRtOperationInfraValidByOper(Long idtOper, String workFlow);
	List<RtOperationInfra> getListRtOperationInfraValidByOper(Long idtOper);
}
