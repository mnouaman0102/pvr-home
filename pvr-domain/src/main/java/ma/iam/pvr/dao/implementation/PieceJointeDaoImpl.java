package ma.iam.pvr.dao.implementation;


import ma.iam.pvr.dao.interfaces.IPieceJointeDao;
import ma.iam.pvr.domain.PieceJointe;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class PieceJointeDaoImpl extends DefaultGenericDao<PieceJointe, Long> implements IPieceJointeDao {

	public PieceJointe getOriginFileName(String fileName) {
		Criteria cr = this.getSession().createCriteria(PieceJointe.class, "pj");
		cr.add(Restrictions.eq("pj.fileName", fileName));
		return (PieceJointe) cr.uniqueResult();
	}
	
}
