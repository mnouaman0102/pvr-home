package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.OperationInfra;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IOperationInfraDao extends IGenericDao<OperationInfra, Long> {

	List<String> findAllNumOperations();

	OperationInfra getByNumeroOperation(String numeroOperation);
	
	List<Object> getListMesIdtForDeploiMultipleValues(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, String idtsEtat, List<String> numOperation);

	List<OperationInfra> getMesByCriteresPagination(List<Long> idsMES, int pageNumber, int pageSize);

	List<Object> getListMesIdtForDeploiDr(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtDr, Long idtSite, Long idtEtat, String numOperation);

	int getTotalMesByCriteres(List<Long> idsMES);

	List<Object> findAllNumOperationsForDeploi();
	
	List<Object> findAllNumOperationsForDr(Long idtDr);

	List<Object> getListMesIdtForDrMultipleValues(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, String idtsEtat, List<String> numOperation);
	
	List<Object> getListMesIdtForDccMultipleValues(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, String idtsEtat, List<String> numOperation);
	
	List<OperationInfra> getOperationInfraListByPvrId(Long idtPvr);
	
	List<Object> getListOperationIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtEtat, String dateDebut, String dateFin, 
			List<Long> idtMes, Long idtDr, List<Long> idSites);
	
	List<Object> getListOperationIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtEtat, String dateDebut, String dateFin, 
			List<Long> idtMes, Long idtDr, List<Long> idSites, boolean excludeDraft);
	
	List<Object> getListOperationIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet,
			Long idtEtat, String dateDebut, String dateFin, List<Long> idtMes, Long idtDr, Long idtSite);

	List<OperationInfra> getListMesByIDT(List<Long> idsMES);
	
	List<OperationInfra> getOperationInfraListByPvdId(Long idtPvd);
	
	List<Object> getListOperationsIdtWithPVDNull(Long idtFournisseur, Long idtContrat,
			Long idtEtat, String dateDebut, String dateFin, List<Long> idtMes, Long idtDr, Long idtSite);
	
	List<Object> getMesIdtByCriteres(String idtsFournisseur, String idtsContrat,
			Long idtStatut, String idtsEtat, String idtsProjet, String idtsTypeProjet,  String idtsDr, String idtsSite, String idtsRt);
	
	List<OperationInfra> getMesByCriteres(List<Long> idsMES);
	
	List<Object> getOperationsReceptionneExploitationInfra(String idtsFournisseur, String idtsContrat,
			String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, List<String> numOperation);
	
	List<Object> findAllNumOperationsForExploitation();
}
