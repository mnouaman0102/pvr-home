package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IRemiseControleDao;
import ma.iam.pvr.domain.RemiseControle;
import ma.iam.pvr.domain.utils.StringUtil;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class RemiseControleDaoImpl extends DefaultGenericDao<RemiseControle, Long> implements IRemiseControleDao {

	@SuppressWarnings("unchecked")
	public List<RemiseControle> getListRemiseControle(String codeEtat, Long idtFournisseur, Long idtRemise, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(RemiseControle.class,"remiseControle");
		cr.createAlias("remiseControle.etatRemiseControle", "etatRemiseControle");
		cr.createAlias("remiseControle.fournisseur", "fournisseur");
		
		if(codeEtat != null && !codeEtat.equals("")) {
			if(codeEtat.equalsIgnoreCase("DRAFT")) {
				cr.createAlias("remiseControle.statutEdition", "statutEdition");
				cr.add(Restrictions.eq("statutEdition.idt", 1L));
				cr.add(Restrictions.eq("etatRemiseControle.code", "INSTANCE"));
			} else if(codeEtat.equalsIgnoreCase("INSTANCE")) {
				cr.createAlias("remiseControle.statutEdition", "statutEdition");
				cr.add(Restrictions.eq("statutEdition.idt", 2L));
				cr.add(Restrictions.eq("etatRemiseControle.code", "INSTANCE"));
			} else {
				cr.add(Restrictions.eq("etatRemiseControle.code", codeEtat));
			}
		}
		if(idtFournisseur != null && idtFournisseur != 0) {
			cr.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		}
		if(idtRemise != null && idtRemise != 0) {
			cr.add(Restrictions.eq("remiseControle.idt", idtRemise));
		}
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("remiseControle.idt"));
			return cr.list();
		}else if(pageNumber > 0 && pageSize > 0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("remiseControle.idt"));
			return cr.list();
		}
		return null;
	}
	
	public Integer getTotalRemiseControle(String codeEtat, Long idtFournisseur, Long idtRemise) {
		
		Criteria cr = this.getSession().createCriteria(RemiseControle.class,"remiseControle");
		cr.createAlias("remiseControle.etatRemiseControle", "etatRemiseControle");
		cr.createAlias("remiseControle.fournisseur", "fournisseur");
		if(codeEtat != null && !codeEtat.equals("")) {
			if(codeEtat.equalsIgnoreCase("DRAFT")) {
				cr.createAlias("remiseControle.statutEdition", "statutEdition");
				cr.add(Restrictions.eq("statutEdition.idt", 1L));
				cr.add(Restrictions.eq("etatRemiseControle.code", "INSTANCE"));
			} else if(codeEtat.equalsIgnoreCase("INSTANCE")) {
				cr.createAlias("remiseControle.statutEdition", "statutEdition");
				cr.add(Restrictions.eq("statutEdition.idt", 2L));
				cr.add(Restrictions.eq("etatRemiseControle.code", "INSTANCE"));
			} else {
				cr.add(Restrictions.eq("etatRemiseControle.code", codeEtat));
			}
		}
		if(idtFournisseur != null && idtFournisseur != 0) {
			cr.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		}
		if(idtRemise != null && idtRemise != 0) {
			cr.add(Restrictions.eq("remiseControle.idt", idtRemise));
		}
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListRemiseIdsControleNative(String idtsFournisseur, String idtsContrat, 
			String idtsProjet, String idtsDr, String idtsSite, 
			String codesEtat, Long idtRemise) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(r.IDT) from PVR_REMISE_CONTROLE r "
				+ " LEFT JOIN PVR_REMISE_MES rm ON r.IDT = rm.IDT_REMISE_CONTROLE "
				+ " LEFT JOIN PVR_MES m ON m.IDT = rm.IDT_MES "
				+ " LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " LEFT JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " LEFT JOIN PVR_STATUT_EDITION sd ON sd.IDT = r.IDT_STATUT_EDITION "
				+ " LEFT JOIN PVR_ETAT_REMISE_CONTROLE er ON er.IDT = r.IDT_ETAT_REMISE_CONTROLE "
				+ " WHERE 1 = 1 AND sd.IDT != 1 " ;
		
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(!StringUtil.isNullOrEmpty(codesEtat)){
			sql += " AND er.CODE in (" + codesEtat + ")";
		}
		if(idtRemise != null && idtRemise != 0){
			sql += " AND r.IDT = " + idtRemise;
		}
		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RemiseControle> getRemiseControleByCriteresPagination(List<Long> idsRemise,
			int pageNumber, int pageSize) {
			Criteria cr = this.getSession().createCriteria(RemiseControle.class, "remiseControle");
			cr.add(Restrictions.in("remiseControle.idt", idsRemise));

			//Pagination
			if(pageNumber>0 && pageSize>0){
				cr.setFirstResult((pageNumber-1)*pageSize);
				cr.setMaxResults(pageSize);
			}else{
				cr.setFirstResult(0);
			}
			cr.addOrder(Order.desc("remiseControle.idt"));

			return cr.list();
	}
	
	public int getTotalRemiseControleByCriteres(List<Long> idsRemise) {
		Criteria cr = this.getSession().createCriteria(RemiseControle.class, "remiseControle");
		cr.add(Restrictions.in("remiseControle.idt", idsRemise));

		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}
}
