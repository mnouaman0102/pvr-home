package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.PjOperationInfra;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IPjOperationInfraDao extends IGenericDao<PjOperationInfra, Long> {

	List<PjOperationInfra> getPJOperationDccListByMesIdt(Long idtMes);
	List<PjOperationInfra> getPJOperationDeploiByMesIdt(Long idtMes);
	List<PjOperationInfra> getPJOperationByMesIdtAndDrIdt(Long idtMes, Long idtDr);
	List<PjOperationInfra> getPJsOperationByMesIdt(Long idtMes);
	List<PjOperationInfra> getPJsOperationByMesIdtAndEntiteIdt(Long idtMes, Long idtEntite);
	List<PjOperationInfra> getPJsOperationByOperIdt(Long idtOper);
	List<PjOperationInfra> getPJOperationDccSuiviListByMesIdt(Long idtMes);
}
