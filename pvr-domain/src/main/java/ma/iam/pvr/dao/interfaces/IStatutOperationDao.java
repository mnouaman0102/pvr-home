package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.StatutOperation;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IStatutOperationDao extends IGenericDao<StatutOperation, Long> {
	
	
}
