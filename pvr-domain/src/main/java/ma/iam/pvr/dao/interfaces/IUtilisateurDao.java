package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Profil;
import ma.iam.pvr.domain.Utilisateur;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IUtilisateurDao extends IGenericDao<Utilisateur, Long> {

	public Utilisateur getUserByID(Long idt);
	
	public Utilisateur getUserByLogin(String login);

	public boolean isAdmin(List<Profil> profils);


	
}
