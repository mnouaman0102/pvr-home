package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.Responsabilite;
//import ma.iam.socle.service.GenericDao;

public interface IResponsabiliteDao extends IGenericDao<Responsabilite, Long> {

	Responsabilite getResponsabiliteByLabel(String label);
}
