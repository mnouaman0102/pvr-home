package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.TypeAntenne;
//import ma.iam.socle.service.GenericDao;

public interface ITypeAntenneDao extends IGenericDao<TypeAntenne, Long> {

	TypeAntenne getTypeAntenneByLabel(String label);
}
