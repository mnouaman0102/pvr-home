package ma.iam.pvr.dao.implementation;


import ma.iam.pvr.dao.interfaces.ITypeProjetDao;
import ma.iam.pvr.domain.TypeProjet;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class TypeProjetDaoImpl extends DefaultGenericDao<TypeProjet, Long> implements ITypeProjetDao {

}
