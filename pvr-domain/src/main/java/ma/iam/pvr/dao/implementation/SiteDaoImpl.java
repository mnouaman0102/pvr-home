package ma.iam.pvr.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.dao.interfaces.ISiteDao;
import ma.iam.pvr.domain.Site;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class SiteDaoImpl extends DefaultGenericDao<Site, Long> implements ISiteDao {

	@SuppressWarnings("unchecked")
	public List<Site> getSiteByRtId(Long idtRt) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.add(Restrictions.eq("site.idt", idtRt));
		cr.addOrder(Order.asc("site.label"));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<Site> getAllSitesByDr(Long idtDr) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.addOrder(Order.asc("site.label"));
		
		if(idtDr != 0){
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		
		return (List<Site>) cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<Site> getAllSitesByListDr(List<Long> idtDrs) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.addOrder(Order.asc("site.label"));
		
		if(idtDrs != null){
			cr.add(Restrictions.in("dr.idt", idtDrs));
		}
		
		return (List<Site>) cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Site> getSitesList(Long idtDr, Long idtSite, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		cr.addOrder(Order.asc("site.label"));
		cr.addOrder(Order.asc("dr.idt"));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Site> getSitesList(Long idtDr, Long idtSite, Long idtEtatSite, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.etatSite", "etatSite");
		cr.add(Restrictions.eq("etatSite.idt", idtEtatSite));
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		cr.addOrder(Order.asc("site.label"));
		cr.addOrder(Order.asc("dr.idt"));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Site> getSitesExceptMesList(Long idtDr, Long idtSite, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.addOrder(Order.asc("site.label"));
		cr.addOrder(Order.asc("dr.idt"));
		
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		cr.add(Restrictions.ne("etatSite.idt", 4L));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	public int getTotalSitesList(Long idtDr, Long idtSite, Long idtEtatSite) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.etatSite", "etatSite");
		cr.add(Restrictions.eq("etatSite.idt", idtEtatSite));
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		cr.addOrder(Order.asc("site.label"));
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}
	
	public int getTotalSites(Long idtDr, Long idtSite) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		cr.addOrder(Order.asc("site.label"));
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}
	
	public int getTotalSitesExceptMesList(Long idtDr, Long idtSite) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.etatSite", "etatSite");
		cr.addOrder(Order.asc("site.label"));
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		cr.add(Restrictions.ne("etatSite.idt", 4L));
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Site> getSitesListByDr(Long idtDr) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.addOrder(Order.asc("site.label"));
		cr.createAlias("site.dr", "dr");
		
		if(idtDr != 0){
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Site> getSitesExceptMesListByDr(Long idtDr) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.etatSite", "etatSite");
		cr.addOrder(Order.asc("site.label"));
		cr.createAlias("site.dr", "dr");
		
		if(idtDr != 0){
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		cr.add(Restrictions.ne("etatSite.idt", 4L));
		return cr.list();
	}


	public boolean isSiteDrAlreadyExist(String labelSite, Long idtDr) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.addOrder(Order.asc("site.label"));
		cr.createAlias("site.dr", "dr");
		cr.add(Restrictions.eq("dr.idt", idtDr));
		cr.add(Restrictions.eq("site.label", labelSite));
		if(cr.uniqueResult() != null) return true;
		else return false;
	}

	public Site getSiteByLabelAndDr(String label, Long idtDr) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.add(Restrictions.eq("dr.idt", idtDr));
		cr.add(Restrictions.eq("site.label", label));
		cr.setMaxResults(1);
		return (Site) cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public Long getSiteIdByLabel(String label) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.add(Restrictions.eq("site.label", label));
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("idt"));
		cr.setProjection(projList);
		List<Long> ids = (List<Long>)cr.list();
		return (ids.isEmpty()) ? null : ids.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Long> getSitesByLabels(List<String> labels) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.add(Restrictions.in("site.label", labels));
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("idt"));
		cr.setProjection(projList);
		List<Long> ids = (List<Long>)cr.list();
		return ids;
	}
	
	public Site getSiteByCode(String code) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.add(Restrictions.eq("site.code", code));
		cr.addOrder(Order.desc("site.idt"));
		cr.setMaxResults(1);
		return (Site) cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Site> getAllSiteByDrAndListEtats(Long idtDr, List<Long> idtEtats) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.etatSite", "etatSite");
		if(idtEtats != null){
			cr.add(Restrictions.in("etatSite.idt", idtEtats));
		}
		
		if(idtDr != 0){
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		
		cr.addOrder(Order.asc("site.label"));
		
		return (List<Site>) cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Site> getSitesByCritere(List<Long> idsSites) {
		Criteria cr_sites = this.getSession().createCriteria(Site.class, "site");
		cr_sites.add(Restrictions.in("site.idt", idsSites));
		cr_sites.addOrder(Order.desc("site.idt"));
		return cr_sites.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Site> getSitesCandidaturesByCriteres(Long idtDr, Long idtSite, String code, String label, 
			List<Long> idtEtatSite, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.etatSite", "etatSite");
		
		if(code != null && !"".equals(code)) {
			cr.add(Restrictions.like("site.code", "%" + code + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(label != null && !"".equals(label)) {
			cr.add(Restrictions.like("site.label", "%" + label + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(idtDr != null && idtDr != 0L) {
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		if(idtEtatSite != null && idtEtatSite.size() > 0) {
			cr.add(Restrictions.in("etatSite.idt", idtEtatSite));
		} else {
			List<Long> ids = new ArrayList<Long>();
			ids.add(1L);
			ids.add(5L);
			ids.add(6L);
			ids.add(7L);
			cr.add(Restrictions.in("etatSite.idt", ids));
		}
		//  Date derniere modification site
		cr.addOrder(Order.asc("site.dateLastModification"));
		cr.addOrder(Order.asc("site.label"));
		// cr.addOrder(Order.asc("dr.idt"));
		if(pageNumber == null || pageSize == null || pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			return cr.list();
		}else if(pageNumber > 0 && pageSize > 0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	public int getTotalSitesCandidaturesByCriteres(Long idtDr, Long idtSite, String code, String label, 
			List<Long> idtEtatSite) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.etatSite", "etatSite");
		
		if(code != null && !"".equals(code)) {
			cr.add(Restrictions.like("site.code", "%" + code + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(label != null && !"".equals(label)) {
			cr.add(Restrictions.like("site.label", "%" + label + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(idtDr != null && idtDr != 0L) {
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		if(idtEtatSite != null && idtEtatSite.size() > 0) {
			cr.add(Restrictions.in("etatSite.idt", idtEtatSite));
		} else {
			List<Long> ids = new ArrayList<Long>();
			ids.add(1L);
			ids.add(5L);
			ids.add(6L);
			ids.add(7L);
			cr.add(Restrictions.in("etatSite.idt", ids));
		}
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllCentreRattachementForBdSite() {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(s.CENTRE_RATTACHEMENT) from PVR_SITE s ";
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllCentreRattachementByDrs(String idtsDr) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(s.CENTRE_RATTACHEMENT) from PVR_SITE s "
				   + "JOIN PVR_DR dr ON s.IDT_DR = dr.IDT ";
		if(idtsDr != null && !idtsDr.isEmpty()){
			sql += " WHERE dr.IDT IN (" + idtsDr + ")";
		}
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Site> getSitesListByDrs(List<Long> idtsDr) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.addOrder(Order.asc("site.label"));
		
		if(idtsDr != null && idtsDr.size() > 0){
			cr.add(Restrictions.in("dr.idt", idtsDr));
		}		
		return (List<Site>) cr.list();
	}
	
	public int getTotalSitesByCriteres(List<Long> idtsDr, List<Long> idtsDc,
			List<Long> idtsSite, List<String> centresRattachement) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.dc", "dc");
		
		if(idtsDr != null && idtsDr.size() > 0){
			cr.add(Restrictions.in("dr.idt", idtsDr));
		}	
		
		if(idtsDc != null && idtsDc.size() > 0){
			cr.add(Restrictions.in("dc.idt", idtsDc));
		}	
		
		if(centresRattachement != null && !centresRattachement.isEmpty()){
			cr.add(Restrictions.in("site.centreRattachement", centresRattachement));
		}
		
		if(idtsSite != null && idtsSite.size() > 0){
			cr.add(Restrictions.in("site.idt", idtsSite));
		}
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Site> getSitesByCriteres(List<Long> idtsDr, List<Long> idtsDc,
			List<Long> idtsSite, List<String>  centresRattachement, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.dc", "dc");
		cr.addOrder(Order.asc("site.label"));
		
		if(idtsDr != null && idtsDr.size() > 0){
			cr.add(Restrictions.in("dr.idt", idtsDr));
		}	
		
		if(idtsDc != null && idtsDc.size() > 0){
			cr.add(Restrictions.in("dc.idt", idtsDc));
		}	
		
		if(centresRattachement != null && !centresRattachement.isEmpty()){
			cr.add(Restrictions.in("site.centreRattachement", centresRattachement));
		}	
		
		if(idtsSite != null && idtsSite.size() > 0){
			cr.add(Restrictions.in("site.idt", idtsSite));
		}
		if(pageNumber == null || pageSize == null || pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			return cr.list();
		}else if(pageNumber > 0 && pageSize > 0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	public int getTotalSitesByCriteresForExploi(Long idtDr, Long idtSite, String code, String label, 
			List<Long> idtEtatSite) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.etatSite", "etatSite");
		
		if(code != null && !"".equals(code)) {
			cr.add(Restrictions.like("site.code", "%" + code + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(label != null && !"".equals(label)) {
			cr.add(Restrictions.like("site.label", "%" + label + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(idtDr != null && idtDr != 0L) {
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		if(idtEtatSite != null && idtEtatSite.size() > 0) {
			cr.add(Restrictions.in("etatSite.idt", idtEtatSite));
		} else {
			List<Long> ids = new ArrayList<Long>();
			ids.add(4L);
			cr.add(Restrictions.in("etatSite.idt", ids));
		}
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Site> getSitesByCriteresForExploi(Long idtDr, Long idtSite, String code, String label, 
			List<Long> idtEtatSite, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		cr.createAlias("site.dr", "dr");
		cr.createAlias("site.etatSite", "etatSite");
		
		if(code != null && !"".equals(code)) {
			cr.add(Restrictions.like("site.code", "%" + code + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(label != null && !"".equals(label)) {
			cr.add(Restrictions.like("site.label", "%" + label + "%", MatchMode.ANYWHERE).ignoreCase());
		}
		
		if(idtDr != null && idtDr != 0L) {
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		if(idtEtatSite != null && idtEtatSite.size() > 0) {
			cr.add(Restrictions.in("etatSite.idt", idtEtatSite));
		} else {
			List<Long> ids = new ArrayList<Long>();
			ids.add(4L);
			cr.add(Restrictions.in("etatSite.idt", ids));
		}
		//  Date derniere modification site
		cr.addOrder(Order.asc("site.dateLastModification"));
		cr.addOrder(Order.asc("site.label"));
		// cr.addOrder(Order.asc("dr.idt"));
		if(pageNumber == null || pageSize == null || pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			return cr.list();
		}else if(pageNumber > 0 && pageSize > 0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}

	public int getTotalSitesByCriteresForMaj(Long idtDr, Long idtSite, List<Long> idtEtatsSite) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		//cr.addOrder(Order.asc("site.label"));
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		if(idtEtatsSite != null && idtEtatsSite.size() > 0) {
			cr.createAlias("site.etatSite", "etatSite");
			cr.add(Restrictions.in("etatSite.idt", idtEtatsSite));
		} else {
			cr.createAlias("site.etatSite", "etatSite");
			cr.add(Restrictions.ne("etatSite.idt", 4L));
		}
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Site> getSitesByCriteresForMaj(Long idtDr, Long idtSite,
			List<Long> idtEtatsSite, Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		//cr.addOrder(Order.asc("site.label"));
		//cr.addOrder(Order.asc("dr.idt"));
		
		if(idtDr != 0 && idtSite == 0){
			// liste des sites par DR
			cr.createAlias("site.dr", "dr");
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}else if(idtDr != 0 && idtSite != 0){
			// Un seul site
			cr.add(Restrictions.eq("site.idt", idtSite));
		}else{
			// liste de tous les sites
		}
		if(idtEtatsSite != null && idtEtatsSite.size() > 0) {
			cr.createAlias("site.etatSite", "etatSite");
			cr.add(Restrictions.in("etatSite.idt", idtEtatsSite));
		} else {
			cr.createAlias("site.etatSite", "etatSite");
			cr.add(Restrictions.ne("etatSite.idt", 4L));
		}
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Site> getSitesForMajByDr(Long idtDr, List<Long> idtEtatsSite) {
		Criteria cr = this.getSession().createCriteria(Site.class, "site");
		//cr.addOrder(Order.asc("site.label"));	
		if(idtDr != null && idtDr != 0){
			cr.createAlias("site.dr", "dr");
			cr.add(Restrictions.eq("dr.idt", idtDr));
		}
		if(idtEtatsSite != null && idtEtatsSite.size() > 0) {
			cr.createAlias("site.etatSite", "etatSite");
			cr.add(Restrictions.in("etatSite.idt", idtEtatsSite));
		} else {
			cr.createAlias("site.etatSite", "etatSite");
			cr.add(Restrictions.ne("etatSite.idt", 4L));
		}
		return cr.list();
	}
}
