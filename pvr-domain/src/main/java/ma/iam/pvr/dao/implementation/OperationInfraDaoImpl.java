package ma.iam.pvr.dao.implementation;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.mysql.cj.util.StringUtils;
import ma.iam.pvr.dao.interfaces.IOperationInfraDao;
import ma.iam.pvr.domain.OperationInfra;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.domain.utils.Utils;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

//import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class OperationInfraDaoImpl extends DefaultGenericDao<OperationInfra, Long> implements IOperationInfraDao {

	@SuppressWarnings("unchecked")
	public List<String> findAllNumOperations() {
		Criteria cr = this.getSession().createCriteria(OperationInfra.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property("numOperation"), "numOperation"));
		cr.addOrder(Order.asc("numOperation"));
		return (List<String>) cr.list();
	}



	public OperationInfra getByNumeroOperation(String numeroOperation) {
		Criteria cr = this.getSession().createCriteria(OperationInfra.class);
		cr.add(Restrictions.eq("numOperation", numeroOperation));
		return (OperationInfra) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDeploiMultipleValues(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, String idtsEtat, List<String> numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE 1 = 1 " ;
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsEtat)){
			sql += " AND m.IDT_ETAT_OPERATION_INFRA in (" + idtsEtat + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET_INFRA in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsTypeProjet)){
			sql += " AND m.IDT_TYPE_PROJET_INFRA in (" + idtsTypeProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + StringUtil.joinListString(numOperation, ",") + ")";
		}
		return session.createSQLQuery(sql).list();
	}



	@SuppressWarnings("unchecked")
	public List<OperationInfra> getMesByCriteresPagination(List<Long> idsMES, int pageNumber, int pageSize) {
		Criteria cr_mes = this.getSession().createCriteria(OperationInfra.class, "mes");
		cr_mes.add(Restrictions.in("mes.idt", idsMES));

		//Pagination
		if(pageNumber>0 && pageSize>0){
			cr_mes.setFirstResult((pageNumber-1)*pageSize);
			cr_mes.setMaxResults(pageSize);
		}else{
			cr_mes.setFirstResult(0);
		}
		cr_mes.addOrder(Order.desc("mes.idt"));

		return cr_mes.list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDeploiDr(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtDr, Long idtSite, Long idtEtat, String numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE 1 = 1 " ;
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}

		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET_INFRA = " + idtProjet;
		}

		if(idtDr != 0){
			sql += " AND s.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sql += " AND m.IDT_SITE = " + idtSite;
		}

		if(idtEtat != 0){
			sql += " AND m.IDT_ETAT_OPERATION_INFRA = " + idtEtat;
		}

		if(numOperation != null && !numOperation.equals("")){
			String num = "'%" + numOperation + "%'";
			sql += " AND m.NUMOPERATION LIKE " + num;
		}
		return session.createSQLQuery(sql).list();
	}



	public int getTotalMesByCriteres(List<Long> idsMES) {
		Criteria cr_mes = this.getSession().createCriteria(OperationInfra.class, "mes");
		cr_mes.add(Restrictions.in("mes.idt", idsMES));

		cr_mes.setProjection(Projections.rowCount());
		return (Integer)cr_mes.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForDeploi() {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE m.IDT_ETAT_OPERATION_INFRA in (1, 2, 3)";

		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForDr(Long idtDr) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE m.IDT_ETAT_OPERATION_INFRA in (1, 2, 3)";

		if(idtDr != 0){
			sql += " AND s.IDT_DR = " + idtDr;
		}
		return session.createSQLQuery(sql).list();
	}

	
	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDrMultipleValues(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, String idtsEtat, List<String> numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE 1 = 1 " ;
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsEtat)){
			sql += " AND m.IDT_ETAT_OPERATION_INFRA in (" + idtsEtat + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET_INFRA in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsTypeProjet)){
			sql += " AND m.IDT_TYPE_PROJET_INFRA in (" + idtsTypeProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + StringUtil.joinListString(numOperation, ",") + ")";
		}
		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDccMultipleValues(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, String idtsEtat, List<String> numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE 1 = 1 " ;
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsEtat)){
			sql += " AND m.IDT_ETAT_OPERATION_INFRA in (" + idtsEtat + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET_INFRA in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsTypeProjet)){
			sql += " AND m.IDT_TYPE_PROJET_INFRA in (" + idtsTypeProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + StringUtil.joinListString(numOperation, ",") + ")";
		}
		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	private List<Long> getAllOperationInfraByPvrId(Long idtPvr) {
		Session session = this.getSession(); 
		String sql = "SELECT m.idt from PVR_OPERATION_INFRA m "
				+ "JOIN PVR_PVR_INFRA pvr ON pvr.IDT = m.IDT_PVR_INFRA "
				+ "WHERE pvr.IDT =" + idtPvr
				+ " UNION "
				+ "SELECT m.idt from PVR_OPERATION_INFRA m " 
				+ "JOIN PVR_PVR_OPERATION_INFRA mesPvr ON mesPvr.IDT_OPERATION_INFRA = m.IDT "
				+ "WHERE mesPvr.IDT_PVR_INFRA =" + idtPvr;
		List<Object> objects = session.createSQLQuery(sql).list();
		final List<Long> list = new LinkedList<Long>();
		for(final Object o : objects) {
		    list.add(((BigInteger)o).longValue());
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationInfra> getOperationInfraListByPvrId(Long idtPvr) {
		List<Long> mesIdt = getAllOperationInfraByPvrId(idtPvr);
		if(mesIdt != null && mesIdt.size() > 0){
			Criteria cr = this.getSession().createCriteria(OperationInfra.class, "mes");
			cr.add(Restrictions.in("mes.idt", mesIdt));
			return cr.list();
		}else{
			Criteria cr = this.getSession().createCriteria(OperationInfra.class, "mes");
			cr.createAlias("mes.pvrInfra", "pvr");
			cr.add(Restrictions.eq("pvr.idt", idtPvr));
			return cr.list();
		}
	}
	
	public List<Object> getListOperationIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtEtat, String dateDebut, String dateFin, 
			List<Long> idtMes, Long idtDr, List<Long> idSites){
		return getListOperationIdtWithPVRNull(idtFournisseur, idtContrat, idtProjet, idtEtat, dateDebut, dateFin
				, idtMes, idtDr, idSites, false);
	}
	@SuppressWarnings("unchecked")
	public List<Object> getListOperationIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtEtat, String dateDebut, String dateFin, 
			List<Long> idtMes, Long idtDr, List<Long> idSites, boolean excludeDraft) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_RT_OPERATION_INFRA mrt ON mrt.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_RT_INFRA rt ON rt.IDT = mrt.IDT_RT_INFRA "
				+ " LEFT JOIN PVR_PVR_INFRA pvr ON pvr.IDT = m.IDT_PVR_INFRA "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT_OPERATION_INFRA IN (10) "
				+ " AND (m.IDT_PVR_INFRA is NULL OR (m.IDT_PVR_INFRA is not NULL AND pvr.IDT_STATUT_EDITION = 1)) ";
				;

		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET_INFRA = " + idtProjet;
		}
		if(idtEtat != 0){
			sql += " AND m.IDT_ETAT_OPERATION_INFRA = " + idtEtat;
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (m.DATE >= '" + date_From + "' AND m.DATE <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}


		if(excludeDraft){
			sql += " AND COALESCE(m.IDT_PVR_INFRA, 0) not in ( select pvr.IDT from PVR_PVR_INFRA pvr " +
					"inner join PVR_STATUT_EDITION s on s.IDT = pvr.IDT_STATUT_EDITION " + 
					"and s.LABEL = 'Draft')";
		}

		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}
		
		if(idSites != null && idSites.size() > 0){
			StringBuilder sitesIds =new StringBuilder();
			for(Long idSite : idSites){
				if(sitesIds.length() > 0){
					sitesIds.append(",");
				}
				sitesIds.append(idSite);
			}
			sql += " AND m.IDT_SITE in (" + sitesIds.toString() + ")";
		}

		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListOperationIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtEtat, String dateDebut, String dateFin, List<Long> idtMes, Long idtDr, Long idtSite) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_RT_OPERATION_INFRA mrt ON mrt.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_RT_INFRA rt ON rt.IDT = mrt.IDT_RT_INFRA "
				+ " LEFT JOIN PVR_PVR_INFRA pvr ON pvr.IDT = m.IDT_PVR_INFRA "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT_OPERATION_INFRA IN (10) "
				+ " AND (m.IDT_PVR_INFRA is NULL OR (m.IDT_PVR_INFRA is not NULL AND pvr.IDT_STATUT_EDITION = 1)) ";
				;

		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET_INFRA = " + idtProjet;
		}
		if(idtEtat != 0){
			sql += " AND m.IDT_ETAT_OPERATION_INFRA = " + idtEtat;
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (m.DATE >= '" + date_From + "' AND m.DATE <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}
		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sql += " AND m.IDT_SITE = " + idtSite;
		}

		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationInfra> getListMesByIDT(List<Long> idsMES) {
		Criteria cr_mes = this.getSession().createCriteria(OperationInfra.class, "mes");
		cr_mes.add(Restrictions.in("mes.idt", idsMES));
		return cr_mes.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationInfra> getOperationInfraListByPvdId(Long idtPvd) {
		Criteria cr = this.getSession().createCriteria(OperationInfra.class, "mes");
		cr.createAlias("mes.pvdInfra", "pvd");
		cr.add(Restrictions.eq("pvd.idt", idtPvd));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListOperationsIdtWithPVDNull(Long idtFournisseur, Long idtContrat,
			Long idtEtat, String dateDebut, String dateFin, List<Long> idtMes, Long idtDr, Long idtSite) {
		Session session = this.getSession(); 
		String sqlWhere = 	"";	
		if(idtContrat != 0){
			sqlWhere += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sqlWhere += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtEtat != 0){
			sqlWhere += " AND m.IDT_ETAT_OPERATION_INFRA = " + idtEtat;
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_Debut = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_Fin = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sqlWhere += " AND (pvr.DATE >= '" + date_Debut + "' AND pvr.DATE <= '" + date_Fin +"')";
			} catch (ParseException e) {
				e.getMessage();
			}			
		}

		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sqlWhere += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sqlWhere += " AND si.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sqlWhere += " AND m.IDT_SITE = " + idtSite;
		}
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_RT_OPERATION_INFRA mrt ON mrt.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_RT_INFRA rt ON rt.IDT = mrt.IDT_RT_INFRA "
				+ " JOIN PVR_PVR_INFRA pvr ON pvr.IDT = m.IDT_PVR_INFRA "
				+ " LEFT JOIN PVR_PVD_INFRA pvd ON pvd.IDT = m.IDT_PVD_INFRA "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT_OPERATION_INFRA in (11) "
				+ " AND (m.IDT_PVD_INFRA is NULL OR (m.IDT_PVD_INFRA is not NULL AND pvd.IDT_STATUT_EDITION = 1)) " ;
				sql += sqlWhere;
				sql +=
				" UNION "
				+ "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_RT_OPERATION_INFRA mrt ON mrt.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_RT_INFRA rt ON rt.IDT = mrt.IDT_RT_INFRA "
				+ " JOIN PVR_PVR_OPERATION_INFRA mPvr ON mPvr.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_PVR_INFRA pvr ON pvr.IDT = mPvr.IDT_PVR_INFRA "
				+ " LEFT JOIN PVR_PVD_INFRA pvd ON pvd.IDT = m.IDT_PVD_INFRA "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT_OPERATION_INFRA in (11) "
				+ " AND (m.IDT_PVD_INFRA is NULL OR (m.IDT_PVD_INFRA is not NULL AND pvd.IDT_STATUT_EDITION = 1)) " ;
				sql += sqlWhere;

		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getMesIdtByCriteres(String idtsFournisseur, String idtsContrat,
			Long idtStatut, String idtsEtat, String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, String idtsRt) {
		Session session = this.getSession();
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " LEFT JOIN PVR_OPERATION_INFRA_RESERVE mr ON mr.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE ";
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " LEFT JOIN PVR_RT_OPERATION_INFRA mrt ON mrt.IDT_OPERATION_INFRA = m.IDT ";
		}
		sql += " WHERE 1=1 ";
		if(!StringUtil.isNullOrEmpty(idtsContrat) && !Utils.containInjectChar(idtsContrat)) {
			sql += " AND c.IDT in (" + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsFournisseur) && !Utils.containInjectChar(idtsFournisseur)) {
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet) && !Utils.containInjectChar(idtsProjet)) {
			sql += " AND m.IDT_PROJET_INFRA in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsTypeProjet) && !Utils.containInjectChar(idtsTypeProjet)) {
			sql += " AND m.IDT_TYPE_PROJET_INFRA in (" + idtsTypeProjet + ")";
		}		
		if(!StringUtil.isNullOrEmpty(idtsDr) && !Utils.containInjectChar(idtsDr)) {
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}		
		if(!StringUtil.isNullOrEmpty(idtsSite) && !Utils.containInjectChar(idtsSite)) {
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}		
		if(!StringUtil.isNullOrEmpty(idtsEtat) && !Utils.containInjectChar(idtsEtat)) {
			sql += " AND m.IDT_ETAT_OPERATION_INFRA in (" + idtsEtat + ")";
		}
		if(idtStatut != 0){
			if(idtStatut == 2){
				sql += " AND (mr.IDT_STATUT is NULL OR m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = 2))";
			}else{
				sql += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = 1)";
			}
		}
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " AND mrt.IDT_RT_INFRA in (" + idtsRt + ")";
		}
		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationInfra> getMesByCriteres(List<Long> idsMES) {
		Criteria cr_mes = this.getSession().createCriteria(OperationInfra.class, "mes");
		cr_mes.add(Restrictions.in("mes.idt", idsMES));
		cr_mes.addOrder(Order.desc("mes.idt"));
		return cr_mes.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getOperationsReceptionneExploitationInfra(String idtsFournisseur, String idtsContrat,
			String idtsProjet, String idtsTypeProjet, String idtsDr, String idtsSite, List<String> numOperation) {
		Session session = this.getSession(); 
		String sqlWhere = 	"";	
		
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sqlWhere += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sqlWhere += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sqlWhere += " AND m.IDT_PROJET_INFRA in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsTypeProjet)){
			sqlWhere += " AND m.IDT_TYPE_PROJET_INFRA in (" + idtsTypeProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sqlWhere += " AND si.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sqlWhere += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sqlWhere += " AND m.NUMOPERATION in (" + StringUtil.joinListString(numOperation, ",") + ")";
		}
		
		String sql = "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_RT_OPERATION_INFRA mrt ON mrt.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_RT_INFRA rt ON rt.IDT = mrt.IDT_RT_INFRA "
				+ " JOIN PVR_PVR_INFRA pvr ON pvr.IDT = m.IDT_PVR_INFRA "
				+ " LEFT JOIN PVR_PVD_INFRA pvd ON pvd.IDT = m.IDT_PVD_INFRA "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND pvr.IDT_STATUT_EDITION = 2 "
				+ " AND m.IDT_PVR_INFRA IS NOT NULL "
				+ " AND m.IDT_ETAT_OPERATION_INFRA in (11, 12) ";
				sql += sqlWhere;
				sql +=
				" UNION "
				+ "SELECT DISTINCT(m.IDT) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_RT_OPERATION_INFRA mrt ON mrt.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_RT_INFRA rt ON rt.IDT = mrt.IDT_RT_INFRA "
				+ " JOIN PVR_PVR_OPERATION_INFRA mPvr ON mPvr.IDT_OPERATION_INFRA = m.IDT "
				+ " JOIN PVR_PVR_INFRA pvr ON pvr.IDT = mPvr.IDT_PVR_INFRA "
				+ " LEFT JOIN PVR_PVD_INFRA pvd ON pvd.IDT = m.IDT_PVD_INFRA "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND pvr.IDT_STATUT_EDITION = 2 "
				+ " AND m.IDT_PVR_INFRA IS NOT NULL "
				+ " AND m.IDT_ETAT_OPERATION_INFRA in (11, 12) ";
				sql += sqlWhere;

		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForExploitation() {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_OPERATION_INFRA m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE m.IDT_ETAT_OPERATION_INFRA in (11, 12)";

		return session.createSQLQuery(sql).list();
	}
}
