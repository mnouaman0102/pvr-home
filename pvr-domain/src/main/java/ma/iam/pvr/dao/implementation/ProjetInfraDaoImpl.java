package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IProjetInfraDao;
import ma.iam.pvr.domain.ProjetInfra;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class ProjetInfraDaoImpl extends DefaultGenericDao<ProjetInfra, Long> implements IProjetInfraDao {


	public ProjetInfra getProjetByLabel(String label) {
			Criteria cr = this.getSession().createCriteria(ProjetInfra.class, "projetInfra");			
			cr.add(Restrictions.eq("projetInfra.label", label));
			cr.setMaxResults(1);
			return (ProjetInfra) cr.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<ProjetInfra> getProjectsInfra(){
		Criteria cr = this.getSession().createCriteria(ProjetInfra.class, "projetInfra");
		cr.add(Restrictions.eq("projetInfra.workflow", "infra"));
		cr.addOrder(Order.asc("projetInfra.idt"));
		return cr.list();
	}
	
}
