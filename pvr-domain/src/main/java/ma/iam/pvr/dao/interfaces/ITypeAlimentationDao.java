package ma.iam.pvr.dao.interfaces;

import ma.iam.pvr.domain.TypeAlimentation;
//import ma.iam.socle.service.GenericDao;
/**
 * @author K.ELBAGUARI
 *
 */
public interface ITypeAlimentationDao extends IGenericDao<TypeAlimentation, Long> {

	TypeAlimentation getTypeAlimentationByLabel(String label);
}
