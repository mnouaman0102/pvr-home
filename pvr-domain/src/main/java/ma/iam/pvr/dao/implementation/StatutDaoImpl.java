package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.IStatutDao;
import ma.iam.pvr.domain.Statut;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class StatutDaoImpl extends DefaultGenericDao<Statut, Long> implements IStatutDao {


	public Statut getByLabel(String statut) {
		Criteria cr = this.getSession().createCriteria(Statut.class);
		cr.add(Restrictions.eq("label", statut).ignoreCase());
		return (Statut) cr.uniqueResult();
	}
	
}

