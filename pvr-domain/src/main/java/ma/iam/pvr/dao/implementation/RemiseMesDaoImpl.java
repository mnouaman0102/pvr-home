package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IRemiseMesDao;
import ma.iam.pvr.domain.RemiseMes;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class RemiseMesDaoImpl extends DefaultGenericDao<RemiseMes, Long> implements IRemiseMesDao {

	public RemiseMes getRemiseMesByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(RemiseMes.class,"mes_remise");
		cr.createAlias("mes_remise.mes", "mes");
		cr.createAlias("mes_remise.remiseControle", "remise");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("remise.dateCreationReelle"));
		cr.addOrder(Order.desc("rremise.idt"));
		cr.setMaxResults(1);
		return (RemiseMes) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<RemiseMes> getListRemiseMesByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(RemiseMes.class,"mes_remise");
		cr.createAlias("mes_remise.mes", "mes");
		cr.createAlias("mes_remise.remiseControle", "remise");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("remise.dateCreationReelle"));
		cr.addOrder(Order.desc("remise.idt"));
		return  cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RemiseMes> getListRemiseMesByRemise(Long idtRemise) {
		Criteria cr = this.getSession().createCriteria(RemiseMes.class,"mes_remise");
		cr.createAlias("mes_remise.remiseControle", "remise");
		cr.add(Restrictions.eq("remise.idt", idtRemise));
		return  cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RemiseMes> getListRemiseMesValidByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(RemiseMes.class,"mes_remise");
		cr.createAlias("mes_remise.mes", "mes");
		cr.createAlias("mes_remise.remiseControle", "remiseControle");
		
		cr.createAlias("remiseControle.etatRemiseControle", "etatRemiseControle");
		cr.add(Restrictions.eq("etatRemiseControle.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("remiseControle.dateCreationReelle"));
		cr.addOrder(Order.desc("remiseControle.idt"));
		return  cr.list();
	}

}

