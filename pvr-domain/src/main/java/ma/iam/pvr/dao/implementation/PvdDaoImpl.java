package ma.iam.pvr.dao.implementation;



import java.util.List;

import ma.iam.pvr.dao.interfaces.IPVDDao;
import ma.iam.pvr.domain.Pvd;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class PvdDaoImpl extends DefaultGenericDao<Pvd, Long> implements IPVDDao {

	@SuppressWarnings("unchecked")
	public List<Pvd> getAllPvd(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Pvd.class, "pvd");
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}
	
	public Integer countAllPvd() {
		Criteria cr = this.getSession().createCriteria(Pvd.class, "pvd");
		cr.setProjection(Projections.rowCount());
		return (Integer) cr.uniqueResult();
	}
	
	public Integer countListPvdDraft() {
		Criteria cr = this.getSession().createCriteria(Pvd.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<Pvd> getListPvdDraft(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Pvd.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}
		return null;
	}
	
	public Integer countListPvdValid() {
		Criteria cr = this.getSession().createCriteria(Pvd.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<Pvd> getListPvdValid(Integer pageNumber, Integer pageSize) {
		Criteria cr = this.getSession().createCriteria(Pvd.class, "pvd");
		cr.createAlias("pvd.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		if(pageNumber == null || pageSize == null || pageNumber<=0 || pageSize<=0){
			//Sans Pagination
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1)*pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("pvd.idt"));
			return cr.list();
		}
		return null;
	}
}
