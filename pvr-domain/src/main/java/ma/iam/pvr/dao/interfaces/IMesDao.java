package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Mes;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IMesDao extends IGenericDao<Mes, Long> {

	int consultTotalMesByCriteres(List<Long> idsMES);

	List<Object> consultMesIdtByCriteres(String idtsFournisseur,
			String idtsContrat, String idtsProjet, String idtsEtat,
			String idtsDr, String idtsSite, String idtsType);

	List<Mes> getMesByCriteresPagination(List<Long> idsMES, int pageNumber,
			int pageSize);

	List<Mes> getMesByCriteres(List<Long> idsMES);

	int getTotalMesByCriteres(List<Long> idsMES);

	List<Object> getMesIdtByCriteres(String idtsFournisseur,
			String idtsContrat, Long idtStatut, String idtsEtat,
			String idtsProjet, String idtsDr, String idtsSite, String idtsRt);

	List<Mes> getMesListByRtId(Long idtRt);

	List<Object> getSiteListByRtId(Long idtRt);

	Integer getTotalSitesByProjetAndFournisseur(Long idtProjet,
			Long idtFournisseur, Long idtTypeProjet);

	Integer getTotalSitesNonReceptByProjetAndFournisseur(Long idtProjet,
			Long idtFournisseur, Long idtTypeProjet);

	Integer getTotalSitesReceptByProjetAndFournisseur(Long idtProjet,
			Long idtFournisseur, Long idtTypeProjet);

	Integer getTotalSitesReceptDefByProjetAndFournisseur(Long idtProjet,
			Long idtFournisseur, Long idtTypeProjet);

	List<Mes> getMesListByRtIdAndSite(Long idtRt, Long idtSite);

	List<Mes> getMesListByPvrId(Long idtPvr);

	List<Object> getSitesNonReceptByProjetAndFournisseur(Long idtProjet,
			Long idtFournisseur);

	List<Mes> getListMesByIDT(List<Long> idsMES);

	List<Object> getListMesIdtWithPVRNull(Long idtFournisseur, Long idtContrat,
			Long idtProjet, Long idtStatut, Long idtEtat, String dateDebut,
			String dateFin, List<Long> idtMes, Long idtDr, Long idtSite);

	List<Object> getListMesIdtWithPVRNull(Long idtFournisseur, Long idtContrat,
			Long idtProjet, Long idtStatut, Long idtEtat, String dateDebut,
			String dateFin, List<Long> idtMes, Long idtDr, List<Long> idSites);

	List<Object> getListMesIdtWithPVRNull(Long idtFournisseur, Long idtContrat,
			Long idtProjet, Long idtStatut, Long idtEtat, String dateDebut,
			String dateFin, List<Long> idtMes, Long idtDr, List<Long> idSites,
			boolean excludeDraft);

	List<Object> getListMesIdtWithPVDNull(Long idtFournisseur, Long idtContrat,
			Long idtStatut, Long idtEtat, String dateDebut, String dateFin,
			List<Long> idtMes, Long idtDr, Long idtSite);

	Integer getTotalSitesControlesByProjetAndFournisseur(Long idtProjet,
			Long idtFournisseur, Long idtTypeProjet);

	List<Object> getListMesIdtWithRTAndNot(Long idtFournisseur,
			Long idtContrat, Long idtProjet, Long idtStatut, String dateDebut,
			String dateFin, List<Long> idtMes, Long idtDr, Long idtSite,
			Long idtTypeProjet, String idtsRt);

	List<Object> getListMesIdtWithRTAndNotMultiSites(Long idtFournisseur,
			Long idtContrat, Long idtProjet, Long idtStatut, String dateDebut,
			String dateFin, List<Long> idtMes, Long idtDr, List<Long> idSites,
			Long idtTypeProjet, String idtsRt);

	List<Object> getListMesIdtWithRTAndNotMultiSites(Long idtFournisseur,
			Long idtContrat, Long idtProjet, Long idtStatut, String dateDebut,
			String dateFin, List<Long> idtMes, Long idtDr, List<Long> idSites,
			Long idtTypeProjet, boolean excludeDraft, String idtsRt);

	List<Object> getListMesIdtForExploitation(String idtFournisseur,
			String idtContrat, String idtProjet, String idtDr, String idtSite,
			String numOperation);

	Mes getByNumeroOperation(String numeroOperation);

	List<Object> getListMesIdtForOptim(String idtsFournisseur,
			String idtsContrat, String idtsProjet, String idtsDr,
			String idtsSite, String numOperation);

	List<Object> getListMesIdtForDcc(String idtsFournisseur,
			String idtsContrat, String idtsProjet, String idtsDr,
			String idtsSite, String numOperation);

	List<Object> getListMesIdtForDeploi(Long idtFournisseur, Long idtContrat,
			Long idtProjet, Long idtDr, Long idtSite, Long idtEtat,
			String numOperation);

	List<Object> getListMesIdtForDeploiMultipleValues(String IdtsFournisseur,
			String IdtsContrat, String IdtsProjet, String IdtsDr,
			String IdtsSite, String IdtsEtat, List<String> numOperation);

	List<String> findAllNumOperations();

	List<Object> findAllNumOperationsForExploitation();

	List<Object> findAllNumOperationsForOptim();

	List<Object> findAllNumOperationsForDcc();

	List<Mes> getMesListByPvdId(Long idtPvd);

	List<Object> findAllNumOperationsForDeploi();

	List<Object> getListMesIdtForDccEnv(String idtsFournisseur,
			String idtsContrat, String idtsProjet, String idtsDr,
			String idtsSite, String numOperation);

	List<Object> findAllNumOperationsForDccEnv();

	List<Object> getSitesRadioEnvNonReceptByProjetAndFournisseur(
			Long idtProjet, Long idtFournisseur, Long idtTypeProjet);

	List<Object> getListMesIdtForQos(String idtsFournisseur,
			String idtsContrat, String idtsProjet, String idtsDr,
			String idtsSite, String numOperation);

	List<Object> findAllNumOperationsForQos();

	List<Object> getMesEnvIdtByCriteres(String idtsFournisseur,
			String idtsContrat, Long idtStatut, String idtsEtat,
			String idtsProjet, String idtsDr, String idtsSite, String idtsRt);

	Integer getTotalSitesMesByProjetAndFournisseur(Long idtProjet,
			Long idtFournisseur, Long idtTypeProjet);

	Integer getTotalSitesSoumisQualif(String idtsProjet, Long idtFournisseur,
			String idtsContrat, Long idtEntite, String idtsDr, String annees);

	Integer getTotalSitesExamine(String idtsProjet, Long idtFournisseur,
			String idtsContrat, Long idtEntite, String idtsDr, String annees);

	Integer getTotalSitesMes(List<Long> idtsProjet, Long idtFournisseur,
			List<Long> idtsContrat, List<Long> idtsDr, List<Long> annees);

	Integer getTotalSitesMes(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String idtsDr, String annees);

	Integer getTotalSitesRtDraft(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalSitesRt(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalMesOneRt(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalRt(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Number getTotalRtLeveeReserves(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalSitesMoreOneRt(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalPvr(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalSitesPvr(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalPvd(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	Integer getTotalSitesPvd(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String mode, String idtsDr, String annees);

	List<Object> getDrListByRtId(Long idtRt);

	Integer getTotalSitesIntegre(String idtsProjet, Long idtFournisseur,
			String idtsContrat, String idtsDr, String annees);

	List<Object[]> rtEtablisParMois(String idtsProjet, String idtsFournisseur,
			String idtsContrat, String idtsDr, String annees);

	List<Object[]> realisationSiteRadioParMois(String idtsProjet,
			String idtsFournisseur, String idtsContrat, String idtsDr,
			String annees);

	List<Object[]> dccTableauBord(String idtsProjet, String idtsFournisseur,
			String idtsContrat, String idtsDr, String annees);

	List<Object> findAllNumOperationsForFournisseur(Long idtFournisseur);

	List<Object> getListMesIdtForFournisseur(Long idtFournisseur,
			Long idtContrat, Long idtProjet, Long idtDr, Long idtSite,
			Long idtEtat, String numOperation);


	List<Object> getListMesIdtForFournisseurMultipleValues(Long idtFournisseur,
			String IdtsContrat, String IdtsProjet, String IdtsDr,
			String IdtsSite, String IdtsEtat, List<String> numOperation);
	
	List<Object> getListMesIdtMultiSitesForRemise(Long idtFournisseur, Long idtContrat, 
			Long idtProjet, String dateDebut, String dateFin, List<Long> idtMes, 
			Long idtDr, List<Long> idSites);
	
	List<Mes> getMesListByRemiseId(Long idtRemise);
	
	Integer getTotalMesRemiseControle(String idtsProjet, Long idtFournisseur, 
			String idtsContrat, String idtsDr, String annees);
	
	List<Mes> getMesListByIdsPagination(List<Long> idsMES, List<Long> selectedMesDto,
			Integer pageNumber, Integer pageSize);
	
	int getTotalMesListByIds(List<Long> idsMES, List<Long> selectedMesDto);
}
