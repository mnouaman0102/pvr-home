package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.PvrInfra;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IPvrInfraDao extends IGenericDao<PvrInfra, Long> {

	List<PvrInfra> getListPvrByContrat(Long idtContrat);
	List<PvrInfra> getAllPvr(Integer pageNumber, Integer pageSize);
	Integer countAllPvr();
	Integer countListPvrDraft();
	List<PvrInfra> getListPvrDraft(Integer pageNumber, Integer pageSize);
	Integer countListPvrValid();
	List<PvrInfra> getListPvrValid(Integer pageNumber, Integer pageSize);
}
