package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Pvr;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IPVRDao extends IGenericDao<Pvr, Long> {

	List<Pvr> getListPvrByContrat(Long idtContrat);
	List<Pvr> getAllPvr(Integer pageNumber, Integer pageSize);
	Integer countAllPvr();
	Integer countListPvrDraft();
	List<Pvr> getListPvrDraft(Integer pageNumber, Integer pageSize);
	Integer countListPvrValid();
	List<Pvr> getListPvrValid(Integer pageNumber, Integer pageSize);
}
