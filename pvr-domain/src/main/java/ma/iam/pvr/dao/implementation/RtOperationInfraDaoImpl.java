package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IRtOperationInfraDao;
import ma.iam.pvr.domain.RtOperationInfra;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class RtOperationInfraDaoImpl extends DefaultGenericDao<RtOperationInfra, Long> implements IRtOperationInfraDao {


	public RtOperationInfra getRtOperationInfraByOper(Long idtOper) {
		Criteria cr = this.getSession().createCriteria(RtOperationInfra.class,"oper_rt");
		cr.createAlias("oper_rt.operationInfra", "oper");
		cr.createAlias("oper_rt.rtInfra", "rtInfra");
		cr.add(Restrictions.eq("oper.idt", idtOper));
		cr.addOrder(Order.desc("rtInfra.date"));
		cr.addOrder(Order.desc("rtInfra.idt"));
		cr.setMaxResults(1);
		return (RtOperationInfra) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<RtOperationInfra> getListRtOperationInfraByOper(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(RtOperationInfra.class,"oper_rt");
		cr.createAlias("oper_rt.operationInfra", "operationInfra");
		cr.createAlias("oper_rt.rtInfra", "rtInfra");
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.addOrder(Order.desc("rtInfra.date"));
		cr.addOrder(Order.desc("rtInfra.idt"));
		return  cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RtOperationInfra> getListRtOperationInfraByRt(Long idtRt) {
		Criteria cr = this.getSession().createCriteria(RtOperationInfra.class,"oper_rt");
		cr.createAlias("oper_rt.rtInfra", "rtInfra");
		cr.add(Restrictions.eq("rtInfra.idt", idtRt));
		return  cr.list();
	}
	
	public RtOperationInfra getLastRtOperationInfraValidByOper(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(RtOperationInfra.class,"oper_rt");
		cr.createAlias("oper_rt.operationInfra", "operationInfra");
		cr.createAlias("oper_rt.rtInfra", "rtInfra");
		
		cr.createAlias("rtInfra.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		
		cr.addOrder(Order.desc("rtInfra.date"));
		cr.addOrder(Order.desc("rtInfra.idt"));
		cr.setMaxResults(1);
		return (RtOperationInfra) cr.uniqueResult();
	}
	
	public RtOperationInfra getLastRtOperationInfraValidByOper(Long idtMes, String workFlow) {
		Criteria cr = this.getSession().createCriteria(RtOperationInfra.class,"oper_rt");
		cr.createAlias("oper_rt.operationInfra", "operationInfra");
		cr.createAlias("oper_rt.rtInfra", "rtInfra");
		
		cr.createAlias("rtInfra.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.add(Restrictions.eq("rtInfra.workflow", workFlow));
		cr.addOrder(Order.desc("rtInfra.date"));
		cr.addOrder(Order.desc("rtInfra.idt"));
		cr.setMaxResults(1);
		return (RtOperationInfra) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<RtOperationInfra> getListRtOperationInfraValidByOper(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(RtOperationInfra.class,"oper_rt");
		cr.createAlias("oper_rt.operationInfra", "operationInfra");
		cr.createAlias("oper_rt.rtInfra", "rtInfra");
		
		cr.createAlias("rtInfra.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.addOrder(Order.desc("rtInfra.date"));
		cr.addOrder(Order.desc("rtInfra.idt"));
		return  cr.list();
	}

}

