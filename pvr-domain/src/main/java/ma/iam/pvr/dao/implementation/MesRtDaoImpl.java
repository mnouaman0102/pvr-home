package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IMesRtDao;
import ma.iam.pvr.domain.MesRt;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class MesRtDaoImpl extends DefaultGenericDao<MesRt, Long> implements IMesRtDao {


	public MesRt getMesRtByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesRt.class,"mes_rt");
		cr.createAlias("mes_rt.mes", "mes");
		cr.createAlias("mes_rt.rt", "rt");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("rt.date"));
		cr.addOrder(Order.desc("rt.idt"));
		cr.setMaxResults(1);
		return (MesRt) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<MesRt> getListMesRtByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesRt.class,"mes_rt");
		cr.createAlias("mes_rt.mes", "mes");
		cr.createAlias("mes_rt.rt", "rt");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("rt.date"));
		cr.addOrder(Order.desc("rt.idt"));
		return  cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<MesRt> getListMesRtByRt(Long idtRt) {
		Criteria cr = this.getSession().createCriteria(MesRt.class,"mes_rt");
		cr.createAlias("mes_rt.rt", "rt");
		cr.add(Restrictions.eq("rt.idt", idtRt));
		return  cr.list();
	}
	
	public MesRt getLastMesRtValidByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesRt.class,"mes_rt");
		cr.createAlias("mes_rt.mes", "mes");
		cr.createAlias("mes_rt.rt", "rt");
		
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		
		cr.addOrder(Order.desc("rt.date"));
		cr.addOrder(Order.desc("rt.idt"));
		cr.setMaxResults(1);
		return (MesRt) cr.uniqueResult();
	}
	
	public MesRt getLastMesRtRadioEnvValidByMes(Long idtMes, String workFlow) {
		Criteria cr = this.getSession().createCriteria(MesRt.class,"mes_rt");
		cr.createAlias("mes_rt.mes", "mes");
		cr.createAlias("mes_rt.rt", "rt");
		
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("rt.workflow", workFlow));
		cr.addOrder(Order.desc("rt.date"));
		cr.addOrder(Order.desc("rt.idt"));
		cr.setMaxResults(1);
		return (MesRt) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<MesRt> getListMesRtValidByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(MesRt.class,"mes_rt");
		cr.createAlias("mes_rt.mes", "mes");
		cr.createAlias("mes_rt.rt", "rt");
		
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("rt.date"));
		cr.addOrder(Order.desc("rt.idt"));
		return  cr.list();
	}

}

