package ma.iam.pvr.dao.implementation;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import ma.iam.pvr.dao.interfaces.IEmailRtDao;
import ma.iam.pvr.domain.EmailRt;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

@Repository
public class EmailRtDaoImpl extends DefaultGenericDao<EmailRt, Long> implements IEmailRtDao {

}
