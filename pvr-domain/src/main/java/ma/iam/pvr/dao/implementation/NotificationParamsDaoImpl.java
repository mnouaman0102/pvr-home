package ma.iam.pvr.dao.implementation;

import ma.iam.pvr.dao.interfaces.INotificationParamsDao;
import ma.iam.pvr.domain.NotificationParams;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class NotificationParamsDaoImpl extends DefaultGenericDao<NotificationParams, Long> implements INotificationParamsDao {

	
	public NotificationParams getNotificationParamsByDrAndEntiteAndFournisseur(Long idtDr, Long idtEntite, Long idtFournisseur) {
		
		Criteria cr = this.getSession().createCriteria(NotificationParams.class, "notificationParams");
		cr.createAlias("notificationParams.dr", "dr");
		cr.createAlias("notificationParams.entite", "entite");
		cr.createAlias("notificationParams.technologie", "fournisseur");
		if(idtDr != null)
			cr.add(Restrictions.eq("dr.idt", idtDr));
		cr.add(Restrictions.eq("entite.idt", idtEntite));
		cr.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		cr.setMaxResults(1);
		return (NotificationParams) cr.uniqueResult();
	}

}

