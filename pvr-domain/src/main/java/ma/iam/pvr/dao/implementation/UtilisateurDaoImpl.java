package ma.iam.pvr.dao.implementation;

import java.util.List;

import ma.iam.pvr.dao.interfaces.IUtilisateurDao;
import ma.iam.pvr.domain.Profil;
import ma.iam.pvr.domain.Utilisateur;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class UtilisateurDaoImpl extends DefaultGenericDao<Utilisateur, Long> implements IUtilisateurDao {


	public Utilisateur getUserByLogin(String login) {
		Criteria cr = this.getSession().createCriteria(Utilisateur.class);
		cr.add(Restrictions.eq("login", login).ignoreCase());
		return (Utilisateur) cr.uniqueResult();
	}

	
	public boolean isAdmin(List<Profil> profils) {
		for(Profil profil:profils){
			if(profil.getLabel().equals("ADMINISTRATEUR")){
				return true;
			}
		}
		return false;
	}


	public Utilisateur getUserByID(Long idt) {
		Criteria cr = this.getSession().createCriteria(Utilisateur.class);
		cr.add(Restrictions.eq("idt", idt).ignoreCase());
		return (Utilisateur) cr.uniqueResult();
	}
	
}

