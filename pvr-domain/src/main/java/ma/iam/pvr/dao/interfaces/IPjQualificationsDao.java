package ma.iam.pvr.dao.interfaces;


import java.util.List;

import ma.iam.pvr.domain.PjQualifications;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IPjQualificationsDao extends IGenericDao<PjQualifications, Long> {

	List<PjQualifications> getAllPjQualifByQualifIdt(Long idtQualif);
}
