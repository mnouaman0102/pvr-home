package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IProjetDao;
import ma.iam.pvr.domain.Projet;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class ProjetDaoImpl extends DefaultGenericDao<Projet, Long> implements IProjetDao {

	@SuppressWarnings("unchecked")
	public List<Projet> getProjectsRadio(){
		Criteria cr = this.getSession().createCriteria(Projet.class, "projet");
		cr.add(Restrictions.ne("projet.workflow", "env"));
		cr.addOrder(Order.asc("projet.idt"));
		return cr.list();
	}
	@SuppressWarnings("unchecked")
	public List<Projet> getProjectsEnv(){
		Criteria cr = this.getSession().createCriteria(Projet.class, "projet");
		cr.add(Restrictions.ne("projet.workflow", "radio"));
		cr.addOrder(Order.asc("projet.idt"));
		return cr.list();
	}
	
	public Projet getProjetByLabel(String label) {
		Criteria cr = this.getSession().createCriteria(Projet.class, "projet");
		
		cr.add(Restrictions.eq("projet.label", label));
		cr.setMaxResults(1);
		return (Projet) cr.uniqueResult();
	}
}
