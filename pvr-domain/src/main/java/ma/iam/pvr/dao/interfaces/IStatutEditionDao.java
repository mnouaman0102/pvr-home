package ma.iam.pvr.dao.interfaces;


import ma.iam.pvr.domain.StatutEdition;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IStatutEditionDao extends IGenericDao<StatutEdition, Long> {
	
	
}
