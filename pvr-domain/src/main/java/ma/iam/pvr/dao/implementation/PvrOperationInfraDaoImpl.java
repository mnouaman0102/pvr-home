package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IPvrOperationInfraDao;
import ma.iam.pvr.domain.PvrOperationInfra;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class PvrOperationInfraDaoImpl extends DefaultGenericDao<PvrOperationInfra, Long> implements IPvrOperationInfraDao {

	public PvrOperationInfra getMesPvrByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PvrOperationInfra.class,"mes_pvr");
		cr.createAlias("mes_pvr.operationInfra", "operationInfra");
		cr.createAlias("mes_pvr.pvrInfra", "pvr");
		cr.add(Restrictions.eq("operationInfra.idt", idtMes));
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		cr.setMaxResults(1);
		return (PvrOperationInfra) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<PvrOperationInfra> getListMesPvrByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PvrOperationInfra.class,"mes_pvr");
		cr.createAlias("mes_pvr.operationInfra", "mes");
		cr.createAlias("mes_pvr.pvrInfra", "pvr");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		return  cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PvrOperationInfra> getListMesPvrByPvr(Long idtPvr) {
		Criteria cr = this.getSession().createCriteria(PvrOperationInfra.class,"mes_pvr");
		cr.createAlias("mes_pvr.pvrInfra", "pvr");
		cr.add(Restrictions.eq("pvr.idt", idtPvr));
		return  cr.list();
	}
	
	public PvrOperationInfra getLastMesPvrValidByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PvrOperationInfra.class,"mes_pvr");
		cr.createAlias("mes_pvr.operationInfra", "mes");
		cr.createAlias("mes_pvr.pvrInfra", "pvr");
		
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		cr.setMaxResults(1);
		return (PvrOperationInfra) cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<PvrOperationInfra> getListMesPvrValidByMes(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(PvrOperationInfra.class,"mes_pvr");
		cr.createAlias("mes_pvr.operationInfra", "mes");
		cr.createAlias("mes_pvr.pvrInfra", "pvr");
		
		cr.createAlias("pvr.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L));
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.addOrder(Order.desc("pvr.date"));
		cr.addOrder(Order.desc("pvr.idt"));
		return  cr.list();
	}

}

