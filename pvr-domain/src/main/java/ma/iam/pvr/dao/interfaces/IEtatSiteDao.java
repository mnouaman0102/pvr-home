package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.EtatSite;
//import ma.iam.socle.service.GenericDao;

public interface IEtatSiteDao extends IGenericDao<EtatSite, Long> {

	EtatSite getByCode(String code);
	
	List<EtatSite> getListEtatSiteCandidat();
	
	List<EtatSite> getListEtatSiteCandidatPhases();

	List<EtatSite> getListEtatSiteExploi();
}
