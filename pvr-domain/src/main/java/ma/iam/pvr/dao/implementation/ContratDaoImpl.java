package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IContratDao;
import ma.iam.pvr.domain.Contrat;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class ContratDaoImpl extends DefaultGenericDao<Contrat, Long> implements IContratDao {


	@SuppressWarnings("unchecked")
	public List<Contrat> getAllContactsByFounisseur(Long idtFournisseur) {
		
		Criteria cr = this.getSession().createCriteria(Contrat.class, "contrat");
		cr.createAlias("contrat.fournisseur", "fournisseur");
		
		if(idtFournisseur != 0){
			cr.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		}
		
		return (List<Contrat>) cr.list();
	}	
	
	@SuppressWarnings("unchecked")
	public List<Contrat> getAllContactsByFounisseurs(List<Long> idtsFournisseur) {
			Criteria cr = this.getSession().createCriteria(Contrat.class, "contrat");
			cr.createAlias("contrat.fournisseur", "fournisseur");
			
			if(idtsFournisseur != null){
				cr.add(Restrictions.in("fournisseur.idt", idtsFournisseur));
			}
			
			return (List<Contrat>) cr.list();
	}
	
	public Contrat getContratByLabelAndFournisseur(String label, Long idtFournisseur) {
		Criteria cr = this.getSession().createCriteria(Contrat.class, "contrat");
		cr.createAlias("contrat.fournisseur", "fournisseur");
		cr.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		cr.add(Restrictions.eq("contrat.label", label));
		cr.setMaxResults(1);
		return (Contrat) cr.uniqueResult();
	}
	
}
