package ma.iam.pvr.dao.implementation;


import java.util.List;

import ma.iam.pvr.dao.interfaces.IQualificationDao;
import ma.iam.pvr.domain.Qualification;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class QualificationDaoImpl extends DefaultGenericDao<Qualification, Long> implements IQualificationDao {

	
	public Qualification getQualificationExploitationByMesId(Long idtMes) {
		
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		cr.createAlias("qualification.statutOperation", "statutOperation");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 2L));
		cr.add(Restrictions.eq("statutOperation.idt", 1L));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}
	
	public Qualification getQualificationOptimByMesId(Long idtMes) {
		
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		cr.createAlias("qualification.statutOperation", "statutOperation");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 3L));
		cr.add(Restrictions.eq("statutOperation.idt", 1L));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<Qualification> getAllQualifsForDeploiByMesId(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.add(Restrictions.eq("mes.idt", idtMes));

		cr.setMaxResults(4);
		
		return cr.list();
	}

	/**
	 * for qualification dcc Radio
	 */
	public Qualification getQualificationDccByMesId(Long idtMes) {
		
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		cr.createAlias("qualification.statutOperation", "statutOperation");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 4L));
		cr.add(Restrictions.eq("statutOperation.idt", 1L));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}


	public Qualification getQualifByMesIdAndEntite(Long idtMes, Long idtEntite) {
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", idtEntite));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}
	
	public Qualification getQualifDccByMesId(Long idtMes) {
		
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		cr.createAlias("qualification.statutOperation", "statutOperation");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 4L));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}
	
	public Qualification getQualifDccEnvByMesId(Long idtMes) {
		
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		cr.createAlias("qualification.statutOperation", "statutOperation");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 5L));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}
	
	/**
	 * for qualification dcc env
	 */
	public Qualification getQualificationDccEnvByMesId(Long idtMes) {
		
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		cr.createAlias("qualification.statutOperation", "statutOperation");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 5L));
		cr.add(Restrictions.eq("statutOperation.idt", 1L));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}
	
	public Qualification getQualificationQosByMesId(Long idtMes) {
		
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.createAlias("qualification.entite", "entite");
		cr.createAlias("qualification.statutOperation", "statutOperation");
		
		cr.add(Restrictions.eq("mes.idt", idtMes));
		cr.add(Restrictions.eq("entite.idt", 6L));
		cr.add(Restrictions.eq("statutOperation.idt", 1L));
		cr.setMaxResults(1);
		
		return (Qualification) cr.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<Qualification> getQualifsByMesIdt(Long idtMes) {
		Criteria cr = this.getSession().createCriteria(Qualification.class, "qualification");
		cr.createAlias("qualification.mes", "mes");
		cr.add(Restrictions.eq("mes.idt", idtMes));
		return cr.list();
	}
	
}
