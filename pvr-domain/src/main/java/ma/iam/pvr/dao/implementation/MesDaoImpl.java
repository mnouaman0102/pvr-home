package ma.iam.pvr.dao.implementation;


import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.mysql.cj.util.StringUtils;
import ma.iam.pvr.dao.interfaces.IMesDao;
import ma.iam.pvr.domain.Mes;
import ma.iam.pvr.domain.MesRt;
import ma.iam.pvr.domain.RemiseMes;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.domain.utils.Utils;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


/**
 * 
 * @author K.ELBAGUARI
 *
 */
@Repository
public class MesDaoImpl extends DefaultGenericDao<Mes, Long> implements IMesDao {


	public Mes getByNumeroOperation(String numeroOperation) {
		Criteria cr = this.getSession().createCriteria(Mes.class);
		cr.add(Restrictions.eq("numOperation", numeroOperation));
		return (Mes) cr.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Mes> getMesByCriteresPagination(List<Long> idsMES,
			int pageNumber, int pageSize) {
			Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
			cr_mes.add(Restrictions.in("mes.idt", idsMES));

			//Pagination
			if(pageNumber>0 && pageSize>0){
				cr_mes.setFirstResult((pageNumber-1)*pageSize);
				cr_mes.setMaxResults(pageSize);
			}else{
				cr_mes.setFirstResult(0);
			}
			cr_mes.addOrder(Order.desc("mes.idt"));

			return cr_mes.list();
	}

	@SuppressWarnings("unchecked")
	public List<Mes> getMesByCriteres(List<Long> idsMES) {
			Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
			cr_mes.add(Restrictions.in("mes.idt", idsMES));
			cr_mes.addOrder(Order.desc("mes.idt"));
			return cr_mes.list();
	}

	public int getTotalMesByCriteres(List<Long> idsMES) {
		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
		cr_mes.add(Restrictions.in("mes.idt", idsMES));

		cr_mes.setProjection(Projections.rowCount());
		return (Integer)cr_mes.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getMesIdtByCriteres(String idtsFournisseur, String idtsContrat,
			Long idtStatut, String idtsEtat, String idtsProjet, String idtsDr, String idtsSite, String idtsRt) {
		Session session = this.getSession();
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE ";
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " LEFT JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT ";
		}
		sql += " WHERE 1=1 AND m.IDT_TYPE_PROJET = 1";
		if(!StringUtil.isNullOrEmpty(idtsContrat) && !Utils.containInjectChar(idtsContrat)) {
			sql += " AND c.IDT in (" + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsFournisseur) && !Utils.containInjectChar(idtsFournisseur)) {
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet) && !Utils.containInjectChar(idtsProjet)) {
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}		
		if(!StringUtil.isNullOrEmpty(idtsDr) && !Utils.containInjectChar(idtsDr)) {
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}		
		if(!StringUtil.isNullOrEmpty(idtsSite) && !Utils.containInjectChar(idtsSite)) {
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}		
		if(!StringUtil.isNullOrEmpty(idtsEtat) && !Utils.containInjectChar(idtsEtat)) {
			sql += " AND m.IDT_ETAT in (" + idtsEtat + ")";
		}
		if(idtStatut != 0){
			if(idtStatut == 2){
				sql += " AND (mr.IDT_STATUT is NULL OR m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = 2))";
			}else{
				sql += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = 1)";
			}
		}
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " AND mrt.IDT_RT in (" + idtsRt + ")";
		}
		return session.createSQLQuery(sql).list();

	}

	@SuppressWarnings("unchecked")
	public List<Mes> getMesListByRtId(Long idtRt) {
		Criteria cr_mesRt = this.getSession().createCriteria(MesRt.class, "mesRt");
		cr_mesRt.createAlias("mesRt.rt", "rt");
		cr_mesRt.createAlias("mesRt.mes", "mes1");
		cr_mesRt.add(Restrictions.eq("rt.idt", idtRt));

		ProjectionList properties = Projections.projectionList();
		properties.add(Projections.property("mes1.idt"));
		cr_mesRt.setProjection(properties);

		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes2");
		if(cr_mesRt.list() != null && cr_mesRt.list().size() > 0){
			cr_mes.add(Restrictions.in("mes2.idt", (List<Long>) cr_mesRt.list()));
			return cr_mes.list();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getSiteListByRtId(Long idtRt) {
		Session session = this.getSession();
		String sql ="SELECT site.IDT FROM PVR_SITE site where site.IDT IN (SELECT  DISTINCT(mes.IDT_SITE) "+
				"FROM  PVR_RT rt LEFT JOIN PVR_MES_RT mesRT  ON rt.IDT=mesRT.IDT_RT LEFT JOIN PVR_MES mes ON"+
				" mesRT.IDT_MES=mes.IDT  WHERE rt.IDT="+idtRt+") ";
		return session.createSQLQuery(sql).list();
	}

	public Integer getTotalSitesByProjetAndFournisseur(Long idtProjet, Long idtFournisseur, Long idtTypeProjet) {


		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
		cr_mes.createAlias("mes.contrat", "contrat");
		cr_mes.createAlias("mes.projet", "projet");
		cr_mes.createAlias("contrat.fournisseur", "fournisseur");
		cr_mes.createAlias("mes.typeProjet", "typeProjet");

		if(idtFournisseur != 0){
			cr_mes.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		}
		if(idtProjet != 0){
			cr_mes.add(Restrictions.eq("projet.idt", idtProjet));
		}
		cr_mes.add(Restrictions.ne("projet.workflow", "env"));

		if(idtTypeProjet != 0){
			cr_mes.add(Restrictions.eq("typeProjet.idt", idtTypeProjet));
		}

		cr_mes.setProjection(Projections.rowCount());
		return (Integer)cr_mes.uniqueResult();

	}

	public Integer getTotalSitesNonReceptByProjetAndFournisseur(Long idtProjet, Long idtFournisseur, Long idtTypeProjet) {

		Session session = this.getSession(); 
		String sql = "SELECT COUNT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " LEFT JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND (m.IDT_PVR is NULL "
				+ " OR (m.IDT_PVR is not NULL AND pvr.IDT_STATUT_EDITION = 1)) AND p.WORKFLOW != 'env' ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtTypeProjet != 0){
			sql += " AND m.IDT_TYPE_PROJET = " + idtTypeProjet;
		}

		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}

	public Integer getTotalSitesReceptByProjetAndFournisseur(Long idtProjet, Long idtFournisseur, Long idtTypeProjet) {

		Session session = this.getSession(); 
		String sql = "SELECT COUNT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " LEFT JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ " WHERE m.IDT_PVR is not null AND rt.IDT_STATUT_EDITION = 2 AND pvr.IDT_STATUT_EDITION = 2 AND p.WORKFLOW != 'env' ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtTypeProjet != 0){
			sql += " AND m.IDT_TYPE_PROJET = " + idtTypeProjet;
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}

	public Integer getTotalSitesReceptDefByProjetAndFournisseur(Long idtProjet, Long idtFournisseur, Long idtTypeProjet) {

		Session session = this.getSession(); 
		String sql = "SELECT COUNT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " LEFT JOIN PVR_PVD pvd ON pvd.IDT = m.IDT_PVD "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_PVD is not NULL AND pvd.IDT_STATUT_EDITION = 2 AND p.WORKFLOW != 'env' ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtTypeProjet != 0){
			sql += " AND m.IDT_TYPE_PROJET = " + idtTypeProjet;
		}

		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();

	}



	@SuppressWarnings("unchecked")
	public List<Mes> getMesListByPvrId(Long idtPvr) {
		List<Long> mesIdt = getAllMesByPvrId(idtPvr);
		if(mesIdt != null && mesIdt.size() > 0){
			Criteria cr = this.getSession().createCriteria(Mes.class, "mes");
			cr.add(Restrictions.in("mes.idt", mesIdt));
			return cr.list();
		}else{
			Criteria cr = this.getSession().createCriteria(Mes.class, "mes");
			cr.createAlias("mes.pvr", "pvr");
			cr.add(Restrictions.eq("pvr.idt", idtPvr));
			return cr.list();
		}
	}

	@SuppressWarnings("unchecked")
	private List<Long> getAllMesByPvrId(Long idtPvr) {
		Session session = this.getSession(); 
		String sql = "SELECT m.idt from PVR_MES m "
				+ "JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ "WHERE pvr.IDT =" + idtPvr
				+ " UNION "
				+ "SELECT m.idt from PVR_MES m " 
				+ "JOIN PVR_MES_PVR mesPvr ON mesPvr.IDT_MES = m.IDT "
				+ "WHERE mesPvr.IDT_PVR =" + idtPvr;
		List<Object> objects = session.createSQLQuery(sql).list();
		final List<Long> list = new LinkedList<Long>();
		for(final Object o : objects) {
		    list.add(((BigInteger)o).longValue());
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Mes> getMesListByRtIdAndSite(Long idtRt, Long idtSite) {
		Criteria cr_mesRt = this.getSession().createCriteria(MesRt.class, "mesRt");
		cr_mesRt.createAlias("mesRt.rt", "rt");
		cr_mesRt.createAlias("mesRt.mes", "mes1");
		cr_mesRt.add(Restrictions.eq("rt.idt", idtRt));

		ProjectionList properties = Projections.projectionList();
		properties.add(Projections.property("mes1.idt"));
		cr_mesRt.setProjection(properties);

		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes2");
		cr_mes.createAlias("mes2.site", "site");
		cr_mes.add(Restrictions.eq("site.idt", idtSite));
		cr_mes.add(Restrictions.in("mes2.idt", (List<Long>) cr_mesRt.list()));

		return cr_mes.list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getSitesNonReceptByProjetAndFournisseur(Long idtProjet, Long idtFournisseur) {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " LEFT JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND (m.IDT_PVR is NULL OR (m.IDT_PVR is not NULL AND pvr.IDT_STATUT_EDITION = 1)) AND p.WORKFLOW != 'env' ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		return session.createSQLQuery(sql).list();

	}

	@SuppressWarnings("unchecked")
	public List<Mes> getListMesByIDT(List<Long> idsMES) {
			Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
			cr_mes.add(Restrictions.in("mes.idt", idsMES));
			return cr_mes.list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtStatut, Long idtEtat, String dateDebut, String dateFin, List<Long> idtMes, Long idtDr, Long idtSite) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " LEFT JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT IN (3,4) "
				+ " AND m.IDT_PVD is NULL AND (m.IDT_PVR is NULL OR (m.IDT_PVR is not NULL AND pvr.IDT_STATUT_EDITION = 1)) ";
				;

		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtEtat != 0){
			sql += " AND m.IDT_ETAT = " + idtEtat;
		}
		if(idtStatut != 0){
			sql += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = " + idtStatut + ")";
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (m.DATE_MES >= '" + date_From + "' AND m.DATE_MES <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}
		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sql += " AND m.IDT_SITE = " + idtSite;
		}

		return session.createSQLQuery(sql).list();
	}
	public List<Object> getListMesIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtStatut, Long idtEtat, String dateDebut, String dateFin, 
			List<Long> idtMes, Long idtDr, List<Long> idSites){
		return getListMesIdtWithPVRNull(idtFournisseur, idtContrat, idtProjet, idtStatut, idtEtat, dateDebut, dateFin
				, idtMes, idtDr, idSites, false);
	}
	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtWithPVRNull(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtStatut, Long idtEtat, String dateDebut, String dateFin, 
			List<Long> idtMes, Long idtDr, List<Long> idSites, boolean excludeDraft) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " LEFT JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT IN (3,4) "
				+ " AND m.IDT_PVD is NULL AND (m.IDT_PVR is NULL OR (m.IDT_PVR is not NULL AND pvr.IDT_STATUT_EDITION = 1)) ";
				;

		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtEtat != 0){
			sql += " AND m.IDT_ETAT = " + idtEtat;
		}
		if(idtStatut != 0){
			sql += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = " + idtStatut + ")";
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (m.DATE_MES >= '" + date_From + "' AND m.DATE_MES <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}


		if(excludeDraft){
			sql += " AND COALESCE(m.IDT_PVR,0) not in ( select pvr.IDT from PVR_PVR pvr " +
					"inner join PVR_STATUT_EDITION s on s.IDT = pvr.IDT_STATUT_EDITION " + 
					"and s.LABEL = 'Draft')";
		}

		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}



		if(idSites != null && idSites.size() > 0){
			StringBuilder sitesIds =new StringBuilder();
			for(Long idSite : idSites){
				if(sitesIds.length() > 0){
					sitesIds.append(",");
				}
				sitesIds.append(idSite);
			}
			sql += " AND m.IDT_SITE in (" + sitesIds.toString() + ")";
		}

		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtWithPVDNull(Long idtFournisseur, Long idtContrat, Long idtStatut, Long idtEtat, String dateDebut, String dateFin, List<Long> idtMes, Long idtDr, Long idtSite) {
		Session session = this.getSession(); 
		String sqlWhere = 	"";	
		if(idtContrat != 0){
			sqlWhere += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sqlWhere += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtEtat != 0){
			sqlWhere += " AND m.IDT_ETAT = " + idtEtat;
		}
		if(idtStatut != 0){
			sqlWhere += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = " + idtStatut + ")";
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_Debut = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_Fin = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sqlWhere += " AND (pvr.DATE >= '" + date_Debut + "' AND pvr.DATE <= '" + date_Fin +"')";
			} catch (ParseException e) {
				e.getMessage();
			}			
		}

		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sqlWhere += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sqlWhere += " AND si.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sqlWhere += " AND m.IDT_SITE = " + idtSite;
		}
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ " LEFT JOIN PVR_PVD pvd ON pvd.IDT = m.IDT_PVD "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT = 4 "
				+ " AND (m.IDT_PVD is NULL OR (m.IDT_PVD is not NULL AND pvd.IDT_STATUT_EDITION = 1)) " ;
				sql += sqlWhere;
				sql +=
				" UNION "
				+ "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_MES_PVR mPvr ON mPvr.IDT_MES = m.IDT "
				+ " JOIN PVR_PVR pvr ON pvr.IDT = mPvr.IDT_PVR "
				+ " LEFT JOIN PVR_PVD pvd ON pvd.IDT = m.IDT_PVD "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND m.IDT_ETAT = 4 "
				+ " AND (m.IDT_PVD is NULL OR (m.IDT_PVD is not NULL AND pvd.IDT_STATUT_EDITION = 1)) " ;
				sql += sqlWhere;

		return session.createSQLQuery(sql).list();
	}

	public Integer getTotalSitesControlesByProjetAndFournisseur(Long idtProjet, Long idtFournisseur, Long idtTypeProjet) {

		Session session = this.getSession(); 
		String sql = "SELECT COUNT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " WHERE 1 = 1 AND rt.IDT_STATUT_EDITION = 2 AND p.WORKFLOW != 'env' ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtTypeProjet != 0){
			sql += " AND m.IDT_TYPE_PROJET = " + idtTypeProjet;
		}

		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtWithRTAndNot(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtStatut, String dateDebut, String dateFin, List<Long> idtMes, 
			Long idtDr, Long idtSite, Long idtTypeProjet, String idtsRt) {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ " LEFT JOIN PVR_REMISE_MES rm ON rm.IDT_MES = m.IDT ";
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " LEFT JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT ";
		}
		sql += " WHERE m.IDT_ETAT in (2, 3, 4) ";
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtStatut != 0){
			sql += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = " + idtStatut + " )";
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (m.DATE_MES >= '" + date_From + "' AND m.DATE_MES <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}

		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sql += " AND m.IDT_SITE = " + idtSite;
		}

		if(idtTypeProjet != 0){
			sql += " AND m.IDT_TYPE_PROJET = " + idtTypeProjet;
		}
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " AND mrt.IDT_RT in (" + idtsRt + ")";
		}

		return session.createSQLQuery(sql).list();

	}


	public List<Object> getListMesIdtWithRTAndNotMultiSites(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtStatut, String dateDebut, String dateFin, List<Long> idtMes, 
			Long idtDr, List<Long> idSites, Long idtTypeProjet, String idtsRt){
		return getListMesIdtWithRTAndNotMultiSites(idtFournisseur, idtContrat, idtProjet, idtStatut, dateDebut, dateFin, idtMes, idtDr, idSites, idtTypeProjet, false, idtsRt);
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtWithRTAndNotMultiSites(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtStatut, String dateDebut, String dateFin, List<Long> idtMes, 
			Long idtDr, List<Long> idSites, Long idtTypeProjet, boolean excludeDraft, String idtsRt) {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
				+ "LEFT JOIN PVR_REMISE_MES rm ON rm.IDT_MES = m.IDT ";
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " LEFT JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT ";
		}
		sql += " WHERE m.IDT_ETAT in (2, 3, 4) ";
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}
		if(idtStatut != 0){
			sql += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = " + idtStatut + " )";
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (m.DATE_MES >= '" + date_From + "' AND m.DATE_MES <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}

		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(excludeDraft){
			sql += " AND m.IDT not in ( select mr.IDT_MES from PVR_MES_RT mr " + 
					"inner join PVR_RT r on mr.IDT_RT = r.IDT inner join PVR_STATUT_EDITION s on s.IDT = r.IDT_STATUT_EDITION " +
					"and s.LABEL = 'Draft')";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}

		if(idSites != null && idSites.size() > 0){
			StringBuilder sitesIds =new StringBuilder();
			for(Long idSite : idSites){
				if(sitesIds.length() > 0){
					sitesIds.append(",");
				}
				sitesIds.append(idSite);
			}
			sql += " AND m.IDT_SITE in (" + sitesIds.toString() + ")";
		}

		if(idtTypeProjet != 0){
			sql += " AND m.IDT_TYPE_PROJET = " + idtTypeProjet;
		}
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " AND mrt.IDT_RT in (" + idtsRt + ")";
		}

		return session.createSQLQuery(sql).list();

	}


	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForExploitation(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsDr, String idtsSite, String numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 2 ";

		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + numOperation + ")";
		}
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForOptim(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsDr, String idtsSite, String numOperation) {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 3 ";

		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + numOperation + ")";
		}
		return session.createSQLQuery(sql).list();
	}
	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDcc(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsDr, String idtsSite, String numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 4 ";
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + numOperation + ")";
		}
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDeploi(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtDr, Long idtSite, Long idtEtat, String numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE 1 = 1 " ;
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}

		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}

		if(idtDr != 0){
			sql += " AND s.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sql += " AND m.IDT_SITE = " + idtSite;
		}

		if(idtEtat != 0){
			sql += " AND m.IDT_ETAT = " + idtEtat;
		}

		if(numOperation != null && !numOperation.equals("")){
			String num = "'%" + numOperation + "%'";
			sql += " AND m.NUMOPERATION LIKE " + num;
		}
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDeploiMultipleValues(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsDr, String idtsSite, String idtsEtat, List<String> numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE 1 = 1 " ;
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsEtat)){
			sql += " AND m.IDT_ETAT in (" + idtsEtat + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + StringUtil.joinListString(numOperation, ",") + ")";
		}
		return session.createSQLQuery(sql).list();
	}

	/**
	 * Consultation
	 */
	@SuppressWarnings("unchecked")
	public List<Object> consultMesIdtByCriteres(String idtsFournisseur, String idtsContrat, 
			String idtsProjet, String idtsEtat, 
			String idtsDr, String idtsSite, String idtsType) {
		Session session = this.getSession();
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE 1=1 ";
		if(idtsContrat != null && !idtsContrat.isEmpty()){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(idtsFournisseur != null && !idtsFournisseur.isEmpty()){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(idtsEtat != null && !idtsEtat.isEmpty()){
			sql += " AND m.IDT_ETAT in (" + idtsEtat + ")";
		}
		if(idtsProjet != null && !idtsProjet.isEmpty()){
			sql += " AND m.IDT_PROJET in (" + idtsProjet+ ")";
		}
		if(idtsDr != null && !idtsDr.isEmpty()){
			sql += " AND s.IDT_DR in (" + idtsDr+ ")";
		}
		if(idtsSite != null && !idtsSite.isEmpty()){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(idtsType != null && !idtsType.isEmpty()){
			sql += " AND m.IDT_TYPE_PROJET in (" + idtsType + ")";
		}
		return session.createSQLQuery(sql).list();

	}

	public int consultTotalMesByCriteres(List<Long> idsMES) {
		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
		cr_mes.add(Restrictions.in("mes.idt", idsMES));

		cr_mes.setProjection(Projections.rowCount());
		return (Integer)cr_mes.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<String> findAllNumOperations() {
		Criteria cr = this.getSession().createCriteria(Mes.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property("numOperation"), "numOperation"));
		cr.addOrder(Order.asc("numOperation"));
		return (List<String>) cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForExploitation() {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 2 ";

		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForOptim() {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 3 ";

		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForDcc() {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 4 ";

		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Mes> getMesListByPvdId(Long idtPvd) {
		Criteria cr = this.getSession().createCriteria(Mes.class, "mes");
		cr.createAlias("mes.pvd", "pvd");
		cr.add(Restrictions.eq("pvd.idt", idtPvd));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForDeploi() {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT in (1, 2, 3)";

		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForDccEnv(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsDr, String idtsSite, String numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 2 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 5 ";
		if(!StringUtil.isNullOrEmpty(idtsFournisseur) && !Utils.containInjectChar(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsContrat) && !Utils.containInjectChar(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet) && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr) && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite) && !Utils.containInjectChar(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(!StringUtil.isNullOrEmpty(numOperation) && !Utils.containInjectChar(numOperation)){
			sql += " AND m.NUMOPERATION in (" + numOperation + ")";
		}
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForDccEnv() {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT = 1 AND m.IDT_TYPE_PROJET = 2 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 5 ";

		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getSitesRadioEnvNonReceptByProjetAndFournisseur(Long idtProjet, Long idtFournisseur, Long idtTypeProjet) {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT "
				+ " JOIN PVR_RT rt ON rt.IDT = mrt.IDT_RT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " LEFT JOIN PVR_PVR pvr ON pvr.IDT = m.IDT_PVR "
				+ " WHERE rt.IDT_STATUT_EDITION = 2 AND (m.IDT_PVR is NULL OR (m.IDT_PVR is not NULL AND pvr.IDT_STATUT_EDITION = 1)) AND p.WORKFLOW != 'env' ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}

		sql += " AND m.IDT_TYPE_PROJET = " + idtTypeProjet;

		return session.createSQLQuery(sql).list();

	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForQos(String idtsFournisseur, String idtsContrat, String idtsProjet, String idtsDr, String idtsSite, String numOperation) {
		Session session = this.getSession(); 
		//Récupérer les opérations MES et RT
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 6 ";
		if(!StringUtil.isNullOrEmpty(idtsFournisseur)){
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsContrat)){
			sql += " AND c.IDT in ( " + idtsContrat + ")"; 
		}
		if(!StringUtil.isNullOrEmpty(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ")";
		}
		if(!StringUtil.isNullOrEmpty(idtsSite)){
			sql += " AND m.IDT_SITE in (" + idtsSite + ")";
		}
		if(numOperation != null && !numOperation.isEmpty()){
			sql += " AND m.NUMOPERATION in (" + numOperation + ")";
		}
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForQos() {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_TYPE_PROJET = 1 AND q.IDT_STATUT_OPERATION = 1 AND q.IDT_ENTITE = 6 ";

		return session.createSQLQuery(sql).list();
	}
	@SuppressWarnings("unchecked")
	public List<Object> getMesEnvIdtByCriteres(String idtsFournisseur, String idtsContrat, 
			Long idtStatut, String idtsEtat, String idtsProjet, String idtsDr, String idtsSite, String idtsRt) {
		Session session = this.getSession();
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE ";
		if(!StringUtil.isNullOrEmpty(idtsRt)) {
			sql += " LEFT JOIN PVR_MES_RT mrt ON mrt.IDT_MES = m.IDT ";
		}
		sql += " WHERE 1=1 AND m.IDT_TYPE_PROJET = 2";
		if(!StringUtil.isNullOrEmpty(idtsContrat)) {
			sql += " AND c.IDT in (" + idtsContrat + ")"; 
		}

		if(!StringUtil.isNullOrEmpty(idtsFournisseur)) {
			sql += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur+ ")";
		}

		if(!StringUtil.isNullOrEmpty(idtsProjet)) {
			sql += " AND m.IDT_PROJET in (" + idtsProjet+ ")";
		}		

		if(!StringUtil.isNullOrEmpty(idtsDr)) {
			sql += " AND s.IDT_DR in (" + idtsDr+ ")";
		}		

		if(!StringUtil.isNullOrEmpty(idtsSite)) {
			sql += " AND m.IDT_SITE in (" + idtsSite+ ")";
		}		

		if(!StringUtil.isNullOrEmpty(idtsEtat)) {
			sql += " AND m.IDT_ETAT in (" + idtsEtat+ ")";
		}
		if(idtStatut != 0){
			if(idtStatut == 2){
				sql += " AND (mr.IDT_STATUT is NULL OR m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = 2))";
			}else{
				sql += " AND m.STATUT like (SELECT s.LABEL FROM PVR_STATUT s WHERE s.IDT = 1)";
			}
		}

		if(!StringUtil.isNullOrEmpty(idtsRt)) {
			sql += " AND mrt.IDT_RT in (" + idtsRt+ ")";
		}
		return session.createSQLQuery(sql).list();

	}

	public Integer getTotalSitesMesByProjetAndFournisseur(Long idtProjet, Long idtFournisseur, Long idtTypeProjet) {


		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
		cr_mes.createAlias("mes.contrat", "contrat");
		cr_mes.createAlias("mes.projet", "projet");
		cr_mes.createAlias("contrat.fournisseur", "fournisseur");
		cr_mes.createAlias("mes.typeProjet", "typeProjet");
		cr_mes.createAlias("mes.etat", "etat");

		if(idtFournisseur != 0){
			cr_mes.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		}
		if(idtProjet != 0){
			cr_mes.add(Restrictions.eq("projet.idt", idtProjet));
		}
		cr_mes.add(Restrictions.ne("projet.workflow", "env"));

		if(idtTypeProjet != 0){
			cr_mes.add(Restrictions.eq("typeProjet.idt", idtTypeProjet));
		}
		cr_mes.add(Restrictions.ne("etat.idt", 1L));

		cr_mes.setProjection(Projections.rowCount());
		return (Integer)cr_mes.uniqueResult();

	}

	public Integer getTotalSitesSoumisQualif(String idtsProjet, Long idtFournisseur, String idtsContrat, Long idtEntite, String idtsDr, String annees){
		Session session = this.getSession(); 
		String sql = "SELECT COUNT(m.IDT) from PVR_MES m "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_QUALIFICATION qu ON qu.IDT_MES = m.IDT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND qu.IDT_STATUT_OPERATION = 1 ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtEntite != 0){
			sql += " AND qu.IDT_ENTITE = " + idtEntite;
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(m.DATE) in (" + annees + ") ";
		}

		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}

	public Integer getTotalSitesExamine(String idtsProjet, Long idtFournisseur, String idtsContrat, Long idtEntite, String idtsDr, String annees){
		Session session = this.getSession();
		String sql = "SELECT COUNT(m.IDT) from PVR_MES m "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_QUALIFICATION qu ON qu.IDT_MES = m.IDT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE  p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND qu.IDT_STATUT_OPERATION != 1 ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty()){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty()){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtEntite != 0){
			sql += " AND qu.IDT_ENTITE = " + idtEntite;
		}
		if(idtsDr != null && !idtsDr.isEmpty()){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty()){
			sql += " AND YEAR(m.DATE) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	public Integer getTotalSitesMes(String idtsProjet, Long idtFournisseur,	String idtsContrat, String idtsDr, String annees) {
		Session session = this.getSession();  
		String sql = "SELECT Count(m.IDT) from PVR_MES m "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+"JOIN PVR_ETAT e ON e.IDT = m.IDT_ETAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND e.IDT != 1";
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(m.DATE_MES) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	public Integer getTotalSitesMes(List<Long> idtsProjet, Long idtFournisseur, List<Long> idtsContrat, List<Long> idtsDr, List<Long> annees){		
		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
		cr_mes.createAlias("mes.contrat", "contrat");
		cr_mes.createAlias("mes.projet", "projet");
		cr_mes.createAlias("contrat.fournisseur", "fournisseur");
		cr_mes.createAlias("mes.typeProjet", "typeProjet");
		cr_mes.createAlias("mes.etat", "etat");

		if(idtsDr != null && !idtsDr.isEmpty()){
			cr_mes.createAlias("mes.site", "site");
			cr_mes.createAlias("site.dr", "dr");
		}
		if(idtFournisseur != 0){
			cr_mes.add(Restrictions.eq("fournisseur.idt", idtFournisseur));
		}

		if(idtsProjet != null && !idtsProjet.isEmpty()){
			cr_mes.add(Restrictions.in("projet.idt", idtsProjet));
		}
		if(idtsContrat != null && !idtsContrat.isEmpty()){
			cr_mes.add(Restrictions.in("contrat.idt", idtsContrat));
		}
		if(idtsDr != null && !idtsDr.isEmpty()){
			cr_mes.add(Restrictions.in("dr.idt", idtsDr));
		}
		if(annees != null && !annees.isEmpty()){
			String years = "";
			for(Long annee : annees){
				years += annee.toString() + ",";
			}				
			cr_mes.add(Restrictions.sqlRestriction("YEAR(DATE_MES) in (" + years.substring(0, years.length() - 1)+ ")"));
		}
		cr_mes.add(Restrictions.ne("projet.workflow", "env"));

		cr_mes.add(Restrictions.eq("typeProjet.idt", 1L));
		cr_mes.add(Restrictions.ne("etat.idt", 1L));

		cr_mes.setProjection(Projections.rowCount());
		return (Integer)cr_mes.uniqueResult();
	}
	// 19/11/2020
	public Integer getTotalSitesRtDraft(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT Count(m.IDT) from PVR_MES m "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "
				+" JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(rt.DATE) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}

	// 19/11/2020
	public Integer getTotalSitesRt(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(IDT) from PVR_MES " 
				+ "WHERE IDT in (SELECT m.IDT from PVR_MES m "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "
				+" JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(rt.DATE) in (" + annees + ") ";
		}
		sql +=")";
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	
	// 29-09-2020 ==> demandée par m.charrouda (Compter le nombre total des RTs  valide par fournisseur)
	public Integer getTotalRt(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(DISTINCT(rt.IDT)) FROM PVR_RT rt "
				+ " JOIN PVR_MES_RT mesRt ON rt.IDT = mesRt.IDT_RT "
				+ " JOIN PVR_MES m ON m.IDT = mesRt.IDT_MES "
				+ " JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 ";

		if(idtFournisseur != 0){
			sql += " AND rt.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND rt.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(rt.DATE) in (" + annees + ") ";
		}
		
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	
	public Integer getTotalMesOneRt(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(IDT) from PVR_MES " 
				+ "WHERE IDT in (SELECT m.IDT from PVR_MES m "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "
				+" JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(rt.DATE) in (" + annees + ") ";
		}
		sql +=")";
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	// 29-09-2020 ==> demandée par m.charrouda (le nombre total des Rts de levée de réserves valide)
	public Number getTotalRtLeveeReserves(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT SUM(c.countRt) FROM ( "
				+ " SELECT (COUNT(DISTINCT(rt.IDT)) - 1) AS countRt "
				+ " FROM PVR_RT rt "
				+ " JOIN PVR_MES_RT mesRt ON rt.IDT = mesRt.IDT_RT "
				+ " JOIN PVR_MES m ON m.IDT = mesRt.IDT_MES "
				+ " JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " 
				+ " JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 ";
				
		if(idtFournisseur != 0){
			sql += " AND rt.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND rt.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(rt.DATE) in (" + annees + ") ";
		}
		sql += " GROUP BY m.IDT ) c";
		return ((Number) session.createSQLQuery(sql).uniqueResult());
	}

	// 19/11/2020
	public Integer getTotalSitesMoreOneRt(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(IDT) from PVR_MES " 
				+ "WHERE IDT in (SELECT m.IDT from PVR_MES m "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "
				+" JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(rt.DATE) in (" + annees + ") ";
		}
		sql +=" GROUP BY m.IDT HAVING count(rt.IDT) > 1)";
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	// 15-10-2020
	public Integer getTotalPvr(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(DISTINCT(pvr.IDT)) from PVR_PVR pvr "
				+" JOIN PVR_MES m ON m.IDT_PVR = pvr.IDT "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "				
				+" JOIN PVR_STATUT_EDITION se ON pvr.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND m.IDT_PVR is not null ";

		if(idtFournisseur != 0){
			sql += " AND pvr.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND pvr.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(pvr.DATE) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	public Integer getTotalSitesPvr(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(DISTINCT(m.IDT)) from PVR_MES m "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "
				+" JOIN PVR_PVR pvr ON m.IDT_PVR = pvr.IDT "
				+" JOIN PVR_STATUT_EDITION se ON pvr.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND m.IDT_PVR is not null ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(m.DATE_MES) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	// 15-10-2020
	public Integer getTotalPvd(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(DISTINCT(pvd.IDT)) from PVR_PVD pvd "
				+" JOIN PVR_MES m ON m.IDT_PVD = pvd.IDT "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "
				+" JOIN PVR_PVR pvr ON m.IDT_PVR = pvr.IDT "
				+" JOIN PVR_STATUT_EDITION se ON pvd.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND m.IDT_PVD is not null ";

		if(idtFournisseur != 0){
			sql += " AND pvd.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(pvd.DATE) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	public Integer getTotalSitesPvd(String idtsProjet, Long idtFournisseur, String idtsContrat, String mode, String idtsDr, String annees){
		Session session = this.getSession();  
		String sql = "SELECT COUNT(DISTINCT(m.IDT)) from PVR_MES m "
				+" JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES "
				+" JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT "
				+" JOIN PVR_PVR pvr ON m.IDT_PVR = pvr.IDT "
				+" JOIN PVR_PVD pvd ON m.IDT_PVD = pvd.IDT "
				+" JOIN PVR_STATUT_EDITION se ON pvd.IDT_STATUT_EDITION = se.IDT "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND m.IDT_PVD is not null ";

		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(mode != null && !Utils.containInjectChar(mode)){
			if(mode.equals("draft")){
				sql += " AND se.IDT = " + 1L;
			}else{
				sql += " AND se.IDT = " + 2L;
			}
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(m.DATE_MES) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getDrListByRtId(Long idtRt) {
		Session session = this.getSession();
		String sql ="SELECT DISTINCT site.IDT_DR FROM PVR_SITE site where site.IDT IN (SELECT  DISTINCT(mes.IDT_SITE) "+
				"FROM  PVR_RT rt LEFT JOIN PVR_MES_RT mesRT  ON rt.IDT=mesRT.IDT_RT LEFT JOIN PVR_MES mes ON"+
				" mesRT.IDT_MES=mes.IDT  WHERE rt.IDT=" + idtRt + ") ";
		return session.createSQLQuery(sql).list();
	}

	public Integer getTotalSitesIntegre(String idtsProjet, Long idtFournisseur,	String idtsContrat, String idtsDr, String annees) {
		Session session = this.getSession();  
		String sql = "SELECT Count(m.IDT) from PVR_MES m "
				+" JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+" JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+" JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+" WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1";
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(m.DATE) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	@SuppressWarnings("unchecked")
	public List<Object[]> realisationSiteRadioParMois(String idtsProjet, String idtsFournisseur, String idtsContrat, String idtsDr, String annees) {
		Session session = this.getSession();
		String sqlWhere = 	"";		
		if(idtsFournisseur != null && !idtsFournisseur.isEmpty() && !Utils.containInjectChar(idtsFournisseur)){
			sqlWhere += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ") ";
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sqlWhere += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sqlWhere += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sqlWhere += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sqlWhere += " AND YEAR(m.DATE_MES) in (" + annees + ") ";
		}
		String sql = "SELECT r.mois, r.label,  " + 
				"case when r.nombre is NULL then 0 ELSE r.nombre END 'nombre' " + 
				"FROM ( " + 
				"(SELECT MONTH(m.DATE_MES) mois, f.label, COUNT(m.IDT) nombre from PVR_MES m " + 
				"RIGHT JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"RIGHT JOIN PVR_FOURNISSEUR f on f.IDT = c.IDT_FOURNISSEUR  " + 
				"WHERE m.IDT in ( " + 
				"SELECT m.IDT from PVR_MES m " + 
				"JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES " + 
				"JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT " + 
				"JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT " + 
				"JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND se.IDT = 2 ";
		sql += sqlWhere;
		sql += ")  " + 
				"group by MONTH(m.DATE_MES), f.label " + 
				") " + 
				"UNION " + 
				"( " + 
				"SELECT MONTH(m.DATE_MES), 'Total', COUNT(m.IDT) from PVR_MES m  " + 
				"WHERE m.IDT in ( " + 
				"SELECT m.IDT from PVR_MES m " + 
				"JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES " + 
				"JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT " + 
				"JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT " + 
				"JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND se.IDT = 2 ";
		sql += sqlWhere;
		sql += ")  " + 
				"group by MONTH(m.DATE_MES) " + 
				") " + 
				") r ";
		return session.createSQLQuery(sql).list();
	}
	@SuppressWarnings("unchecked")
	public List<Object[]> rtEtablisParMois(String idtsProjet, String idtsFournisseur, String idtsContrat, String idtsDr, String annees) {
		Session session = this.getSession();
		String sqlWhere = 	"";		
		if(idtsFournisseur != null && !idtsFournisseur.isEmpty() && !Utils.containInjectChar(idtsFournisseur)){
			sqlWhere += " AND rt.IDT_FOURNISSEUR in (" + idtsFournisseur + ") ";
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sqlWhere += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sqlWhere += " AND rt.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sqlWhere += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sqlWhere += " AND YEAR(rt.DATE) in (" + annees + ") ";
		}
		String sql = "SELECT r.mois, r.label,  " + 
				"case when r.nombre is NULL then 0 ELSE r.nombre END 'nombre' " + 
				"FROM ( " + 
				"(SELECT MONTH(rt.DATE) mois,f.label, COUNT(DISTINCT(rt.IDT)) nombre FROM PVR_RT rt " + 
				"JOIN PVR_MES_RT mesRt ON rt.IDT = mesRt.IDT_RT " + 
				"JOIN PVR_MES m ON m.IDT = mesRt.IDT_MES " + 
				"JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT " + 
				"JOIN PVR_FOURNISSEUR f on f.IDT = rt.IDT_FOURNISSEUR " + 
				"JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND se.IDT = 2 ";
		sql += sqlWhere;
		sql += " " + 
				"group by MONTH(rt.DATE), f.label " + 
				") " + 
				"UNION " + 
				"( " + 
				"SELECT MONTH(rt.DATE),'Total', COUNT(DISTINCT(rt.IDT)) FROM PVR_RT rt " + 
				"JOIN PVR_MES_RT mesRt ON rt.IDT = mesRt.IDT_RT " + 
				"JOIN PVR_MES m ON m.IDT = mesRt.IDT_MES " + 
				"JOIN PVR_STATUT_EDITION se ON rt.IDT_STATUT_EDITION = se.IDT " + 
				"JOIN PVR_FOURNISSEUR f on f.IDT = rt.IDT_FOURNISSEUR " + 
				"JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND se.IDT = 2 ";
		sql += sqlWhere;
		sql += " " + 
				"group by MONTH(rt.DATE) " + 
				") " + 
				") r ";
		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> dccTableauBord(String idtsProjet, String idtsFournisseur, String idtsContrat, String idtsDr, String annees) {
		Session session = this.getSession();
		String sqlWhere = 	"";		
		if(idtsFournisseur != null && !idtsFournisseur.isEmpty() && !Utils.containInjectChar(idtsFournisseur)){
			sqlWhere += " AND c.IDT_FOURNISSEUR in (" + idtsFournisseur + ") ";
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sqlWhere += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sqlWhere += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sqlWhere += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sqlWhere += " AND YEAR(m.DATE) in (" + annees + ") ";
		}
		String sql = "SELECT r.mois, r.label, r.realisation, case when r.realisation = 0 then 0 else GREATEST(round(coalesce(DMC.SUM_DMC,0)/r.realisation, 1),0) end as DMC  " + 
				" FROM ( " + 
				"	(SELECT MONTH(m.DATE) mois, d.LABEL, COUNT(m.IDT) realisation" + 
				"	from PVR_MES m " + 
				"	RIGHT JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"	RIGHT JOIN PVR_FOURNISSEUR f on f.IDT = c.IDT_FOURNISSEUR " + 
				"	RIGHT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"	RIGHT JOIN PVR_DR d on d.IDT = s.IDT_DR" + 
				"	WHERE m.IDT in ( SELECT m.IDT from PVR_MES m " + 
				"	JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"	JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"	JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"	WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1";
		sql += sqlWhere; 
		sql += "   )" + 
				"	group by MONTH(m.DATE), d.LABEL) " + 
				"	UNION" + 
				"	( SELECT MONTH(m.DATE), 'Global', COUNT(m.IDT) " + 
				"	from PVR_MES m  	" + 
				"	 WHERE m.IDT in ( SELECT m.IDT from PVR_MES m " + 
				"	 JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"	 JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"	 JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"	 WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1" ;
		sql += sqlWhere; 
		sql += "   )" + 
				"	 group by MONTH(m.DATE)) " + 
				") r " + 
				"left join (" + 
				"SELECT d.mois, d.label, sum(d.SUM_DMC) SUM_DMC" + 
				" FROM ( " + 
				"	(SELECT MONTH(m.DATE) mois, dr.LABEL" + 
				"	, sum(Coalesce(rt.DATE_VALIDATION - rt.DATE, 0) + Coalesce(Date(q.DATE_REPONSE) - Date(q.DATE), 0) + 2) SUM_DMC " + 
				"	from PVR_MES m " + 
				"	RIGHT JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"	RIGHT JOIN PVR_FOURNISSEUR f on f.IDT = c.IDT_FOURNISSEUR " + 
				"	RIGHT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"	RIGHT JOIN PVR_DR dr on dr.IDT = s.IDT_DR" + 
				"	LEFT JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES " + 
				"	JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT " + 
				"	LEFT JOIN PVR_QUALIFICATION q ON q.IDT_MES = m.IDT" + 
				"	JOIN PVR_ENTITE e ON q.IDT_ENTITE = e.IDT" + 
				"	WHERE m.IDT in ( SELECT m.IDT from PVR_MES m " + 
				"	JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"	JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"	JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"	WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND e.IDT = 4";
		sql += sqlWhere; 
		sql += "	AND" + 
				"   rt.idt = (select MIN(rt.idt)" + 
				"					from PVR_RT rt " + 
				"					JOIN PVR_MES_RT mesRt ON rt.IDT = mesRt.IDT_RT" + 
				"					WHERE mesRt.IDT_MES = m.IDT" + 
				"					AND rt.WORKFLOW  = 'radio'  " + 
				"					AND rt.DATE = (select MIN(rt_.DATE) from PVR_RT rt_ " + 
				"					JOIN PVR_MES_RT mesRt ON rt_.IDT = mesRt.IDT_RT" + 
				"					WHERE mesRt.IDT_MES = m.IDT" + 
				"					AND rt_.WORKFLOW  = 'radio')" + 
				"					 )" + 
				"  )" + 
				"	group by MONTH(m.DATE), dr.LABEL) " +
				" UNION" +
				"	(SELECT MONTH(m.DATE) mois, dr.LABEL" + 
				"	, sum(Coalesce(rt.DATE_VALIDATION - rt.DATE, 0) + Coalesce(Date(q.DATE_REPONSE) - Date(q.DATE), 0) + 1) SUM_DMC " + 
				"	from PVR_MES m " + 
				"	RIGHT JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"	RIGHT JOIN PVR_FOURNISSEUR f on f.IDT = c.IDT_FOURNISSEUR " + 
				"	RIGHT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"	RIGHT JOIN PVR_DR dr on dr.IDT = s.IDT_DR" + 
				"	LEFT JOIN PVR_MES_RT mesRt ON m.IDT = mesRt.IDT_MES " + 
				"	JOIN PVR_RT rt ON rt.IDT = mesRt.IDT_RT " + 
				"	LEFT JOIN PVR_QUALIFICATION q ON q.IDT_MES = m.IDT" + 
				"	JOIN PVR_ENTITE e ON q.IDT_ENTITE = e.IDT" + 
				"	WHERE m.IDT in ( SELECT m.IDT from PVR_MES m " + 
				"	JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT " + 
				"	JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET " + 
				"	JOIN PVR_SITE s ON s.IDT = m.IDT_SITE " + 
				"	WHERE p.WORKFLOW != 'env' AND m.IDT_TYPE_PROJET = 1 AND e.IDT = 4";
		sql += sqlWhere; 
		sql += "	AND" + 
				"   rt.idt != (select MIN(rt.idt)" + 
				"					from PVR_RT rt " + 
				"					JOIN PVR_MES_RT mesRt ON rt.IDT = mesRt.IDT_RT" + 
				"					WHERE mesRt.IDT_MES = m.IDT" + 
				"					AND rt.WORKFLOW  = 'radio'  " + 
				"					AND rt.DATE = (select MIN(rt_.DATE) from PVR_RT rt_ " + 
				"					JOIN PVR_MES_RT mesRt ON rt_.IDT = mesRt.IDT_RT" + 
				"					WHERE mesRt.IDT_MES = m.IDT" + 
				"					AND rt_.WORKFLOW  = 'radio')" + 
				"					 )" +
				"	AND" + 
				"   rt.idt in (select rt.idt" + 
				"					from PVR_RT rt " + 
				"					JOIN PVR_MES_RT mesRt ON rt.IDT = mesRt.IDT_RT" + 
				"					WHERE mesRt.IDT_MES = m.IDT" + 
				"					AND rt.WORKFLOW  = 'radio'  " +
				"					 )" +
				"  )" + 
				"	group by MONTH(m.DATE), dr.LABEL) " +
				") d group by d.mois, d.label) DMC on r.mois = DMC.mois and r.label = DMC.label";
		return session.createSQLQuery(sql).list();
	}

	@SuppressWarnings("unchecked")
	public List<Object> findAllNumOperationsForFournisseur(Long idtFournisseur) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.NUMOPERATION) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " WHERE m.IDT_ETAT in (1, 2, 3) AND c.IDT_FOURNISSEUR = " + idtFournisseur;

		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForFournisseur(Long idtFournisseur, Long idtContrat, Long idtProjet, Long idtDr, Long idtSite, Long idtEtat, String numOperation) {
		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
				+ " LEFT JOIN PVR_TYPE_PROJET tp ON tp.IDT = m.IDT_TYPE_PROJET "
				+ " WHERE 1 = 1 " ;
		sql += " AND tp.IDT IN (1,2) " ;
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}

		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}

		if(idtDr != 0){
			sql += " AND s.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sql += " AND m.IDT_SITE = " + idtSite;
		}

		if(idtEtat != 0){
			sql += " AND m.IDT_ETAT = " + idtEtat;
		}

		if(numOperation != null && !numOperation.equals("")){
			String num = "'%" + numOperation + "%'";
			sql += " AND m.NUMOPERATION LIKE " + num;
		}
		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtMultiSitesForRemise(Long idtFournisseur, Long idtContrat, Long idtProjet, String dateDebut, String dateFin, List<Long> idtMes, 
			Long idtDr, List<Long> idSites) {

		Session session = this.getSession();
		
		String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
		+ " LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
		+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
		+ " LEFT JOIN PVR_SITE si ON m.IDT_SITE = si.IDT "
		+ " WHERE NOT EXISTS "
		+ " (SELECT 1 FROM PVR_REMISE_MES rm "
		+ " LEFT JOIN PVR_REMISE_CONTROLE rc ON rc.IDT = rm.IDT_REMISE_CONTROLE "
		+ " WHERE rm.IDT_MES = m.IDT AND rc.IDT_ETAT_REMISE_CONTROLE != 3) "
		+ " AND m.IDT_ETAT = 2 ";
		
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND m.IDT_PROJET = " + idtProjet;
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (m.DATE_MES >= '" + date_From + "' AND m.DATE_MES <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}

		if(idtMes != null && idtMes.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtMes){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND m.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}

		if(idSites != null && idSites.size() > 0){
			StringBuilder sitesIds =new StringBuilder();
			for(Long idSite : idSites){
				if(sitesIds.length() > 0){
					sitesIds.append(",");
				}
				sitesIds.append(idSite);
			}
			sql += " AND m.IDT_SITE in (" + sitesIds.toString() + ")";
		}

		return session.createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Mes> getMesListByRemiseId(Long idtRemise) {
		Criteria cr_mesRemise = this.getSession().createCriteria(RemiseMes.class, "remiseMes");
		cr_mesRemise.createAlias("remiseMes.remiseControle", "remiseControle");
		cr_mesRemise.createAlias("remiseMes.mes", "mes1");
		cr_mesRemise.add(Restrictions.eq("remiseControle.idt", idtRemise));

		ProjectionList properties = Projections.projectionList();
		properties.add(Projections.property("mes1.idt"));
		cr_mesRemise.setProjection(properties);

		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes2");
		if(cr_mesRemise.list() != null && cr_mesRemise.list().size() > 0){
			cr_mes.add(Restrictions.in("mes2.idt", (List<Long>) cr_mesRemise.list()));
			return cr_mes.list();
		}

		return null;
	}
	
	public Integer getTotalMesRemiseControle(String idtsProjet, Long idtFournisseur, String idtsContrat, String idtsDr, String annees) {
		Session session = this.getSession();
				
		String sql = "SELECT COUNT(DISTINCT(m.IDT)) FROM PVR_MES m "
				+ " LEFT JOIN PVR_MES_RESERVE mr ON mr.IDT_MES = m.IDT "
				+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
				+ " JOIN PVR_ETAT e ON e.IDT = m.IDT_ETAT "
				+ " JOIN PVR_PROJET p ON p.IDT = m.IDT_PROJET "
				+ " JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
				+ " JOIN PVR_REMISE_MES rm ON m.IDT = rm.IDT_MES "
				+ " JOIN PVR_REMISE_CONTROLE rc ON rc.IDT = rm.IDT_REMISE_CONTROLE "
				+ " WHERE p.WORKFLOW != 'env' "
				+ " AND m.IDT_TYPE_PROJET = 1 "
				+ " AND e.IDT = 2 "
				+ " AND rc.IDT_ETAT_REMISE_CONTROLE = 2 ";
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtsProjet != null && !idtsProjet.isEmpty() && !Utils.containInjectChar(idtsProjet)){
			sql += " AND m.IDT_PROJET in (" + idtsProjet + ") ";
		}
		if(idtsContrat != null && !idtsContrat.isEmpty() && !Utils.containInjectChar(idtsContrat)){
			sql += " AND m.IDT_CONTRAT in (" + idtsContrat + ") ";
		}
		if(idtsDr != null && !idtsDr.isEmpty() && !Utils.containInjectChar(idtsDr)){
			sql += " AND s.IDT_DR in (" + idtsDr + ") ";
		}
		if(annees != null && !annees.isEmpty() && !Utils.containInjectChar(annees)){
			sql += " AND YEAR(m.DATE_MES) in (" + annees + ") ";
		}
		return ((BigInteger) session.createSQLQuery(sql).uniqueResult()).intValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<Mes> getMesListByIdsPagination(List<Long> idsMES, List<Long> selectedMesDto,
			Integer pageNumber, Integer pageSize) {
			Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
			if(idsMES != null && idsMES.size() > 0) {
				cr_mes.add(Restrictions.in("mes.idt", idsMES));
			}
			if(selectedMesDto != null && selectedMesDto.size() > 0) {
				cr_mes.add(Restrictions.not(Restrictions.in("mes.idt", selectedMesDto)));
			}
			//Pagination
			if(pageNumber != null && pageSize != null) {
				if(pageNumber>0 && pageSize>0){
					cr_mes.setFirstResult((pageNumber-1)*pageSize);
					cr_mes.setMaxResults(pageSize);
				}else{
					cr_mes.setFirstResult(0);
				}
			}
			cr_mes.addOrder(Order.desc("mes.idt"));

			return cr_mes.list();
	}
	
	public int getTotalMesListByIds(List<Long> idsMES, List<Long> selectedMesDto) {
		Criteria cr_mes = this.getSession().createCriteria(Mes.class, "mes");
		if(idsMES != null && idsMES.size() > 0) {
			cr_mes.add(Restrictions.in("mes.idt", idsMES));
		}
		if(selectedMesDto != null && selectedMesDto.size() > 0) {
			cr_mes.add(Restrictions.not(Restrictions.in("mes.idt", selectedMesDto)));
		}
		cr_mes.setProjection(Projections.rowCount());
		return (Integer)cr_mes.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Object> getListMesIdtForFournisseurMultipleValues(
			Long idtFournisseur, String idtsContrat, String idtsProjet,
			String idtsDr, String idtsSite, String idtsEtat,
			List<String> numOperation) {
			Session session = this.getSession(); 
			String sql = "SELECT DISTINCT(m.IDT) from PVR_MES m "
					+ " JOIN PVR_CONTRAT c ON c.IDT = m.IDT_CONTRAT "
					+ " LEFT JOIN PVR_SITE s ON s.IDT = m.IDT_SITE "
					+ " LEFT JOIN PVR_QUALIFICATION q ON m.IDT = q.IDT_MES "
					+ " WHERE 1 = 1 " ;
			if(idtFournisseur != 0){
				sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
			}
			if(!StringUtil.isNullOrEmpty(idtsContrat)){
				sql += " AND c.IDT in ( " + idtsContrat + ")"; 
			}
			if(!StringUtil.isNullOrEmpty(idtsEtat)){
				sql += " AND m.IDT_ETAT in (" + idtsEtat + ")";
			}
			if(!StringUtil.isNullOrEmpty(idtsProjet)){
				sql += " AND m.IDT_PROJET in (" + idtsProjet + ")";
			}
			if(!StringUtil.isNullOrEmpty(idtsDr)){
				sql += " AND s.IDT_DR in (" + idtsDr + ")";
			}
			if(!StringUtil.isNullOrEmpty(idtsSite)){
				sql += " AND m.IDT_SITE in (" + idtsSite + ")";
			}
			if(numOperation != null && !numOperation.isEmpty()){
				sql += " AND m.NUMOPERATION in (" + StringUtil.joinListString(numOperation, ",") + ")";
			}
			return session.createSQLQuery(sql).list();
	}
}
