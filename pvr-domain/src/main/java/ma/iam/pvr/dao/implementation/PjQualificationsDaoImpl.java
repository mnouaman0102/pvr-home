package ma.iam.pvr.dao.implementation;



import java.util.List;

import ma.iam.pvr.dao.interfaces.IPjQualificationsDao;
import ma.iam.pvr.domain.PjQualifications;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class PjQualificationsDaoImpl extends DefaultGenericDao<PjQualifications, Long> implements IPjQualificationsDao {


	@SuppressWarnings("unchecked")
	public List<PjQualifications> getAllPjQualifByQualifIdt(Long idtQualif) {
		Criteria cr = this.getSession().createCriteria(PjQualifications.class, "pjQualifications");		
		cr.add(Restrictions.eq("pjQualifications.qualification.idt", idtQualif));		
		return cr.list();
	}

}
