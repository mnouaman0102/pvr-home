package ma.iam.pvr.dao.implementation;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.mysql.cj.util.StringUtils;
import ma.iam.pvr.dao.interfaces.IRtInfraDao;
import ma.iam.pvr.domain.OperationInfra;
import ma.iam.pvr.domain.PjOperationInfra;
import ma.iam.pvr.domain.RtInfra;
import ma.iam.pvr.domain.RtOperationInfra;
import ma.iam.pvr.domain.utils.StringUtil;
import ma.iam.pvr.domain.utils.Utils;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

//import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
@Repository
public class RtInfraDaoImpl extends DefaultGenericDao<RtInfra, Long> implements IRtInfraDao {

	@SuppressWarnings("unchecked")
	public List<RtInfra> getListRtInfraByContrat(Long idtContrat) {
		Criteria cr = this.getSession().createCriteria(RtInfra.class,"rt");
		cr.createAlias("rt.contrat", "contrat");
		cr.add(Restrictions.eq("contrat.idt", idtContrat));
		return cr.list();
	}
	
	public Integer countAllRtInfra(String workflow) {
		Criteria cr = this.getSession().createCriteria(RtInfra.class,"rt");
		cr.add(Restrictions.eq("rt.workflow", workflow));
		cr.setProjection(Projections.rowCount());
		return (Integer) cr.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<RtInfra> getAllRtInfra(Integer pageNumber, Integer pageSize, String workflow) {
		Criteria cr = this.getSession().createCriteria(RtInfra.class,"rt");
		cr.add(Restrictions.eq("rt.workflow", workflow));
		if(pageNumber == null || pageSize == null 
				|| pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.addOrder(Order.desc("rt.idt"));
			cr.setFirstResult((pageNumber-1) * pageSize);
			cr.setMaxResults(pageSize);
			return cr.list();
		}
		return null;
	}

	public Integer countListRtInfraDraft(String workflow) {
		Criteria cr = this.getSession().createCriteria(RtInfra.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}

	
	@SuppressWarnings("unchecked")
	public List<RtInfra> getListRtInfraDraft(Integer pageNumber, Integer pageSize, String workflow) {
		Criteria cr = this.getSession().createCriteria(RtInfra.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 1L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		
		if(pageNumber == null || pageSize == null 
				|| pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1) * pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}
		return null;
	}
	
	public Integer countListRtInfraValid(String workflow) {
		
		Criteria cr = this.getSession().createCriteria(RtInfra.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		cr.setProjection(Projections.rowCount());
		return (Integer)cr.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<RtInfra> getListRtInfraValid(Integer pageNumber, Integer pageSize, String workflow) {
		Criteria cr = this.getSession().createCriteria(RtInfra.class,"rt");
		cr.createAlias("rt.statutEdition", "statutEdition");
		cr.add(Restrictions.eq("statutEdition.idt", 2L ));
		cr.add(Restrictions.eq("rt.workflow", workflow));
		if(pageNumber == null || pageSize == null || pageNumber <= 0 || pageSize <= 0){
			//Sans Pagination
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}else if(pageNumber>0 && pageSize>0){
			//Pagination
			cr.setFirstResult((pageNumber-1) * pageSize);
			cr.setMaxResults(pageSize);
			cr.addOrder(Order.desc("rt.idt"));
			return cr.list();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListOperationIdtWithRTAndNot(Long idtFournisseur, Long idtContrat, Long idtProjet, String dateDebut, String dateFin, List<Long> idtOper, 
			Long idtDr, Long idtSite, Long idtTypeProjet, String idtsRt) {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(op.IDT) from PVR_OPERATION_INFRA op "
				+ " JOIN PVR_CONTRAT c ON c.IDT = op.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON op.IDT_SITE = si.IDT ";
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " LEFT JOIN PVR_RT_OPERATION_INFRA opRt ON opRt.IDT_OPERATION_INFRA = op.IDT ";
		}
		sql += " WHERE op.IDT_ETAT_OPERATION_INFRA in (9) ";
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND op.IDT_PROJET_INFRA = " + idtProjet;
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (op.DATE >= '" + date_From + "' AND op.DATE <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}

		if(idtOper != null && idtOper.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtOper){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND op.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}

		if(idtSite != 0){
			sql += " AND op.IDT_SITE = " + idtSite;
		}

		if(idtTypeProjet != 0){
			sql += " AND op.IDT_TYPE_PROJET_INFRA = " + idtTypeProjet;
		}
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " AND opRt.IDT_RT_INFRA in (" + idtsRt + ")";
		}

		return session.createSQLQuery(sql).list();

	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getListOperIdtWithRTAndNotMultiSites(Long idtFournisseur, Long idtContrat, Long idtProjet, String dateDebut, String dateFin, List<Long> idtsOper, 
			Long idtDr, List<Long> idSites, Long idtTypeProjet, boolean excludeDraft, String idtsRt) {

		Session session = this.getSession(); 
		String sql = "SELECT DISTINCT(op.IDT) from PVR_OPERATION_INFRA op "
				+ " JOIN PVR_CONTRAT c ON c.IDT = op.IDT_CONTRAT "
				+ " LEFT JOIN PVR_SITE si ON op.IDT_SITE = si.IDT ";
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " LEFT JOIN PVR_RT_OPERATION_INFRA opRt ON opRt.IDT_OPERATION_INFRA = op.IDT ";
		}
		sql += " WHERE op.IDT_ETAT_OPERATION_INFRA in (9) ";
		if(idtContrat != 0){
			sql += " AND c.IDT = " + idtContrat; 
		}
		if(idtFournisseur != 0){
			sql += " AND c.IDT_FOURNISSEUR = " + idtFournisseur;
		}
		if(idtProjet != 0){
			sql += " AND op.IDT_PROJET_INFRA = " + idtProjet;
		}

		if(!StringUtils.isNullOrEmpty(dateDebut) && !StringUtils.isNullOrEmpty(dateFin)){

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(dateDebut);
				Date date2 = sdf.parse(dateFin);
				String date_From = new SimpleDateFormat("yyyy-MM-dd").format(date1);
				String date_To = new SimpleDateFormat("yyyy-MM-dd").format(date2);
				sql += " AND (op.DATE >= '" + date_From + "' AND op.DATE <= '" + date_To +"')";
			} catch (ParseException e) {
				e.getMessage();
			}	
		}

		if(idtsOper != null && idtsOper.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Long id: idtsOper){
				if(id != null && !id.equals(0L))
					sb.append(""+id + ",");
			}
			sql += " AND op.IDT not in (" + sb.substring(0, sb.length() - 1) + ") ";
		}

		if(excludeDraft){
			sql += " AND op.IDT not in ( select mr.IDT_OPERATION_INFRA from PVR_RT_OPERATION_INFRA mr "
					+ " inner join PVR_RT_INFRA r on mr.IDT_RT_INFRA = r.IDT "
					+ " inner join PVR_STATUT_EDITION s on s.IDT = r.IDT_STATUT_EDITION "
					+ " and s.LABEL = 'Draft') ";
		}

		if(idtDr != 0){
			sql += " AND si.IDT_DR = " + idtDr;
		}

		if(idSites != null && idSites.size() > 0){
			StringBuilder sitesIds =new StringBuilder();
			for(Long idSite : idSites){
				if(sitesIds.length() > 0){
					sitesIds.append(",");
				}
				sitesIds.append(idSite);
			}
			sql += " AND op.IDT_SITE in (" + sitesIds.toString() + ")";
		}

		if(idtTypeProjet != 0){
			sql += " AND op.IDT_TYPE_PROJET_INFRA = " + idtTypeProjet;
		}
		
		if(!StringUtil.isNullOrEmpty(idtsRt) && !Utils.containInjectChar(idtsRt)) {
			sql += " AND opRt.IDT_RT_INFRA in (" + idtsRt + ")";
		}

		return session.createSQLQuery(sql).list();

	}
	
	public List<Object> getListOperIdtWithRTAndNotMultiSites(Long idtFournisseur, Long idtContrat, Long idtProjet, String dateDebut, String dateFin, List<Long> idtMes, 
			Long idtDr, List<Long> idSites, Long idtTypeProjet, String idtsRt) {
		return getListOperIdtWithRTAndNotMultiSites(idtFournisseur, idtContrat, idtProjet, dateDebut, dateFin, idtMes, idtDr, idSites, idtTypeProjet, false, idtsRt);
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationInfra> getOperListByRtId(Long idtRt) {
		Criteria cr_mesRt = this.getSession().createCriteria(RtOperationInfra.class, "mesRt");
		cr_mesRt.createAlias("mesRt.rtInfra", "rt");
		cr_mesRt.createAlias("mesRt.operationInfra", "mes1");
		cr_mesRt.add(Restrictions.eq("rt.idt", idtRt));

		ProjectionList properties = Projections.projectionList();
		properties.add(Projections.property("mes1.idt"));
		cr_mesRt.setProjection(properties);

		Criteria cr_mes = this.getSession().createCriteria(OperationInfra.class, "mes2");
		if(cr_mesRt.list() != null && cr_mesRt.list().size() > 0){
			cr_mes.add(Restrictions.in("mes2.idt", (List<Long>) cr_mesRt.list()));
			return cr_mes.list();
		}

		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationInfra> getListOperationInfraByIDT(List<Long> idsMES) {
		Criteria cr_mes = this.getSession().createCriteria(OperationInfra.class, "mes");
		cr_mes.add(Restrictions.in("mes.idt", idsMES));
		return cr_mes.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<PjOperationInfra> getPJsOperationByOperIdt(Long idtOper) {
		Criteria cr = this.getSession().createCriteria(PjOperationInfra.class, "pjOperation");
		cr.createAlias("pjOperation.operationInfra", "operationInfra");
		cr.add(Restrictions.eq("operationInfra.idt", idtOper));
		return cr.list();
	}
	
}
