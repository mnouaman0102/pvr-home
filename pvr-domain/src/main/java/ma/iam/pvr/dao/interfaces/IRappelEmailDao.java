package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.RappelEmail;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IRappelEmailDao extends IGenericDao<RappelEmail, Long> {

	List<RappelEmail> getRappelsByEmailId(Long idtEmail);
}
