package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.OperationInfra;
import ma.iam.pvr.domain.PjOperationInfra;
import ma.iam.pvr.domain.RtInfra;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IRtInfraDao extends IGenericDao<RtInfra, Long> {

	List<RtInfra> getListRtInfraByContrat(Long idtContrat);
	
	List<RtInfra> getAllRtInfra(Integer pageNumber, Integer pageSize, String workflow);
	
	Integer countAllRtInfra(String workflow);
	
	List<RtInfra> getListRtInfraDraft(Integer pageNumber, Integer pageSize, String workflow);
	
	Integer countListRtInfraDraft(String workflow);
	
	List<RtInfra> getListRtInfraValid(Integer pageNumber, Integer pageSize, String workflow);
	
	Integer countListRtInfraValid(String workflow);
	
	List<Object> getListOperationIdtWithRTAndNot(Long idtFournisseur, Long idtContrat, Long idtProjet, String dateDebut, String dateFin, List<Long> idtOper, 
			Long idtDr, Long idtSite, Long idtTypeProjet, String idtsRt);
	
	List<Object> getListOperIdtWithRTAndNotMultiSites(Long idtFournisseur, Long idtContrat, Long idtProjet, String dateDebut, String dateFin, List<Long> idtsOper, 
			Long idtDr, List<Long> idSites, Long idtTypeProjet, boolean excludeDraft, String idtsRt);
	
	List<Object> getListOperIdtWithRTAndNotMultiSites(Long idtFournisseur, Long idtContrat, Long idtProjet, String dateDebut, String dateFin, List<Long> idtMes, 
			Long idtDr, List<Long> idSites, Long idtTypeProjet, String idtsRt);
	
	List<OperationInfra> getOperListByRtId(Long idtRt);
	
	List<OperationInfra> getListOperationInfraByIDT(List<Long> idsMES);
	
	List<PjOperationInfra> getPJsOperationByOperIdt(Long idtOper);
}
