package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.ProjetInfra;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IProjetInfraDao extends IGenericDao<ProjetInfra, Long> {

	ProjetInfra getProjetByLabel(String label);
	List<ProjetInfra> getProjectsInfra();

}
