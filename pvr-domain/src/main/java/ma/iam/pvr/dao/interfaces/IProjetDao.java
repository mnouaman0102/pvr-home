package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Projet;
//import ma.iam.socle.service.GenericDao;

/**
 * 
 * @author Z.BELGHAOUTI
 *
 */
public interface IProjetDao extends IGenericDao<Projet, Long> {

	List<Projet> getProjectsRadio();
	List<Projet> getProjectsEnv();
	Projet getProjetByLabel(String label);
}
