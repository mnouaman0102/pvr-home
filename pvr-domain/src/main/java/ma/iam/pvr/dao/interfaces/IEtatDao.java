package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.Etat;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IEtatDao extends IGenericDao<Etat, Long> {
	
	List<Etat> getListEtatsByIdts(List<Long> idts);
}
