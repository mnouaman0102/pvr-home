package ma.iam.pvr.dao.interfaces;

import java.util.List;

import ma.iam.pvr.domain.PjOperation;
//import ma.iam.socle.service.GenericDao;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public interface IPjOperationDao extends IGenericDao<PjOperation, Long> {
	
	List<PjOperation> getPJOperationExploitationListByMesIdt(Long idtMes);
	List<PjOperation> getPJOperationOptimListByMesIdt(Long idtMes);
	List<PjOperation> getPJOperationDccListByMesIdt(Long idtMes);
	PjOperation getPJOperationDeploiByMesIdt(Long idtMes);
	List<PjOperation> getPJOperationDccEnvListByMesIdt(Long idtMes);
	List<PjOperation> getPJOperationQosListByMesIdt(Long idtMes);
	List<PjOperation> getPJsOperationByMesIdt(Long idtMes);
	List<PjOperation> getPJsOperationByMesIdtAndEntiteIdt(Long idtMes, Long idtEntite);
	List<PjOperation> getPJOperationDccSuiviListByMesIdt(Long idtMes);
	List<PjOperation> getPJOperationDccEnvSuiviListByMesIdt(Long idtMes);
	List<PjOperation> getPJDeploiByMesIdt(Long idtMes);
}
