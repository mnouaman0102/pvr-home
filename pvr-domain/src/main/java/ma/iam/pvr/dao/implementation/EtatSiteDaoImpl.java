package ma.iam.pvr.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import ma.iam.pvr.dao.interfaces.IEtatSiteDao;
import ma.iam.pvr.domain.EtatSite;
//import ma.iam.socle.service.GenericDaoHibernateImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
/**
 * 
 * @author K.ELBAGUARI
 *
 */
public class EtatSiteDaoImpl extends DefaultGenericDao<EtatSite, Long> implements IEtatSiteDao {

	public EtatSite getByCode(String code) {
		Criteria cr = this.getSession().createCriteria(EtatSite.class, "etatSite");
		cr.add(Restrictions.eq("etatSite.code", code).ignoreCase());
		cr.setMaxResults(1);
		return (EtatSite) cr.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<EtatSite> getListEtatSiteCandidat() {
		Criteria cr = this.getSession().createCriteria(EtatSite.class, "etatSite");
		List<Long> idtEtatsSite = new ArrayList<Long>();
		idtEtatsSite.add(1L);
		idtEtatsSite.add(5L);
		idtEtatsSite.add(6L);
		idtEtatsSite.add(7L);
		cr.add(Restrictions.in("etatSite.idt", idtEtatsSite));
		return cr.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<EtatSite> getListEtatSiteCandidatPhases() {
		Criteria cr = this.getSession().createCriteria(EtatSite.class, "etatSite");
		List<Long> idtEtatsSite = new ArrayList<Long>();
		idtEtatsSite.add(1L);
		idtEtatsSite.add(7L);
		idtEtatsSite.add(5L);
		idtEtatsSite.add(8L);
		idtEtatsSite.add(9L);
		idtEtatsSite.add(10L);
		idtEtatsSite.add(6L);
		cr.add(Restrictions.in("etatSite.idt", idtEtatsSite));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<EtatSite> getListEtatSiteExploi() {
		Criteria cr = this.getSession().createCriteria(EtatSite.class, "etatSite");
		List<Long> idtEtatsSite = new ArrayList<Long>();
		idtEtatsSite.add(4L);
		cr.add(Restrictions.in("etatSite.idt", idtEtatsSite));
		return cr.list();
	}
}
